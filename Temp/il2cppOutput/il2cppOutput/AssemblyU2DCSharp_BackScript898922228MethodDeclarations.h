﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BackScript
struct BackScript_t898922228;

#include "codegen/il2cpp-codegen.h"

// System.Void BackScript::.ctor()
extern "C"  void BackScript__ctor_m1052658935 (BackScript_t898922228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackScript::Start()
extern "C"  void BackScript_Start_m22409459 (BackScript_t898922228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackScript::Update()
extern "C"  void BackScript_Update_m1431074326 (BackScript_t898922228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackScript::ParentBack()
extern "C"  void BackScript_ParentBack_m832573860 (BackScript_t898922228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackScript::SlideCBack()
extern "C"  void BackScript_SlideCBack_m4229489334 (BackScript_t898922228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackScript::SlideEBack()
extern "C"  void BackScript_SlideEBack_m2291409068 (BackScript_t898922228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackScript::threemenuback()
extern "C"  void BackScript_threemenuback_m1611424717 (BackScript_t898922228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackScript::nextpage()
extern "C"  void BackScript_nextpage_m2104000087 (BackScript_t898922228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackScript::backReg()
extern "C"  void BackScript_backReg_m1108799176 (BackScript_t898922228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackScript::Membership()
extern "C"  void BackScript_Membership_m3841128233 (BackScript_t898922228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackScript::enablepurchase()
extern "C"  void BackScript_enablepurchase_m1721384685 (BackScript_t898922228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
