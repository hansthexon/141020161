﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.IO.Stream
struct Stream_t3255436806;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_IO_Stream3255436806.h"

// System.String LipingShare.LCLib.Asn1Processor.Asn1Util::FormatString(System.String,System.Int32,System.Int32)
extern "C"  String_t* Asn1Util_FormatString_m1519920834 (Il2CppObject * __this /* static, unused */, String_t* ___inStr0, int32_t ___lineLen1, int32_t ___groupLen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LipingShare.LCLib.Asn1Processor.Asn1Util::GenStr(System.Int32,System.Char)
extern "C"  String_t* Asn1Util_GenStr_m3970829795 (Il2CppObject * __this /* static, unused */, int32_t ___len0, Il2CppChar ___xch1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Util::BytesToLong(System.Byte[])
extern "C"  int64_t Asn1Util_BytesToLong_m1963652187 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LipingShare.LCLib.Asn1Processor.Asn1Util::BytesToString(System.Byte[])
extern "C"  String_t* Asn1Util_BytesToString_m2403670248 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LipingShare.LCLib.Asn1Processor.Asn1Util::ToHexString(System.Byte[])
extern "C"  String_t* Asn1Util_ToHexString_m3932902392 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LipingShare.LCLib.Asn1Processor.Asn1Util::BytePrecision(System.UInt64)
extern "C"  int32_t Asn1Util_BytePrecision_m2588186404 (Il2CppObject * __this /* static, unused */, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LipingShare.LCLib.Asn1Processor.Asn1Util::DERLengthEncode(System.IO.Stream,System.UInt64)
extern "C"  int32_t Asn1Util_DERLengthEncode_m3789574000 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___xdata0, uint64_t ___length1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Util::DerLengthDecode(System.IO.Stream,System.Boolean&)
extern "C"  int64_t Asn1Util_DerLengthDecode_m23376285 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___bt0, bool* ___isIndefiniteLength1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LipingShare.LCLib.Asn1Processor.Asn1Util::GetTagName(System.Byte)
extern "C"  String_t* Asn1Util_GetTagName_m2899215986 (Il2CppObject * __this /* static, unused */, uint8_t ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LipingShare.LCLib.Asn1Processor.Asn1Util::.cctor()
extern "C"  void Asn1Util__cctor_m4280155068 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
