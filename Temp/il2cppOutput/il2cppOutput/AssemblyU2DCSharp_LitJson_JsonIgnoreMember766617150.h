﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t362681087;

#include "mscorlib_System_Attribute542643598.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonIgnoreMember
struct  JsonIgnoreMember_t766617150  : public Attribute_t542643598
{
public:
	// System.Collections.Generic.HashSet`1<System.String> LitJson.JsonIgnoreMember::<Members>k__BackingField
	HashSet_1_t362681087 * ___U3CMembersU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CMembersU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JsonIgnoreMember_t766617150, ___U3CMembersU3Ek__BackingField_0)); }
	inline HashSet_1_t362681087 * get_U3CMembersU3Ek__BackingField_0() const { return ___U3CMembersU3Ek__BackingField_0; }
	inline HashSet_1_t362681087 ** get_address_of_U3CMembersU3Ek__BackingField_0() { return &___U3CMembersU3Ek__BackingField_0; }
	inline void set_U3CMembersU3Ek__BackingField_0(HashSet_1_t362681087 * value)
	{
		___U3CMembersU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMembersU3Ek__BackingField_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
