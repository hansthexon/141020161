﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey13
struct U3CInsertAllU3Ec__AnonStorey13_t3050074398;

#include "codegen/il2cpp-codegen.h"

// System.Void SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey13::.ctor()
extern "C"  void U3CInsertAllU3Ec__AnonStorey13__ctor_m1763325313 (U3CInsertAllU3Ec__AnonStorey13_t3050074398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey13::<>m__7()
extern "C"  void U3CInsertAllU3Ec__AnonStorey13_U3CU3Em__7_m318710093 (U3CInsertAllU3Ec__AnonStorey13_t3050074398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
