﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct DefaultComparer_t2205520327;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor()
extern "C"  void DefaultComparer__ctor_m1129894432_gshared (DefaultComparer_t2205520327 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1129894432(__this, method) ((  void (*) (DefaultComparer_t2205520327 *, const MethodInfo*))DefaultComparer__ctor_m1129894432_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3994715483_gshared (DefaultComparer_t2205520327 * __this, KeyValuePair_2_t3716250094  ___x0, KeyValuePair_2_t3716250094  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m3994715483(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t2205520327 *, KeyValuePair_2_t3716250094 , KeyValuePair_2_t3716250094 , const MethodInfo*))DefaultComparer_Compare_m3994715483_gshared)(__this, ___x0, ___y1, method)
