﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Purchasing.IStoreController
struct IStoreController_t92554892;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t2460996543;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// UnityEngine.Purchasing.Security.CrossPlatformValidator
struct CrossPlatformValidator_t305796431;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Purchaser
struct  Purchaser_t1507804203  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Purchaser::canupload
	bool ___canupload_2;
	// System.String Purchaser::expDate
	String_t* ___expDate_20;
	// System.String Purchaser::PackName
	String_t* ___PackName_21;
	// System.String Purchaser::packdeatils
	String_t* ___packdeatils_22;
	// UnityEngine.WWW Purchaser::www
	WWW_t2919945039 * ___www_23;
	// UnityEngine.Purchasing.Security.CrossPlatformValidator Purchaser::validator
	CrossPlatformValidator_t305796431 * ___validator_24;
	// System.String Purchaser::setpackname
	String_t* ___setpackname_25;
	// UnityEngine.UI.Text Purchaser::child
	Text_t356221433 * ___child_26;
	// UnityEngine.UI.Text Purchaser::pname
	Text_t356221433 * ___pname_27;
	// UnityEngine.UI.Text Purchaser::email
	Text_t356221433 * ___email_28;
	// UnityEngine.UI.Text Purchaser::add
	Text_t356221433 * ___add_29;
	// UnityEngine.UI.Text Purchaser::day
	Text_t356221433 * ___day_30;
	// UnityEngine.UI.Text Purchaser::month
	Text_t356221433 * ___month_31;
	// UnityEngine.UI.Text Purchaser::year
	Text_t356221433 * ___year_32;
	// UnityEngine.UI.Text Purchaser::scode
	Text_t356221433 * ___scode_33;
	// System.String Purchaser::Platforms
	String_t* ___Platforms_34;
	// System.String Purchaser::schooolscode
	String_t* ___schooolscode_35;

public:
	inline static int32_t get_offset_of_canupload_2() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___canupload_2)); }
	inline bool get_canupload_2() const { return ___canupload_2; }
	inline bool* get_address_of_canupload_2() { return &___canupload_2; }
	inline void set_canupload_2(bool value)
	{
		___canupload_2 = value;
	}

	inline static int32_t get_offset_of_expDate_20() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___expDate_20)); }
	inline String_t* get_expDate_20() const { return ___expDate_20; }
	inline String_t** get_address_of_expDate_20() { return &___expDate_20; }
	inline void set_expDate_20(String_t* value)
	{
		___expDate_20 = value;
		Il2CppCodeGenWriteBarrier(&___expDate_20, value);
	}

	inline static int32_t get_offset_of_PackName_21() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___PackName_21)); }
	inline String_t* get_PackName_21() const { return ___PackName_21; }
	inline String_t** get_address_of_PackName_21() { return &___PackName_21; }
	inline void set_PackName_21(String_t* value)
	{
		___PackName_21 = value;
		Il2CppCodeGenWriteBarrier(&___PackName_21, value);
	}

	inline static int32_t get_offset_of_packdeatils_22() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___packdeatils_22)); }
	inline String_t* get_packdeatils_22() const { return ___packdeatils_22; }
	inline String_t** get_address_of_packdeatils_22() { return &___packdeatils_22; }
	inline void set_packdeatils_22(String_t* value)
	{
		___packdeatils_22 = value;
		Il2CppCodeGenWriteBarrier(&___packdeatils_22, value);
	}

	inline static int32_t get_offset_of_www_23() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___www_23)); }
	inline WWW_t2919945039 * get_www_23() const { return ___www_23; }
	inline WWW_t2919945039 ** get_address_of_www_23() { return &___www_23; }
	inline void set_www_23(WWW_t2919945039 * value)
	{
		___www_23 = value;
		Il2CppCodeGenWriteBarrier(&___www_23, value);
	}

	inline static int32_t get_offset_of_validator_24() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___validator_24)); }
	inline CrossPlatformValidator_t305796431 * get_validator_24() const { return ___validator_24; }
	inline CrossPlatformValidator_t305796431 ** get_address_of_validator_24() { return &___validator_24; }
	inline void set_validator_24(CrossPlatformValidator_t305796431 * value)
	{
		___validator_24 = value;
		Il2CppCodeGenWriteBarrier(&___validator_24, value);
	}

	inline static int32_t get_offset_of_setpackname_25() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___setpackname_25)); }
	inline String_t* get_setpackname_25() const { return ___setpackname_25; }
	inline String_t** get_address_of_setpackname_25() { return &___setpackname_25; }
	inline void set_setpackname_25(String_t* value)
	{
		___setpackname_25 = value;
		Il2CppCodeGenWriteBarrier(&___setpackname_25, value);
	}

	inline static int32_t get_offset_of_child_26() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___child_26)); }
	inline Text_t356221433 * get_child_26() const { return ___child_26; }
	inline Text_t356221433 ** get_address_of_child_26() { return &___child_26; }
	inline void set_child_26(Text_t356221433 * value)
	{
		___child_26 = value;
		Il2CppCodeGenWriteBarrier(&___child_26, value);
	}

	inline static int32_t get_offset_of_pname_27() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___pname_27)); }
	inline Text_t356221433 * get_pname_27() const { return ___pname_27; }
	inline Text_t356221433 ** get_address_of_pname_27() { return &___pname_27; }
	inline void set_pname_27(Text_t356221433 * value)
	{
		___pname_27 = value;
		Il2CppCodeGenWriteBarrier(&___pname_27, value);
	}

	inline static int32_t get_offset_of_email_28() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___email_28)); }
	inline Text_t356221433 * get_email_28() const { return ___email_28; }
	inline Text_t356221433 ** get_address_of_email_28() { return &___email_28; }
	inline void set_email_28(Text_t356221433 * value)
	{
		___email_28 = value;
		Il2CppCodeGenWriteBarrier(&___email_28, value);
	}

	inline static int32_t get_offset_of_add_29() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___add_29)); }
	inline Text_t356221433 * get_add_29() const { return ___add_29; }
	inline Text_t356221433 ** get_address_of_add_29() { return &___add_29; }
	inline void set_add_29(Text_t356221433 * value)
	{
		___add_29 = value;
		Il2CppCodeGenWriteBarrier(&___add_29, value);
	}

	inline static int32_t get_offset_of_day_30() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___day_30)); }
	inline Text_t356221433 * get_day_30() const { return ___day_30; }
	inline Text_t356221433 ** get_address_of_day_30() { return &___day_30; }
	inline void set_day_30(Text_t356221433 * value)
	{
		___day_30 = value;
		Il2CppCodeGenWriteBarrier(&___day_30, value);
	}

	inline static int32_t get_offset_of_month_31() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___month_31)); }
	inline Text_t356221433 * get_month_31() const { return ___month_31; }
	inline Text_t356221433 ** get_address_of_month_31() { return &___month_31; }
	inline void set_month_31(Text_t356221433 * value)
	{
		___month_31 = value;
		Il2CppCodeGenWriteBarrier(&___month_31, value);
	}

	inline static int32_t get_offset_of_year_32() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___year_32)); }
	inline Text_t356221433 * get_year_32() const { return ___year_32; }
	inline Text_t356221433 ** get_address_of_year_32() { return &___year_32; }
	inline void set_year_32(Text_t356221433 * value)
	{
		___year_32 = value;
		Il2CppCodeGenWriteBarrier(&___year_32, value);
	}

	inline static int32_t get_offset_of_scode_33() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___scode_33)); }
	inline Text_t356221433 * get_scode_33() const { return ___scode_33; }
	inline Text_t356221433 ** get_address_of_scode_33() { return &___scode_33; }
	inline void set_scode_33(Text_t356221433 * value)
	{
		___scode_33 = value;
		Il2CppCodeGenWriteBarrier(&___scode_33, value);
	}

	inline static int32_t get_offset_of_Platforms_34() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___Platforms_34)); }
	inline String_t* get_Platforms_34() const { return ___Platforms_34; }
	inline String_t** get_address_of_Platforms_34() { return &___Platforms_34; }
	inline void set_Platforms_34(String_t* value)
	{
		___Platforms_34 = value;
		Il2CppCodeGenWriteBarrier(&___Platforms_34, value);
	}

	inline static int32_t get_offset_of_schooolscode_35() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___schooolscode_35)); }
	inline String_t* get_schooolscode_35() const { return ___schooolscode_35; }
	inline String_t** get_address_of_schooolscode_35() { return &___schooolscode_35; }
	inline void set_schooolscode_35(String_t* value)
	{
		___schooolscode_35 = value;
		Il2CppCodeGenWriteBarrier(&___schooolscode_35, value);
	}
};

struct Purchaser_t1507804203_StaticFields
{
public:
	// UnityEngine.Purchasing.IStoreController Purchaser::m_StoreController
	Il2CppObject * ___m_StoreController_3;
	// UnityEngine.Purchasing.IExtensionProvider Purchaser::m_StoreExtensionProvider
	Il2CppObject * ___m_StoreExtensionProvider_4;
	// System.String Purchaser::kProductIDConsumable
	String_t* ___kProductIDConsumable_5;
	// System.String Purchaser::kProductIDNonConsumable
	String_t* ___kProductIDNonConsumable_6;
	// System.String Purchaser::kProductIDSubscription
	String_t* ___kProductIDSubscription_7;
	// System.String Purchaser::kProductIDSubscription24
	String_t* ___kProductIDSubscription24_8;
	// System.String Purchaser::kProductIDSubscription36
	String_t* ___kProductIDSubscription36_9;
	// System.String Purchaser::kProductNameAppleConsumable
	String_t* ___kProductNameAppleConsumable_10;
	// System.String Purchaser::kProductNameAppleNonConsumable
	String_t* ___kProductNameAppleNonConsumable_11;
	// System.String Purchaser::kProductNameAppleSubscription
	String_t* ___kProductNameAppleSubscription_12;
	// System.String Purchaser::kProductNameAppleSubscription24
	String_t* ___kProductNameAppleSubscription24_13;
	// System.String Purchaser::kProductNameAppleSubscription36
	String_t* ___kProductNameAppleSubscription36_14;
	// System.String Purchaser::kProductNameGooglePlayConsumable
	String_t* ___kProductNameGooglePlayConsumable_15;
	// System.String Purchaser::kProductNameGooglePlayNonConsumable
	String_t* ___kProductNameGooglePlayNonConsumable_16;
	// System.String Purchaser::kProductNameGooglePlaySubscription
	String_t* ___kProductNameGooglePlaySubscription_17;
	// System.String Purchaser::kProductNameGooglePlaySubscription24
	String_t* ___kProductNameGooglePlaySubscription24_18;
	// System.String Purchaser::kProductNameGooglePlaySubscription36
	String_t* ___kProductNameGooglePlaySubscription36_19;
	// System.Action`1<System.Boolean> Purchaser::<>f__am$cache22
	Action_1_t3627374100 * ___U3CU3Ef__amU24cache22_36;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Purchaser::<>f__switch$map0
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map0_37;

public:
	inline static int32_t get_offset_of_m_StoreController_3() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___m_StoreController_3)); }
	inline Il2CppObject * get_m_StoreController_3() const { return ___m_StoreController_3; }
	inline Il2CppObject ** get_address_of_m_StoreController_3() { return &___m_StoreController_3; }
	inline void set_m_StoreController_3(Il2CppObject * value)
	{
		___m_StoreController_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_StoreController_3, value);
	}

	inline static int32_t get_offset_of_m_StoreExtensionProvider_4() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___m_StoreExtensionProvider_4)); }
	inline Il2CppObject * get_m_StoreExtensionProvider_4() const { return ___m_StoreExtensionProvider_4; }
	inline Il2CppObject ** get_address_of_m_StoreExtensionProvider_4() { return &___m_StoreExtensionProvider_4; }
	inline void set_m_StoreExtensionProvider_4(Il2CppObject * value)
	{
		___m_StoreExtensionProvider_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_StoreExtensionProvider_4, value);
	}

	inline static int32_t get_offset_of_kProductIDConsumable_5() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductIDConsumable_5)); }
	inline String_t* get_kProductIDConsumable_5() const { return ___kProductIDConsumable_5; }
	inline String_t** get_address_of_kProductIDConsumable_5() { return &___kProductIDConsumable_5; }
	inline void set_kProductIDConsumable_5(String_t* value)
	{
		___kProductIDConsumable_5 = value;
		Il2CppCodeGenWriteBarrier(&___kProductIDConsumable_5, value);
	}

	inline static int32_t get_offset_of_kProductIDNonConsumable_6() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductIDNonConsumable_6)); }
	inline String_t* get_kProductIDNonConsumable_6() const { return ___kProductIDNonConsumable_6; }
	inline String_t** get_address_of_kProductIDNonConsumable_6() { return &___kProductIDNonConsumable_6; }
	inline void set_kProductIDNonConsumable_6(String_t* value)
	{
		___kProductIDNonConsumable_6 = value;
		Il2CppCodeGenWriteBarrier(&___kProductIDNonConsumable_6, value);
	}

	inline static int32_t get_offset_of_kProductIDSubscription_7() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductIDSubscription_7)); }
	inline String_t* get_kProductIDSubscription_7() const { return ___kProductIDSubscription_7; }
	inline String_t** get_address_of_kProductIDSubscription_7() { return &___kProductIDSubscription_7; }
	inline void set_kProductIDSubscription_7(String_t* value)
	{
		___kProductIDSubscription_7 = value;
		Il2CppCodeGenWriteBarrier(&___kProductIDSubscription_7, value);
	}

	inline static int32_t get_offset_of_kProductIDSubscription24_8() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductIDSubscription24_8)); }
	inline String_t* get_kProductIDSubscription24_8() const { return ___kProductIDSubscription24_8; }
	inline String_t** get_address_of_kProductIDSubscription24_8() { return &___kProductIDSubscription24_8; }
	inline void set_kProductIDSubscription24_8(String_t* value)
	{
		___kProductIDSubscription24_8 = value;
		Il2CppCodeGenWriteBarrier(&___kProductIDSubscription24_8, value);
	}

	inline static int32_t get_offset_of_kProductIDSubscription36_9() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductIDSubscription36_9)); }
	inline String_t* get_kProductIDSubscription36_9() const { return ___kProductIDSubscription36_9; }
	inline String_t** get_address_of_kProductIDSubscription36_9() { return &___kProductIDSubscription36_9; }
	inline void set_kProductIDSubscription36_9(String_t* value)
	{
		___kProductIDSubscription36_9 = value;
		Il2CppCodeGenWriteBarrier(&___kProductIDSubscription36_9, value);
	}

	inline static int32_t get_offset_of_kProductNameAppleConsumable_10() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductNameAppleConsumable_10)); }
	inline String_t* get_kProductNameAppleConsumable_10() const { return ___kProductNameAppleConsumable_10; }
	inline String_t** get_address_of_kProductNameAppleConsumable_10() { return &___kProductNameAppleConsumable_10; }
	inline void set_kProductNameAppleConsumable_10(String_t* value)
	{
		___kProductNameAppleConsumable_10 = value;
		Il2CppCodeGenWriteBarrier(&___kProductNameAppleConsumable_10, value);
	}

	inline static int32_t get_offset_of_kProductNameAppleNonConsumable_11() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductNameAppleNonConsumable_11)); }
	inline String_t* get_kProductNameAppleNonConsumable_11() const { return ___kProductNameAppleNonConsumable_11; }
	inline String_t** get_address_of_kProductNameAppleNonConsumable_11() { return &___kProductNameAppleNonConsumable_11; }
	inline void set_kProductNameAppleNonConsumable_11(String_t* value)
	{
		___kProductNameAppleNonConsumable_11 = value;
		Il2CppCodeGenWriteBarrier(&___kProductNameAppleNonConsumable_11, value);
	}

	inline static int32_t get_offset_of_kProductNameAppleSubscription_12() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductNameAppleSubscription_12)); }
	inline String_t* get_kProductNameAppleSubscription_12() const { return ___kProductNameAppleSubscription_12; }
	inline String_t** get_address_of_kProductNameAppleSubscription_12() { return &___kProductNameAppleSubscription_12; }
	inline void set_kProductNameAppleSubscription_12(String_t* value)
	{
		___kProductNameAppleSubscription_12 = value;
		Il2CppCodeGenWriteBarrier(&___kProductNameAppleSubscription_12, value);
	}

	inline static int32_t get_offset_of_kProductNameAppleSubscription24_13() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductNameAppleSubscription24_13)); }
	inline String_t* get_kProductNameAppleSubscription24_13() const { return ___kProductNameAppleSubscription24_13; }
	inline String_t** get_address_of_kProductNameAppleSubscription24_13() { return &___kProductNameAppleSubscription24_13; }
	inline void set_kProductNameAppleSubscription24_13(String_t* value)
	{
		___kProductNameAppleSubscription24_13 = value;
		Il2CppCodeGenWriteBarrier(&___kProductNameAppleSubscription24_13, value);
	}

	inline static int32_t get_offset_of_kProductNameAppleSubscription36_14() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductNameAppleSubscription36_14)); }
	inline String_t* get_kProductNameAppleSubscription36_14() const { return ___kProductNameAppleSubscription36_14; }
	inline String_t** get_address_of_kProductNameAppleSubscription36_14() { return &___kProductNameAppleSubscription36_14; }
	inline void set_kProductNameAppleSubscription36_14(String_t* value)
	{
		___kProductNameAppleSubscription36_14 = value;
		Il2CppCodeGenWriteBarrier(&___kProductNameAppleSubscription36_14, value);
	}

	inline static int32_t get_offset_of_kProductNameGooglePlayConsumable_15() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductNameGooglePlayConsumable_15)); }
	inline String_t* get_kProductNameGooglePlayConsumable_15() const { return ___kProductNameGooglePlayConsumable_15; }
	inline String_t** get_address_of_kProductNameGooglePlayConsumable_15() { return &___kProductNameGooglePlayConsumable_15; }
	inline void set_kProductNameGooglePlayConsumable_15(String_t* value)
	{
		___kProductNameGooglePlayConsumable_15 = value;
		Il2CppCodeGenWriteBarrier(&___kProductNameGooglePlayConsumable_15, value);
	}

	inline static int32_t get_offset_of_kProductNameGooglePlayNonConsumable_16() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductNameGooglePlayNonConsumable_16)); }
	inline String_t* get_kProductNameGooglePlayNonConsumable_16() const { return ___kProductNameGooglePlayNonConsumable_16; }
	inline String_t** get_address_of_kProductNameGooglePlayNonConsumable_16() { return &___kProductNameGooglePlayNonConsumable_16; }
	inline void set_kProductNameGooglePlayNonConsumable_16(String_t* value)
	{
		___kProductNameGooglePlayNonConsumable_16 = value;
		Il2CppCodeGenWriteBarrier(&___kProductNameGooglePlayNonConsumable_16, value);
	}

	inline static int32_t get_offset_of_kProductNameGooglePlaySubscription_17() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductNameGooglePlaySubscription_17)); }
	inline String_t* get_kProductNameGooglePlaySubscription_17() const { return ___kProductNameGooglePlaySubscription_17; }
	inline String_t** get_address_of_kProductNameGooglePlaySubscription_17() { return &___kProductNameGooglePlaySubscription_17; }
	inline void set_kProductNameGooglePlaySubscription_17(String_t* value)
	{
		___kProductNameGooglePlaySubscription_17 = value;
		Il2CppCodeGenWriteBarrier(&___kProductNameGooglePlaySubscription_17, value);
	}

	inline static int32_t get_offset_of_kProductNameGooglePlaySubscription24_18() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductNameGooglePlaySubscription24_18)); }
	inline String_t* get_kProductNameGooglePlaySubscription24_18() const { return ___kProductNameGooglePlaySubscription24_18; }
	inline String_t** get_address_of_kProductNameGooglePlaySubscription24_18() { return &___kProductNameGooglePlaySubscription24_18; }
	inline void set_kProductNameGooglePlaySubscription24_18(String_t* value)
	{
		___kProductNameGooglePlaySubscription24_18 = value;
		Il2CppCodeGenWriteBarrier(&___kProductNameGooglePlaySubscription24_18, value);
	}

	inline static int32_t get_offset_of_kProductNameGooglePlaySubscription36_19() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductNameGooglePlaySubscription36_19)); }
	inline String_t* get_kProductNameGooglePlaySubscription36_19() const { return ___kProductNameGooglePlaySubscription36_19; }
	inline String_t** get_address_of_kProductNameGooglePlaySubscription36_19() { return &___kProductNameGooglePlaySubscription36_19; }
	inline void set_kProductNameGooglePlaySubscription36_19(String_t* value)
	{
		___kProductNameGooglePlaySubscription36_19 = value;
		Il2CppCodeGenWriteBarrier(&___kProductNameGooglePlaySubscription36_19, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache22_36() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___U3CU3Ef__amU24cache22_36)); }
	inline Action_1_t3627374100 * get_U3CU3Ef__amU24cache22_36() const { return ___U3CU3Ef__amU24cache22_36; }
	inline Action_1_t3627374100 ** get_address_of_U3CU3Ef__amU24cache22_36() { return &___U3CU3Ef__amU24cache22_36; }
	inline void set_U3CU3Ef__amU24cache22_36(Action_1_t3627374100 * value)
	{
		___U3CU3Ef__amU24cache22_36 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache22_36, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_37() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___U3CU3Ef__switchU24map0_37)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map0_37() const { return ___U3CU3Ef__switchU24map0_37; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map0_37() { return &___U3CU3Ef__switchU24map0_37; }
	inline void set_U3CU3Ef__switchU24map0_37(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map0_37 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map0_37, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
