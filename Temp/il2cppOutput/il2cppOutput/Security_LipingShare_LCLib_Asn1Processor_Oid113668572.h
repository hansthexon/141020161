﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Specialized.StringDictionary
struct StringDictionary_t1070889667;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LipingShare.LCLib.Asn1Processor.Oid
struct  Oid_t113668572  : public Il2CppObject
{
public:

public:
};

struct Oid_t113668572_StaticFields
{
public:
	// System.Collections.Specialized.StringDictionary LipingShare.LCLib.Asn1Processor.Oid::oidDictionary
	StringDictionary_t1070889667 * ___oidDictionary_0;

public:
	inline static int32_t get_offset_of_oidDictionary_0() { return static_cast<int32_t>(offsetof(Oid_t113668572_StaticFields, ___oidDictionary_0)); }
	inline StringDictionary_t1070889667 * get_oidDictionary_0() const { return ___oidDictionary_0; }
	inline StringDictionary_t1070889667 ** get_address_of_oidDictionary_0() { return &___oidDictionary_0; }
	inline void set_oidDictionary_0(StringDictionary_t1070889667 * value)
	{
		___oidDictionary_0 = value;
		Il2CppCodeGenWriteBarrier(&___oidDictionary_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
