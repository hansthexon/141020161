﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey11
struct U3CInsertAllU3Ec__AnonStorey11_t4212873812;

#include "codegen/il2cpp-codegen.h"

// System.Void SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey11::.ctor()
extern "C"  void U3CInsertAllU3Ec__AnonStorey11__ctor_m4018499895 (U3CInsertAllU3Ec__AnonStorey11_t4212873812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey11::<>m__5()
extern "C"  void U3CInsertAllU3Ec__AnonStorey11_U3CU3Em__5_m2007352269 (U3CInsertAllU3Ec__AnonStorey11_t4212873812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
