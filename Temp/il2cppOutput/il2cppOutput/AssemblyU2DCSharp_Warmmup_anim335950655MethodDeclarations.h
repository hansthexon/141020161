﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Warmmup_anim
struct Warmmup_anim_t335950655;

#include "codegen/il2cpp-codegen.h"

// System.Void Warmmup_anim::.ctor()
extern "C"  void Warmmup_anim__ctor_m212908930 (Warmmup_anim_t335950655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Warmmup_anim::Start()
extern "C"  void Warmmup_anim_Start_m1045651454 (Warmmup_anim_t335950655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Warmmup_anim::Update()
extern "C"  void Warmmup_anim_Update_m2599861931 (Warmmup_anim_t335950655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Warmmup_anim::warmmupAnimate()
extern "C"  void Warmmup_anim_warmmupAnimate_m2842209740 (Warmmup_anim_t335950655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
