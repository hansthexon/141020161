﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t2376585677;
// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t2994157524;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3842368571;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.LinkedListNode`1<System.Int32>
struct LinkedListNode_1_t967983361;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Collections.Generic.LinkedListNode`1<System.Object>
struct LinkedListNode_1_t1585555208;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct List_1_t43281120;
// System.Collections.Generic.List`1<System.Byte>
struct List_1_t3052225568;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct List_1_t3085371226;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct List_1_t3702943073;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>
struct List_1_t3758245971;
// System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>
struct List_1_t867319046;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t243638650;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t3685274804;
// System.Collections.Generic.List`1<UnityEngine.Experimental.Director.Playable>
struct List_1_t3036666680;
// System.Collections.Generic.List`1<UnityEngine.RuntimePlatform>
struct List_1_t1238706099;
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t2425757932;
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t2990399006;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t573379950;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t1612828713;
// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>
struct List_1_t2379651176;
// System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>
struct List_1_t1327847638;
// System.Collections.Generic.IEnumerable`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct IEnumerable_1_t966287033;
// System.Collections.Generic.IEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct IEnumerator_1_t2444651111;
// System.Collections.Generic.ICollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct ICollection_1_t1626235293;
// System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct ReadOnlyCollection_1_t859945680;
// SQLite4Unity3d.SQLiteConnection/IndexedColumn[]
struct IndexedColumnU5BU5D_t4219656765;
// System.Predicate`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct Predicate_1_t3412097399;
// System.Collections.Generic.IComparer`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct IComparer_1_t2923590406;
// System.Comparison`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct Comparison_1_t1935898839;
// System.Collections.Generic.IEnumerable`1<System.Byte>
struct IEnumerable_1_t3975231481;
// System.Collections.Generic.IEnumerator`1<System.Byte>
struct IEnumerator_1_t1158628263;
// System.Collections.Generic.ICollection`1<System.Byte>
struct ICollection_1_t340212445;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Byte>
struct ReadOnlyCollection_1_t3868890128;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Predicate`1<System.Byte>
struct Predicate_1_t2126074551;
// System.Collections.Generic.IComparer`1<System.Byte>
struct IComparer_1_t1637567558;
// System.Comparison`1<System.Byte>
struct Comparison_1_t649875991;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23007666127.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23007666127MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1947527974.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22284023804.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22284023804MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1223885651.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23653207108.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23653207108MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_ArrayMetadata2008834462.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21345327748.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21345327748MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_ObjectMetadata3995922398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21043231486.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21043231486MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_PropertyMetadata3693826136.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22492407411.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22492407411MethodDeclarations.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I848034765.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Boolean3825574718MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21683227291.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21683227291MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22631255257.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22631255257MethodDeclarations.h"
#include "mscorlib_System_UInt16986882611.h"
#include "mscorlib_System_UInt16986882611MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23369039134.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23369039134MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile_Prof1724666488.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22141048052.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22141048052MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT3010530044.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu1199491699.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu1199491699MethodDeclarations.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2376585677.h"
#include "System_System_Collections_Generic_LinkedListNode_1_967983361.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_ObjectDisposedException2695136451MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException2695136451.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "System_System_Collections_Generic_LinkedListNode_1_967983361MethodDeclarations.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu1817063546.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu1817063546MethodDeclarations.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2994157524.h"
#include "System_System_Collections_Generic_LinkedListNode_11585555208.h"
#include "System_System_Collections_Generic_LinkedListNode_11585555208MethodDeclarations.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2376585677MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_ArgumentNullException628810857MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2994157524MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3872978090.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3872978090MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen43281120.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I674159988.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2586955242.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2586955242MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3052225568.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2620100900.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2620100900MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3085371226.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3237672747.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3237672747MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3702943073.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat975728254.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat975728254MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3292975645.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3292975645MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3758245971.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgum94157543.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat402048720.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat402048720MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen867319046.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg1498197914.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4073335620.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4073335620MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen243638650.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "UnityEngine_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3220004478.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3220004478MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3685274804.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "UnityEngine.UI_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2571396354.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2571396354MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3036666680.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3667545548.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat773435773.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat773435773MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1238706099.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1960487606.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1960487606MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2425757932.h"
#include "UnityEngine_UnityEngine_UICharInfo3056636800.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2525128680.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2525128680MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2990399006.h"
#include "UnityEngine_UnityEngine_UILineInfo3621277874.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat108109624.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat108109624MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen573379950.h"
#include "UnityEngine_UnityEngine_UIVertex1204258818.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1147558385.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1147558385MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828711.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1147558386.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1147558386MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828712.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1147558387.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1147558387MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828713.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1914380850.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1914380850MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2379651176.h"
#include "Vuforia.UnityExtensions_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat862577312.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat862577312MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1327847638.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Targe1958726506.h"
#include "mscorlib_System_Collections_Generic_List_1_gen43281120MethodDeclarations.h"
#include "mscorlib_System_NullReferenceException3156209119.h"
#include "mscorlib_System_InvalidCastException3625212209.h"
#include "mscorlib_System_Math2022911894MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCol859945680.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCol859945680MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen3412097399.h"
#include "mscorlib_System_Predicate_1_gen3412097399MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3859136403MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3859136403.h"
#include "mscorlib_System_Comparison_1_gen1935898839.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3052225568MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo3868890128.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo3868890128MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen2126074551.h"
#include "mscorlib_System_Predicate_1_gen2126074551MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2573113555MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2573113555.h"
#include "mscorlib_System_Comparison_1_gen649875991.h"

// System.Int32 System.Array::IndexOf<SQLite4Unity3d.SQLiteConnection/IndexedColumn>(!!0[],!!0,System.Int32,System.Int32)
extern "C"  int32_t Array_IndexOf_TisIndexedColumn_t674159988_m3711501861_gshared (Il2CppObject * __this /* static, unused */, IndexedColumnU5BU5D_t4219656765* p0, IndexedColumn_t674159988  p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisIndexedColumn_t674159988_m3711501861(__this /* static, unused */, p0, p1, p2, p3, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, IndexedColumnU5BU5D_t4219656765*, IndexedColumn_t674159988 , int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisIndexedColumn_t674159988_m3711501861_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<SQLite4Unity3d.SQLiteConnection/IndexedColumn>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C"  void Array_Sort_TisIndexedColumn_t674159988_m3644909758_gshared (Il2CppObject * __this /* static, unused */, IndexedColumnU5BU5D_t4219656765* p0, int32_t p1, int32_t p2, Il2CppObject* p3, const MethodInfo* method);
#define Array_Sort_TisIndexedColumn_t674159988_m3644909758(__this /* static, unused */, p0, p1, p2, p3, method) ((  void (*) (Il2CppObject * /* static, unused */, IndexedColumnU5BU5D_t4219656765*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))Array_Sort_TisIndexedColumn_t674159988_m3644909758_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<SQLite4Unity3d.SQLiteConnection/IndexedColumn>(!!0[],System.Int32,System.Comparison`1<!!0>)
extern "C"  void Array_Sort_TisIndexedColumn_t674159988_m1554502144_gshared (Il2CppObject * __this /* static, unused */, IndexedColumnU5BU5D_t4219656765* p0, int32_t p1, Comparison_1_t1935898839 * p2, const MethodInfo* method);
#define Array_Sort_TisIndexedColumn_t674159988_m1554502144(__this /* static, unused */, p0, p1, p2, method) ((  void (*) (Il2CppObject * /* static, unused */, IndexedColumnU5BU5D_t4219656765*, int32_t, Comparison_1_t1935898839 *, const MethodInfo*))Array_Sort_TisIndexedColumn_t674159988_m1554502144_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void System.Array::Resize<SQLite4Unity3d.SQLiteConnection/IndexedColumn>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisIndexedColumn_t674159988_m1821953009_gshared (Il2CppObject * __this /* static, unused */, IndexedColumnU5BU5D_t4219656765** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisIndexedColumn_t674159988_m1821953009(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, IndexedColumnU5BU5D_t4219656765**, int32_t, const MethodInfo*))Array_Resize_TisIndexedColumn_t674159988_m1821953009_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Byte>(!!0[],!!0,System.Int32,System.Int32)
extern "C"  int32_t Array_IndexOf_TisByte_t3683104436_m1782606156_gshared (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* p0, uint8_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisByte_t3683104436_m1782606156(__this /* static, unused */, p0, p1, p2, p3, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t3397334013*, uint8_t, int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisByte_t3683104436_m1782606156_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<System.Byte>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C"  void Array_Sort_TisByte_t3683104436_m1262898913_gshared (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* p0, int32_t p1, int32_t p2, Il2CppObject* p3, const MethodInfo* method);
#define Array_Sort_TisByte_t3683104436_m1262898913(__this /* static, unused */, p0, p1, p2, p3, method) ((  void (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t3397334013*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))Array_Sort_TisByte_t3683104436_m1262898913_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<System.Byte>(!!0[],System.Int32,System.Comparison`1<!!0>)
extern "C"  void Array_Sort_TisByte_t3683104436_m605564569_gshared (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* p0, int32_t p1, Comparison_1_t649875991 * p2, const MethodInfo* method);
#define Array_Sort_TisByte_t3683104436_m605564569(__this /* static, unused */, p0, p1, p2, method) ((  void (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t3397334013*, int32_t, Comparison_1_t649875991 *, const MethodInfo*))Array_Sort_TisByte_t3683104436_m605564569_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void System.Array::Resize<System.Byte>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisByte_t3683104436_m4242698446_gshared (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisByte_t3683104436_m4242698446(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t3397334013**, int32_t, const MethodInfo*))Array_Resize_TisByte_t3683104436_m4242698446_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3201181706_gshared (KeyValuePair_2_t3749587448 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m1350990071((KeyValuePair_2_t3749587448 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m2726037047((KeyValuePair_2_t3749587448 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3201181706_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	KeyValuePair_2__ctor_m3201181706(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1435832840_gshared (KeyValuePair_2_t3749587448 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m1435832840_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1435832840(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1350990071_gshared (KeyValuePair_2_t3749587448 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1350990071_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	KeyValuePair_2_set_Key_m1350990071(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3690000728_gshared (KeyValuePair_2_t3749587448 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3690000728_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3690000728(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2726037047_gshared (KeyValuePair_2_t3749587448 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2726037047_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	KeyValuePair_2_set_Value_m2726037047(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m1391611625_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1391611625_gshared (KeyValuePair_2_t3749587448 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1391611625_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m1435832840((KeyValuePair_2_t3749587448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m1435832840((KeyValuePair_2_t3749587448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m2960866144((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		Il2CppObject * L_8 = KeyValuePair_2_get_Value_m3690000728((KeyValuePair_2_t3749587448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m3690000728((KeyValuePair_2_t3749587448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1391611625_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	return KeyValuePair_2_ToString_m1391611625(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m4017094104_gshared (KeyValuePair_2_t3007666127 * __this, int32_t ___key0, TrackableResultData_t1947527974  ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m3007662271((KeyValuePair_2_t3007666127 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TrackableResultData_t1947527974  L_1 = ___value1;
		KeyValuePair_2_set_Value_m4293346479((KeyValuePair_2_t3007666127 *)__this, (TrackableResultData_t1947527974 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m4017094104_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, TrackableResultData_t1947527974  ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3007666127 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3007666127 *>(__this + 1);
	KeyValuePair_2__ctor_m4017094104(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2161654122_gshared (KeyValuePair_2_t3007666127 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m2161654122_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3007666127 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3007666127 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2161654122(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3007662271_gshared (KeyValuePair_2_t3007666127 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3007662271_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3007666127 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3007666127 *>(__this + 1);
	KeyValuePair_2_set_Key_m3007662271(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Value()
extern "C"  TrackableResultData_t1947527974  KeyValuePair_2_get_Value_m616722410_gshared (KeyValuePair_2_t3007666127 * __this, const MethodInfo* method)
{
	{
		TrackableResultData_t1947527974  L_0 = (TrackableResultData_t1947527974 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  TrackableResultData_t1947527974  KeyValuePair_2_get_Value_m616722410_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3007666127 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3007666127 *>(__this + 1);
	return KeyValuePair_2_get_Value_m616722410(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m4293346479_gshared (KeyValuePair_2_t3007666127 * __this, TrackableResultData_t1947527974  ___value0, const MethodInfo* method)
{
	{
		TrackableResultData_t1947527974  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m4293346479_AdjustorThunk (Il2CppObject * __this, TrackableResultData_t1947527974  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3007666127 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3007666127 *>(__this + 1);
	KeyValuePair_2_set_Value_m4293346479(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m1671329_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1671329_gshared (KeyValuePair_2_t3007666127 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1671329_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	TrackableResultData_t1947527974  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m2161654122((KeyValuePair_2_t3007666127 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m2161654122((KeyValuePair_2_t3007666127 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m2960866144((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		TrackableResultData_t1947527974  L_8 = KeyValuePair_2_get_Value_m616722410((KeyValuePair_2_t3007666127 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		TrackableResultData_t1947527974  L_9 = KeyValuePair_2_get_Value_m616722410((KeyValuePair_2_t3007666127 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (TrackableResultData_t1947527974 )L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_13 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral372029425);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1671329_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3007666127 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3007666127 *>(__this + 1);
	return KeyValuePair_2_ToString_m1671329(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3037433849_gshared (KeyValuePair_2_t2284023804 * __this, int32_t ___key0, VirtualButtonData_t1223885651  ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m1250158214((KeyValuePair_2_t2284023804 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		VirtualButtonData_t1223885651  L_1 = ___value1;
		KeyValuePair_2_set_Value_m1534004038((KeyValuePair_2_t2284023804 *)__this, (VirtualButtonData_t1223885651 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3037433849_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, VirtualButtonData_t1223885651  ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2284023804 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2284023804 *>(__this + 1);
	KeyValuePair_2__ctor_m3037433849(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m737348899_gshared (KeyValuePair_2_t2284023804 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m737348899_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2284023804 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2284023804 *>(__this + 1);
	return KeyValuePair_2_get_Key_m737348899(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1250158214_gshared (KeyValuePair_2_t2284023804 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1250158214_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2284023804 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2284023804 *>(__this + 1);
	KeyValuePair_2_set_Key_m1250158214(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Value()
extern "C"  VirtualButtonData_t1223885651  KeyValuePair_2_get_Value_m2492481859_gshared (KeyValuePair_2_t2284023804 * __this, const MethodInfo* method)
{
	{
		VirtualButtonData_t1223885651  L_0 = (VirtualButtonData_t1223885651 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  VirtualButtonData_t1223885651  KeyValuePair_2_get_Value_m2492481859_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2284023804 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2284023804 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2492481859(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1534004038_gshared (KeyValuePair_2_t2284023804 * __this, VirtualButtonData_t1223885651  ___value0, const MethodInfo* method)
{
	{
		VirtualButtonData_t1223885651  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1534004038_AdjustorThunk (Il2CppObject * __this, VirtualButtonData_t1223885651  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2284023804 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2284023804 *>(__this + 1);
	KeyValuePair_2_set_Value_m1534004038(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m471642322_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m471642322_gshared (KeyValuePair_2_t2284023804 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m471642322_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	VirtualButtonData_t1223885651  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m737348899((KeyValuePair_2_t2284023804 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m737348899((KeyValuePair_2_t2284023804 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m2960866144((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		VirtualButtonData_t1223885651  L_8 = KeyValuePair_2_get_Value_m2492481859((KeyValuePair_2_t2284023804 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		VirtualButtonData_t1223885651  L_9 = KeyValuePair_2_get_Value_m2492481859((KeyValuePair_2_t2284023804 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (VirtualButtonData_t1223885651 )L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_13 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral372029425);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m471642322_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2284023804 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2284023804 *>(__this + 1);
	return KeyValuePair_2_ToString_m471642322(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1237286508_gshared (KeyValuePair_2_t3653207108 * __this, Il2CppObject * ___key0, ArrayMetadata_t2008834462  ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m2725591729((KeyValuePair_2_t3653207108 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ArrayMetadata_t2008834462  L_1 = ___value1;
		KeyValuePair_2_set_Value_m1176068673((KeyValuePair_2_t3653207108 *)__this, (ArrayMetadata_t2008834462 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1237286508_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, ArrayMetadata_t2008834462  ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3653207108 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3653207108 *>(__this + 1);
	KeyValuePair_2__ctor_m1237286508(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3635972942_gshared (KeyValuePair_2_t3653207108 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3635972942_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3653207108 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3653207108 *>(__this + 1);
	return KeyValuePair_2_get_Key_m3635972942(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2725591729_gshared (KeyValuePair_2_t3653207108 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2725591729_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3653207108 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3653207108 *>(__this + 1);
	KeyValuePair_2_set_Key_m2725591729(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>::get_Value()
extern "C"  ArrayMetadata_t2008834462  KeyValuePair_2_get_Value_m2561735502_gshared (KeyValuePair_2_t3653207108 * __this, const MethodInfo* method)
{
	{
		ArrayMetadata_t2008834462  L_0 = (ArrayMetadata_t2008834462 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  ArrayMetadata_t2008834462  KeyValuePair_2_get_Value_m2561735502_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3653207108 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3653207108 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2561735502(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1176068673_gshared (KeyValuePair_2_t3653207108 * __this, ArrayMetadata_t2008834462  ___value0, const MethodInfo* method)
{
	{
		ArrayMetadata_t2008834462  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1176068673_AdjustorThunk (Il2CppObject * __this, ArrayMetadata_t2008834462  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3653207108 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3653207108 *>(__this + 1);
	KeyValuePair_2_set_Value_m1176068673(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m4118098691_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m4118098691_gshared (KeyValuePair_2_t3653207108 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m4118098691_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	ArrayMetadata_t2008834462  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m3635972942((KeyValuePair_2_t3653207108 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m3635972942((KeyValuePair_2_t3653207108 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		ArrayMetadata_t2008834462  L_8 = KeyValuePair_2_get_Value_m2561735502((KeyValuePair_2_t3653207108 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		ArrayMetadata_t2008834462  L_9 = KeyValuePair_2_get_Value_m2561735502((KeyValuePair_2_t3653207108 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (ArrayMetadata_t2008834462 )L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_13 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral372029425);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m4118098691_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3653207108 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3653207108 *>(__this + 1);
	return KeyValuePair_2_ToString_m4118098691(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3056189548_gshared (KeyValuePair_2_t1345327748 * __this, Il2CppObject * ___key0, ObjectMetadata_t3995922398  ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m2315183517((KeyValuePair_2_t1345327748 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectMetadata_t3995922398  L_1 = ___value1;
		KeyValuePair_2_set_Value_m3049331685((KeyValuePair_2_t1345327748 *)__this, (ObjectMetadata_t3995922398 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3056189548_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, ObjectMetadata_t3995922398  ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1345327748 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1345327748 *>(__this + 1);
	KeyValuePair_2__ctor_m3056189548(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3628483074_gshared (KeyValuePair_2_t1345327748 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3628483074_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1345327748 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1345327748 *>(__this + 1);
	return KeyValuePair_2_get_Key_m3628483074(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2315183517_gshared (KeyValuePair_2_t1345327748 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2315183517_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1345327748 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1345327748 *>(__this + 1);
	KeyValuePair_2_set_Key_m2315183517(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>::get_Value()
extern "C"  ObjectMetadata_t3995922398  KeyValuePair_2_get_Value_m1238388442_gshared (KeyValuePair_2_t1345327748 * __this, const MethodInfo* method)
{
	{
		ObjectMetadata_t3995922398  L_0 = (ObjectMetadata_t3995922398 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  ObjectMetadata_t3995922398  KeyValuePair_2_get_Value_m1238388442_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1345327748 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1345327748 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1238388442(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3049331685_gshared (KeyValuePair_2_t1345327748 * __this, ObjectMetadata_t3995922398  ___value0, const MethodInfo* method)
{
	{
		ObjectMetadata_t3995922398  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3049331685_AdjustorThunk (Il2CppObject * __this, ObjectMetadata_t3995922398  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1345327748 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1345327748 *>(__this + 1);
	KeyValuePair_2_set_Value_m3049331685(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m3119357263_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3119357263_gshared (KeyValuePair_2_t1345327748 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3119357263_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	ObjectMetadata_t3995922398  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m3628483074((KeyValuePair_2_t1345327748 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m3628483074((KeyValuePair_2_t1345327748 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		ObjectMetadata_t3995922398  L_8 = KeyValuePair_2_get_Value_m1238388442((KeyValuePair_2_t1345327748 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		ObjectMetadata_t3995922398  L_9 = KeyValuePair_2_get_Value_m1238388442((KeyValuePair_2_t1345327748 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (ObjectMetadata_t3995922398 )L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_13 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral372029425);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3119357263_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1345327748 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1345327748 *>(__this + 1);
	return KeyValuePair_2_ToString_m3119357263(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3835749886_gshared (KeyValuePair_2_t1043231486 * __this, Il2CppObject * ___key0, PropertyMetadata_t3693826136  ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m3697022715((KeyValuePair_2_t1043231486 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		PropertyMetadata_t3693826136  L_1 = ___value1;
		KeyValuePair_2_set_Value_m603240643((KeyValuePair_2_t1043231486 *)__this, (PropertyMetadata_t3693826136 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3835749886_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, PropertyMetadata_t3693826136  ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1043231486 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1043231486 *>(__this + 1);
	KeyValuePair_2__ctor_m3835749886(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2516001448_gshared (KeyValuePair_2_t1043231486 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2516001448_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1043231486 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1043231486 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2516001448(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3697022715_gshared (KeyValuePair_2_t1043231486 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3697022715_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1043231486 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1043231486 *>(__this + 1);
	KeyValuePair_2_set_Key_m3697022715(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>::get_Value()
extern "C"  PropertyMetadata_t3693826136  KeyValuePair_2_get_Value_m264611424_gshared (KeyValuePair_2_t1043231486 * __this, const MethodInfo* method)
{
	{
		PropertyMetadata_t3693826136  L_0 = (PropertyMetadata_t3693826136 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  PropertyMetadata_t3693826136  KeyValuePair_2_get_Value_m264611424_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1043231486 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1043231486 *>(__this + 1);
	return KeyValuePair_2_get_Value_m264611424(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m603240643_gshared (KeyValuePair_2_t1043231486 * __this, PropertyMetadata_t3693826136  ___value0, const MethodInfo* method)
{
	{
		PropertyMetadata_t3693826136  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m603240643_AdjustorThunk (Il2CppObject * __this, PropertyMetadata_t3693826136  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1043231486 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1043231486 *>(__this + 1);
	KeyValuePair_2_set_Value_m603240643(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m2105819377_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2105819377_gshared (KeyValuePair_2_t1043231486 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2105819377_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	PropertyMetadata_t3693826136  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2516001448((KeyValuePair_2_t1043231486 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m2516001448((KeyValuePair_2_t1043231486 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		PropertyMetadata_t3693826136  L_8 = KeyValuePair_2_get_Value_m264611424((KeyValuePair_2_t1043231486 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		PropertyMetadata_t3693826136  L_9 = KeyValuePair_2_get_Value_m264611424((KeyValuePair_2_t1043231486 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (PropertyMetadata_t3693826136 )L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_13 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral372029425);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2105819377_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1043231486 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1043231486 *>(__this + 1);
	return KeyValuePair_2_ToString_m2105819377(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m950010662_gshared (KeyValuePair_2_t2492407411 * __this, Il2CppObject * ___key0, IndexInfo_t848034765  ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m2868193553((KeyValuePair_2_t2492407411 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IndexInfo_t848034765  L_1 = ___value1;
		KeyValuePair_2_set_Value_m2932767089((KeyValuePair_2_t2492407411 *)__this, (IndexInfo_t848034765 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m950010662_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, IndexInfo_t848034765  ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2492407411 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2492407411 *>(__this + 1);
	KeyValuePair_2__ctor_m950010662(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3094063940_gshared (KeyValuePair_2_t2492407411 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3094063940_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2492407411 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2492407411 *>(__this + 1);
	return KeyValuePair_2_get_Key_m3094063940(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2868193553_gshared (KeyValuePair_2_t2492407411 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2868193553_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2492407411 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2492407411 *>(__this + 1);
	KeyValuePair_2_set_Key_m2868193553(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Value()
extern "C"  IndexInfo_t848034765  KeyValuePair_2_get_Value_m1745261316_gshared (KeyValuePair_2_t2492407411 * __this, const MethodInfo* method)
{
	{
		IndexInfo_t848034765  L_0 = (IndexInfo_t848034765 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  IndexInfo_t848034765  KeyValuePair_2_get_Value_m1745261316_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2492407411 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2492407411 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1745261316(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2932767089_gshared (KeyValuePair_2_t2492407411 * __this, IndexInfo_t848034765  ___value0, const MethodInfo* method)
{
	{
		IndexInfo_t848034765  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2932767089_AdjustorThunk (Il2CppObject * __this, IndexInfo_t848034765  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2492407411 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2492407411 *>(__this + 1);
	KeyValuePair_2_set_Value_m2932767089(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m3738640143_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3738640143_gshared (KeyValuePair_2_t2492407411 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3738640143_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	IndexInfo_t848034765  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m3094063940((KeyValuePair_2_t2492407411 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m3094063940((KeyValuePair_2_t2492407411 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		IndexInfo_t848034765  L_8 = KeyValuePair_2_get_Value_m1745261316((KeyValuePair_2_t2492407411 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		IndexInfo_t848034765  L_9 = KeyValuePair_2_get_Value_m1745261316((KeyValuePair_2_t2492407411 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (IndexInfo_t848034765 )L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_13 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral372029425);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3738640143_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2492407411 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2492407411 *>(__this + 1);
	return KeyValuePair_2_ToString_m3738640143(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m4040336782_gshared (KeyValuePair_2_t1174980068 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m1222844869((KeyValuePair_2_t1174980068 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_1 = ___value1;
		KeyValuePair_2_set_Value_m965533293((KeyValuePair_2_t1174980068 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m4040336782_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	KeyValuePair_2__ctor_m4040336782(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2113318928_gshared (KeyValuePair_2_t1174980068 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2113318928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2113318928(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1222844869_gshared (KeyValuePair_2_t1174980068 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1222844869_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	KeyValuePair_2_set_Key_m1222844869(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C"  bool KeyValuePair_2_get_Value_m1916631176_gshared (KeyValuePair_2_t1174980068 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_value_1();
		return L_0;
	}
}
extern "C"  bool KeyValuePair_2_get_Value_m1916631176_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1916631176(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m965533293_gshared (KeyValuePair_2_t1174980068 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m965533293_AdjustorThunk (Il2CppObject * __this, bool ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	KeyValuePair_2_set_Value_m965533293(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m1739958171_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1739958171_gshared (KeyValuePair_2_t1174980068 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1739958171_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2113318928((KeyValuePair_2_t1174980068 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m2113318928((KeyValuePair_2_t1174980068 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		bool L_8 = KeyValuePair_2_get_Value_m1916631176((KeyValuePair_2_t1174980068 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		bool L_9 = KeyValuePair_2_get_Value_m1916631176((KeyValuePair_2_t1174980068 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (bool)L_9;
		String_t* L_10 = Boolean_ToString_m1253164328((bool*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1739958171_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	return KeyValuePair_2_ToString_m1739958171(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m400583398_gshared (KeyValuePair_2_t1683227291 * __this, Il2CppObject * ___key0, KeyValuePair_2_t38854645  ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m3257200763((KeyValuePair_2_t1683227291 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t38854645  L_1 = ___value1;
		KeyValuePair_2_set_Value_m815956739((KeyValuePair_2_t1683227291 *)__this, (KeyValuePair_2_t38854645 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m400583398_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, KeyValuePair_2_t38854645  ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1683227291 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1683227291 *>(__this + 1);
	KeyValuePair_2__ctor_m400583398(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m4247498472_gshared (KeyValuePair_2_t1683227291 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m4247498472_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1683227291 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1683227291 *>(__this + 1);
	return KeyValuePair_2_get_Key_m4247498472(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3257200763_gshared (KeyValuePair_2_t1683227291 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3257200763_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1683227291 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1683227291 *>(__this + 1);
	KeyValuePair_2_set_Key_m3257200763(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Value()
extern "C"  KeyValuePair_2_t38854645  KeyValuePair_2_get_Value_m667219360_gshared (KeyValuePair_2_t1683227291 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = (KeyValuePair_2_t38854645 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t38854645  KeyValuePair_2_get_Value_m667219360_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1683227291 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1683227291 *>(__this + 1);
	return KeyValuePair_2_get_Value_m667219360(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m815956739_gshared (KeyValuePair_2_t1683227291 * __this, KeyValuePair_2_t38854645  ___value0, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m815956739_AdjustorThunk (Il2CppObject * __this, KeyValuePair_2_t38854645  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1683227291 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1683227291 *>(__this + 1);
	KeyValuePair_2_set_Value_m815956739(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m4177387161_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m4177387161_gshared (KeyValuePair_2_t1683227291 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m4177387161_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	KeyValuePair_2_t38854645  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m4247498472((KeyValuePair_2_t1683227291 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m4247498472((KeyValuePair_2_t1683227291 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		KeyValuePair_2_t38854645  L_8 = KeyValuePair_2_get_Value_m667219360((KeyValuePair_2_t1683227291 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		KeyValuePair_2_t38854645  L_9 = KeyValuePair_2_get_Value_m667219360((KeyValuePair_2_t1683227291 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (KeyValuePair_2_t38854645 )L_9;
		Il2CppClass* il2cpp_this_typeinfo_104 = IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5);
		String_t* L_10 = ((  String_t* (*) (Il2CppObject *, const MethodInfo*))il2cpp_this_typeinfo_104->vtable[3].methodPtr)((Il2CppObject *)il2cpp_codegen_fake_box((&V_1)), /*hidden argument*/il2cpp_this_typeinfo_104->vtable[3].method);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m4177387161_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1683227291 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1683227291 *>(__this + 1);
	return KeyValuePair_2_ToString_m4177387161(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1877755778_gshared (KeyValuePair_2_t3716250094 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m1307112735((KeyValuePair_2_t3716250094 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m1921288671((KeyValuePair_2_t3716250094 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1877755778_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	KeyValuePair_2__ctor_m1877755778(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1454531804_gshared (KeyValuePair_2_t3716250094 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1454531804_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1454531804(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1307112735_gshared (KeyValuePair_2_t3716250094 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1307112735_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	KeyValuePair_2_set_Key_m1307112735(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3699669100_gshared (KeyValuePair_2_t3716250094 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m3699669100_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3699669100(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1921288671_gshared (KeyValuePair_2_t3716250094 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1921288671_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	KeyValuePair_2_set_Value_m1921288671(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m1394661909_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1394661909_gshared (KeyValuePair_2_t3716250094 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1394661909_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m1454531804((KeyValuePair_2_t3716250094 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m1454531804((KeyValuePair_2_t3716250094 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		int32_t L_8 = KeyValuePair_2_get_Value_m3699669100((KeyValuePair_2_t3716250094 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = KeyValuePair_2_get_Value_m3699669100((KeyValuePair_2_t3716250094 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		String_t* L_10 = Int32_ToString_m2960866144((int32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1394661909_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	return KeyValuePair_2_ToString_m1394661909(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1640124561_gshared (KeyValuePair_2_t38854645 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m744486900((KeyValuePair_2_t38854645 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m1416408204((KeyValuePair_2_t38854645 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1640124561_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	KeyValuePair_2__ctor_m1640124561(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2561166459_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2561166459_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2561166459(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m744486900_gshared (KeyValuePair_2_t38854645 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m744486900_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	KeyValuePair_2_set_Key_m744486900(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m499643803_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m499643803_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	return KeyValuePair_2_get_Value_m499643803(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1416408204_gshared (KeyValuePair_2_t38854645 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1416408204_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	KeyValuePair_2_set_Value_m1416408204(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m2613351884_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2613351884_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2613351884_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		Il2CppObject * L_8 = KeyValuePair_2_get_Value_m499643803((KeyValuePair_2_t38854645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m499643803((KeyValuePair_2_t38854645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2613351884_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	return KeyValuePair_2_ToString_m2613351884(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2351688349_gshared (KeyValuePair_2_t2631255257 * __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m3410524048((KeyValuePair_2_t2631255257 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint16_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m637397616((KeyValuePair_2_t2631255257 *)__this, (uint16_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2351688349_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2631255257 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2631255257 *>(__this + 1);
	KeyValuePair_2__ctor_m2351688349(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2800370719_gshared (KeyValuePair_2_t2631255257 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2800370719_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2631255257 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2631255257 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2800370719(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3410524048_gshared (KeyValuePair_2_t2631255257 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3410524048_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2631255257 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2631255257 *>(__this + 1);
	KeyValuePair_2_set_Key_m3410524048(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::get_Value()
extern "C"  uint16_t KeyValuePair_2_get_Value_m3658972159_gshared (KeyValuePair_2_t2631255257 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  uint16_t KeyValuePair_2_get_Value_m3658972159_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2631255257 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2631255257 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3658972159(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m637397616_gshared (KeyValuePair_2_t2631255257 * __this, uint16_t ___value0, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m637397616_AdjustorThunk (Il2CppObject * __this, uint16_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2631255257 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2631255257 *>(__this + 1);
	KeyValuePair_2_set_Value_m637397616(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m3637890248_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3637890248_gshared (KeyValuePair_2_t2631255257 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3637890248_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	uint16_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2800370719((KeyValuePair_2_t2631255257 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m2800370719((KeyValuePair_2_t2631255257 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		uint16_t L_8 = KeyValuePair_2_get_Value_m3658972159((KeyValuePair_2_t2631255257 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		uint16_t L_9 = KeyValuePair_2_get_Value_m3658972159((KeyValuePair_2_t2631255257 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (uint16_t)L_9;
		String_t* L_10 = UInt16_ToString_m2038947049((uint16_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3637890248_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2631255257 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2631255257 *>(__this + 1);
	return KeyValuePair_2_ToString_m3637890248(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m833149801_gshared (KeyValuePair_2_t3369039134 * __this, Il2CppObject * ___key0, ProfileData_t1724666488  ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m531783430((KeyValuePair_2_t3369039134 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ProfileData_t1724666488  L_1 = ___value1;
		KeyValuePair_2_set_Value_m3095194094((KeyValuePair_2_t3369039134 *)__this, (ProfileData_t1724666488 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m833149801_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, ProfileData_t1724666488  ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3369039134 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3369039134 *>(__this + 1);
	KeyValuePair_2__ctor_m833149801(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1277606655_gshared (KeyValuePair_2_t3369039134 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1277606655_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3369039134 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3369039134 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1277606655(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m531783430_gshared (KeyValuePair_2_t3369039134 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m531783430_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3369039134 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3369039134 *>(__this + 1);
	KeyValuePair_2_set_Key_m531783430(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Value()
extern "C"  ProfileData_t1724666488  KeyValuePair_2_get_Value_m699551839_gshared (KeyValuePair_2_t3369039134 * __this, const MethodInfo* method)
{
	{
		ProfileData_t1724666488  L_0 = (ProfileData_t1724666488 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  ProfileData_t1724666488  KeyValuePair_2_get_Value_m699551839_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3369039134 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3369039134 *>(__this + 1);
	return KeyValuePair_2_get_Value_m699551839(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3095194094_gshared (KeyValuePair_2_t3369039134 * __this, ProfileData_t1724666488  ___value0, const MethodInfo* method)
{
	{
		ProfileData_t1724666488  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3095194094_AdjustorThunk (Il2CppObject * __this, ProfileData_t1724666488  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3369039134 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3369039134 *>(__this + 1);
	KeyValuePair_2_set_Value_m3095194094(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m1711784974_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1711784974_gshared (KeyValuePair_2_t3369039134 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1711784974_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	ProfileData_t1724666488  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m1277606655((KeyValuePair_2_t3369039134 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m1277606655((KeyValuePair_2_t3369039134 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		ProfileData_t1724666488  L_8 = KeyValuePair_2_get_Value_m699551839((KeyValuePair_2_t3369039134 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		ProfileData_t1724666488  L_9 = KeyValuePair_2_get_Value_m699551839((KeyValuePair_2_t3369039134 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (ProfileData_t1724666488 )L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_13 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral372029425);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1711784974_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3369039134 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3369039134 *>(__this + 1);
	return KeyValuePair_2_ToString_m1711784974(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1737106953_gshared (KeyValuePair_2_t2141048052 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m3739204162((KeyValuePair_2_t2141048052 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m4035525298((KeyValuePair_2_t2141048052 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1737106953_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2141048052 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2141048052 *>(__this + 1);
	KeyValuePair_2__ctor_m1737106953(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1279069287_gshared (KeyValuePair_2_t2141048052 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m1279069287_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2141048052 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2141048052 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1279069287(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3739204162_gshared (KeyValuePair_2_t2141048052 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3739204162_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2141048052 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2141048052 *>(__this + 1);
	KeyValuePair_2_set_Key_m3739204162(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m4144029831_gshared (KeyValuePair_2_t2141048052 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m4144029831_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2141048052 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2141048052 *>(__this + 1);
	return KeyValuePair_2_get_Value_m4144029831(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m4035525298_gshared (KeyValuePair_2_t2141048052 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m4035525298_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2141048052 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2141048052 *>(__this + 1);
	KeyValuePair_2_set_Value_m4035525298(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m2641661512_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2641661512_gshared (KeyValuePair_2_t2141048052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2641661512_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m1279069287((KeyValuePair_2_t2141048052 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m1279069287((KeyValuePair_2_t2141048052 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral811305474);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_8 = (StringU5BU5D_t1642385972*)L_7;
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m4144029831((KeyValuePair_2_t2141048052 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 3;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_10 = KeyValuePair_2_get_Value_m4144029831((KeyValuePair_2_t2141048052 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_13 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral372029425);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2641661512_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2141048052 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2141048052 *>(__this + 1);
	return KeyValuePair_2_ToString_m2641661512(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C"  void Enumerator__ctor_m2750375776_gshared (Enumerator_t1199491699 * __this, LinkedList_1_t2376585677 * ___parent0, const MethodInfo* method)
{
	{
		LinkedList_1_t2376585677 * L_0 = ___parent0;
		__this->set_list_0(L_0);
		__this->set_current_1((LinkedListNode_1_t967983361 *)NULL);
		__this->set_index_2((-1));
		LinkedList_1_t2376585677 * L_1 = ___parent0;
		NullCheck(L_1);
		uint32_t L_2 = (uint32_t)L_1->get_version_1();
		__this->set_version_3(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2750375776_AdjustorThunk (Il2CppObject * __this, LinkedList_1_t2376585677 * ___parent0, const MethodInfo* method)
{
	Enumerator_t1199491699 * _thisAdjusted = reinterpret_cast<Enumerator_t1199491699 *>(__this + 1);
	Enumerator__ctor_m2750375776(_thisAdjusted, ___parent0, method);
}
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m31448777_gshared (Enumerator_t1199491699 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_Current_m2893808644((Enumerator_t1199491699 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m31448777_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1199491699 * _thisAdjusted = reinterpret_cast<Enumerator_t1199491699 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m31448777(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1112410187;
extern const uint32_t Enumerator_System_Collections_IEnumerator_Reset_m2493813781_MetadataUsageId;
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2493813781_gshared (Enumerator_t1199491699 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_Reset_m2493813781_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2376585677 * L_0 = (LinkedList_1_t2376585677 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t2376585677 * L_3 = (LinkedList_1_t2376585677 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_1();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral1112410187, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		__this->set_current_1((LinkedListNode_1_t967983361 *)NULL);
		__this->set_index_2((-1));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2493813781_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1199491699 * _thisAdjusted = reinterpret_cast<Enumerator_t1199491699 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2493813781(_thisAdjusted, method);
}
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::get_Current()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_get_Current_m2893808644_MetadataUsageId;
extern "C"  int32_t Enumerator_get_Current_m2893808644_gshared (Enumerator_t1199491699 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_get_Current_m2893808644_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2376585677 * L_0 = (LinkedList_1_t2376585677 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		LinkedListNode_1_t967983361 * L_2 = (LinkedListNode_1_t967983361 *)__this->get_current_1();
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		LinkedListNode_1_t967983361 * L_4 = (LinkedListNode_1_t967983361 *)__this->get_current_1();
		NullCheck((LinkedListNode_1_t967983361 *)L_4);
		int32_t L_5 = ((  int32_t (*) (LinkedListNode_1_t967983361 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((LinkedListNode_1_t967983361 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_5;
	}
}
extern "C"  int32_t Enumerator_get_Current_m2893808644_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1199491699 * _thisAdjusted = reinterpret_cast<Enumerator_t1199491699 *>(__this + 1);
	return Enumerator_get_Current_m2893808644(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::MoveNext()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1112410187;
extern const uint32_t Enumerator_MoveNext_m2295891353_MetadataUsageId;
extern "C"  bool Enumerator_MoveNext_m2295891353_gshared (Enumerator_t1199491699 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_MoveNext_m2295891353_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2376585677 * L_0 = (LinkedList_1_t2376585677 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t2376585677 * L_3 = (LinkedList_1_t2376585677 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_1();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral1112410187, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		LinkedListNode_1_t967983361 * L_6 = (LinkedListNode_1_t967983361 *)__this->get_current_1();
		if (L_6)
		{
			goto IL_0054;
		}
	}
	{
		LinkedList_1_t2376585677 * L_7 = (LinkedList_1_t2376585677 *)__this->get_list_0();
		NullCheck(L_7);
		LinkedListNode_1_t967983361 * L_8 = (LinkedListNode_1_t967983361 *)L_7->get_first_3();
		__this->set_current_1(L_8);
		goto IL_0082;
	}

IL_0054:
	{
		LinkedListNode_1_t967983361 * L_9 = (LinkedListNode_1_t967983361 *)__this->get_current_1();
		NullCheck(L_9);
		LinkedListNode_1_t967983361 * L_10 = (LinkedListNode_1_t967983361 *)L_9->get_forward_2();
		__this->set_current_1(L_10);
		LinkedListNode_1_t967983361 * L_11 = (LinkedListNode_1_t967983361 *)__this->get_current_1();
		LinkedList_1_t2376585677 * L_12 = (LinkedList_1_t2376585677 *)__this->get_list_0();
		NullCheck(L_12);
		LinkedListNode_1_t967983361 * L_13 = (LinkedListNode_1_t967983361 *)L_12->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t967983361 *)L_11) == ((Il2CppObject*)(LinkedListNode_1_t967983361 *)L_13))))
		{
			goto IL_0082;
		}
	}
	{
		__this->set_current_1((LinkedListNode_1_t967983361 *)NULL);
	}

IL_0082:
	{
		LinkedListNode_1_t967983361 * L_14 = (LinkedListNode_1_t967983361 *)__this->get_current_1();
		if (L_14)
		{
			goto IL_0096;
		}
	}
	{
		__this->set_index_2((-1));
		return (bool)0;
	}

IL_0096:
	{
		int32_t L_15 = (int32_t)__this->get_index_2();
		__this->set_index_2(((int32_t)((int32_t)L_15+(int32_t)1)));
		return (bool)1;
	}
}
extern "C"  bool Enumerator_MoveNext_m2295891353_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1199491699 * _thisAdjusted = reinterpret_cast<Enumerator_t1199491699 *>(__this + 1);
	return Enumerator_MoveNext_m2295891353(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::Dispose()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_Dispose_m4265648796_MetadataUsageId;
extern "C"  void Enumerator_Dispose_m4265648796_gshared (Enumerator_t1199491699 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_Dispose_m4265648796_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2376585677 * L_0 = (LinkedList_1_t2376585677 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		__this->set_current_1((LinkedListNode_1_t967983361 *)NULL);
		__this->set_list_0((LinkedList_1_t2376585677 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m4265648796_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1199491699 * _thisAdjusted = reinterpret_cast<Enumerator_t1199491699 *>(__this + 1);
	Enumerator_Dispose_m4265648796(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C"  void Enumerator__ctor_m1586864815_gshared (Enumerator_t1817063546 * __this, LinkedList_1_t2994157524 * ___parent0, const MethodInfo* method)
{
	{
		LinkedList_1_t2994157524 * L_0 = ___parent0;
		__this->set_list_0(L_0);
		__this->set_current_1((LinkedListNode_1_t1585555208 *)NULL);
		__this->set_index_2((-1));
		LinkedList_1_t2994157524 * L_1 = ___parent0;
		NullCheck(L_1);
		uint32_t L_2 = (uint32_t)L_1->get_version_1();
		__this->set_version_3(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1586864815_AdjustorThunk (Il2CppObject * __this, LinkedList_1_t2994157524 * ___parent0, const MethodInfo* method)
{
	Enumerator_t1817063546 * _thisAdjusted = reinterpret_cast<Enumerator_t1817063546 *>(__this + 1);
	Enumerator__ctor_m1586864815(_thisAdjusted, ___parent0, method);
}
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3175039148_gshared (Enumerator_t1817063546 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_Current_m3158498407((Enumerator_t1817063546 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3175039148_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1817063546 * _thisAdjusted = reinterpret_cast<Enumerator_t1817063546 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3175039148(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1112410187;
extern const uint32_t Enumerator_System_Collections_IEnumerator_Reset_m1061591080_MetadataUsageId;
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1061591080_gshared (Enumerator_t1817063546 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_Reset_m1061591080_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2994157524 * L_0 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t2994157524 * L_3 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_1();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral1112410187, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		__this->set_current_1((LinkedListNode_1_t1585555208 *)NULL);
		__this->set_index_2((-1));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1061591080_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1817063546 * _thisAdjusted = reinterpret_cast<Enumerator_t1817063546 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1061591080(_thisAdjusted, method);
}
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_get_Current_m3158498407_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_get_Current_m3158498407_gshared (Enumerator_t1817063546 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_get_Current_m3158498407_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2994157524 * L_0 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		LinkedListNode_1_t1585555208 * L_2 = (LinkedListNode_1_t1585555208 *)__this->get_current_1();
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		LinkedListNode_1_t1585555208 * L_4 = (LinkedListNode_1_t1585555208 *)__this->get_current_1();
		NullCheck((LinkedListNode_1_t1585555208 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((LinkedListNode_1_t1585555208 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_5;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m3158498407_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1817063546 * _thisAdjusted = reinterpret_cast<Enumerator_t1817063546 *>(__this + 1);
	return Enumerator_get_Current_m3158498407(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1112410187;
extern const uint32_t Enumerator_MoveNext_m1957727328_MetadataUsageId;
extern "C"  bool Enumerator_MoveNext_m1957727328_gshared (Enumerator_t1817063546 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_MoveNext_m1957727328_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2994157524 * L_0 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t2994157524 * L_3 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_1();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral1112410187, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		LinkedListNode_1_t1585555208 * L_6 = (LinkedListNode_1_t1585555208 *)__this->get_current_1();
		if (L_6)
		{
			goto IL_0054;
		}
	}
	{
		LinkedList_1_t2994157524 * L_7 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		NullCheck(L_7);
		LinkedListNode_1_t1585555208 * L_8 = (LinkedListNode_1_t1585555208 *)L_7->get_first_3();
		__this->set_current_1(L_8);
		goto IL_0082;
	}

IL_0054:
	{
		LinkedListNode_1_t1585555208 * L_9 = (LinkedListNode_1_t1585555208 *)__this->get_current_1();
		NullCheck(L_9);
		LinkedListNode_1_t1585555208 * L_10 = (LinkedListNode_1_t1585555208 *)L_9->get_forward_2();
		__this->set_current_1(L_10);
		LinkedListNode_1_t1585555208 * L_11 = (LinkedListNode_1_t1585555208 *)__this->get_current_1();
		LinkedList_1_t2994157524 * L_12 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		NullCheck(L_12);
		LinkedListNode_1_t1585555208 * L_13 = (LinkedListNode_1_t1585555208 *)L_12->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_11) == ((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_13))))
		{
			goto IL_0082;
		}
	}
	{
		__this->set_current_1((LinkedListNode_1_t1585555208 *)NULL);
	}

IL_0082:
	{
		LinkedListNode_1_t1585555208 * L_14 = (LinkedListNode_1_t1585555208 *)__this->get_current_1();
		if (L_14)
		{
			goto IL_0096;
		}
	}
	{
		__this->set_index_2((-1));
		return (bool)0;
	}

IL_0096:
	{
		int32_t L_15 = (int32_t)__this->get_index_2();
		__this->set_index_2(((int32_t)((int32_t)L_15+(int32_t)1)));
		return (bool)1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1957727328_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1817063546 * _thisAdjusted = reinterpret_cast<Enumerator_t1817063546 *>(__this + 1);
	return Enumerator_MoveNext_m1957727328(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_Dispose_m3217456423_MetadataUsageId;
extern "C"  void Enumerator_Dispose_m3217456423_gshared (Enumerator_t1817063546 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_Dispose_m3217456423_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2994157524 * L_0 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		__this->set_current_1((LinkedListNode_1_t1585555208 *)NULL);
		__this->set_list_0((LinkedList_1_t2994157524 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3217456423_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1817063546 * _thisAdjusted = reinterpret_cast<Enumerator_t1817063546 *>(__this + 1);
	Enumerator_Dispose_m3217456423(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LinkedList_1__ctor_m2999180666_MetadataUsageId;
extern "C"  void LinkedList_1__ctor_m2999180666_gshared (LinkedList_1_t2376585677 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m2999180666_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		__this->set_syncRoot_2(L_0);
		__this->set_first_3((LinkedListNode_1_t967983361 *)NULL);
		int32_t L_1 = (int32_t)0;
		V_0 = (uint32_t)L_1;
		__this->set_version_1(L_1);
		uint32_t L_2 = V_0;
		__this->set_count_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LinkedList_1__ctor_m2235619199_MetadataUsageId;
extern "C"  void LinkedList_1__ctor_m2235619199_gshared (LinkedList_1_t2376585677 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m2235619199_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((LinkedList_1_t2376585677 *)__this);
		((  void (*) (LinkedList_1_t2376585677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((LinkedList_1_t2376585677 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		SerializationInfo_t228987430 * L_0 = ___info0;
		__this->set_si_4(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_1, /*hidden argument*/NULL);
		__this->set_syncRoot_2(L_1);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1241496853_gshared (LinkedList_1_t2376585677 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		NullCheck((LinkedList_1_t2376585677 *)__this);
		((  LinkedListNode_1_t967983361 * (*) (LinkedList_1_t2376585677 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((LinkedList_1_t2376585677 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1185213181;
extern const uint32_t LinkedList_1_System_Collections_ICollection_CopyTo_m4084947380_MetadataUsageId;
extern "C"  void LinkedList_1_System_Collections_ICollection_CopyTo_m4084947380_gshared (LinkedList_1_t2376585677 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_System_Collections_ICollection_CopyTo_m4084947380_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Int32U5BU5D_t3030399641* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (Int32U5BU5D_t3030399641*)((Int32U5BU5D_t3030399641*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Int32U5BU5D_t3030399641* L_1 = V_0;
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, (String_t*)_stringLiteral1185213181, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		Int32U5BU5D_t3030399641* L_3 = V_0;
		int32_t L_4 = ___index1;
		NullCheck((LinkedList_1_t2376585677 *)__this);
		((  void (*) (LinkedList_1_t2376585677 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((LinkedList_1_t2376585677 *)__this, (Int32U5BU5D_t3030399641*)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1850278238_gshared (LinkedList_1_t2376585677 * __this, const MethodInfo* method)
{
	{
		NullCheck((LinkedList_1_t2376585677 *)__this);
		Enumerator_t1199491699  L_0 = ((  Enumerator_t1199491699  (*) (LinkedList_1_t2376585677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((LinkedList_1_t2376585677 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Enumerator_t1199491699  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m437131773_gshared (LinkedList_1_t2376585677 * __this, const MethodInfo* method)
{
	{
		NullCheck((LinkedList_1_t2376585677 *)__this);
		Enumerator_t1199491699  L_0 = ((  Enumerator_t1199491699  (*) (LinkedList_1_t2376585677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((LinkedList_1_t2376585677 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Enumerator_t1199491699  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3216615461_gshared (LinkedList_1_t2376585677 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m1115541056_gshared (LinkedList_1_t2376585677 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m3124823866_gshared (LinkedList_1_t2376585677 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_2();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1414245146;
extern const uint32_t LinkedList_1_VerifyReferencedNode_m4026260407_MetadataUsageId;
extern "C"  void LinkedList_1_VerifyReferencedNode_m4026260407_gshared (LinkedList_1_t2376585677 * __this, LinkedListNode_1_t967983361 * ___node0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_VerifyReferencedNode_m4026260407_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedListNode_1_t967983361 * L_0 = ___node0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral1414245146, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		LinkedListNode_1_t967983361 * L_2 = ___node0;
		NullCheck((LinkedListNode_1_t967983361 *)L_2);
		LinkedList_1_t2376585677 * L_3 = ((  LinkedList_1_t2376585677 * (*) (LinkedListNode_1_t967983361 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((LinkedListNode_1_t967983361 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((Il2CppObject*)(LinkedList_1_t2376585677 *)L_3) == ((Il2CppObject*)(LinkedList_1_t2376585677 *)__this)))
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t721527559 * L_4 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::AddLast(T)
extern "C"  LinkedListNode_1_t967983361 * LinkedList_1_AddLast_m1405237197_gshared (LinkedList_1_t2376585677 * __this, int32_t ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t967983361 * V_0 = NULL;
	{
		LinkedListNode_1_t967983361 * L_0 = (LinkedListNode_1_t967983361 *)__this->get_first_3();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_1 = ___value0;
		LinkedListNode_1_t967983361 * L_2 = (LinkedListNode_1_t967983361 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t967983361 *, LinkedList_1_t2376585677 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_2, (LinkedList_1_t2376585677 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_0 = (LinkedListNode_1_t967983361 *)L_2;
		LinkedListNode_1_t967983361 * L_3 = V_0;
		__this->set_first_3(L_3);
		goto IL_0038;
	}

IL_001f:
	{
		int32_t L_4 = ___value0;
		LinkedListNode_1_t967983361 * L_5 = (LinkedListNode_1_t967983361 *)__this->get_first_3();
		NullCheck(L_5);
		LinkedListNode_1_t967983361 * L_6 = (LinkedListNode_1_t967983361 *)L_5->get_back_3();
		LinkedListNode_1_t967983361 * L_7 = (LinkedListNode_1_t967983361 *)__this->get_first_3();
		LinkedListNode_1_t967983361 * L_8 = (LinkedListNode_1_t967983361 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t967983361 *, LinkedList_1_t2376585677 *, int32_t, LinkedListNode_1_t967983361 *, LinkedListNode_1_t967983361 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(L_8, (LinkedList_1_t2376585677 *)__this, (int32_t)L_4, (LinkedListNode_1_t967983361 *)L_6, (LinkedListNode_1_t967983361 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_0 = (LinkedListNode_1_t967983361 *)L_8;
	}

IL_0038:
	{
		uint32_t L_9 = (uint32_t)__this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_9+(int32_t)1)));
		uint32_t L_10 = (uint32_t)__this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_10+(int32_t)1)));
		LinkedListNode_1_t967983361 * L_11 = V_0;
		return L_11;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::Clear()
extern "C"  void LinkedList_1_Clear_m642487645_gshared (LinkedList_1_t2376585677 * __this, const MethodInfo* method)
{
	{
		goto IL_000b;
	}

IL_0005:
	{
		NullCheck((LinkedList_1_t2376585677 *)__this);
		((  void (*) (LinkedList_1_t2376585677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((LinkedList_1_t2376585677 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_000b:
	{
		LinkedListNode_1_t967983361 * L_0 = (LinkedListNode_1_t967983361 *)__this->get_first_3();
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Contains(T)
extern "C"  bool LinkedList_1_Contains_m672313738_gshared (LinkedList_1_t2376585677 * __this, int32_t ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t967983361 * V_0 = NULL;
	{
		LinkedListNode_1_t967983361 * L_0 = (LinkedListNode_1_t967983361 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t967983361 *)L_0;
		LinkedListNode_1_t967983361 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		LinkedListNode_1_t967983361 * L_2 = V_0;
		NullCheck((LinkedListNode_1_t967983361 *)L_2);
		int32_t L_3 = ((  int32_t (*) (LinkedListNode_1_t967983361 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t967983361 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 12), &L_4);
		bool L_6 = Int32_Equals_m753832628((int32_t*)(&___value0), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002e;
		}
	}
	{
		return (bool)1;
	}

IL_002e:
	{
		LinkedListNode_1_t967983361 * L_7 = V_0;
		NullCheck(L_7);
		LinkedListNode_1_t967983361 * L_8 = (LinkedListNode_1_t967983361 *)L_7->get_forward_2();
		V_0 = (LinkedListNode_1_t967983361 *)L_8;
		LinkedListNode_1_t967983361 * L_9 = V_0;
		LinkedListNode_1_t967983361 * L_10 = (LinkedListNode_1_t967983361 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t967983361 *)L_9) == ((Il2CppObject*)(LinkedListNode_1_t967983361 *)L_10))))
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::CopyTo(T[],System.Int32)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1185213181;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern Il2CppCodeGenString* _stringLiteral374702583;
extern Il2CppCodeGenString* _stringLiteral2752284169;
extern const uint32_t LinkedList_1_CopyTo_m3638740377_MetadataUsageId;
extern "C"  void LinkedList_1_CopyTo_m3638740377_gshared (LinkedList_1_t2376585677 * __this, Int32U5BU5D_t3030399641* ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_CopyTo_m3638740377_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedListNode_1_t967983361 * V_0 = NULL;
	{
		Int32U5BU5D_t3030399641* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral1185213181, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index1;
		Int32U5BU5D_t3030399641* L_3 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_3);
		int32_t L_4 = Array_GetLowerBound_m3733237204((Il2CppArray *)(Il2CppArray *)L_3, (int32_t)0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) < ((uint32_t)L_4))))
		{
			goto IL_0029;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_5 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_5, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0029:
	{
		Int32U5BU5D_t3030399641* L_6 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_6);
		int32_t L_7 = Array_get_Rank_m3837250695((Il2CppArray *)(Il2CppArray *)L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)1)))
		{
			goto IL_0045;
		}
	}
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_8, (String_t*)_stringLiteral1185213181, (String_t*)_stringLiteral374702583, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0045:
	{
		Int32U5BU5D_t3030399641* L_9 = ___array0;
		NullCheck(L_9);
		int32_t L_10 = ___index1;
		Int32U5BU5D_t3030399641* L_11 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_11);
		int32_t L_12 = Array_GetLowerBound_m3733237204((Il2CppArray *)(Il2CppArray *)L_11, (int32_t)0, /*hidden argument*/NULL);
		uint32_t L_13 = (uint32_t)__this->get_count_0();
		if ((((int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))-(int32_t)L_10))+(int32_t)L_12)))))) >= ((int64_t)(((int64_t)((uint64_t)L_13))))))
		{
			goto IL_006a;
		}
	}
	{
		ArgumentException_t3259014390 * L_14 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_14, (String_t*)_stringLiteral2752284169, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_006a:
	{
		LinkedListNode_1_t967983361 * L_15 = (LinkedListNode_1_t967983361 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t967983361 *)L_15;
		LinkedListNode_1_t967983361 * L_16 = (LinkedListNode_1_t967983361 *)__this->get_first_3();
		if (L_16)
		{
			goto IL_007d;
		}
	}
	{
		return;
	}

IL_007d:
	{
		Int32U5BU5D_t3030399641* L_17 = ___array0;
		int32_t L_18 = ___index1;
		LinkedListNode_1_t967983361 * L_19 = V_0;
		NullCheck((LinkedListNode_1_t967983361 *)L_19);
		int32_t L_20 = ((  int32_t (*) (LinkedListNode_1_t967983361 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t967983361 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (int32_t)L_20);
		int32_t L_21 = ___index1;
		___index1 = (int32_t)((int32_t)((int32_t)L_21+(int32_t)1));
		LinkedListNode_1_t967983361 * L_22 = V_0;
		NullCheck(L_22);
		LinkedListNode_1_t967983361 * L_23 = (LinkedListNode_1_t967983361 *)L_22->get_forward_2();
		V_0 = (LinkedListNode_1_t967983361 *)L_23;
		LinkedListNode_1_t967983361 * L_24 = V_0;
		LinkedListNode_1_t967983361 * L_25 = (LinkedListNode_1_t967983361 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t967983361 *)L_24) == ((Il2CppObject*)(LinkedListNode_1_t967983361 *)L_25))))
		{
			goto IL_007d;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::Find(T)
extern "C"  LinkedListNode_1_t967983361 * LinkedList_1_Find_m1681505513_gshared (LinkedList_1_t2376585677 * __this, int32_t ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t967983361 * V_0 = NULL;
	{
		LinkedListNode_1_t967983361 * L_0 = (LinkedListNode_1_t967983361 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t967983361 *)L_0;
		LinkedListNode_1_t967983361 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t967983361 *)NULL;
	}

IL_000f:
	{
		goto IL_002a;
	}
	{
		LinkedListNode_1_t967983361 * L_3 = V_0;
		NullCheck((LinkedListNode_1_t967983361 *)L_3);
		int32_t L_4 = ((  int32_t (*) (LinkedListNode_1_t967983361 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t967983361 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
	}

IL_002a:
	{
	}
	{
		LinkedListNode_1_t967983361 * L_6 = V_0;
		NullCheck((LinkedListNode_1_t967983361 *)L_6);
		int32_t L_7 = ((  int32_t (*) (LinkedListNode_1_t967983361 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t967983361 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 12), &L_8);
		bool L_10 = Int32_Equals_m753832628((int32_t*)(&___value0), (Il2CppObject *)L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0054;
		}
	}

IL_0052:
	{
		LinkedListNode_1_t967983361 * L_11 = V_0;
		return L_11;
	}

IL_0054:
	{
		LinkedListNode_1_t967983361 * L_12 = V_0;
		NullCheck(L_12);
		LinkedListNode_1_t967983361 * L_13 = (LinkedListNode_1_t967983361 *)L_12->get_forward_2();
		V_0 = (LinkedListNode_1_t967983361 *)L_13;
		LinkedListNode_1_t967983361 * L_14 = V_0;
		LinkedListNode_1_t967983361 * L_15 = (LinkedListNode_1_t967983361 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t967983361 *)L_14) == ((Il2CppObject*)(LinkedListNode_1_t967983361 *)L_15))))
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t967983361 *)NULL;
	}
}
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Int32>::GetEnumerator()
extern "C"  Enumerator_t1199491699  LinkedList_1_GetEnumerator_m3906189317_gshared (LinkedList_1_t2376585677 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1199491699  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m2750375776(&L_0, (LinkedList_1_t2376585677 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1747804205;
extern Il2CppCodeGenString* _stringLiteral3617362;
extern const uint32_t LinkedList_1_GetObjectData_m3163302640_MetadataUsageId;
extern "C"  void LinkedList_1_GetObjectData_m3163302640_gshared (LinkedList_1_t2376585677 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_GetObjectData_m3163302640_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Int32U5BU5D_t3030399641* V_0 = NULL;
	{
		uint32_t L_0 = (uint32_t)__this->get_count_0();
		V_0 = (Int32U5BU5D_t3030399641*)((Int32U5BU5D_t3030399641*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (uint32_t)(((uintptr_t)L_0))));
		Int32U5BU5D_t3030399641* L_1 = V_0;
		NullCheck((LinkedList_1_t2376585677 *)__this);
		((  void (*) (LinkedList_1_t2376585677 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((LinkedList_1_t2376585677 *)__this, (Int32U5BU5D_t3030399641*)L_1, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		SerializationInfo_t228987430 * L_2 = ___info0;
		Int32U5BU5D_t3030399641* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t228987430 *)L_2);
		SerializationInfo_AddValue_m1781549036((SerializationInfo_t228987430 *)L_2, (String_t*)_stringLiteral1747804205, (Il2CppObject *)(Il2CppObject *)L_3, (Type_t *)L_4, /*hidden argument*/NULL);
		SerializationInfo_t228987430 * L_5 = ___info0;
		uint32_t L_6 = (uint32_t)__this->get_version_1();
		NullCheck((SerializationInfo_t228987430 *)L_5);
		SerializationInfo_AddValue_m383491877((SerializationInfo_t228987430 *)L_5, (String_t*)_stringLiteral3617362, (uint32_t)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::OnDeserialization(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1747804205;
extern Il2CppCodeGenString* _stringLiteral3617362;
extern const uint32_t LinkedList_1_OnDeserialization_m3795794854_MetadataUsageId;
extern "C"  void LinkedList_1_OnDeserialization_m3795794854_gshared (LinkedList_1_t2376585677 * __this, Il2CppObject * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_OnDeserialization_m3795794854_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Int32U5BU5D_t3030399641* V_0 = NULL;
	int32_t V_1 = 0;
	Int32U5BU5D_t3030399641* V_2 = NULL;
	int32_t V_3 = 0;
	{
		SerializationInfo_t228987430 * L_0 = (SerializationInfo_t228987430 *)__this->get_si_4();
		if (!L_0)
		{
			goto IL_0074;
		}
	}
	{
		SerializationInfo_t228987430 * L_1 = (SerializationInfo_t228987430 *)__this->get_si_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t228987430 *)L_1);
		Il2CppObject * L_3 = SerializationInfo_GetValue_m1127314592((SerializationInfo_t228987430 *)L_1, (String_t*)_stringLiteral1747804205, (Type_t *)L_2, /*hidden argument*/NULL);
		V_0 = (Int32U5BU5D_t3030399641*)((Int32U5BU5D_t3030399641*)Castclass(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Int32U5BU5D_t3030399641* L_4 = V_0;
		if (!L_4)
		{
			goto IL_0057;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_5 = V_0;
		V_2 = (Int32U5BU5D_t3030399641*)L_5;
		V_3 = (int32_t)0;
		goto IL_004e;
	}

IL_003a:
	{
		Int32U5BU5D_t3030399641* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		int32_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (int32_t)L_9;
		int32_t L_10 = V_1;
		NullCheck((LinkedList_1_t2376585677 *)__this);
		((  LinkedListNode_1_t967983361 * (*) (LinkedList_1_t2376585677 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((LinkedList_1_t2376585677 *)__this, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		int32_t L_11 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_12 = V_3;
		Int32U5BU5D_t3030399641* L_13 = V_2;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_003a;
		}
	}

IL_0057:
	{
		SerializationInfo_t228987430 * L_14 = (SerializationInfo_t228987430 *)__this->get_si_4();
		NullCheck((SerializationInfo_t228987430 *)L_14);
		uint32_t L_15 = SerializationInfo_GetUInt32_m3393505633((SerializationInfo_t228987430 *)L_14, (String_t*)_stringLiteral3617362, /*hidden argument*/NULL);
		__this->set_version_1(L_15);
		__this->set_si_4((SerializationInfo_t228987430 *)NULL);
	}

IL_0074:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Remove(T)
extern "C"  bool LinkedList_1_Remove_m2841776017_gshared (LinkedList_1_t2376585677 * __this, int32_t ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t967983361 * V_0 = NULL;
	{
		int32_t L_0 = ___value0;
		NullCheck((LinkedList_1_t2376585677 *)__this);
		LinkedListNode_1_t967983361 * L_1 = ((  LinkedListNode_1_t967983361 * (*) (LinkedList_1_t2376585677 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((LinkedList_1_t2376585677 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		V_0 = (LinkedListNode_1_t967983361 *)L_1;
		LinkedListNode_1_t967983361 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0010;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		LinkedListNode_1_t967983361 * L_3 = V_0;
		NullCheck((LinkedList_1_t2376585677 *)__this);
		((  void (*) (LinkedList_1_t2376585677 *, LinkedListNode_1_t967983361 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LinkedList_1_t2376585677 *)__this, (LinkedListNode_1_t967983361 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		return (bool)1;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedList_1_Remove_m2349142508_gshared (LinkedList_1_t2376585677 * __this, LinkedListNode_1_t967983361 * ___node0, const MethodInfo* method)
{
	{
		LinkedListNode_1_t967983361 * L_0 = ___node0;
		NullCheck((LinkedList_1_t2376585677 *)__this);
		((  void (*) (LinkedList_1_t2376585677 *, LinkedListNode_1_t967983361 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((LinkedList_1_t2376585677 *)__this, (LinkedListNode_1_t967983361 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		uint32_t L_1 = (uint32_t)__this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_1-(int32_t)1)));
		uint32_t L_2 = (uint32_t)__this->get_count_0();
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		__this->set_first_3((LinkedListNode_1_t967983361 *)NULL);
	}

IL_0027:
	{
		LinkedListNode_1_t967983361 * L_3 = ___node0;
		LinkedListNode_1_t967983361 * L_4 = (LinkedListNode_1_t967983361 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t967983361 *)L_3) == ((Il2CppObject*)(LinkedListNode_1_t967983361 *)L_4))))
		{
			goto IL_0044;
		}
	}
	{
		LinkedListNode_1_t967983361 * L_5 = (LinkedListNode_1_t967983361 *)__this->get_first_3();
		NullCheck(L_5);
		LinkedListNode_1_t967983361 * L_6 = (LinkedListNode_1_t967983361 *)L_5->get_forward_2();
		__this->set_first_3(L_6);
	}

IL_0044:
	{
		uint32_t L_7 = (uint32_t)__this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		LinkedListNode_1_t967983361 * L_8 = ___node0;
		NullCheck((LinkedListNode_1_t967983361 *)L_8);
		((  void (*) (LinkedListNode_1_t967983361 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((LinkedListNode_1_t967983361 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::RemoveLast()
extern "C"  void LinkedList_1_RemoveLast_m158819622_gshared (LinkedList_1_t2376585677 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t967983361 * L_0 = (LinkedListNode_1_t967983361 *)__this->get_first_3();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		LinkedListNode_1_t967983361 * L_1 = (LinkedListNode_1_t967983361 *)__this->get_first_3();
		NullCheck(L_1);
		LinkedListNode_1_t967983361 * L_2 = (LinkedListNode_1_t967983361 *)L_1->get_back_3();
		NullCheck((LinkedList_1_t2376585677 *)__this);
		((  void (*) (LinkedList_1_t2376585677 *, LinkedListNode_1_t967983361 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LinkedList_1_t2376585677 *)__this, (LinkedListNode_1_t967983361 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001c:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.LinkedList`1<System.Int32>::get_Count()
extern "C"  int32_t LinkedList_1_get_Count_m212295514_gshared (LinkedList_1_t2376585677 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = (uint32_t)__this->get_count_0();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::get_First()
extern "C"  LinkedListNode_1_t967983361 * LinkedList_1_get_First_m278223248_gshared (LinkedList_1_t2376585677 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t967983361 * L_0 = (LinkedListNode_1_t967983361 *)__this->get_first_3();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LinkedList_1__ctor_m1337573311_MetadataUsageId;
extern "C"  void LinkedList_1__ctor_m1337573311_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m1337573311_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		__this->set_syncRoot_2(L_0);
		__this->set_first_3((LinkedListNode_1_t1585555208 *)NULL);
		int32_t L_1 = (int32_t)0;
		V_0 = (uint32_t)L_1;
		__this->set_version_1(L_1);
		uint32_t L_2 = V_0;
		__this->set_count_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LinkedList_1__ctor_m1570295574_MetadataUsageId;
extern "C"  void LinkedList_1__ctor_m1570295574_gshared (LinkedList_1_t2994157524 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m1570295574_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  void (*) (LinkedList_1_t2994157524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((LinkedList_1_t2994157524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		SerializationInfo_t228987430 * L_0 = ___info0;
		__this->set_si_4(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_1, /*hidden argument*/NULL);
		__this->set_syncRoot_2(L_1);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3874300024_gshared (LinkedList_1_t2994157524 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  LinkedListNode_1_t1585555208 * (*) (LinkedList_1_t2994157524 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((LinkedList_1_t2994157524 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1185213181;
extern const uint32_t LinkedList_1_System_Collections_ICollection_CopyTo_m3851520323_MetadataUsageId;
extern "C"  void LinkedList_1_System_Collections_ICollection_CopyTo_m3851520323_gshared (LinkedList_1_t2994157524 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_System_Collections_ICollection_CopyTo_m3851520323_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, (String_t*)_stringLiteral1185213181, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		ObjectU5BU5D_t3614634134* L_3 = V_0;
		int32_t L_4 = ___index1;
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  void (*) (LinkedList_1_t2994157524 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((LinkedList_1_t2994157524 *)__this, (ObjectU5BU5D_t3614634134*)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4026686671_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		NullCheck((LinkedList_1_t2994157524 *)__this);
		Enumerator_t1817063546  L_0 = ((  Enumerator_t1817063546  (*) (LinkedList_1_t2994157524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((LinkedList_1_t2994157524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Enumerator_t1817063546  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m1585231526_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		NullCheck((LinkedList_1_t2994157524 *)__this);
		Enumerator_t1817063546  L_0 = ((  Enumerator_t1817063546  (*) (LinkedList_1_t2994157524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((LinkedList_1_t2994157524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Enumerator_t1817063546  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m382194114_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m2849171683_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m550341923_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_2();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1414245146;
extern const uint32_t LinkedList_1_VerifyReferencedNode_m1333791742_MetadataUsageId;
extern "C"  void LinkedList_1_VerifyReferencedNode_m1333791742_gshared (LinkedList_1_t2994157524 * __this, LinkedListNode_1_t1585555208 * ___node0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_VerifyReferencedNode_m1333791742_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedListNode_1_t1585555208 * L_0 = ___node0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral1414245146, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		LinkedListNode_1_t1585555208 * L_2 = ___node0;
		NullCheck((LinkedListNode_1_t1585555208 *)L_2);
		LinkedList_1_t2994157524 * L_3 = ((  LinkedList_1_t2994157524 * (*) (LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((LinkedListNode_1_t1585555208 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((Il2CppObject*)(LinkedList_1_t2994157524 *)L_3) == ((Il2CppObject*)(LinkedList_1_t2994157524 *)__this)))
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t721527559 * L_4 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(T)
extern "C"  LinkedListNode_1_t1585555208 * LinkedList_1_AddLast_m3575571360_gshared (LinkedList_1_t2994157524 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t1585555208 * V_0 = NULL;
	{
		LinkedListNode_1_t1585555208 * L_0 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject * L_1 = ___value0;
		LinkedListNode_1_t1585555208 * L_2 = (LinkedListNode_1_t1585555208 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t1585555208 *, LinkedList_1_t2994157524 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_2, (LinkedList_1_t2994157524 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_0 = (LinkedListNode_1_t1585555208 *)L_2;
		LinkedListNode_1_t1585555208 * L_3 = V_0;
		__this->set_first_3(L_3);
		goto IL_0038;
	}

IL_001f:
	{
		Il2CppObject * L_4 = ___value0;
		LinkedListNode_1_t1585555208 * L_5 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		NullCheck(L_5);
		LinkedListNode_1_t1585555208 * L_6 = (LinkedListNode_1_t1585555208 *)L_5->get_back_3();
		LinkedListNode_1_t1585555208 * L_7 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		LinkedListNode_1_t1585555208 * L_8 = (LinkedListNode_1_t1585555208 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t1585555208 *, LinkedList_1_t2994157524 *, Il2CppObject *, LinkedListNode_1_t1585555208 *, LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(L_8, (LinkedList_1_t2994157524 *)__this, (Il2CppObject *)L_4, (LinkedListNode_1_t1585555208 *)L_6, (LinkedListNode_1_t1585555208 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_0 = (LinkedListNode_1_t1585555208 *)L_8;
	}

IL_0038:
	{
		uint32_t L_9 = (uint32_t)__this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_9+(int32_t)1)));
		uint32_t L_10 = (uint32_t)__this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_10+(int32_t)1)));
		LinkedListNode_1_t1585555208 * L_11 = V_0;
		return L_11;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Clear()
extern "C"  void LinkedList_1_Clear_m3288132428_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		goto IL_000b;
	}

IL_0005:
	{
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  void (*) (LinkedList_1_t2994157524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((LinkedList_1_t2994157524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_000b:
	{
		LinkedListNode_1_t1585555208 * L_0 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Contains(T)
extern "C"  bool LinkedList_1_Contains_m3220831510_gshared (LinkedList_1_t2994157524 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t1585555208 * V_0 = NULL;
	{
		LinkedListNode_1_t1585555208 * L_0 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t1585555208 *)L_0;
		LinkedListNode_1_t1585555208 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		LinkedListNode_1_t1585555208 * L_2 = V_0;
		NullCheck((LinkedListNode_1_t1585555208 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t1585555208 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck((Il2CppObject *)(*(&___value0)));
		bool L_4 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*(&___value0)), (Il2CppObject *)L_3);
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		return (bool)1;
	}

IL_002e:
	{
		LinkedListNode_1_t1585555208 * L_5 = V_0;
		NullCheck(L_5);
		LinkedListNode_1_t1585555208 * L_6 = (LinkedListNode_1_t1585555208 *)L_5->get_forward_2();
		V_0 = (LinkedListNode_1_t1585555208 *)L_6;
		LinkedListNode_1_t1585555208 * L_7 = V_0;
		LinkedListNode_1_t1585555208 * L_8 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_7) == ((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_8))))
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1185213181;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern Il2CppCodeGenString* _stringLiteral374702583;
extern Il2CppCodeGenString* _stringLiteral2752284169;
extern const uint32_t LinkedList_1_CopyTo_m3700485924_MetadataUsageId;
extern "C"  void LinkedList_1_CopyTo_m3700485924_gshared (LinkedList_1_t2994157524 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_CopyTo_m3700485924_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedListNode_1_t1585555208 * V_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral1185213181, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index1;
		ObjectU5BU5D_t3614634134* L_3 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_3);
		int32_t L_4 = Array_GetLowerBound_m3733237204((Il2CppArray *)(Il2CppArray *)L_3, (int32_t)0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) < ((uint32_t)L_4))))
		{
			goto IL_0029;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_5 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_5, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0029:
	{
		ObjectU5BU5D_t3614634134* L_6 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_6);
		int32_t L_7 = Array_get_Rank_m3837250695((Il2CppArray *)(Il2CppArray *)L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)1)))
		{
			goto IL_0045;
		}
	}
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_8, (String_t*)_stringLiteral1185213181, (String_t*)_stringLiteral374702583, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0045:
	{
		ObjectU5BU5D_t3614634134* L_9 = ___array0;
		NullCheck(L_9);
		int32_t L_10 = ___index1;
		ObjectU5BU5D_t3614634134* L_11 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_11);
		int32_t L_12 = Array_GetLowerBound_m3733237204((Il2CppArray *)(Il2CppArray *)L_11, (int32_t)0, /*hidden argument*/NULL);
		uint32_t L_13 = (uint32_t)__this->get_count_0();
		if ((((int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))-(int32_t)L_10))+(int32_t)L_12)))))) >= ((int64_t)(((int64_t)((uint64_t)L_13))))))
		{
			goto IL_006a;
		}
	}
	{
		ArgumentException_t3259014390 * L_14 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_14, (String_t*)_stringLiteral2752284169, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_006a:
	{
		LinkedListNode_1_t1585555208 * L_15 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t1585555208 *)L_15;
		LinkedListNode_1_t1585555208 * L_16 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		if (L_16)
		{
			goto IL_007d;
		}
	}
	{
		return;
	}

IL_007d:
	{
		ObjectU5BU5D_t3614634134* L_17 = ___array0;
		int32_t L_18 = ___index1;
		LinkedListNode_1_t1585555208 * L_19 = V_0;
		NullCheck((LinkedListNode_1_t1585555208 *)L_19);
		Il2CppObject * L_20 = ((  Il2CppObject * (*) (LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t1585555208 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (Il2CppObject *)L_20);
		int32_t L_21 = ___index1;
		___index1 = (int32_t)((int32_t)((int32_t)L_21+(int32_t)1));
		LinkedListNode_1_t1585555208 * L_22 = V_0;
		NullCheck(L_22);
		LinkedListNode_1_t1585555208 * L_23 = (LinkedListNode_1_t1585555208 *)L_22->get_forward_2();
		V_0 = (LinkedListNode_1_t1585555208 *)L_23;
		LinkedListNode_1_t1585555208 * L_24 = V_0;
		LinkedListNode_1_t1585555208 * L_25 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_24) == ((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_25))))
		{
			goto IL_007d;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::Find(T)
extern "C"  LinkedListNode_1_t1585555208 * LinkedList_1_Find_m1218372336_gshared (LinkedList_1_t2994157524 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t1585555208 * V_0 = NULL;
	{
		LinkedListNode_1_t1585555208 * L_0 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t1585555208 *)L_0;
		LinkedListNode_1_t1585555208 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t1585555208 *)NULL;
	}

IL_000f:
	{
		Il2CppObject * L_2 = ___value0;
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		LinkedListNode_1_t1585555208 * L_3 = V_0;
		NullCheck((LinkedListNode_1_t1585555208 *)L_3);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t1585555208 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		if (!L_4)
		{
			goto IL_0052;
		}
	}

IL_002a:
	{
		Il2CppObject * L_5 = ___value0;
		if (!L_5)
		{
			goto IL_0054;
		}
	}
	{
		LinkedListNode_1_t1585555208 * L_6 = V_0;
		NullCheck((LinkedListNode_1_t1585555208 *)L_6);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t1585555208 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck((Il2CppObject *)(*(&___value0)));
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*(&___value0)), (Il2CppObject *)L_7);
		if (!L_8)
		{
			goto IL_0054;
		}
	}

IL_0052:
	{
		LinkedListNode_1_t1585555208 * L_9 = V_0;
		return L_9;
	}

IL_0054:
	{
		LinkedListNode_1_t1585555208 * L_10 = V_0;
		NullCheck(L_10);
		LinkedListNode_1_t1585555208 * L_11 = (LinkedListNode_1_t1585555208 *)L_10->get_forward_2();
		V_0 = (LinkedListNode_1_t1585555208 *)L_11;
		LinkedListNode_1_t1585555208 * L_12 = V_0;
		LinkedListNode_1_t1585555208 * L_13 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_12) == ((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_13))))
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t1585555208 *)NULL;
	}
}
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t1817063546  LinkedList_1_GetEnumerator_m4181273888_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1817063546  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m1586864815(&L_0, (LinkedList_1_t2994157524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1747804205;
extern Il2CppCodeGenString* _stringLiteral3617362;
extern const uint32_t LinkedList_1_GetObjectData_m2848049047_MetadataUsageId;
extern "C"  void LinkedList_1_GetObjectData_m2848049047_gshared (LinkedList_1_t2994157524 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_GetObjectData_m2848049047_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		uint32_t L_0 = (uint32_t)__this->get_count_0();
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (uint32_t)(((uintptr_t)L_0))));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  void (*) (LinkedList_1_t2994157524 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((LinkedList_1_t2994157524 *)__this, (ObjectU5BU5D_t3614634134*)L_1, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		SerializationInfo_t228987430 * L_2 = ___info0;
		ObjectU5BU5D_t3614634134* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t228987430 *)L_2);
		SerializationInfo_AddValue_m1781549036((SerializationInfo_t228987430 *)L_2, (String_t*)_stringLiteral1747804205, (Il2CppObject *)(Il2CppObject *)L_3, (Type_t *)L_4, /*hidden argument*/NULL);
		SerializationInfo_t228987430 * L_5 = ___info0;
		uint32_t L_6 = (uint32_t)__this->get_version_1();
		NullCheck((SerializationInfo_t228987430 *)L_5);
		SerializationInfo_AddValue_m383491877((SerializationInfo_t228987430 *)L_5, (String_t*)_stringLiteral3617362, (uint32_t)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::OnDeserialization(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1747804205;
extern Il2CppCodeGenString* _stringLiteral3617362;
extern const uint32_t LinkedList_1_OnDeserialization_m264209791_MetadataUsageId;
extern "C"  void LinkedList_1_OnDeserialization_m264209791_gshared (LinkedList_1_t2994157524 * __this, Il2CppObject * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_OnDeserialization_m264209791_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ObjectU5BU5D_t3614634134* V_2 = NULL;
	int32_t V_3 = 0;
	{
		SerializationInfo_t228987430 * L_0 = (SerializationInfo_t228987430 *)__this->get_si_4();
		if (!L_0)
		{
			goto IL_0074;
		}
	}
	{
		SerializationInfo_t228987430 * L_1 = (SerializationInfo_t228987430 *)__this->get_si_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t228987430 *)L_1);
		Il2CppObject * L_3 = SerializationInfo_GetValue_m1127314592((SerializationInfo_t228987430 *)L_1, (String_t*)_stringLiteral1747804205, (Type_t *)L_2, /*hidden argument*/NULL);
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)Castclass(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		ObjectU5BU5D_t3614634134* L_4 = V_0;
		if (!L_4)
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_5 = V_0;
		V_2 = (ObjectU5BU5D_t3614634134*)L_5;
		V_3 = (int32_t)0;
		goto IL_004e;
	}

IL_003a:
	{
		ObjectU5BU5D_t3614634134* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (Il2CppObject *)L_9;
		Il2CppObject * L_10 = V_1;
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  LinkedListNode_1_t1585555208 * (*) (LinkedList_1_t2994157524 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((LinkedList_1_t2994157524 *)__this, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		int32_t L_11 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_12 = V_3;
		ObjectU5BU5D_t3614634134* L_13 = V_2;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_003a;
		}
	}

IL_0057:
	{
		SerializationInfo_t228987430 * L_14 = (SerializationInfo_t228987430 *)__this->get_si_4();
		NullCheck((SerializationInfo_t228987430 *)L_14);
		uint32_t L_15 = SerializationInfo_GetUInt32_m3393505633((SerializationInfo_t228987430 *)L_14, (String_t*)_stringLiteral3617362, /*hidden argument*/NULL);
		__this->set_version_1(L_15);
		__this->set_si_4((SerializationInfo_t228987430 *)NULL);
	}

IL_0074:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Remove(T)
extern "C"  bool LinkedList_1_Remove_m2925241645_gshared (LinkedList_1_t2994157524 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t1585555208 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck((LinkedList_1_t2994157524 *)__this);
		LinkedListNode_1_t1585555208 * L_1 = ((  LinkedListNode_1_t1585555208 * (*) (LinkedList_1_t2994157524 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((LinkedList_1_t2994157524 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		V_0 = (LinkedListNode_1_t1585555208 *)L_1;
		LinkedListNode_1_t1585555208 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0010;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		LinkedListNode_1_t1585555208 * L_3 = V_0;
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  void (*) (LinkedList_1_t2994157524 *, LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LinkedList_1_t2994157524 *)__this, (LinkedListNode_1_t1585555208 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		return (bool)1;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedList_1_Remove_m18160224_gshared (LinkedList_1_t2994157524 * __this, LinkedListNode_1_t1585555208 * ___node0, const MethodInfo* method)
{
	{
		LinkedListNode_1_t1585555208 * L_0 = ___node0;
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  void (*) (LinkedList_1_t2994157524 *, LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((LinkedList_1_t2994157524 *)__this, (LinkedListNode_1_t1585555208 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		uint32_t L_1 = (uint32_t)__this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_1-(int32_t)1)));
		uint32_t L_2 = (uint32_t)__this->get_count_0();
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		__this->set_first_3((LinkedListNode_1_t1585555208 *)NULL);
	}

IL_0027:
	{
		LinkedListNode_1_t1585555208 * L_3 = ___node0;
		LinkedListNode_1_t1585555208 * L_4 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_3) == ((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_4))))
		{
			goto IL_0044;
		}
	}
	{
		LinkedListNode_1_t1585555208 * L_5 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		NullCheck(L_5);
		LinkedListNode_1_t1585555208 * L_6 = (LinkedListNode_1_t1585555208 *)L_5->get_forward_2();
		__this->set_first_3(L_6);
	}

IL_0044:
	{
		uint32_t L_7 = (uint32_t)__this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		LinkedListNode_1_t1585555208 * L_8 = ___node0;
		NullCheck((LinkedListNode_1_t1585555208 *)L_8);
		((  void (*) (LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((LinkedListNode_1_t1585555208 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::RemoveLast()
extern "C"  void LinkedList_1_RemoveLast_m2904732739_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t1585555208 * L_0 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		LinkedListNode_1_t1585555208 * L_1 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		NullCheck(L_1);
		LinkedListNode_1_t1585555208 * L_2 = (LinkedListNode_1_t1585555208 *)L_1->get_back_3();
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  void (*) (LinkedList_1_t2994157524 *, LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LinkedList_1_t2994157524 *)__this, (LinkedListNode_1_t1585555208 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001c:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.LinkedList`1<System.Object>::get_Count()
extern "C"  int32_t LinkedList_1_get_Count_m402269195_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = (uint32_t)__this->get_count_0();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_First()
extern "C"  LinkedListNode_1_t1585555208 * LinkedList_1_get_First_m732033480_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t1585555208 * L_0 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Int32>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
extern "C"  void LinkedListNode_1__ctor_m3717885607_gshared (LinkedListNode_1_t967983361 * __this, LinkedList_1_t2376585677 * ___list0, int32_t ___value1, const MethodInfo* method)
{
	LinkedListNode_1_t967983361 * V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		LinkedList_1_t2376585677 * L_0 = ___list0;
		__this->set_container_1(L_0);
		int32_t L_1 = ___value1;
		__this->set_item_0(L_1);
		V_0 = (LinkedListNode_1_t967983361 *)__this;
		__this->set_forward_2(__this);
		LinkedListNode_1_t967983361 * L_2 = V_0;
		__this->set_back_3(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Int32>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedListNode_1__ctor_m1109227571_gshared (LinkedListNode_1_t967983361 * __this, LinkedList_1_t2376585677 * ___list0, int32_t ___value1, LinkedListNode_1_t967983361 * ___previousNode2, LinkedListNode_1_t967983361 * ___nextNode3, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		LinkedList_1_t2376585677 * L_0 = ___list0;
		__this->set_container_1(L_0);
		int32_t L_1 = ___value1;
		__this->set_item_0(L_1);
		LinkedListNode_1_t967983361 * L_2 = ___previousNode2;
		__this->set_back_3(L_2);
		LinkedListNode_1_t967983361 * L_3 = ___nextNode3;
		__this->set_forward_2(L_3);
		LinkedListNode_1_t967983361 * L_4 = ___previousNode2;
		NullCheck(L_4);
		L_4->set_forward_2(__this);
		LinkedListNode_1_t967983361 * L_5 = ___nextNode3;
		NullCheck(L_5);
		L_5->set_back_3(__this);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Int32>::Detach()
extern "C"  void LinkedListNode_1_Detach_m3376839027_gshared (LinkedListNode_1_t967983361 * __this, const MethodInfo* method)
{
	LinkedListNode_1_t967983361 * V_0 = NULL;
	{
		LinkedListNode_1_t967983361 * L_0 = (LinkedListNode_1_t967983361 *)__this->get_back_3();
		LinkedListNode_1_t967983361 * L_1 = (LinkedListNode_1_t967983361 *)__this->get_forward_2();
		NullCheck(L_0);
		L_0->set_forward_2(L_1);
		LinkedListNode_1_t967983361 * L_2 = (LinkedListNode_1_t967983361 *)__this->get_forward_2();
		LinkedListNode_1_t967983361 * L_3 = (LinkedListNode_1_t967983361 *)__this->get_back_3();
		NullCheck(L_2);
		L_2->set_back_3(L_3);
		V_0 = (LinkedListNode_1_t967983361 *)NULL;
		__this->set_back_3((LinkedListNode_1_t967983361 *)NULL);
		LinkedListNode_1_t967983361 * L_4 = V_0;
		__this->set_forward_2(L_4);
		__this->set_container_1((LinkedList_1_t2376585677 *)NULL);
		return;
	}
}
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Int32>::get_List()
extern "C"  LinkedList_1_t2376585677 * LinkedListNode_1_get_List_m934609283_gshared (LinkedListNode_1_t967983361 * __this, const MethodInfo* method)
{
	{
		LinkedList_1_t2376585677 * L_0 = (LinkedList_1_t2376585677 *)__this->get_container_1();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Int32>::get_Next()
extern "C"  LinkedListNode_1_t967983361 * LinkedListNode_1_get_Next_m1225954497_gshared (LinkedListNode_1_t967983361 * __this, const MethodInfo* method)
{
	LinkedListNode_1_t967983361 * G_B4_0 = NULL;
	{
		LinkedList_1_t2376585677 * L_0 = (LinkedList_1_t2376585677 *)__this->get_container_1();
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		LinkedListNode_1_t967983361 * L_1 = (LinkedListNode_1_t967983361 *)__this->get_forward_2();
		LinkedList_1_t2376585677 * L_2 = (LinkedList_1_t2376585677 *)__this->get_container_1();
		NullCheck(L_2);
		LinkedListNode_1_t967983361 * L_3 = (LinkedListNode_1_t967983361 *)L_2->get_first_3();
		if ((((Il2CppObject*)(LinkedListNode_1_t967983361 *)L_1) == ((Il2CppObject*)(LinkedListNode_1_t967983361 *)L_3)))
		{
			goto IL_002c;
		}
	}
	{
		LinkedListNode_1_t967983361 * L_4 = (LinkedListNode_1_t967983361 *)__this->get_forward_2();
		G_B4_0 = L_4;
		goto IL_002d;
	}

IL_002c:
	{
		G_B4_0 = ((LinkedListNode_1_t967983361 *)(NULL));
	}

IL_002d:
	{
		return G_B4_0;
	}
}
// T System.Collections.Generic.LinkedListNode`1<System.Int32>::get_Value()
extern "C"  int32_t LinkedListNode_1_get_Value_m4037355866_gshared (LinkedListNode_1_t967983361 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_item_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
extern "C"  void LinkedListNode_1__ctor_m231316848_gshared (LinkedListNode_1_t1585555208 * __this, LinkedList_1_t2994157524 * ___list0, Il2CppObject * ___value1, const MethodInfo* method)
{
	LinkedListNode_1_t1585555208 * V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		LinkedList_1_t2994157524 * L_0 = ___list0;
		__this->set_container_1(L_0);
		Il2CppObject * L_1 = ___value1;
		__this->set_item_0(L_1);
		V_0 = (LinkedListNode_1_t1585555208 *)__this;
		__this->set_forward_2(__this);
		LinkedListNode_1_t1585555208 * L_2 = V_0;
		__this->set_back_3(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedListNode_1__ctor_m127450634_gshared (LinkedListNode_1_t1585555208 * __this, LinkedList_1_t2994157524 * ___list0, Il2CppObject * ___value1, LinkedListNode_1_t1585555208 * ___previousNode2, LinkedListNode_1_t1585555208 * ___nextNode3, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		LinkedList_1_t2994157524 * L_0 = ___list0;
		__this->set_container_1(L_0);
		Il2CppObject * L_1 = ___value1;
		__this->set_item_0(L_1);
		LinkedListNode_1_t1585555208 * L_2 = ___previousNode2;
		__this->set_back_3(L_2);
		LinkedListNode_1_t1585555208 * L_3 = ___nextNode3;
		__this->set_forward_2(L_3);
		LinkedListNode_1_t1585555208 * L_4 = ___previousNode2;
		NullCheck(L_4);
		L_4->set_forward_2(__this);
		LinkedListNode_1_t1585555208 * L_5 = ___nextNode3;
		NullCheck(L_5);
		L_5->set_back_3(__this);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::Detach()
extern "C"  void LinkedListNode_1_Detach_m3110429670_gshared (LinkedListNode_1_t1585555208 * __this, const MethodInfo* method)
{
	LinkedListNode_1_t1585555208 * V_0 = NULL;
	{
		LinkedListNode_1_t1585555208 * L_0 = (LinkedListNode_1_t1585555208 *)__this->get_back_3();
		LinkedListNode_1_t1585555208 * L_1 = (LinkedListNode_1_t1585555208 *)__this->get_forward_2();
		NullCheck(L_0);
		L_0->set_forward_2(L_1);
		LinkedListNode_1_t1585555208 * L_2 = (LinkedListNode_1_t1585555208 *)__this->get_forward_2();
		LinkedListNode_1_t1585555208 * L_3 = (LinkedListNode_1_t1585555208 *)__this->get_back_3();
		NullCheck(L_2);
		L_2->set_back_3(L_3);
		V_0 = (LinkedListNode_1_t1585555208 *)NULL;
		__this->set_back_3((LinkedListNode_1_t1585555208 *)NULL);
		LinkedListNode_1_t1585555208 * L_4 = V_0;
		__this->set_forward_2(L_4);
		__this->set_container_1((LinkedList_1_t2994157524 *)NULL);
		return;
	}
}
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_List()
extern "C"  LinkedList_1_t2994157524 * LinkedListNode_1_get_List_m2861065156_gshared (LinkedListNode_1_t1585555208 * __this, const MethodInfo* method)
{
	{
		LinkedList_1_t2994157524 * L_0 = (LinkedList_1_t2994157524 *)__this->get_container_1();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_Next()
extern "C"  LinkedListNode_1_t1585555208 * LinkedListNode_1_get_Next_m1067948741_gshared (LinkedListNode_1_t1585555208 * __this, const MethodInfo* method)
{
	LinkedListNode_1_t1585555208 * G_B4_0 = NULL;
	{
		LinkedList_1_t2994157524 * L_0 = (LinkedList_1_t2994157524 *)__this->get_container_1();
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		LinkedListNode_1_t1585555208 * L_1 = (LinkedListNode_1_t1585555208 *)__this->get_forward_2();
		LinkedList_1_t2994157524 * L_2 = (LinkedList_1_t2994157524 *)__this->get_container_1();
		NullCheck(L_2);
		LinkedListNode_1_t1585555208 * L_3 = (LinkedListNode_1_t1585555208 *)L_2->get_first_3();
		if ((((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_1) == ((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_3)))
		{
			goto IL_002c;
		}
	}
	{
		LinkedListNode_1_t1585555208 * L_4 = (LinkedListNode_1_t1585555208 *)__this->get_forward_2();
		G_B4_0 = L_4;
		goto IL_002d;
	}

IL_002c:
	{
		G_B4_0 = ((LinkedListNode_1_t1585555208 *)(NULL));
	}

IL_002d:
	{
		return G_B4_0;
	}
}
// T System.Collections.Generic.LinkedListNode`1<System.Object>::get_Value()
extern "C"  Il2CppObject * LinkedListNode_1_get_Value_m201900116_gshared (LinkedListNode_1_t1585555208 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_item_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3053486497_gshared (Enumerator_t3872978090 * __this, List_1_t43281120 * ___l0, const MethodInfo* method)
{
	{
		List_1_t43281120 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t43281120 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3053486497_AdjustorThunk (Il2CppObject * __this, List_1_t43281120 * ___l0, const MethodInfo* method)
{
	Enumerator_t3872978090 * _thisAdjusted = reinterpret_cast<Enumerator_t3872978090 *>(__this + 1);
	Enumerator__ctor_m3053486497(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2658021897_gshared (Enumerator_t3872978090 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m731607703((Enumerator_t3872978090 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2658021897_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3872978090 * _thisAdjusted = reinterpret_cast<Enumerator_t3872978090 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2658021897(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1210011841_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1210011841_gshared (Enumerator_t3872978090 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1210011841_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m731607703((Enumerator_t3872978090 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		IndexedColumn_t674159988  L_2 = (IndexedColumn_t674159988 )__this->get_current_3();
		IndexedColumn_t674159988  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1210011841_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3872978090 * _thisAdjusted = reinterpret_cast<Enumerator_t3872978090 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1210011841(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Dispose()
extern "C"  void Enumerator_Dispose_m3164348436_gshared (Enumerator_t3872978090 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t43281120 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3164348436_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3872978090 * _thisAdjusted = reinterpret_cast<Enumerator_t3872978090 *>(__this + 1);
	Enumerator_Dispose_m3164348436(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m731607703_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m731607703_gshared (Enumerator_t3872978090 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m731607703_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t43281120 * L_0 = (List_1_t43281120 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3872978090  L_1 = (*(Enumerator_t3872978090 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t43281120 * L_7 = (List_1_t43281120 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m731607703_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3872978090 * _thisAdjusted = reinterpret_cast<Enumerator_t3872978090 *>(__this + 1);
	Enumerator_VerifyState_m731607703(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2843108425_gshared (Enumerator_t3872978090 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m731607703((Enumerator_t3872978090 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t43281120 * L_2 = (List_1_t43281120 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t43281120 * L_4 = (List_1_t43281120 *)__this->get_l_0();
		NullCheck(L_4);
		IndexedColumnU5BU5D_t4219656765* L_5 = (IndexedColumnU5BU5D_t4219656765*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		IndexedColumn_t674159988  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2843108425_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3872978090 * _thisAdjusted = reinterpret_cast<Enumerator_t3872978090 *>(__this + 1);
	return Enumerator_MoveNext_m2843108425(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::get_Current()
extern "C"  IndexedColumn_t674159988  Enumerator_get_Current_m2946794598_gshared (Enumerator_t3872978090 * __this, const MethodInfo* method)
{
	{
		IndexedColumn_t674159988  L_0 = (IndexedColumn_t674159988 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  IndexedColumn_t674159988  Enumerator_get_Current_m2946794598_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3872978090 * _thisAdjusted = reinterpret_cast<Enumerator_t3872978090 *>(__this + 1);
	return Enumerator_get_Current_m2946794598(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3029487006_gshared (Enumerator_t2586955242 * __this, List_1_t3052225568 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3052225568 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3052225568 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3029487006_AdjustorThunk (Il2CppObject * __this, List_1_t3052225568 * ___l0, const MethodInfo* method)
{
	Enumerator_t2586955242 * _thisAdjusted = reinterpret_cast<Enumerator_t2586955242 *>(__this + 1);
	Enumerator__ctor_m3029487006(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1762141580_gshared (Enumerator_t2586955242 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m4227308372((Enumerator_t2586955242 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1762141580_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2586955242 * _thisAdjusted = reinterpret_cast<Enumerator_t2586955242 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1762141580(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Byte>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2262489960_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2262489960_gshared (Enumerator_t2586955242 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2262489960_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m4227308372((Enumerator_t2586955242 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		uint8_t L_2 = (uint8_t)__this->get_current_3();
		uint8_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2262489960_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2586955242 * _thisAdjusted = reinterpret_cast<Enumerator_t2586955242 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2262489960(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte>::Dispose()
extern "C"  void Enumerator_Dispose_m2589402619_gshared (Enumerator_t2586955242 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3052225568 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2589402619_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2586955242 * _thisAdjusted = reinterpret_cast<Enumerator_t2586955242 *>(__this + 1);
	Enumerator_Dispose_m2589402619(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m4227308372_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m4227308372_gshared (Enumerator_t2586955242 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m4227308372_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3052225568 * L_0 = (List_1_t3052225568 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t2586955242  L_1 = (*(Enumerator_t2586955242 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3052225568 * L_7 = (List_1_t3052225568 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m4227308372_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2586955242 * _thisAdjusted = reinterpret_cast<Enumerator_t2586955242 *>(__this + 1);
	Enumerator_VerifyState_m4227308372(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Byte>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3877991572_gshared (Enumerator_t2586955242 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m4227308372((Enumerator_t2586955242 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3052225568 * L_2 = (List_1_t3052225568 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3052225568 * L_4 = (List_1_t3052225568 *)__this->get_l_0();
		NullCheck(L_4);
		ByteU5BU5D_t3397334013* L_5 = (ByteU5BU5D_t3397334013*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		uint8_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3877991572_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2586955242 * _thisAdjusted = reinterpret_cast<Enumerator_t2586955242 *>(__this + 1);
	return Enumerator_MoveNext_m3877991572(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Byte>::get_Current()
extern "C"  uint8_t Enumerator_get_Current_m3313884163_gshared (Enumerator_t2586955242 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (uint8_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  uint8_t Enumerator_get_Current_m3313884163_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2586955242 * _thisAdjusted = reinterpret_cast<Enumerator_t2586955242 *>(__this + 1);
	return Enumerator_get_Current_m3313884163(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3896932057_gshared (Enumerator_t2620100900 * __this, List_1_t3085371226 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3085371226 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3085371226 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3896932057_AdjustorThunk (Il2CppObject * __this, List_1_t3085371226 * ___l0, const MethodInfo* method)
{
	Enumerator_t2620100900 * _thisAdjusted = reinterpret_cast<Enumerator_t2620100900 *>(__this + 1);
	Enumerator__ctor_m3896932057(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4116876945_gshared (Enumerator_t2620100900 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m570381259((Enumerator_t2620100900 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4116876945_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2620100900 * _thisAdjusted = reinterpret_cast<Enumerator_t2620100900 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m4116876945(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2417842817_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2417842817_gshared (Enumerator_t2620100900 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2417842817_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m570381259((Enumerator_t2620100900 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		KeyValuePair_2_t3716250094  L_2 = (KeyValuePair_2_t3716250094 )__this->get_current_3();
		KeyValuePair_2_t3716250094  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2417842817_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2620100900 * _thisAdjusted = reinterpret_cast<Enumerator_t2620100900 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2417842817(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Dispose()
extern "C"  void Enumerator_Dispose_m643661990_gshared (Enumerator_t2620100900 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3085371226 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m643661990_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2620100900 * _thisAdjusted = reinterpret_cast<Enumerator_t2620100900 *>(__this + 1);
	Enumerator_Dispose_m643661990(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m570381259_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m570381259_gshared (Enumerator_t2620100900 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m570381259_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3085371226 * L_0 = (List_1_t3085371226 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t2620100900  L_1 = (*(Enumerator_t2620100900 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3085371226 * L_7 = (List_1_t3085371226 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m570381259_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2620100900 * _thisAdjusted = reinterpret_cast<Enumerator_t2620100900 *>(__this + 1);
	Enumerator_VerifyState_m570381259(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1558544689_gshared (Enumerator_t2620100900 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m570381259((Enumerator_t2620100900 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3085371226 * L_2 = (List_1_t3085371226 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3085371226 * L_4 = (List_1_t3085371226 *)__this->get_l_0();
		NullCheck(L_4);
		KeyValuePair_2U5BU5D_t2270685851* L_5 = (KeyValuePair_2U5BU5D_t2270685851*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		KeyValuePair_2_t3716250094  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1558544689_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2620100900 * _thisAdjusted = reinterpret_cast<Enumerator_t2620100900 *>(__this + 1);
	return Enumerator_MoveNext_m1558544689(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Current()
extern "C"  KeyValuePair_2_t3716250094  Enumerator_get_Current_m3902255900_gshared (Enumerator_t2620100900 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3716250094  L_0 = (KeyValuePair_2_t3716250094 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t3716250094  Enumerator_get_Current_m3902255900_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2620100900 * _thisAdjusted = reinterpret_cast<Enumerator_t2620100900 *>(__this + 1);
	return Enumerator_get_Current_m3902255900(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1246468418_gshared (Enumerator_t3237672747 * __this, List_1_t3702943073 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3702943073 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3702943073 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1246468418_AdjustorThunk (Il2CppObject * __this, List_1_t3702943073 * ___l0, const MethodInfo* method)
{
	Enumerator_t3237672747 * _thisAdjusted = reinterpret_cast<Enumerator_t3237672747 *>(__this + 1);
	Enumerator__ctor_m1246468418(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m372155760_gshared (Enumerator_t3237672747 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1266371732((Enumerator_t3237672747 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m372155760_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3237672747 * _thisAdjusted = reinterpret_cast<Enumerator_t3237672747 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m372155760(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1324042990_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1324042990_gshared (Enumerator_t3237672747 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1324042990_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m1266371732((Enumerator_t3237672747 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		KeyValuePair_2_t38854645  L_2 = (KeyValuePair_2_t38854645 )__this->get_current_3();
		KeyValuePair_2_t38854645  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1324042990_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3237672747 * _thisAdjusted = reinterpret_cast<Enumerator_t3237672747 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1324042990(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void Enumerator_Dispose_m2341522715_gshared (Enumerator_t3237672747 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3702943073 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2341522715_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3237672747 * _thisAdjusted = reinterpret_cast<Enumerator_t3237672747 *>(__this + 1);
	Enumerator_Dispose_m2341522715(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m1266371732_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1266371732_gshared (Enumerator_t3237672747 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1266371732_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3702943073 * L_0 = (List_1_t3702943073 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3237672747  L_1 = (*(Enumerator_t3237672747 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3702943073 * L_7 = (List_1_t3702943073 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1266371732_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3237672747 * _thisAdjusted = reinterpret_cast<Enumerator_t3237672747 *>(__this + 1);
	Enumerator_VerifyState_m1266371732(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3337940480_gshared (Enumerator_t3237672747 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m1266371732((Enumerator_t3237672747 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3702943073 * L_2 = (List_1_t3702943073 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3702943073 * L_4 = (List_1_t3702943073 *)__this->get_l_0();
		NullCheck(L_4);
		KeyValuePair_2U5BU5D_t2854920344* L_5 = (KeyValuePair_2U5BU5D_t2854920344*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		KeyValuePair_2_t38854645  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3337940480_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3237672747 * _thisAdjusted = reinterpret_cast<Enumerator_t3237672747 *>(__this + 1);
	return Enumerator_MoveNext_m3337940480(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t38854645  Enumerator_get_Current_m305664535_gshared (Enumerator_t3237672747 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = (KeyValuePair_2_t38854645 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t38854645  Enumerator_get_Current_m305664535_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3237672747 * _thisAdjusted = reinterpret_cast<Enumerator_t3237672747 *>(__this + 1);
	return Enumerator_get_Current_m305664535(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1614742070_gshared (Enumerator_t975728254 * __this, List_1_t1440998580 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1440998580 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1440998580 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1614742070_AdjustorThunk (Il2CppObject * __this, List_1_t1440998580 * ___l0, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	Enumerator__ctor_m1614742070(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1016756388_gshared (Enumerator_t975728254 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2167629240((Enumerator_t975728254 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1016756388_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1016756388(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2154261170_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2154261170_gshared (Enumerator_t975728254 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2154261170_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m2167629240((Enumerator_t975728254 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_current_3();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2154261170_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2154261170(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m1274756239_gshared (Enumerator_t975728254 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1440998580 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1274756239_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	Enumerator_Dispose_m1274756239(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m2167629240_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2167629240_gshared (Enumerator_t975728254 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2167629240_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t975728254  L_1 = (*(Enumerator_t975728254 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1440998580 * L_7 = (List_1_t1440998580 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2167629240_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	Enumerator_VerifyState_m2167629240(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2584216872_gshared (Enumerator_t975728254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2167629240((Enumerator_t975728254 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1440998580 * L_2 = (List_1_t1440998580 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1440998580 * L_4 = (List_1_t1440998580 *)__this->get_l_0();
		NullCheck(L_4);
		Int32U5BU5D_t3030399641* L_5 = (Int32U5BU5D_t3030399641*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2584216872_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	return Enumerator_MoveNext_m2584216872(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2400786426_gshared (Enumerator_t975728254 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  int32_t Enumerator_get_Current_m2400786426_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	return Enumerator_get_Current_m2400786426(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3769601633_gshared (Enumerator_t1593300101 * __this, List_1_t2058570427 * ___l0, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t2058570427 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3769601633_AdjustorThunk (Il2CppObject * __this, List_1_t2058570427 * ___l0, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	Enumerator__ctor_m3769601633(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m825848279((Enumerator_t1593300101 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3440386353_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3440386353(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m825848279((Enumerator_t1593300101 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_current_3();
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2853089017(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3736175406_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t2058570427 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3736175406_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	Enumerator_Dispose_m3736175406(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m825848279_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m825848279_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m825848279_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1593300101  L_1 = (*(Enumerator_t1593300101 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t2058570427 * L_7 = (List_1_t2058570427 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m825848279_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	Enumerator_VerifyState_m825848279(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4021114635_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m825848279((Enumerator_t1593300101 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t2058570427 * L_2 = (List_1_t2058570427 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t2058570427 * L_4 = (List_1_t2058570427 *)__this->get_l_0();
		NullCheck(L_4);
		ObjectU5BU5D_t3614634134* L_5 = (ObjectU5BU5D_t3614634134*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m4021114635_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	return Enumerator_MoveNext_m4021114635(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1222341175_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_current_3();
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m1222341175_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	return Enumerator_get_Current_m1222341175(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3021143890_gshared (Enumerator_t3292975645 * __this, List_1_t3758245971 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3758245971 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3758245971 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3021143890_AdjustorThunk (Il2CppObject * __this, List_1_t3758245971 * ___l0, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	Enumerator__ctor_m3021143890(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m610822832_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m739025304((Enumerator_t3292975645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m610822832_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m610822832(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1278092846_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1278092846_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1278092846_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m739025304((Enumerator_t3292975645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		CustomAttributeNamedArgument_t94157543  L_2 = (CustomAttributeNamedArgument_t94157543 )__this->get_current_3();
		CustomAttributeNamedArgument_t94157543  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1278092846_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1278092846(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m3704913451_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3758245971 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3704913451_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	Enumerator_Dispose_m3704913451(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m739025304_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m739025304_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m739025304_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3758245971 * L_0 = (List_1_t3758245971 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3292975645  L_1 = (*(Enumerator_t3292975645 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3758245971 * L_7 = (List_1_t3758245971 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m739025304_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	Enumerator_VerifyState_m739025304(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m598197344_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m739025304((Enumerator_t3292975645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3758245971 * L_2 = (List_1_t3758245971 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3758245971 * L_4 = (List_1_t3758245971 *)__this->get_l_0();
		NullCheck(L_4);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_5 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		CustomAttributeNamedArgument_t94157543  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m598197344_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	return Enumerator_MoveNext_m598197344(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern "C"  CustomAttributeNamedArgument_t94157543  Enumerator_get_Current_m3860473239_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t94157543  L_0 = (CustomAttributeNamedArgument_t94157543 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  CustomAttributeNamedArgument_t94157543  Enumerator_get_Current_m3860473239_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	return Enumerator_get_Current_m3860473239(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3421311553_gshared (Enumerator_t402048720 * __this, List_1_t867319046 * ___l0, const MethodInfo* method)
{
	{
		List_1_t867319046 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t867319046 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3421311553_AdjustorThunk (Il2CppObject * __this, List_1_t867319046 * ___l0, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	Enumerator__ctor_m3421311553(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1436660297_gshared (Enumerator_t402048720 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m435841047((Enumerator_t402048720 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1436660297_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1436660297(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m355114893_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m355114893_gshared (Enumerator_t402048720 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m355114893_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m435841047((Enumerator_t402048720 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		CustomAttributeTypedArgument_t1498197914  L_2 = (CustomAttributeTypedArgument_t1498197914 )__this->get_current_3();
		CustomAttributeTypedArgument_t1498197914  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m355114893_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m355114893(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m3434518394_gshared (Enumerator_t402048720 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t867319046 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3434518394_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	Enumerator_Dispose_m3434518394(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m435841047_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m435841047_gshared (Enumerator_t402048720 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m435841047_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t867319046 * L_0 = (List_1_t867319046 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t402048720  L_1 = (*(Enumerator_t402048720 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t867319046 * L_7 = (List_1_t867319046 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m435841047_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	Enumerator_VerifyState_m435841047(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1792725673_gshared (Enumerator_t402048720 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m435841047((Enumerator_t402048720 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t867319046 * L_2 = (List_1_t867319046 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t867319046 * L_4 = (List_1_t867319046 *)__this->get_l_0();
		NullCheck(L_4);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_5 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		CustomAttributeTypedArgument_t1498197914  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1792725673_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	return Enumerator_MoveNext_m1792725673(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern "C"  CustomAttributeTypedArgument_t1498197914  Enumerator_get_Current_m1371324410_gshared (Enumerator_t402048720 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t1498197914  L_0 = (CustomAttributeTypedArgument_t1498197914 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  CustomAttributeTypedArgument_t1498197914  Enumerator_get_Current_m1371324410_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	return Enumerator_get_Current_m1371324410(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2054046066_gshared (Enumerator_t4073335620 * __this, List_1_t243638650 * ___l0, const MethodInfo* method)
{
	{
		List_1_t243638650 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t243638650 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2054046066_AdjustorThunk (Il2CppObject * __this, List_1_t243638650 * ___l0, const MethodInfo* method)
{
	Enumerator_t4073335620 * _thisAdjusted = reinterpret_cast<Enumerator_t4073335620 *>(__this + 1);
	Enumerator__ctor_m2054046066(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1344379320_gshared (Enumerator_t4073335620 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1677639504((Enumerator_t4073335620 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1344379320_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4073335620 * _thisAdjusted = reinterpret_cast<Enumerator_t4073335620 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1344379320(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3979461448_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3979461448_gshared (Enumerator_t4073335620 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3979461448_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m1677639504((Enumerator_t4073335620 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Color32_t874517518  L_2 = (Color32_t874517518 )__this->get_current_3();
		Color32_t874517518  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3979461448_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4073335620 * _thisAdjusted = reinterpret_cast<Enumerator_t4073335620 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3979461448(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::Dispose()
extern "C"  void Enumerator_Dispose_m1300762389_gshared (Enumerator_t4073335620 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t243638650 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1300762389_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4073335620 * _thisAdjusted = reinterpret_cast<Enumerator_t4073335620 *>(__this + 1);
	Enumerator_Dispose_m1300762389(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m1677639504_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1677639504_gshared (Enumerator_t4073335620 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1677639504_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t243638650 * L_0 = (List_1_t243638650 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t4073335620  L_1 = (*(Enumerator_t4073335620 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t243638650 * L_7 = (List_1_t243638650 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1677639504_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4073335620 * _thisAdjusted = reinterpret_cast<Enumerator_t4073335620 *>(__this + 1);
	Enumerator_VerifyState_m1677639504(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2625246500_gshared (Enumerator_t4073335620 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m1677639504((Enumerator_t4073335620 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t243638650 * L_2 = (List_1_t243638650 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t243638650 * L_4 = (List_1_t243638650 *)__this->get_l_0();
		NullCheck(L_4);
		Color32U5BU5D_t30278651* L_5 = (Color32U5BU5D_t30278651*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		Color32_t874517518  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2625246500_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4073335620 * _thisAdjusted = reinterpret_cast<Enumerator_t4073335620 *>(__this + 1);
	return Enumerator_MoveNext_m2625246500(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::get_Current()
extern "C"  Color32_t874517518  Enumerator_get_Current_m1482710541_gshared (Enumerator_t4073335620 * __this, const MethodInfo* method)
{
	{
		Color32_t874517518  L_0 = (Color32_t874517518 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Color32_t874517518  Enumerator_get_Current_m1482710541_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4073335620 * _thisAdjusted = reinterpret_cast<Enumerator_t4073335620 *>(__this + 1);
	return Enumerator_get_Current_m1482710541(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3979168432_gshared (Enumerator_t3220004478 * __this, List_1_t3685274804 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3685274804 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3685274804 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3979168432_AdjustorThunk (Il2CppObject * __this, List_1_t3685274804 * ___l0, const MethodInfo* method)
{
	Enumerator_t3220004478 * _thisAdjusted = reinterpret_cast<Enumerator_t3220004478 *>(__this + 1);
	Enumerator__ctor_m3979168432(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m336811426_gshared (Enumerator_t3220004478 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2948867230((Enumerator_t3220004478 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m336811426_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3220004478 * _thisAdjusted = reinterpret_cast<Enumerator_t3220004478 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m336811426(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3079057684_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3079057684_gshared (Enumerator_t3220004478 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3079057684_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m2948867230((Enumerator_t3220004478 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		RaycastResult_t21186376  L_2 = (RaycastResult_t21186376 )__this->get_current_3();
		RaycastResult_t21186376  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3079057684_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3220004478 * _thisAdjusted = reinterpret_cast<Enumerator_t3220004478 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3079057684(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C"  void Enumerator_Dispose_m3455280711_gshared (Enumerator_t3220004478 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3685274804 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3455280711_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3220004478 * _thisAdjusted = reinterpret_cast<Enumerator_t3220004478 *>(__this + 1);
	Enumerator_Dispose_m3455280711(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m2948867230_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2948867230_gshared (Enumerator_t3220004478 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2948867230_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3685274804 * L_0 = (List_1_t3685274804 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3220004478  L_1 = (*(Enumerator_t3220004478 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3685274804 * L_7 = (List_1_t3685274804 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2948867230_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3220004478 * _thisAdjusted = reinterpret_cast<Enumerator_t3220004478 *>(__this + 1);
	Enumerator_VerifyState_m2948867230(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2628556578_gshared (Enumerator_t3220004478 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2948867230((Enumerator_t3220004478 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3685274804 * L_2 = (List_1_t3685274804 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3685274804 * L_4 = (List_1_t3685274804 *)__this->get_l_0();
		NullCheck(L_4);
		RaycastResultU5BU5D_t603556505* L_5 = (RaycastResultU5BU5D_t603556505*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		RaycastResult_t21186376  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2628556578_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3220004478 * _thisAdjusted = reinterpret_cast<Enumerator_t3220004478 *>(__this + 1);
	return Enumerator_MoveNext_m2628556578(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C"  RaycastResult_t21186376  Enumerator_get_Current_m2728219003_gshared (Enumerator_t3220004478 * __this, const MethodInfo* method)
{
	{
		RaycastResult_t21186376  L_0 = (RaycastResult_t21186376 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  RaycastResult_t21186376  Enumerator_get_Current_m2728219003_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3220004478 * _thisAdjusted = reinterpret_cast<Enumerator_t3220004478 *>(__this + 1);
	return Enumerator_get_Current_m2728219003(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Experimental.Director.Playable>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3272552146_gshared (Enumerator_t2571396354 * __this, List_1_t3036666680 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3036666680 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3036666680 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3272552146_AdjustorThunk (Il2CppObject * __this, List_1_t3036666680 * ___l0, const MethodInfo* method)
{
	Enumerator_t2571396354 * _thisAdjusted = reinterpret_cast<Enumerator_t2571396354 *>(__this + 1);
	Enumerator__ctor_m3272552146(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Experimental.Director.Playable>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4029239424_gshared (Enumerator_t2571396354 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3501381548((Enumerator_t2571396354 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4029239424_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2571396354 * _thisAdjusted = reinterpret_cast<Enumerator_t2571396354 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m4029239424(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Experimental.Director.Playable>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m4221072214_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4221072214_gshared (Enumerator_t2571396354 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m4221072214_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m3501381548((Enumerator_t2571396354 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Playable_t3667545548  L_2 = (Playable_t3667545548 )__this->get_current_3();
		Playable_t3667545548  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4221072214_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2571396354 * _thisAdjusted = reinterpret_cast<Enumerator_t2571396354 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m4221072214(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Experimental.Director.Playable>::Dispose()
extern "C"  void Enumerator_Dispose_m2291099515_gshared (Enumerator_t2571396354 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3036666680 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2291099515_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2571396354 * _thisAdjusted = reinterpret_cast<Enumerator_t2571396354 *>(__this + 1);
	Enumerator_Dispose_m2291099515(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Experimental.Director.Playable>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m3501381548_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3501381548_gshared (Enumerator_t2571396354 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3501381548_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3036666680 * L_0 = (List_1_t3036666680 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t2571396354  L_1 = (*(Enumerator_t2571396354 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3036666680 * L_7 = (List_1_t3036666680 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3501381548_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2571396354 * _thisAdjusted = reinterpret_cast<Enumerator_t2571396354 *>(__this + 1);
	Enumerator_VerifyState_m3501381548(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Experimental.Director.Playable>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1706162224_gshared (Enumerator_t2571396354 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m3501381548((Enumerator_t2571396354 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3036666680 * L_2 = (List_1_t3036666680 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3036666680 * L_4 = (List_1_t3036666680 *)__this->get_l_0();
		NullCheck(L_4);
		PlayableU5BU5D_t4034110853* L_5 = (PlayableU5BU5D_t4034110853*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		Playable_t3667545548  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1706162224_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2571396354 * _thisAdjusted = reinterpret_cast<Enumerator_t2571396354 *>(__this + 1);
	return Enumerator_MoveNext_m1706162224(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Experimental.Director.Playable>::get_Current()
extern "C"  Playable_t3667545548  Enumerator_get_Current_m552964111_gshared (Enumerator_t2571396354 * __this, const MethodInfo* method)
{
	{
		Playable_t3667545548  L_0 = (Playable_t3667545548 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Playable_t3667545548  Enumerator_get_Current_m552964111_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2571396354 * _thisAdjusted = reinterpret_cast<Enumerator_t2571396354 *>(__this + 1);
	return Enumerator_get_Current_m552964111(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RuntimePlatform>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1907247367_gshared (Enumerator_t773435773 * __this, List_1_t1238706099 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1238706099 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1238706099 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1907247367_AdjustorThunk (Il2CppObject * __this, List_1_t1238706099 * ___l0, const MethodInfo* method)
{
	Enumerator_t773435773 * _thisAdjusted = reinterpret_cast<Enumerator_t773435773 *>(__this + 1);
	Enumerator__ctor_m1907247367(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RuntimePlatform>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2147559323_gshared (Enumerator_t773435773 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m672438241((Enumerator_t773435773 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2147559323_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t773435773 * _thisAdjusted = reinterpret_cast<Enumerator_t773435773 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2147559323(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.RuntimePlatform>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3298729487_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3298729487_gshared (Enumerator_t773435773 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3298729487_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m672438241((Enumerator_t773435773 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_current_3();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3298729487_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t773435773 * _thisAdjusted = reinterpret_cast<Enumerator_t773435773 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3298729487(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RuntimePlatform>::Dispose()
extern "C"  void Enumerator_Dispose_m2117526090_gshared (Enumerator_t773435773 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1238706099 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2117526090_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t773435773 * _thisAdjusted = reinterpret_cast<Enumerator_t773435773 *>(__this + 1);
	Enumerator_Dispose_m2117526090(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RuntimePlatform>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m672438241_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m672438241_gshared (Enumerator_t773435773 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m672438241_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1238706099 * L_0 = (List_1_t1238706099 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t773435773  L_1 = (*(Enumerator_t773435773 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1238706099 * L_7 = (List_1_t1238706099 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m672438241_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t773435773 * _thisAdjusted = reinterpret_cast<Enumerator_t773435773 *>(__this + 1);
	Enumerator_VerifyState_m672438241(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.RuntimePlatform>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3096514239_gshared (Enumerator_t773435773 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m672438241((Enumerator_t773435773 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1238706099 * L_2 = (List_1_t1238706099 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1238706099 * L_4 = (List_1_t1238706099 *)__this->get_l_0();
		NullCheck(L_4);
		RuntimePlatformU5BU5D_t1634724030* L_5 = (RuntimePlatformU5BU5D_t1634724030*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3096514239_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t773435773 * _thisAdjusted = reinterpret_cast<Enumerator_t773435773 *>(__this + 1);
	return Enumerator_MoveNext_m3096514239(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.RuntimePlatform>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3598702868_gshared (Enumerator_t773435773 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  int32_t Enumerator_get_Current_m3598702868_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t773435773 * _thisAdjusted = reinterpret_cast<Enumerator_t773435773 *>(__this + 1);
	return Enumerator_get_Current_m3598702868(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3512622280_gshared (Enumerator_t1960487606 * __this, List_1_t2425757932 * ___l0, const MethodInfo* method)
{
	{
		List_1_t2425757932 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t2425757932 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3512622280_AdjustorThunk (Il2CppObject * __this, List_1_t2425757932 * ___l0, const MethodInfo* method)
{
	Enumerator_t1960487606 * _thisAdjusted = reinterpret_cast<Enumerator_t1960487606 *>(__this + 1);
	Enumerator__ctor_m3512622280(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2200349770_gshared (Enumerator_t1960487606 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2358705882((Enumerator_t1960487606 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2200349770_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1960487606 * _thisAdjusted = reinterpret_cast<Enumerator_t1960487606 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2200349770(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3461301268_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3461301268_gshared (Enumerator_t1960487606 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3461301268_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m2358705882((Enumerator_t1960487606 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		UICharInfo_t3056636800  L_2 = (UICharInfo_t3056636800 )__this->get_current_3();
		UICharInfo_t3056636800  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3461301268_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1960487606 * _thisAdjusted = reinterpret_cast<Enumerator_t1960487606 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3461301268(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m3756179807_gshared (Enumerator_t1960487606 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t2425757932 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3756179807_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1960487606 * _thisAdjusted = reinterpret_cast<Enumerator_t1960487606 *>(__this + 1);
	Enumerator_Dispose_m3756179807(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m2358705882_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2358705882_gshared (Enumerator_t1960487606 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2358705882_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t2425757932 * L_0 = (List_1_t2425757932 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1960487606  L_1 = (*(Enumerator_t1960487606 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t2425757932 * L_7 = (List_1_t2425757932 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2358705882_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1960487606 * _thisAdjusted = reinterpret_cast<Enumerator_t1960487606 *>(__this + 1);
	Enumerator_VerifyState_m2358705882(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m848781978_gshared (Enumerator_t1960487606 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2358705882((Enumerator_t1960487606 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t2425757932 * L_2 = (List_1_t2425757932 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t2425757932 * L_4 = (List_1_t2425757932 *)__this->get_l_0();
		NullCheck(L_4);
		UICharInfoU5BU5D_t2749705857* L_5 = (UICharInfoU5BU5D_t2749705857*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		UICharInfo_t3056636800  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m848781978_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1960487606 * _thisAdjusted = reinterpret_cast<Enumerator_t1960487606 *>(__this + 1);
	return Enumerator_MoveNext_m848781978(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::get_Current()
extern "C"  UICharInfo_t3056636800  Enumerator_get_Current_m3839136987_gshared (Enumerator_t1960487606 * __this, const MethodInfo* method)
{
	{
		UICharInfo_t3056636800  L_0 = (UICharInfo_t3056636800 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  UICharInfo_t3056636800  Enumerator_get_Current_m3839136987_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1960487606 * _thisAdjusted = reinterpret_cast<Enumerator_t1960487606 *>(__this + 1);
	return Enumerator_get_Current_m3839136987(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3903095790_gshared (Enumerator_t2525128680 * __this, List_1_t2990399006 * ___l0, const MethodInfo* method)
{
	{
		List_1_t2990399006 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t2990399006 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3903095790_AdjustorThunk (Il2CppObject * __this, List_1_t2990399006 * ___l0, const MethodInfo* method)
{
	Enumerator_t2525128680 * _thisAdjusted = reinterpret_cast<Enumerator_t2525128680 *>(__this + 1);
	Enumerator__ctor_m3903095790(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m925111644_gshared (Enumerator_t2525128680 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m4188527104((Enumerator_t2525128680 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m925111644_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2525128680 * _thisAdjusted = reinterpret_cast<Enumerator_t2525128680 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m925111644(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3228580602_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3228580602_gshared (Enumerator_t2525128680 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3228580602_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m4188527104((Enumerator_t2525128680 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		UILineInfo_t3621277874  L_2 = (UILineInfo_t3621277874 )__this->get_current_3();
		UILineInfo_t3621277874  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3228580602_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2525128680 * _thisAdjusted = reinterpret_cast<Enumerator_t2525128680 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3228580602(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m3109097029_gshared (Enumerator_t2525128680 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t2990399006 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3109097029_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2525128680 * _thisAdjusted = reinterpret_cast<Enumerator_t2525128680 *>(__this + 1);
	Enumerator_Dispose_m3109097029(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m4188527104_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m4188527104_gshared (Enumerator_t2525128680 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m4188527104_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t2990399006 * L_0 = (List_1_t2990399006 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t2525128680  L_1 = (*(Enumerator_t2525128680 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t2990399006 * L_7 = (List_1_t2990399006 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m4188527104_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2525128680 * _thisAdjusted = reinterpret_cast<Enumerator_t2525128680 *>(__this + 1);
	Enumerator_VerifyState_m4188527104(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2504790928_gshared (Enumerator_t2525128680 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m4188527104((Enumerator_t2525128680 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t2990399006 * L_2 = (List_1_t2990399006 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t2990399006 * L_4 = (List_1_t2990399006 *)__this->get_l_0();
		NullCheck(L_4);
		UILineInfoU5BU5D_t3471944775* L_5 = (UILineInfoU5BU5D_t3471944775*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		UILineInfo_t3621277874  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2504790928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2525128680 * _thisAdjusted = reinterpret_cast<Enumerator_t2525128680 *>(__this + 1);
	return Enumerator_MoveNext_m2504790928(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::get_Current()
extern "C"  UILineInfo_t3621277874  Enumerator_get_Current_m657641165_gshared (Enumerator_t2525128680 * __this, const MethodInfo* method)
{
	{
		UILineInfo_t3621277874  L_0 = (UILineInfo_t3621277874 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  UILineInfo_t3621277874  Enumerator_get_Current_m657641165_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2525128680 * _thisAdjusted = reinterpret_cast<Enumerator_t2525128680 *>(__this + 1);
	return Enumerator_get_Current_m657641165(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2578663110_gshared (Enumerator_t108109624 * __this, List_1_t573379950 * ___l0, const MethodInfo* method)
{
	{
		List_1_t573379950 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t573379950 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2578663110_AdjustorThunk (Il2CppObject * __this, List_1_t573379950 * ___l0, const MethodInfo* method)
{
	Enumerator_t108109624 * _thisAdjusted = reinterpret_cast<Enumerator_t108109624 *>(__this + 1);
	Enumerator__ctor_m2578663110(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3052395060_gshared (Enumerator_t108109624 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2807892176((Enumerator_t108109624 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3052395060_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t108109624 * _thisAdjusted = reinterpret_cast<Enumerator_t108109624 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3052395060(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m38564970_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m38564970_gshared (Enumerator_t108109624 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m38564970_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m2807892176((Enumerator_t108109624 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		UIVertex_t1204258818  L_2 = (UIVertex_t1204258818 )__this->get_current_3();
		UIVertex_t1204258818  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m38564970_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t108109624 * _thisAdjusted = reinterpret_cast<Enumerator_t108109624 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m38564970(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::Dispose()
extern "C"  void Enumerator_Dispose_m1292917021_gshared (Enumerator_t108109624 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t573379950 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1292917021_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t108109624 * _thisAdjusted = reinterpret_cast<Enumerator_t108109624 *>(__this + 1);
	Enumerator_Dispose_m1292917021(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m2807892176_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2807892176_gshared (Enumerator_t108109624 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2807892176_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t573379950 * L_0 = (List_1_t573379950 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t108109624  L_1 = (*(Enumerator_t108109624 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t573379950 * L_7 = (List_1_t573379950 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2807892176_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t108109624 * _thisAdjusted = reinterpret_cast<Enumerator_t108109624 *>(__this + 1);
	Enumerator_VerifyState_m2807892176(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m138320264_gshared (Enumerator_t108109624 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2807892176((Enumerator_t108109624 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t573379950 * L_2 = (List_1_t573379950 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t573379950 * L_4 = (List_1_t573379950 *)__this->get_l_0();
		NullCheck(L_4);
		UIVertexU5BU5D_t3048644023* L_5 = (UIVertexU5BU5D_t3048644023*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		UIVertex_t1204258818  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m138320264_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t108109624 * _thisAdjusted = reinterpret_cast<Enumerator_t108109624 *>(__this + 1);
	return Enumerator_MoveNext_m138320264(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::get_Current()
extern "C"  UIVertex_t1204258818  Enumerator_get_Current_m2585076237_gshared (Enumerator_t108109624 * __this, const MethodInfo* method)
{
	{
		UIVertex_t1204258818  L_0 = (UIVertex_t1204258818 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  UIVertex_t1204258818  Enumerator_get_Current_m2585076237_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t108109624 * _thisAdjusted = reinterpret_cast<Enumerator_t108109624 *>(__this + 1);
	return Enumerator_get_Current_m2585076237(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3172601063_gshared (Enumerator_t1147558385 * __this, List_1_t1612828711 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1612828711 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1612828711 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3172601063_AdjustorThunk (Il2CppObject * __this, List_1_t1612828711 * ___l0, const MethodInfo* method)
{
	Enumerator_t1147558385 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558385 *>(__this + 1);
	Enumerator__ctor_m3172601063(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1334470667_gshared (Enumerator_t1147558385 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3913376581((Enumerator_t1147558385 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1334470667_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558385 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558385 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1334470667(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3542273247_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3542273247_gshared (Enumerator_t1147558385 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3542273247_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m3913376581((Enumerator_t1147558385 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Vector2_t2243707579  L_2 = (Vector2_t2243707579 )__this->get_current_3();
		Vector2_t2243707579  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3542273247_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558385 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558385 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3542273247(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::Dispose()
extern "C"  void Enumerator_Dispose_m3717265706_gshared (Enumerator_t1147558385 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1612828711 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3717265706_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558385 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558385 *>(__this + 1);
	Enumerator_Dispose_m3717265706(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m3913376581_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3913376581_gshared (Enumerator_t1147558385 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3913376581_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1612828711 * L_0 = (List_1_t1612828711 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1147558385  L_1 = (*(Enumerator_t1147558385 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1612828711 * L_7 = (List_1_t1612828711 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3913376581_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558385 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558385 *>(__this + 1);
	Enumerator_VerifyState_m3913376581(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3483405135_gshared (Enumerator_t1147558385 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m3913376581((Enumerator_t1147558385 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1612828711 * L_2 = (List_1_t1612828711 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1612828711 * L_4 = (List_1_t1612828711 *)__this->get_l_0();
		NullCheck(L_4);
		Vector2U5BU5D_t686124026* L_5 = (Vector2U5BU5D_t686124026*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		Vector2_t2243707579  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3483405135_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558385 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558385 *>(__this + 1);
	return Enumerator_MoveNext_m3483405135(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::get_Current()
extern "C"  Vector2_t2243707579  Enumerator_get_Current_m1551076836_gshared (Enumerator_t1147558385 * __this, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = (Vector2_t2243707579 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Vector2_t2243707579  Enumerator_get_Current_m1551076836_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558385 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558385 *>(__this + 1);
	return Enumerator_get_Current_m1551076836(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1365181512_gshared (Enumerator_t1147558386 * __this, List_1_t1612828712 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1612828712 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1612828712 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1365181512_AdjustorThunk (Il2CppObject * __this, List_1_t1612828712 * ___l0, const MethodInfo* method)
{
	Enumerator_t1147558386 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558386 *>(__this + 1);
	Enumerator__ctor_m1365181512(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3796537546_gshared (Enumerator_t1147558386 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3639069574((Enumerator_t1147558386 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3796537546_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558386 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558386 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3796537546(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1103666686_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1103666686_gshared (Enumerator_t1147558386 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1103666686_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m3639069574((Enumerator_t1147558386 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Vector3_t2243707580  L_2 = (Vector3_t2243707580 )__this->get_current_3();
		Vector3_t2243707580  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1103666686_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558386 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558386 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1103666686(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::Dispose()
extern "C"  void Enumerator_Dispose_m3215924523_gshared (Enumerator_t1147558386 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1612828712 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3215924523_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558386 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558386 *>(__this + 1);
	Enumerator_Dispose_m3215924523(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m3639069574_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3639069574_gshared (Enumerator_t1147558386 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3639069574_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1612828712 * L_0 = (List_1_t1612828712 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1147558386  L_1 = (*(Enumerator_t1147558386 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1612828712 * L_7 = (List_1_t1612828712 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3639069574_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558386 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558386 *>(__this + 1);
	Enumerator_VerifyState_m3639069574(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1367380970_gshared (Enumerator_t1147558386 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m3639069574((Enumerator_t1147558386 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1612828712 * L_2 = (List_1_t1612828712 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1612828712 * L_4 = (List_1_t1612828712 *)__this->get_l_0();
		NullCheck(L_4);
		Vector3U5BU5D_t1172311765* L_5 = (Vector3U5BU5D_t1172311765*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		Vector3_t2243707580  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1367380970_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558386 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558386 *>(__this + 1);
	return Enumerator_MoveNext_m1367380970(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::get_Current()
extern "C"  Vector3_t2243707580  Enumerator_get_Current_m827571811_gshared (Enumerator_t1147558386 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = (Vector3_t2243707580 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Vector3_t2243707580  Enumerator_get_Current_m827571811_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558386 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558386 *>(__this + 1);
	return Enumerator_get_Current_m827571811(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m425576865_gshared (Enumerator_t1147558387 * __this, List_1_t1612828713 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1612828713 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1612828713 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m425576865_AdjustorThunk (Il2CppObject * __this, List_1_t1612828713 * ___l0, const MethodInfo* method)
{
	Enumerator_t1147558387 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558387 *>(__this + 1);
	Enumerator__ctor_m425576865(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2621684617_gshared (Enumerator_t1147558387 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3775669055((Enumerator_t1147558387 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2621684617_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558387 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558387 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2621684617(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3866069145_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3866069145_gshared (Enumerator_t1147558387 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3866069145_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m3775669055((Enumerator_t1147558387 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Vector4_t2243707581  L_2 = (Vector4_t2243707581 )__this->get_current_3();
		Vector4_t2243707581  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3866069145_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558387 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558387 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3866069145(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::Dispose()
extern "C"  void Enumerator_Dispose_m2705653668_gshared (Enumerator_t1147558387 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1612828713 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2705653668_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558387 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558387 *>(__this + 1);
	Enumerator_Dispose_m2705653668(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m3775669055_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3775669055_gshared (Enumerator_t1147558387 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3775669055_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1612828713 * L_0 = (List_1_t1612828713 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1147558387  L_1 = (*(Enumerator_t1147558387 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1612828713 * L_7 = (List_1_t1612828713 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3775669055_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558387 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558387 *>(__this + 1);
	Enumerator_VerifyState_m3775669055(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3293920409_gshared (Enumerator_t1147558387 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m3775669055((Enumerator_t1147558387 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1612828713 * L_2 = (List_1_t1612828713 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1612828713 * L_4 = (List_1_t1612828713 *)__this->get_l_0();
		NullCheck(L_4);
		Vector4U5BU5D_t1658499504* L_5 = (Vector4U5BU5D_t1658499504*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		Vector4_t2243707581  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3293920409_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558387 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558387 *>(__this + 1);
	return Enumerator_MoveNext_m3293920409(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::get_Current()
extern "C"  Vector4_t2243707581  Enumerator_get_Current_m2657372766_gshared (Enumerator_t1147558387 * __this, const MethodInfo* method)
{
	{
		Vector4_t2243707581  L_0 = (Vector4_t2243707581 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Vector4_t2243707581  Enumerator_get_Current_m2657372766_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558387 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558387 *>(__this + 1);
	return Enumerator_get_Current_m2657372766(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3017507881_gshared (Enumerator_t1914380850 * __this, List_1_t2379651176 * ___l0, const MethodInfo* method)
{
	{
		List_1_t2379651176 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t2379651176 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3017507881_AdjustorThunk (Il2CppObject * __this, List_1_t2379651176 * ___l0, const MethodInfo* method)
{
	Enumerator_t1914380850 * _thisAdjusted = reinterpret_cast<Enumerator_t1914380850 *>(__this + 1);
	Enumerator__ctor_m3017507881(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4109641409_gshared (Enumerator_t1914380850 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3417431579((Enumerator_t1914380850 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4109641409_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1914380850 * _thisAdjusted = reinterpret_cast<Enumerator_t1914380850 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m4109641409(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1231602997_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1231602997_gshared (Enumerator_t1914380850 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1231602997_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m3417431579((Enumerator_t1914380850 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_current_3();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1231602997_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1914380850 * _thisAdjusted = reinterpret_cast<Enumerator_t1914380850 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1231602997(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::Dispose()
extern "C"  void Enumerator_Dispose_m3141544672_gshared (Enumerator_t1914380850 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t2379651176 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3141544672_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1914380850 * _thisAdjusted = reinterpret_cast<Enumerator_t1914380850 *>(__this + 1);
	Enumerator_Dispose_m3141544672(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m3417431579_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3417431579_gshared (Enumerator_t1914380850 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3417431579_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t2379651176 * L_0 = (List_1_t2379651176 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1914380850  L_1 = (*(Enumerator_t1914380850 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t2379651176 * L_7 = (List_1_t2379651176 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3417431579_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1914380850 * _thisAdjusted = reinterpret_cast<Enumerator_t1914380850 *>(__this + 1);
	Enumerator_VerifyState_m3417431579(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2636851345_gshared (Enumerator_t1914380850 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m3417431579((Enumerator_t1914380850 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t2379651176 * L_2 = (List_1_t2379651176 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t2379651176 * L_4 = (List_1_t2379651176 *)__this->get_l_0();
		NullCheck(L_4);
		PIXEL_FORMATU5BU5D_t1421860245* L_5 = (PIXEL_FORMATU5BU5D_t1421860245*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2636851345_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1914380850 * _thisAdjusted = reinterpret_cast<Enumerator_t1914380850 *>(__this + 1);
	return Enumerator_MoveNext_m2636851345(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m107053352_gshared (Enumerator_t1914380850 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  int32_t Enumerator_get_Current_m107053352_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1914380850 * _thisAdjusted = reinterpret_cast<Enumerator_t1914380850 *>(__this + 1);
	return Enumerator_get_Current_m107053352(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1988137983_gshared (Enumerator_t862577312 * __this, List_1_t1327847638 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1327847638 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1327847638 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1988137983_AdjustorThunk (Il2CppObject * __this, List_1_t1327847638 * ___l0, const MethodInfo* method)
{
	Enumerator_t862577312 * _thisAdjusted = reinterpret_cast<Enumerator_t862577312 *>(__this + 1);
	Enumerator__ctor_m1988137983(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m588581539_gshared (Enumerator_t862577312 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3960339045((Enumerator_t862577312 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m588581539_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t862577312 * _thisAdjusted = reinterpret_cast<Enumerator_t862577312 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m588581539(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m4198962367_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4198962367_gshared (Enumerator_t862577312 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m4198962367_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_VerifyState_m3960339045((Enumerator_t862577312 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		TargetSearchResult_t1958726506  L_2 = (TargetSearchResult_t1958726506 )__this->get_current_3();
		TargetSearchResult_t1958726506  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4198962367_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t862577312 * _thisAdjusted = reinterpret_cast<Enumerator_t862577312 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m4198962367(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::Dispose()
extern "C"  void Enumerator_Dispose_m3651367106_gshared (Enumerator_t862577312 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1327847638 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3651367106_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t862577312 * _thisAdjusted = reinterpret_cast<Enumerator_t862577312 *>(__this + 1);
	Enumerator_Dispose_m3651367106(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m3960339045_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3960339045_gshared (Enumerator_t862577312 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3960339045_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1327847638 * L_0 = (List_1_t1327847638 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t862577312  L_1 = (*(Enumerator_t862577312 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1327847638 * L_7 = (List_1_t1327847638 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3960339045_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t862577312 * _thisAdjusted = reinterpret_cast<Enumerator_t862577312 *>(__this + 1);
	Enumerator_VerifyState_m3960339045(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3852517079_gshared (Enumerator_t862577312 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m3960339045((Enumerator_t862577312 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1327847638 * L_2 = (List_1_t1327847638 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1327847638 * L_4 = (List_1_t1327847638 *)__this->get_l_0();
		NullCheck(L_4);
		TargetSearchResultU5BU5D_t2085623727* L_5 = (TargetSearchResultU5BU5D_t2085623727*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		TargetSearchResult_t1958726506  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3852517079_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t862577312 * _thisAdjusted = reinterpret_cast<Enumerator_t862577312 *>(__this + 1);
	return Enumerator_MoveNext_m3852517079(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::get_Current()
extern "C"  TargetSearchResult_t1958726506  Enumerator_get_Current_m1704033956_gshared (Enumerator_t862577312 * __this, const MethodInfo* method)
{
	{
		TargetSearchResult_t1958726506  L_0 = (TargetSearchResult_t1958726506 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  TargetSearchResult_t1958726506  Enumerator_get_Current_m1704033956_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t862577312 * _thisAdjusted = reinterpret_cast<Enumerator_t862577312 *>(__this + 1);
	return Enumerator_get_Current_m1704033956(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.ctor()
extern "C"  void List_1__ctor_m2177611879_gshared (List_1_t43281120 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		IndexedColumnU5BU5D_t4219656765* L_0 = ((List_1_t43281120_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3989671355_gshared (List_1_t43281120 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t43281120 *)__this);
		((  void (*) (List_1_t43281120 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t43281120 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		IndexedColumnU5BU5D_t4219656765* L_3 = ((List_1_t43281120_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_3);
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t43281120 *)__this);
		((  void (*) (List_1_t43281120 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t43281120 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		goto IL_0049;
	}

IL_0031:
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_5);
		__this->set__items_1(((IndexedColumnU5BU5D_t4219656765*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_6)));
		Il2CppObject* L_7 = V_0;
		NullCheck((List_1_t43281120 *)__this);
		((  void (*) (List_1_t43281120 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t43281120 *)__this, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.ctor(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3404069002;
extern const uint32_t List_1__ctor_m4204710169_MetadataUsageId;
extern "C"  void List_1__ctor_m4204710169_gshared (List_1_t43281120 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m4204710169_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_1 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_1, (String_t*)_stringLiteral3404069002, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((IndexedColumnU5BU5D_t4219656765*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.cctor()
extern "C"  void List_1__cctor_m1995328883_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t43281120_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_4(((IndexedColumnU5BU5D_t4219656765*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2560244226_gshared (List_1_t43281120 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t43281120 *)__this);
		Enumerator_t3872978090  L_0 = ((  Enumerator_t3872978090  (*) (List_1_t43281120 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t43281120 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t3872978090  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m266102924_gshared (List_1_t43281120 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		IndexedColumnU5BU5D_t4219656765* L_0 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m2834930177_gshared (List_1_t43281120 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t43281120 *)__this);
		Enumerator_t3872978090  L_0 = ((  Enumerator_t3872978090  (*) (List_1_t43281120 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t43281120 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t3872978090  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3168545717;
extern const uint32_t List_1_System_Collections_IList_Add_m1439241948_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_Add_m1439241948_gshared (List_1_t43281120 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m1439241948_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t43281120 *)__this);
			((  void (*) (List_1_t43281120 *, IndexedColumn_t674159988 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t43281120 *)__this, (IndexedColumn_t674159988 )((*(IndexedColumn_t674159988 *)((IndexedColumn_t674159988 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, (String_t*)_stringLiteral3168545717, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Contains_m3729349938_MetadataUsageId;
extern "C"  bool List_1_System_Collections_IList_Contains_m3729349938_gshared (List_1_t43281120 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m3729349938_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t43281120 *)__this);
			bool L_1 = ((  bool (*) (List_1_t43281120 *, IndexedColumn_t674159988 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t43281120 *)__this, (IndexedColumn_t674159988 )((*(IndexedColumn_t674159988 *)((IndexedColumn_t674159988 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m3723295914_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3723295914_gshared (List_1_t43281120 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m3723295914_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t43281120 *)__this);
			int32_t L_1 = ((  int32_t (*) (List_1_t43281120 *, IndexedColumn_t674159988 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t43281120 *)__this, (IndexedColumn_t674159988 )((*(IndexedColumn_t674159988 *)((IndexedColumn_t674159988 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3168545717;
extern const uint32_t List_1_System_Collections_IList_Insert_m2594309881_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Insert_m2594309881_gshared (List_1_t43281120 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m2594309881_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t43281120 *)__this);
		((  void (*) (List_1_t43281120 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t43281120 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			Il2CppObject * L_2 = ___item1;
			NullCheck((List_1_t43281120 *)__this);
			((  void (*) (List_1_t43281120 *, int32_t, IndexedColumn_t674159988 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t43281120 *)__this, (int32_t)L_1, (IndexedColumn_t674159988 )((*(IndexedColumn_t674159988 *)((IndexedColumn_t674159988 *)UnBox (L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t3259014390 * L_3 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_3, (String_t*)_stringLiteral3168545717, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Remove_m3381705393_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Remove_m3381705393_gshared (List_1_t43281120 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m3381705393_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t43281120 *)__this);
			((  bool (*) (List_1_t43281120 *, IndexedColumn_t674159988 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t43281120 *)__this, (IndexedColumn_t674159988 )((*(IndexedColumn_t674159988 *)((IndexedColumn_t674159988 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3480933197_gshared (List_1_t43281120 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m1768514220_gshared (List_1_t43281120 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1464552284_gshared (List_1_t43281120 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m1070203109_gshared (List_1_t43281120 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m544361872_gshared (List_1_t43281120 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2588552949_gshared (List_1_t43281120 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t43281120 *)__this);
		IndexedColumn_t674159988  L_1 = ((  IndexedColumn_t674159988  (*) (List_1_t43281120 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t43281120 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		IndexedColumn_t674159988  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern const uint32_t List_1_System_Collections_IList_set_Item_m742815526_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_set_Item_m742815526_gshared (List_1_t43281120 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m742815526_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			Il2CppObject * L_1 = ___value1;
			NullCheck((List_1_t43281120 *)__this);
			((  void (*) (List_1_t43281120 *, int32_t, IndexedColumn_t674159988 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t43281120 *)__this, (int32_t)L_0, (IndexedColumn_t674159988 )((*(IndexedColumn_t674159988 *)((IndexedColumn_t674159988 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, (String_t*)_stringLiteral1803325615, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Add(T)
extern "C"  void List_1_Add_m1039228243_gshared (List_1_t43281120 * __this, IndexedColumn_t674159988  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		IndexedColumnU5BU5D_t4219656765* L_1 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t43281120 *)__this);
		((  void (*) (List_1_t43281120 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t43281120 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001a:
	{
		IndexedColumnU5BU5D_t4219656765* L_2 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		IndexedColumn_t674159988  L_6 = ___item0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (IndexedColumn_t674159988 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m529225910_gshared (List_1_t43281120 * __this, int32_t ___newCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		IndexedColumnU5BU5D_t4219656765* L_3 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t43281120 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t43281120 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((List_1_t43281120 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		int32_t L_5 = Math_Max_m2671311541(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m2671311541(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t43281120 *)__this);
		((  void (*) (List_1_t43281120 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t43281120 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m4021244222_gshared (List_1_t43281120 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t43281120 *)__this);
		((  void (*) (List_1_t43281120 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t43281120 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Il2CppObject* L_4 = ___collection0;
		IndexedColumnU5BU5D_t4219656765* L_5 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< IndexedColumnU5BU5D_t4219656765*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_4, (IndexedColumnU5BU5D_t4219656765*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t List_1_AddEnumerable_m543446830_MetadataUsageId;
extern "C"  void List_1_AddEnumerable_m543446830_gshared (List_1_t43281120 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m543446830_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IndexedColumn_t674159988  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___enumerable0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			IndexedColumn_t674159988  L_3 = InterfaceFuncInvoker0< IndexedColumn_t674159988  >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21), (Il2CppObject*)L_2);
			V_0 = (IndexedColumn_t674159988 )L_3;
			IndexedColumn_t674159988  L_4 = V_0;
			NullCheck((List_1_t43281120 *)__this);
			((  void (*) (List_1_t43281120 *, IndexedColumn_t674159988 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t43281120 *)__this, (IndexedColumn_t674159988 )L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		}

IL_001a:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m173034401_gshared (List_1_t43281120 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t43281120 *)__this);
		((  void (*) (List_1_t43281120 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t43281120 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((List_1_t43281120 *)__this);
		((  void (*) (List_1_t43281120 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t43281120 *)__this, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		goto IL_0027;
	}

IL_0020:
	{
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t43281120 *)__this);
		((  void (*) (List_1_t43281120 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t43281120 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t859945680 * List_1_AsReadOnly_m1881111912_gshared (List_1_t43281120 * __this, const MethodInfo* method)
{
	{
		ReadOnlyCollection_1_t859945680 * L_0 = (ReadOnlyCollection_1_t859945680 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22));
		((  void (*) (ReadOnlyCollection_1_t859945680 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23)->methodPointer)(L_0, (Il2CppObject*)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_0;
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Clear()
extern "C"  void List_1_Clear_m2591881969_gshared (List_1_t43281120 * __this, const MethodInfo* method)
{
	{
		IndexedColumnU5BU5D_t4219656765* L_0 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		IndexedColumnU5BU5D_t4219656765* L_1 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Contains(T)
extern "C"  bool List_1_Contains_m645061595_gshared (List_1_t43281120 * __this, IndexedColumn_t674159988  ___item0, const MethodInfo* method)
{
	{
		IndexedColumnU5BU5D_t4219656765* L_0 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		IndexedColumn_t674159988  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IndexedColumnU5BU5D_t4219656765*, IndexedColumn_t674159988 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)(NULL /*static, unused*/, (IndexedColumnU5BU5D_t4219656765*)L_0, (IndexedColumn_t674159988 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m38513116_gshared (List_1_t43281120 * __this, IndexedColumnU5BU5D_t4219656765* ___array0, const MethodInfo* method)
{
	{
		IndexedColumnU5BU5D_t4219656765* L_0 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		IndexedColumnU5BU5D_t4219656765* L_1 = ___array0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2904850141_gshared (List_1_t43281120 * __this, IndexedColumnU5BU5D_t4219656765* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		IndexedColumnU5BU5D_t4219656765* L_0 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		IndexedColumnU5BU5D_t4219656765* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// T System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Find(System.Predicate`1<T>)
extern Il2CppClass* IndexedColumn_t674159988_il2cpp_TypeInfo_var;
extern const uint32_t List_1_Find_m1987600831_MetadataUsageId;
extern "C"  IndexedColumn_t674159988  List_1_Find_m1987600831_gshared (List_1_t43281120 * __this, Predicate_1_t3412097399 * ___match0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_Find_m1987600831_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	IndexedColumn_t674159988  V_1;
	memset(&V_1, 0, sizeof(V_1));
	IndexedColumn_t674159988  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		Predicate_1_t3412097399 * L_0 = ___match0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3412097399 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)(NULL /*static, unused*/, (Predicate_1_t3412097399 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		int32_t L_1 = (int32_t)__this->get__size_2();
		Predicate_1_t3412097399 * L_2 = ___match0;
		NullCheck((List_1_t43281120 *)__this);
		int32_t L_3 = ((  int32_t (*) (List_1_t43281120 *, int32_t, int32_t, Predicate_1_t3412097399 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)((List_1_t43281120 *)__this, (int32_t)0, (int32_t)L_1, (Predicate_1_t3412097399 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_002d;
		}
	}
	{
		IndexedColumnU5BU5D_t4219656765* L_5 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		IndexedColumn_t674159988  L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		G_B3_0 = L_8;
		goto IL_0036;
	}

IL_002d:
	{
		Initobj (IndexedColumn_t674159988_il2cpp_TypeInfo_var, (&V_1));
		IndexedColumn_t674159988  L_9 = V_1;
		G_B3_0 = L_9;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::CheckMatch(System.Predicate`1<T>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3322341559;
extern const uint32_t List_1_CheckMatch_m688679932_MetadataUsageId;
extern "C"  void List_1_CheckMatch_m688679932_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t3412097399 * ___match0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckMatch_m688679932_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Predicate_1_t3412097399 * L_0 = ___match0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3322341559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3335490459_gshared (List_1_t43281120 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t3412097399 * ___match2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex0;
		int32_t L_1 = ___count1;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex0;
		V_1 = (int32_t)L_2;
		goto IL_0028;
	}

IL_000b:
	{
		Predicate_1_t3412097399 * L_3 = ___match2;
		IndexedColumnU5BU5D_t4219656765* L_4 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		IndexedColumn_t674159988  L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck((Predicate_1_t3412097399 *)L_3);
		bool L_8 = ((  bool (*) (Predicate_1_t3412097399 *, IndexedColumn_t674159988 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((Predicate_1_t3412097399 *)L_3, (IndexedColumn_t674159988 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		if (!L_8)
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0024:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000b;
		}
	}
	{
		return (-1);
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::GetEnumerator()
extern "C"  Enumerator_t3872978090  List_1_GetEnumerator_m3398976172_gshared (List_1_t43281120 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3872978090  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m3053486497(&L_0, (List_1_t43281120 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 28));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3435406113_gshared (List_1_t43281120 * __this, IndexedColumn_t674159988  ___item0, const MethodInfo* method)
{
	{
		IndexedColumnU5BU5D_t4219656765* L_0 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		IndexedColumn_t674159988  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, IndexedColumnU5BU5D_t4219656765*, IndexedColumn_t674159988 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)(NULL /*static, unused*/, (IndexedColumnU5BU5D_t4219656765*)L_0, (IndexedColumn_t674159988 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3451272920_gshared (List_1_t43281120 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		IndexedColumnU5BU5D_t4219656765* L_5 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		int32_t L_6 = ___start0;
		IndexedColumnU5BU5D_t4219656765* L_7 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		IndexedColumnU5BU5D_t4219656765* L_15 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::CheckIndex(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_CheckIndex_m398277233_MetadataUsageId;
extern "C"  void List_1_CheckIndex_m398277233_gshared (List_1_t43281120 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m398277233_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_3, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m448615326_gshared (List_1_t43281120 * __this, int32_t ___index0, IndexedColumn_t674159988  ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t43281120 *)__this);
		((  void (*) (List_1_t43281120 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t43281120 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = (int32_t)__this->get__size_2();
		IndexedColumnU5BU5D_t4219656765* L_2 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t43281120 *)__this);
		((  void (*) (List_1_t43281120 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t43281120 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t43281120 *)__this);
		((  void (*) (List_1_t43281120 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)((List_1_t43281120 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		IndexedColumnU5BU5D_t4219656765* L_4 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		int32_t L_5 = ___index0;
		IndexedColumn_t674159988  L_6 = ___item1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (IndexedColumn_t674159988 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1063029886;
extern const uint32_t List_1_CheckCollection_m2966359231_MetadataUsageId;
extern "C"  void List_1_CheckCollection_m2966359231_gshared (List_1_t43281120 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m2966359231_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral1063029886, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Remove(T)
extern "C"  bool List_1_Remove_m2030530920_gshared (List_1_t43281120 * __this, IndexedColumn_t674159988  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		IndexedColumn_t674159988  L_0 = ___item0;
		NullCheck((List_1_t43281120 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t43281120 *, IndexedColumn_t674159988 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t43281120 *)__this, (IndexedColumn_t674159988 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t43281120 *)__this);
		((  void (*) (List_1_t43281120 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((List_1_t43281120 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m37964820_gshared (List_1_t43281120 * __this, Predicate_1_t3412097399 * ___match0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Predicate_1_t3412097399 * L_0 = ___match0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3412097399 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)(NULL /*static, unused*/, (Predicate_1_t3412097399 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		V_0 = (int32_t)0;
		V_1 = (int32_t)0;
		V_0 = (int32_t)0;
		goto IL_0031;
	}

IL_0011:
	{
		Predicate_1_t3412097399 * L_1 = ___match0;
		IndexedColumnU5BU5D_t4219656765* L_2 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		IndexedColumn_t674159988  L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck((Predicate_1_t3412097399 *)L_1);
		bool L_6 = ((  bool (*) (Predicate_1_t3412097399 *, IndexedColumn_t674159988 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((Predicate_1_t3412097399 *)L_1, (IndexedColumn_t674159988 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		if (!L_6)
		{
			goto IL_002d;
		}
	}
	{
		goto IL_003d;
	}

IL_002d:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0011;
		}
	}

IL_003d:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_004b;
		}
	}
	{
		return 0;
	}

IL_004b:
	{
		int32_t L_12 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = V_0;
		V_1 = (int32_t)((int32_t)((int32_t)L_13+(int32_t)1));
		goto IL_0099;
	}

IL_0062:
	{
		Predicate_1_t3412097399 * L_14 = ___match0;
		IndexedColumnU5BU5D_t4219656765* L_15 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		IndexedColumn_t674159988  L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		NullCheck((Predicate_1_t3412097399 *)L_14);
		bool L_19 = ((  bool (*) (Predicate_1_t3412097399 *, IndexedColumn_t674159988 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((Predicate_1_t3412097399 *)L_14, (IndexedColumn_t674159988 )L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		if (L_19)
		{
			goto IL_0095;
		}
	}
	{
		IndexedColumnU5BU5D_t4219656765* L_20 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		int32_t L_21 = V_0;
		int32_t L_22 = (int32_t)L_21;
		V_0 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
		IndexedColumnU5BU5D_t4219656765* L_23 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		int32_t L_24 = V_1;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		IndexedColumn_t674159988  L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (IndexedColumn_t674159988 )L_26);
	}

IL_0095:
	{
		int32_t L_27 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_0099:
	{
		int32_t L_28 = V_1;
		int32_t L_29 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_30 = V_1;
		int32_t L_31 = V_0;
		if ((((int32_t)((int32_t)((int32_t)L_30-(int32_t)L_31))) <= ((int32_t)0)))
		{
			goto IL_00bd;
		}
	}
	{
		IndexedColumnU5BU5D_t4219656765* L_32 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		int32_t L_33 = V_0;
		int32_t L_34 = V_1;
		int32_t L_35 = V_0;
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_32, (int32_t)L_33, (int32_t)((int32_t)((int32_t)L_34-(int32_t)L_35)), /*hidden argument*/NULL);
	}

IL_00bd:
	{
		int32_t L_36 = V_0;
		__this->set__size_2(L_36);
		int32_t L_37 = V_1;
		int32_t L_38 = V_0;
		return ((int32_t)((int32_t)L_37-(int32_t)L_38));
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::RemoveAt(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_RemoveAt_m4120205290_MetadataUsageId;
extern "C"  void List_1_RemoveAt_m4120205290_gshared (List_1_t43281120 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m4120205290_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_3, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t43281120 *)__this);
		((  void (*) (List_1_t43281120 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)((List_1_t43281120 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		IndexedColumnU5BU5D_t4219656765* L_5 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Reverse()
extern "C"  void List_1_Reverse_m2520075922_gshared (List_1_t43281120 * __this, const MethodInfo* method)
{
	{
		IndexedColumnU5BU5D_t4219656765* L_0 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Array_Reverse_m3433347928(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)L_1, /*hidden argument*/NULL);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Sort()
extern "C"  void List_1_Sort_m1662150106_gshared (List_1_t43281120 * __this, const MethodInfo* method)
{
	{
		IndexedColumnU5BU5D_t4219656765* L_0 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 32));
		Comparer_1_t3859136403 * L_2 = ((  Comparer_1_t3859136403 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31));
		((  void (*) (Il2CppObject * /* static, unused */, IndexedColumnU5BU5D_t4219656765*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33)->methodPointer)(NULL /*static, unused*/, (IndexedColumnU5BU5D_t4219656765*)L_0, (int32_t)0, (int32_t)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1527224475_gshared (List_1_t43281120 * __this, Comparison_1_t1935898839 * ___comparison0, const MethodInfo* method)
{
	{
		IndexedColumnU5BU5D_t4219656765* L_0 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Comparison_1_t1935898839 * L_2 = ___comparison0;
		((  void (*) (Il2CppObject * /* static, unused */, IndexedColumnU5BU5D_t4219656765*, int32_t, Comparison_1_t1935898839 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34)->methodPointer)(NULL /*static, unused*/, (IndexedColumnU5BU5D_t4219656765*)L_0, (int32_t)L_1, (Comparison_1_t1935898839 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::ToArray()
extern "C"  IndexedColumnU5BU5D_t4219656765* List_1_ToArray_m1726840235_gshared (List_1_t43281120 * __this, const MethodInfo* method)
{
	IndexedColumnU5BU5D_t4219656765* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (IndexedColumnU5BU5D_t4219656765*)((IndexedColumnU5BU5D_t4219656765*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_0));
		IndexedColumnU5BU5D_t4219656765* L_1 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		IndexedColumnU5BU5D_t4219656765* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2363740072(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		IndexedColumnU5BU5D_t4219656765* L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2532032509_gshared (List_1_t43281120 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		NullCheck((List_1_t43281120 *)__this);
		((  void (*) (List_1_t43281120 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t43281120 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2338338523_gshared (List_1_t43281120 * __this, const MethodInfo* method)
{
	{
		IndexedColumnU5BU5D_t4219656765* L_0 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::set_Capacity(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern const uint32_t List_1_set_Capacity_m3871896830_MetadataUsageId;
extern "C"  void List_1_set_Capacity_m3871896830_gshared (List_1_t43281120 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m3871896830_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m15523695(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		IndexedColumnU5BU5D_t4219656765** L_3 = (IndexedColumnU5BU5D_t4219656765**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, IndexedColumnU5BU5D_t4219656765**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 35)->methodPointer)(NULL /*static, unused*/, (IndexedColumnU5BU5D_t4219656765**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 35));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::get_Count()
extern "C"  int32_t List_1_get_Count_m1991288812_gshared (List_1_t43281120 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_get_Item_m537033892_MetadataUsageId;
extern "C"  IndexedColumn_t674159988  List_1_get_Item_m537033892_gshared (List_1_t43281120 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m537033892_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		IndexedColumnU5BU5D_t4219656765* L_3 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		IndexedColumn_t674159988  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::set_Item(System.Int32,T)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_set_Item_m666324973_MetadataUsageId;
extern "C"  void List_1_set_Item_m666324973_gshared (List_1_t43281120 * __this, int32_t ___index0, IndexedColumn_t674159988  ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m666324973_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t43281120 *)__this);
		((  void (*) (List_1_t43281120 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t43281120 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_3, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		IndexedColumnU5BU5D_t4219656765* L_4 = (IndexedColumnU5BU5D_t4219656765*)__this->get__items_1();
		int32_t L_5 = ___index0;
		IndexedColumn_t674159988  L_6 = ___value1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (IndexedColumn_t674159988 )L_6);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::.ctor()
extern "C"  void List_1__ctor_m4049563743_gshared (List_1_t3052225568 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		ByteU5BU5D_t3397334013* L_0 = ((List_1_t3052225568_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m2080105584_gshared (List_1_t3052225568 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t3052225568 *)__this);
		((  void (*) (List_1_t3052225568 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t3052225568 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		ByteU5BU5D_t3397334013* L_3 = ((List_1_t3052225568_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_3);
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t3052225568 *)__this);
		((  void (*) (List_1_t3052225568 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t3052225568 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		goto IL_0049;
	}

IL_0031:
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Byte>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_5);
		__this->set__items_1(((ByteU5BU5D_t3397334013*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_6)));
		Il2CppObject* L_7 = V_0;
		NullCheck((List_1_t3052225568 *)__this);
		((  void (*) (List_1_t3052225568 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t3052225568 *)__this, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::.ctor(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3404069002;
extern const uint32_t List_1__ctor_m2367726798_MetadataUsageId;
extern "C"  void List_1__ctor_m2367726798_gshared (List_1_t3052225568 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m2367726798_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_1 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_1, (String_t*)_stringLiteral3404069002, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((ByteU5BU5D_t3397334013*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::.cctor()
extern "C"  void List_1__cctor_m2358667882_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t3052225568_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_4(((ByteU5BU5D_t3397334013*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Byte>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m533999703_gshared (List_1_t3052225568 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t3052225568 *)__this);
		Enumerator_t2586955242  L_0 = ((  Enumerator_t2586955242  (*) (List_1_t3052225568 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3052225568 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t2586955242  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m2457555843_gshared (List_1_t3052225568 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Byte>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m2803524680_gshared (List_1_t3052225568 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t3052225568 *)__this);
		Enumerator_t2586955242  L_0 = ((  Enumerator_t2586955242  (*) (List_1_t3052225568 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3052225568 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t2586955242  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3168545717;
extern const uint32_t List_1_System_Collections_IList_Add_m4125826051_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_Add_m4125826051_gshared (List_1_t3052225568 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m4125826051_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t3052225568 *)__this);
			((  void (*) (List_1_t3052225568 *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t3052225568 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, (String_t*)_stringLiteral3168545717, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Contains_m917118923_MetadataUsageId;
extern "C"  bool List_1_System_Collections_IList_Contains_m917118923_gshared (List_1_t3052225568 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m917118923_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t3052225568 *)__this);
			bool L_1 = ((  bool (*) (List_1_t3052225568 *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t3052225568 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m2885537393_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m2885537393_gshared (List_1_t3052225568 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m2885537393_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t3052225568 *)__this);
			int32_t L_1 = ((  int32_t (*) (List_1_t3052225568 *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t3052225568 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3168545717;
extern const uint32_t List_1_System_Collections_IList_Insert_m16123522_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Insert_m16123522_gshared (List_1_t3052225568 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m16123522_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t3052225568 *)__this);
		((  void (*) (List_1_t3052225568 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t3052225568 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			Il2CppObject * L_2 = ___item1;
			NullCheck((List_1_t3052225568 *)__this);
			((  void (*) (List_1_t3052225568 *, int32_t, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t3052225568 *)__this, (int32_t)L_1, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t3259014390 * L_3 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_3, (String_t*)_stringLiteral3168545717, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Remove_m3098324092_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Remove_m3098324092_gshared (List_1_t3052225568 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m3098324092_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t3052225568 *)__this);
			((  bool (*) (List_1_t3052225568 *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t3052225568 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Byte>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3807385908_gshared (List_1_t3052225568 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Byte>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m913761043_gshared (List_1_t3052225568 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Byte>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3259322579_gshared (List_1_t3052225568 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3623555262_gshared (List_1_t3052225568 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m3477162999_gshared (List_1_t3052225568 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m663115306_gshared (List_1_t3052225568 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t3052225568 *)__this);
		uint8_t L_1 = ((  uint8_t (*) (List_1_t3052225568 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t3052225568 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		uint8_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern const uint32_t List_1_System_Collections_IList_set_Item_m1363419121_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_set_Item_m1363419121_gshared (List_1_t3052225568 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m1363419121_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			Il2CppObject * L_1 = ___value1;
			NullCheck((List_1_t3052225568 *)__this);
			((  void (*) (List_1_t3052225568 *, int32_t, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t3052225568 *)__this, (int32_t)L_0, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, (String_t*)_stringLiteral1803325615, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::Add(T)
extern "C"  void List_1_Add_m3822574_gshared (List_1_t3052225568 * __this, uint8_t ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		ByteU5BU5D_t3397334013* L_1 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t3052225568 *)__this);
		((  void (*) (List_1_t3052225568 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t3052225568 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001a:
	{
		ByteU5BU5D_t3397334013* L_2 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		uint8_t L_6 = ___item0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (uint8_t)L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1453529343_gshared (List_1_t3052225568 * __this, int32_t ___newCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		ByteU5BU5D_t3397334013* L_3 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t3052225568 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t3052225568 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((List_1_t3052225568 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		int32_t L_5 = Math_Max_m2671311541(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m2671311541(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t3052225568 *)__this);
		((  void (*) (List_1_t3052225568 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t3052225568 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m1772309383_gshared (List_1_t3052225568 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Byte>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t3052225568 *)__this);
		((  void (*) (List_1_t3052225568 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t3052225568 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Il2CppObject* L_4 = ___collection0;
		ByteU5BU5D_t3397334013* L_5 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< ByteU5BU5D_t3397334013*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Byte>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_4, (ByteU5BU5D_t3397334013*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t List_1_AddEnumerable_m1914753399_MetadataUsageId;
extern "C"  void List_1_AddEnumerable_m1914753399_gshared (List_1_t3052225568 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m1914753399_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint8_t V_0 = 0x0;
	Il2CppObject* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___enumerable0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Byte>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			uint8_t L_3 = InterfaceFuncInvoker0< uint8_t >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Byte>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21), (Il2CppObject*)L_2);
			V_0 = (uint8_t)L_3;
			uint8_t L_4 = V_0;
			NullCheck((List_1_t3052225568 *)__this);
			((  void (*) (List_1_t3052225568 *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t3052225568 *)__this, (uint8_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		}

IL_001a:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3872295241_gshared (List_1_t3052225568 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t3052225568 *)__this);
		((  void (*) (List_1_t3052225568 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t3052225568 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((List_1_t3052225568 *)__this);
		((  void (*) (List_1_t3052225568 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t3052225568 *)__this, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		goto IL_0027;
	}

IL_0020:
	{
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t3052225568 *)__this);
		((  void (*) (List_1_t3052225568 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t3052225568 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Byte>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t3868890128 * List_1_AsReadOnly_m2086543759_gshared (List_1_t3052225568 * __this, const MethodInfo* method)
{
	{
		ReadOnlyCollection_1_t3868890128 * L_0 = (ReadOnlyCollection_1_t3868890128 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22));
		((  void (*) (ReadOnlyCollection_1_t3868890128 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23)->methodPointer)(L_0, (Il2CppObject*)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_0;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::Clear()
extern "C"  void List_1_Clear_m3849438586_gshared (List_1_t3052225568 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		ByteU5BU5D_t3397334013* L_1 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Byte>::Contains(T)
extern "C"  bool List_1_Contains_m3760597204_gshared (List_1_t3052225568 * __this, uint8_t ___item0, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		uint8_t L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t3397334013*, uint8_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)(NULL /*static, unused*/, (ByteU5BU5D_t3397334013*)L_0, (uint8_t)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m2258427655_gshared (List_1_t3052225568 * __this, ByteU5BU5D_t3397334013* ___array0, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		ByteU5BU5D_t3397334013* L_1 = ___array0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2416550002_gshared (List_1_t3052225568 * __this, ByteU5BU5D_t3397334013* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		ByteU5BU5D_t3397334013* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// T System.Collections.Generic.List`1<System.Byte>::Find(System.Predicate`1<T>)
extern Il2CppClass* Byte_t3683104436_il2cpp_TypeInfo_var;
extern const uint32_t List_1_Find_m4275251498_MetadataUsageId;
extern "C"  uint8_t List_1_Find_m4275251498_gshared (List_1_t3052225568 * __this, Predicate_1_t2126074551 * ___match0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_Find_m4275251498_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	uint8_t V_1 = 0x0;
	uint8_t G_B3_0 = 0x0;
	{
		Predicate_1_t2126074551 * L_0 = ___match0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2126074551 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)(NULL /*static, unused*/, (Predicate_1_t2126074551 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		int32_t L_1 = (int32_t)__this->get__size_2();
		Predicate_1_t2126074551 * L_2 = ___match0;
		NullCheck((List_1_t3052225568 *)__this);
		int32_t L_3 = ((  int32_t (*) (List_1_t3052225568 *, int32_t, int32_t, Predicate_1_t2126074551 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)((List_1_t3052225568 *)__this, (int32_t)0, (int32_t)L_1, (Predicate_1_t2126074551 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_002d;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_5 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		uint8_t L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		G_B3_0 = L_8;
		goto IL_0036;
	}

IL_002d:
	{
		Initobj (Byte_t3683104436_il2cpp_TypeInfo_var, (&V_1));
		uint8_t L_9 = V_1;
		G_B3_0 = L_9;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::CheckMatch(System.Predicate`1<T>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3322341559;
extern const uint32_t List_1_CheckMatch_m905797491_MetadataUsageId;
extern "C"  void List_1_CheckMatch_m905797491_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2126074551 * ___match0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckMatch_m905797491_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Predicate_1_t2126074551 * L_0 = ___match0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3322341559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Byte>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m2959371234_gshared (List_1_t3052225568 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2126074551 * ___match2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex0;
		int32_t L_1 = ___count1;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex0;
		V_1 = (int32_t)L_2;
		goto IL_0028;
	}

IL_000b:
	{
		Predicate_1_t2126074551 * L_3 = ___match2;
		ByteU5BU5D_t3397334013* L_4 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		uint8_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck((Predicate_1_t2126074551 *)L_3);
		bool L_8 = ((  bool (*) (Predicate_1_t2126074551 *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((Predicate_1_t2126074551 *)L_3, (uint8_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		if (!L_8)
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0024:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000b;
		}
	}
	{
		return (-1);
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Byte>::GetEnumerator()
extern "C"  Enumerator_t2586955242  List_1_GetEnumerator_m1910663159_gshared (List_1_t3052225568 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2586955242  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m3029487006(&L_0, (List_1_t3052225568 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 28));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Byte>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2191428772_gshared (List_1_t3052225568 * __this, uint8_t ___item0, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		uint8_t L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t3397334013*, uint8_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)(NULL /*static, unused*/, (ByteU5BU5D_t3397334013*)L_0, (uint8_t)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m950637711_gshared (List_1_t3052225568 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_5 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		int32_t L_6 = ___start0;
		ByteU5BU5D_t3397334013* L_7 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_15 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::CheckIndex(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_CheckIndex_m622843962_MetadataUsageId;
extern "C"  void List_1_CheckIndex_m622843962_gshared (List_1_t3052225568 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m622843962_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_3, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m948653589_gshared (List_1_t3052225568 * __this, int32_t ___index0, uint8_t ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t3052225568 *)__this);
		((  void (*) (List_1_t3052225568 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t3052225568 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = (int32_t)__this->get__size_2();
		ByteU5BU5D_t3397334013* L_2 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t3052225568 *)__this);
		((  void (*) (List_1_t3052225568 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t3052225568 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t3052225568 *)__this);
		((  void (*) (List_1_t3052225568 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)((List_1_t3052225568 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		ByteU5BU5D_t3397334013* L_4 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		int32_t L_5 = ___index0;
		uint8_t L_6 = ___item1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (uint8_t)L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1063029886;
extern const uint32_t List_1_CheckCollection_m4083988744_MetadataUsageId;
extern "C"  void List_1_CheckCollection_m4083988744_gshared (List_1_t3052225568 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m4083988744_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral1063029886, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Byte>::Remove(T)
extern "C"  bool List_1_Remove_m2059743237_gshared (List_1_t3052225568 * __this, uint8_t ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		uint8_t L_0 = ___item0;
		NullCheck((List_1_t3052225568 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t3052225568 *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t3052225568 *)__this, (uint8_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t3052225568 *)__this);
		((  void (*) (List_1_t3052225568 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((List_1_t3052225568 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Byte>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m2367946715_gshared (List_1_t3052225568 * __this, Predicate_1_t2126074551 * ___match0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Predicate_1_t2126074551 * L_0 = ___match0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2126074551 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)(NULL /*static, unused*/, (Predicate_1_t2126074551 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		V_0 = (int32_t)0;
		V_1 = (int32_t)0;
		V_0 = (int32_t)0;
		goto IL_0031;
	}

IL_0011:
	{
		Predicate_1_t2126074551 * L_1 = ___match0;
		ByteU5BU5D_t3397334013* L_2 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		uint8_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck((Predicate_1_t2126074551 *)L_1);
		bool L_6 = ((  bool (*) (Predicate_1_t2126074551 *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((Predicate_1_t2126074551 *)L_1, (uint8_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		if (!L_6)
		{
			goto IL_002d;
		}
	}
	{
		goto IL_003d;
	}

IL_002d:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0011;
		}
	}

IL_003d:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_004b;
		}
	}
	{
		return 0;
	}

IL_004b:
	{
		int32_t L_12 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = V_0;
		V_1 = (int32_t)((int32_t)((int32_t)L_13+(int32_t)1));
		goto IL_0099;
	}

IL_0062:
	{
		Predicate_1_t2126074551 * L_14 = ___match0;
		ByteU5BU5D_t3397334013* L_15 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		uint8_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		NullCheck((Predicate_1_t2126074551 *)L_14);
		bool L_19 = ((  bool (*) (Predicate_1_t2126074551 *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((Predicate_1_t2126074551 *)L_14, (uint8_t)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		if (L_19)
		{
			goto IL_0095;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_20 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		int32_t L_21 = V_0;
		int32_t L_22 = (int32_t)L_21;
		V_0 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
		ByteU5BU5D_t3397334013* L_23 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		int32_t L_24 = V_1;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		uint8_t L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (uint8_t)L_26);
	}

IL_0095:
	{
		int32_t L_27 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_0099:
	{
		int32_t L_28 = V_1;
		int32_t L_29 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_30 = V_1;
		int32_t L_31 = V_0;
		if ((((int32_t)((int32_t)((int32_t)L_30-(int32_t)L_31))) <= ((int32_t)0)))
		{
			goto IL_00bd;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_32 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		int32_t L_33 = V_0;
		int32_t L_34 = V_1;
		int32_t L_35 = V_0;
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_32, (int32_t)L_33, (int32_t)((int32_t)((int32_t)L_34-(int32_t)L_35)), /*hidden argument*/NULL);
	}

IL_00bd:
	{
		int32_t L_36 = V_0;
		__this->set__size_2(L_36);
		int32_t L_37 = V_1;
		int32_t L_38 = V_0;
		return ((int32_t)((int32_t)L_37-(int32_t)L_38));
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::RemoveAt(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_RemoveAt_m207713089_MetadataUsageId;
extern "C"  void List_1_RemoveAt_m207713089_gshared (List_1_t3052225568 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m207713089_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_3, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t3052225568 *)__this);
		((  void (*) (List_1_t3052225568 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)((List_1_t3052225568 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		ByteU5BU5D_t3397334013* L_5 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::Reverse()
extern "C"  void List_1_Reverse_m3197022535_gshared (List_1_t3052225568 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Array_Reverse_m3433347928(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)L_1, /*hidden argument*/NULL);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::Sort()
extern "C"  void List_1_Sort_m437946789_gshared (List_1_t3052225568 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 32));
		Comparer_1_t2573113555 * L_2 = ((  Comparer_1_t2573113555 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31));
		((  void (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t3397334013*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33)->methodPointer)(NULL /*static, unused*/, (ByteU5BU5D_t3397334013*)L_0, (int32_t)0, (int32_t)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m3019961298_gshared (List_1_t3052225568 * __this, Comparison_1_t649875991 * ___comparison0, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Comparison_1_t649875991 * L_2 = ___comparison0;
		((  void (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t3397334013*, int32_t, Comparison_1_t649875991 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34)->methodPointer)(NULL /*static, unused*/, (ByteU5BU5D_t3397334013*)L_0, (int32_t)L_1, (Comparison_1_t649875991 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<System.Byte>::ToArray()
extern "C"  ByteU5BU5D_t3397334013* List_1_ToArray_m2815648607_gshared (List_1_t3052225568 * __this, const MethodInfo* method)
{
	ByteU5BU5D_t3397334013* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (ByteU5BU5D_t3397334013*)((ByteU5BU5D_t3397334013*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_0));
		ByteU5BU5D_t3397334013* L_1 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		ByteU5BU5D_t3397334013* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2363740072(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::TrimExcess()
extern "C"  void List_1_TrimExcess_m505787576_gshared (List_1_t3052225568 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		NullCheck((List_1_t3052225568 *)__this);
		((  void (*) (List_1_t3052225568 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t3052225568 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Byte>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m864653630_gshared (List_1_t3052225568 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::set_Capacity(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern const uint32_t List_1_set_Capacity_m2542608647_MetadataUsageId;
extern "C"  void List_1_set_Capacity_m2542608647_gshared (List_1_t3052225568 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m2542608647_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m15523695(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		ByteU5BU5D_t3397334013** L_3 = (ByteU5BU5D_t3397334013**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t3397334013**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 35)->methodPointer)(NULL /*static, unused*/, (ByteU5BU5D_t3397334013**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 35));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Byte>::get_Count()
extern "C"  int32_t List_1_get_Count_m280349363_gshared (List_1_t3052225568 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<System.Byte>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_get_Item_m2565966317_MetadataUsageId;
extern "C"  uint8_t List_1_get_Item_m2565966317_gshared (List_1_t3052225568 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m2565966317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		ByteU5BU5D_t3397334013* L_3 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		uint8_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Collections.Generic.List`1<System.Byte>::set_Item(System.Int32,T)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_set_Item_m4142971320_MetadataUsageId;
extern "C"  void List_1_set_Item_m4142971320_gshared (List_1_t3052225568 * __this, int32_t ___index0, uint8_t ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m4142971320_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t3052225568 *)__this);
		((  void (*) (List_1_t3052225568 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t3052225568 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_3, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		ByteU5BU5D_t3397334013* L_4 = (ByteU5BU5D_t3397334013*)__this->get__items_1();
		int32_t L_5 = ___index0;
		uint8_t L_6 = ___value1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (uint8_t)L_6);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
