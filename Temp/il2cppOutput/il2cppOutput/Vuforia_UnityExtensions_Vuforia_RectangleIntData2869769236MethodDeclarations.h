﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.RectangleIntData
struct RectangleIntData_t2869769236;
struct RectangleIntData_t2869769236_marshaled_pinvoke;
struct RectangleIntData_t2869769236_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct RectangleIntData_t2869769236;
struct RectangleIntData_t2869769236_marshaled_pinvoke;

extern "C" void RectangleIntData_t2869769236_marshal_pinvoke(const RectangleIntData_t2869769236& unmarshaled, RectangleIntData_t2869769236_marshaled_pinvoke& marshaled);
extern "C" void RectangleIntData_t2869769236_marshal_pinvoke_back(const RectangleIntData_t2869769236_marshaled_pinvoke& marshaled, RectangleIntData_t2869769236& unmarshaled);
extern "C" void RectangleIntData_t2869769236_marshal_pinvoke_cleanup(RectangleIntData_t2869769236_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct RectangleIntData_t2869769236;
struct RectangleIntData_t2869769236_marshaled_com;

extern "C" void RectangleIntData_t2869769236_marshal_com(const RectangleIntData_t2869769236& unmarshaled, RectangleIntData_t2869769236_marshaled_com& marshaled);
extern "C" void RectangleIntData_t2869769236_marshal_com_back(const RectangleIntData_t2869769236_marshaled_com& marshaled, RectangleIntData_t2869769236& unmarshaled);
extern "C" void RectangleIntData_t2869769236_marshal_com_cleanup(RectangleIntData_t2869769236_marshaled_com& marshaled);
