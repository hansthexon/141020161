﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Expressions.Expression
struct Expression_t114864668;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.ParameterInfo
struct ParameterInfo_t2249040075;
// System.Linq.Expressions.BinaryExpression
struct BinaryExpression_t2159924157;
// System.Linq.Expressions.ConstantExpression
struct ConstantExpression_t305952364;
// System.Object
struct Il2CppObject;
// System.Linq.Expressions.MemberExpression
struct MemberExpression_t1790982958;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.ParameterExpression>
struct ReadOnlyCollection_1_t3201290647;
// System.Reflection.MethodBase
struct MethodBase_t904190842;
// System.Linq.Expressions.ParameterExpression
struct ParameterExpression_t3015504955;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Linq.Expressions.UnaryExpression
struct UnaryExpression_t4253534555;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Linq_Expressions_ExpressionType1567188220.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Reflection_ParameterInfo2249040075.h"
#include "System_Core_System_Linq_Expressions_Expression114864668.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_FieldInfo255040150.h"
#include "mscorlib_System_Reflection_MethodBase904190842.h"

// System.Void System.Linq.Expressions.Expression::.ctor(System.Linq.Expressions.ExpressionType,System.Type)
extern "C"  void Expression__ctor_m2709855914 (Expression_t114864668 * __this, int32_t ___node_type0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Linq.Expressions.ExpressionType System.Linq.Expressions.Expression::get_NodeType()
extern "C"  int32_t Expression_get_NodeType_m950578925 (Expression_t114864668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Linq.Expressions.Expression::get_Type()
extern "C"  Type_t * Expression_get_Type_m1554952480 (Expression_t114864668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Linq.Expressions.Expression::ToString()
extern "C"  String_t* Expression_ToString_m1198926046 (Expression_t114864668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Linq.Expressions.Expression::GetUnaryOperator(System.String,System.Type,System.Type,System.Type)
extern "C"  MethodInfo_t * Expression_GetUnaryOperator_m2357867931 (Il2CppObject * __this /* static, unused */, String_t* ___oper_name0, Type_t * ___declaring1, Type_t * ___param2, Type_t * ___ret3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Linq.Expressions.Expression::GetTrueOperator(System.Type)
extern "C"  MethodInfo_t * Expression_GetTrueOperator_m3800030614 (Il2CppObject * __this /* static, unused */, Type_t * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Linq.Expressions.Expression::GetFalseOperator(System.Type)
extern "C"  MethodInfo_t * Expression_GetFalseOperator_m3847547021 (Il2CppObject * __this /* static, unused */, Type_t * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Linq.Expressions.Expression::GetBooleanOperator(System.String,System.Type)
extern "C"  MethodInfo_t * Expression_GetBooleanOperator_m3172083622 (Il2CppObject * __this /* static, unused */, String_t* ___op0, Type_t * ___self1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Linq.Expressions.Expression::IsAssignableToParameterType(System.Type,System.Reflection.ParameterInfo)
extern "C"  bool Expression_IsAssignableToParameterType_m4156514816 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, ParameterInfo_t2249040075 * ___param1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Linq.Expressions.Expression::GetBinaryOperator(System.String,System.Type,System.Linq.Expressions.Expression,System.Linq.Expressions.Expression)
extern "C"  MethodInfo_t * Expression_GetBinaryOperator_m2285471793 (Il2CppObject * __this /* static, unused */, String_t* ___oper_name0, Type_t * ___on_type1, Expression_t114864668 * ___left2, Expression_t114864668 * ___right3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Linq.Expressions.Expression::BinaryCoreCheck(System.String,System.Linq.Expressions.Expression,System.Linq.Expressions.Expression,System.Reflection.MethodInfo)
extern "C"  MethodInfo_t * Expression_BinaryCoreCheck_m2633005174 (Il2CppObject * __this /* static, unused */, String_t* ___oper_name0, Expression_t114864668 * ___left1, Expression_t114864668 * ___right2, MethodInfo_t * ___method3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Linq.Expressions.Expression::IsAssignableToOperatorParameter(System.Linq.Expressions.Expression,System.Reflection.ParameterInfo)
extern "C"  bool Expression_IsAssignableToOperatorParameter_m3014252017 (Il2CppObject * __this /* static, unused */, Expression_t114864668 * ___expression0, ParameterInfo_t2249040075 * ___parameter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Linq.Expressions.BinaryExpression System.Linq.Expressions.Expression::MakeBoolBinary(System.Linq.Expressions.ExpressionType,System.Linq.Expressions.Expression,System.Linq.Expressions.Expression,System.Boolean,System.Reflection.MethodInfo)
extern "C"  BinaryExpression_t2159924157 * Expression_MakeBoolBinary_m3009009886 (Il2CppObject * __this /* static, unused */, int32_t ___et0, Expression_t114864668 * ___left1, Expression_t114864668 * ___right2, bool ___liftToNull3, MethodInfo_t * ___method4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Linq.Expressions.BinaryExpression System.Linq.Expressions.Expression::AndAlso(System.Linq.Expressions.Expression,System.Linq.Expressions.Expression)
extern "C"  BinaryExpression_t2159924157 * Expression_AndAlso_m86695741 (Il2CppObject * __this /* static, unused */, Expression_t114864668 * ___left0, Expression_t114864668 * ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Linq.Expressions.BinaryExpression System.Linq.Expressions.Expression::AndAlso(System.Linq.Expressions.Expression,System.Linq.Expressions.Expression,System.Reflection.MethodInfo)
extern "C"  BinaryExpression_t2159924157 * Expression_AndAlso_m3521428690 (Il2CppObject * __this /* static, unused */, Expression_t114864668 * ___left0, Expression_t114864668 * ___right1, MethodInfo_t * ___method2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Linq.Expressions.Expression::ConditionalBinaryCheck(System.String,System.Linq.Expressions.Expression,System.Linq.Expressions.Expression,System.Reflection.MethodInfo)
extern "C"  MethodInfo_t * Expression_ConditionalBinaryCheck_m1930146735 (Il2CppObject * __this /* static, unused */, String_t* ___oper0, Expression_t114864668 * ___left1, Expression_t114864668 * ___right2, MethodInfo_t * ___method3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Linq.Expressions.BinaryExpression System.Linq.Expressions.Expression::Equal(System.Linq.Expressions.Expression,System.Linq.Expressions.Expression,System.Boolean,System.Reflection.MethodInfo)
extern "C"  BinaryExpression_t2159924157 * Expression_Equal_m821187187 (Il2CppObject * __this /* static, unused */, Expression_t114864668 * ___left0, Expression_t114864668 * ___right1, bool ___liftToNull2, MethodInfo_t * ___method3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Linq.Expressions.ConstantExpression System.Linq.Expressions.Expression::Constant(System.Object)
extern "C"  ConstantExpression_t305952364 * Expression_Constant_m3753058682 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Linq.Expressions.ConstantExpression System.Linq.Expressions.Expression::Constant(System.Object,System.Type)
extern "C"  ConstantExpression_t305952364 * Expression_Constant_m1993206821 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Linq.Expressions.MemberExpression System.Linq.Expressions.Expression::Field(System.Linq.Expressions.Expression,System.Reflection.FieldInfo)
extern "C"  MemberExpression_t1790982958 * Expression_Field_m1177296838 (Il2CppObject * __this /* static, unused */, Expression_t114864668 * ___expression0, FieldInfo_t * ___field1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Linq.Expressions.Expression::CanAssign(System.Type,System.Type)
extern "C"  bool Expression_CanAssign_m2750935008 (Il2CppObject * __this /* static, unused */, Type_t * ___target0, Type_t * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Linq.Expressions.Expression System.Linq.Expressions.Expression::CheckLambda(System.Type,System.Linq.Expressions.Expression,System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.ParameterExpression>)
extern "C"  Expression_t114864668 * Expression_CheckLambda_m2029139154 (Il2CppObject * __this /* static, unused */, Type_t * ___delegateType0, Expression_t114864668 * ___body1, ReadOnlyCollection_1_t3201290647 * ___parameters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.Expression::CheckNonGenericMethod(System.Reflection.MethodBase)
extern "C"  void Expression_CheckNonGenericMethod_m3792228212 (Il2CppObject * __this /* static, unused */, MethodBase_t904190842 * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.Expression::CheckNotVoid(System.Type)
extern "C"  void Expression_CheckNotVoid_m1314601213 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Linq.Expressions.ParameterExpression System.Linq.Expressions.Expression::Parameter(System.Type,System.String)
extern "C"  ParameterExpression_t3015504955 * Expression_Parameter_m3021873061 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Linq.Expressions.MemberExpression System.Linq.Expressions.Expression::Property(System.Linq.Expressions.Expression,System.Reflection.MethodInfo)
extern "C"  MemberExpression_t1790982958 * Expression_Property_m2481075526 (Il2CppObject * __this /* static, unused */, Expression_t114864668 * ___expression0, MethodInfo_t * ___propertyAccessor1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyInfo System.Linq.Expressions.Expression::GetAssociatedProperty(System.Reflection.MethodInfo)
extern "C"  PropertyInfo_t * Expression_GetAssociatedProperty_m2697577879 (Il2CppObject * __this /* static, unused */, MethodInfo_t * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Linq.Expressions.UnaryExpression System.Linq.Expressions.Expression::Quote(System.Linq.Expressions.Expression)
extern "C"  UnaryExpression_t4253534555 * Expression_Quote_m1626172201 (Il2CppObject * __this /* static, unused */, Expression_t114864668 * ___expression0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Linq.Expressions.Expression::IsInt(System.Type)
extern "C"  bool Expression_IsInt_m268403699 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Linq.Expressions.Expression::IsNumber(System.Type)
extern "C"  bool Expression_IsNumber_m3569598455 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
