﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pic_anim
struct Pic_anim_t3151297592;

#include "codegen/il2cpp-codegen.h"

// System.Void Pic_anim::.ctor()
extern "C"  void Pic_anim__ctor_m3369555657 (Pic_anim_t3151297592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pic_anim::Start()
extern "C"  void Pic_anim_Start_m273061017 (Pic_anim_t3151297592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pic_anim::Update()
extern "C"  void Pic_anim_Update_m1449176418 (Pic_anim_t3151297592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pic_anim::picAnimate()
extern "C"  void Pic_anim_picAnimate_m2918892 (Pic_anim_t3151297592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
