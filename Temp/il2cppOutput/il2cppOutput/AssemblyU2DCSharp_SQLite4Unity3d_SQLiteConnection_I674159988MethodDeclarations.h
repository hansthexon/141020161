﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.SQLiteConnection/IndexedColumn
struct IndexedColumn_t674159988;
struct IndexedColumn_t674159988_marshaled_pinvoke;
struct IndexedColumn_t674159988_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct IndexedColumn_t674159988;
struct IndexedColumn_t674159988_marshaled_pinvoke;

extern "C" void IndexedColumn_t674159988_marshal_pinvoke(const IndexedColumn_t674159988& unmarshaled, IndexedColumn_t674159988_marshaled_pinvoke& marshaled);
extern "C" void IndexedColumn_t674159988_marshal_pinvoke_back(const IndexedColumn_t674159988_marshaled_pinvoke& marshaled, IndexedColumn_t674159988& unmarshaled);
extern "C" void IndexedColumn_t674159988_marshal_pinvoke_cleanup(IndexedColumn_t674159988_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct IndexedColumn_t674159988;
struct IndexedColumn_t674159988_marshaled_com;

extern "C" void IndexedColumn_t674159988_marshal_com(const IndexedColumn_t674159988& unmarshaled, IndexedColumn_t674159988_marshaled_com& marshaled);
extern "C" void IndexedColumn_t674159988_marshal_com_back(const IndexedColumn_t674159988_marshaled_com& marshaled, IndexedColumn_t674159988& unmarshaled);
extern "C" void IndexedColumn_t674159988_marshal_com_cleanup(IndexedColumn_t674159988_marshaled_com& marshaled);
