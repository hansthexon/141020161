﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// SQLite4Unity3d.TableMapping/Column
struct Column_t441055761;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Collections.Generic.IEnumerable`1<SQLite4Unity3d.IndexedAttribute>
struct IEnumerable_1_t2977028236;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableMapping_Colum441055761.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "mscorlib_System_Reflection_PropertyInfo2253729065.h"

// System.String SQLite4Unity3d.Orm::SqlDecl(SQLite4Unity3d.TableMapping/Column,System.Boolean)
extern "C"  String_t* Orm_SqlDecl_m3576979970 (Il2CppObject * __this /* static, unused */, Column_t441055761 * ___p0, bool ___storeDateTimeAsTicks1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.Orm::SqlType(SQLite4Unity3d.TableMapping/Column,System.Boolean)
extern "C"  String_t* Orm_SqlType_m2362299026 (Il2CppObject * __this /* static, unused */, Column_t441055761 * ___p0, bool ___storeDateTimeAsTicks1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLite4Unity3d.Orm::IsPK(System.Reflection.MemberInfo)
extern "C"  bool Orm_IsPK_m324239432 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.Orm::Collation(System.Reflection.MemberInfo)
extern "C"  String_t* Orm_Collation_m2080096383 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLite4Unity3d.Orm::IsAutoInc(System.Reflection.MemberInfo)
extern "C"  bool Orm_IsAutoInc_m4019293136 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<SQLite4Unity3d.IndexedAttribute> SQLite4Unity3d.Orm::GetIndices(System.Reflection.MemberInfo)
extern "C"  Il2CppObject* Orm_GetIndices_m3362801073 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> SQLite4Unity3d.Orm::MaxStringLength(System.Reflection.PropertyInfo)
extern "C"  Nullable_1_t334943763  Orm_MaxStringLength_m4017384462 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLite4Unity3d.Orm::IsMarkedNotNull(System.Reflection.MemberInfo)
extern "C"  bool Orm_IsMarkedNotNull_m568362847 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
