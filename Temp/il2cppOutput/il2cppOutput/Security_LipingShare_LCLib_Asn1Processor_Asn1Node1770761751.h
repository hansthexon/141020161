﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.String
struct String_t;
// LipingShare.LCLib.Asn1Processor.Asn1Node
struct Asn1Node_t1770761751;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LipingShare.LCLib.Asn1Processor.Asn1Node
struct  Asn1Node_t1770761751  : public Il2CppObject
{
public:
	// System.Byte LipingShare.LCLib.Asn1Processor.Asn1Node::tag
	uint8_t ___tag_0;
	// System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Node::dataOffset
	int64_t ___dataOffset_1;
	// System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Node::dataLength
	int64_t ___dataLength_2;
	// System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Node::lengthFieldBytes
	int64_t ___lengthFieldBytes_3;
	// System.Byte[] LipingShare.LCLib.Asn1Processor.Asn1Node::data
	ByteU5BU5D_t3397334013* ___data_4;
	// System.Collections.ArrayList LipingShare.LCLib.Asn1Processor.Asn1Node::childNodeList
	ArrayList_t4252133567 * ___childNodeList_5;
	// System.Byte LipingShare.LCLib.Asn1Processor.Asn1Node::unusedBits
	uint8_t ___unusedBits_6;
	// System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Node::deepness
	int64_t ___deepness_7;
	// System.String LipingShare.LCLib.Asn1Processor.Asn1Node::path
	String_t* ___path_8;
	// LipingShare.LCLib.Asn1Processor.Asn1Node LipingShare.LCLib.Asn1Processor.Asn1Node::parentNode
	Asn1Node_t1770761751 * ___parentNode_10;
	// System.Boolean LipingShare.LCLib.Asn1Processor.Asn1Node::requireRecalculatePar
	bool ___requireRecalculatePar_11;
	// System.Boolean LipingShare.LCLib.Asn1Processor.Asn1Node::isIndefiniteLength
	bool ___isIndefiniteLength_12;
	// System.Boolean LipingShare.LCLib.Asn1Processor.Asn1Node::parseEncapsulatedData
	bool ___parseEncapsulatedData_13;

public:
	inline static int32_t get_offset_of_tag_0() { return static_cast<int32_t>(offsetof(Asn1Node_t1770761751, ___tag_0)); }
	inline uint8_t get_tag_0() const { return ___tag_0; }
	inline uint8_t* get_address_of_tag_0() { return &___tag_0; }
	inline void set_tag_0(uint8_t value)
	{
		___tag_0 = value;
	}

	inline static int32_t get_offset_of_dataOffset_1() { return static_cast<int32_t>(offsetof(Asn1Node_t1770761751, ___dataOffset_1)); }
	inline int64_t get_dataOffset_1() const { return ___dataOffset_1; }
	inline int64_t* get_address_of_dataOffset_1() { return &___dataOffset_1; }
	inline void set_dataOffset_1(int64_t value)
	{
		___dataOffset_1 = value;
	}

	inline static int32_t get_offset_of_dataLength_2() { return static_cast<int32_t>(offsetof(Asn1Node_t1770761751, ___dataLength_2)); }
	inline int64_t get_dataLength_2() const { return ___dataLength_2; }
	inline int64_t* get_address_of_dataLength_2() { return &___dataLength_2; }
	inline void set_dataLength_2(int64_t value)
	{
		___dataLength_2 = value;
	}

	inline static int32_t get_offset_of_lengthFieldBytes_3() { return static_cast<int32_t>(offsetof(Asn1Node_t1770761751, ___lengthFieldBytes_3)); }
	inline int64_t get_lengthFieldBytes_3() const { return ___lengthFieldBytes_3; }
	inline int64_t* get_address_of_lengthFieldBytes_3() { return &___lengthFieldBytes_3; }
	inline void set_lengthFieldBytes_3(int64_t value)
	{
		___lengthFieldBytes_3 = value;
	}

	inline static int32_t get_offset_of_data_4() { return static_cast<int32_t>(offsetof(Asn1Node_t1770761751, ___data_4)); }
	inline ByteU5BU5D_t3397334013* get_data_4() const { return ___data_4; }
	inline ByteU5BU5D_t3397334013** get_address_of_data_4() { return &___data_4; }
	inline void set_data_4(ByteU5BU5D_t3397334013* value)
	{
		___data_4 = value;
		Il2CppCodeGenWriteBarrier(&___data_4, value);
	}

	inline static int32_t get_offset_of_childNodeList_5() { return static_cast<int32_t>(offsetof(Asn1Node_t1770761751, ___childNodeList_5)); }
	inline ArrayList_t4252133567 * get_childNodeList_5() const { return ___childNodeList_5; }
	inline ArrayList_t4252133567 ** get_address_of_childNodeList_5() { return &___childNodeList_5; }
	inline void set_childNodeList_5(ArrayList_t4252133567 * value)
	{
		___childNodeList_5 = value;
		Il2CppCodeGenWriteBarrier(&___childNodeList_5, value);
	}

	inline static int32_t get_offset_of_unusedBits_6() { return static_cast<int32_t>(offsetof(Asn1Node_t1770761751, ___unusedBits_6)); }
	inline uint8_t get_unusedBits_6() const { return ___unusedBits_6; }
	inline uint8_t* get_address_of_unusedBits_6() { return &___unusedBits_6; }
	inline void set_unusedBits_6(uint8_t value)
	{
		___unusedBits_6 = value;
	}

	inline static int32_t get_offset_of_deepness_7() { return static_cast<int32_t>(offsetof(Asn1Node_t1770761751, ___deepness_7)); }
	inline int64_t get_deepness_7() const { return ___deepness_7; }
	inline int64_t* get_address_of_deepness_7() { return &___deepness_7; }
	inline void set_deepness_7(int64_t value)
	{
		___deepness_7 = value;
	}

	inline static int32_t get_offset_of_path_8() { return static_cast<int32_t>(offsetof(Asn1Node_t1770761751, ___path_8)); }
	inline String_t* get_path_8() const { return ___path_8; }
	inline String_t** get_address_of_path_8() { return &___path_8; }
	inline void set_path_8(String_t* value)
	{
		___path_8 = value;
		Il2CppCodeGenWriteBarrier(&___path_8, value);
	}

	inline static int32_t get_offset_of_parentNode_10() { return static_cast<int32_t>(offsetof(Asn1Node_t1770761751, ___parentNode_10)); }
	inline Asn1Node_t1770761751 * get_parentNode_10() const { return ___parentNode_10; }
	inline Asn1Node_t1770761751 ** get_address_of_parentNode_10() { return &___parentNode_10; }
	inline void set_parentNode_10(Asn1Node_t1770761751 * value)
	{
		___parentNode_10 = value;
		Il2CppCodeGenWriteBarrier(&___parentNode_10, value);
	}

	inline static int32_t get_offset_of_requireRecalculatePar_11() { return static_cast<int32_t>(offsetof(Asn1Node_t1770761751, ___requireRecalculatePar_11)); }
	inline bool get_requireRecalculatePar_11() const { return ___requireRecalculatePar_11; }
	inline bool* get_address_of_requireRecalculatePar_11() { return &___requireRecalculatePar_11; }
	inline void set_requireRecalculatePar_11(bool value)
	{
		___requireRecalculatePar_11 = value;
	}

	inline static int32_t get_offset_of_isIndefiniteLength_12() { return static_cast<int32_t>(offsetof(Asn1Node_t1770761751, ___isIndefiniteLength_12)); }
	inline bool get_isIndefiniteLength_12() const { return ___isIndefiniteLength_12; }
	inline bool* get_address_of_isIndefiniteLength_12() { return &___isIndefiniteLength_12; }
	inline void set_isIndefiniteLength_12(bool value)
	{
		___isIndefiniteLength_12 = value;
	}

	inline static int32_t get_offset_of_parseEncapsulatedData_13() { return static_cast<int32_t>(offsetof(Asn1Node_t1770761751, ___parseEncapsulatedData_13)); }
	inline bool get_parseEncapsulatedData_13() const { return ___parseEncapsulatedData_13; }
	inline bool* get_address_of_parseEncapsulatedData_13() { return &___parseEncapsulatedData_13; }
	inline void set_parseEncapsulatedData_13(bool value)
	{
		___parseEncapsulatedData_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
