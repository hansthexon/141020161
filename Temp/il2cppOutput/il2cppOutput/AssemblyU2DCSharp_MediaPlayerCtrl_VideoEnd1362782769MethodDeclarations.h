﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MediaPlayerCtrl/VideoEnd
struct VideoEnd_t1362782769;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void MediaPlayerCtrl/VideoEnd::.ctor(System.Object,System.IntPtr)
extern "C"  void VideoEnd__ctor_m214281012 (VideoEnd_t1362782769 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl/VideoEnd::Invoke()
extern "C"  void VideoEnd_Invoke_m2436798718 (VideoEnd_t1362782769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MediaPlayerCtrl/VideoEnd::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * VideoEnd_BeginInvoke_m4162039167 (VideoEnd_t1362782769 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl/VideoEnd::EndInvoke(System.IAsyncResult)
extern "C"  void VideoEnd_EndInvoke_m146227998 (VideoEnd_t1362782769 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
