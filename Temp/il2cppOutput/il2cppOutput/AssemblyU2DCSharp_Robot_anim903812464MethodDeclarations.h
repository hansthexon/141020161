﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Robot_anim
struct Robot_anim_t903812464;

#include "codegen/il2cpp-codegen.h"

// System.Void Robot_anim::.ctor()
extern "C"  void Robot_anim__ctor_m2059520091 (Robot_anim_t903812464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Robot_anim::Start()
extern "C"  void Robot_anim_Start_m3426161775 (Robot_anim_t903812464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Robot_anim::Update()
extern "C"  void Robot_anim_Update_m4141161234 (Robot_anim_t903812464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Robot_anim::robotAnimate()
extern "C"  void Robot_anim_robotAnimate_m1439973100 (Robot_anim_t903812464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
