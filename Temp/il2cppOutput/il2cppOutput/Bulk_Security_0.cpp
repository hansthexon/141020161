﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// LipingShare.LCLib.Asn1Processor.Asn1Node
struct Asn1Node_t1770761751;
// System.String
struct String_t;
// System.IO.Stream
struct Stream_t3255436806;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// LipingShare.LCLib.Asn1Processor.Asn1Parser
struct Asn1Parser_t914015216;
// LipingShare.LCLib.Asn1Processor.Oid
struct Oid_t113668572;
// LipingShare.LCLib.Asn1Processor.RelativeOid
struct RelativeOid_t880150712;
// UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt
struct AppleInAppPurchaseReceipt_t3271698749;
// UnityEngine.Purchasing.Security.AppleReceipt
struct AppleReceipt_t3991411794;
// UnityEngine.Purchasing.Security.AppleValidator
struct AppleValidator_t3837389912;
// UnityEngine.Purchasing.Security.CrossPlatformValidator
struct CrossPlatformValidator_t305796431;
// UnityEngine.Purchasing.Security.IPurchaseReceipt[]
struct IPurchaseReceiptU5BU5D_t988864285;
// UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt[]
struct AppleInAppPurchaseReceiptU5BU5D_t579068208;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt>
struct IEnumerable_1_t3563825794;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// UnityEngine.Purchasing.Security.DistinguishedName
struct DistinguishedName_t1881593989;
// UnityEngine.Purchasing.Security.GooglePlayReceipt
struct GooglePlayReceipt_t2643016893;
// UnityEngine.Purchasing.Security.GooglePlayValidator
struct GooglePlayValidator_t4061171767;
// UnityEngine.Purchasing.Security.IAPSecurityException
struct IAPSecurityException_t3038093501;
// UnityEngine.Purchasing.Security.InvalidBundleIdException
struct InvalidBundleIdException_t3011288315;
// UnityEngine.Purchasing.Security.InvalidPKCS7Data
struct InvalidPKCS7Data_t4123278833;
// UnityEngine.Purchasing.Security.InvalidReceiptDataException
struct InvalidReceiptDataException_t1032464292;
// UnityEngine.Purchasing.Security.InvalidRSAData
struct InvalidRSAData_t1674954323;
// UnityEngine.Purchasing.Security.InvalidSignatureException
struct InvalidSignatureException_t488933488;
// UnityEngine.Purchasing.Security.InvalidTimeFormat
struct InvalidTimeFormat_t3933748955;
// UnityEngine.Purchasing.Security.InvalidX509Data
struct InvalidX509Data_t1630759105;
// UnityEngine.Purchasing.Security.MissingStoreSecretException
struct MissingStoreSecretException_t3547862692;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.IEnumerable`1<System.Byte>
struct IEnumerable_1_t3975231481;
// System.Func`2<System.Byte,System.Byte>
struct Func_2_t3601859201;
// UnityEngine.Purchasing.Security.Obfuscator/<DeObfuscate>c__AnonStorey0
struct U3CDeObfuscateU3Ec__AnonStorey0_t1106656056;
// UnityEngine.Purchasing.Security.PKCS7
struct PKCS7_t1974940522;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.Security.SignerInfo>
struct List_1_t3491469936;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.Security.X509Cert>
struct List_1_t4145897706;
// UnityEngine.Purchasing.Security.X509Cert
struct X509Cert_t481809278;
// UnityEngine.Purchasing.Security.RSAKey
struct RSAKey_t446464277;
// System.Security.Cryptography.RSACryptoServiceProvider
struct RSACryptoServiceProvider_t4229286967;
// UnityEngine.Purchasing.Security.SignerInfo
struct SignerInfo_t4122348804;
// UnityEngine.Purchasing.Security.StoreNotSupportedException
struct StoreNotSupportedException_t958190973;
// UnityEngine.Purchasing.Security.UnsupportedSignerInfoVersion
struct UnsupportedSignerInfoVersion_t2780725255;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "Security_U3CModuleU3E3783534214.h"
#include "Security_U3CModuleU3E3783534214MethodDeclarations.h"
#include "Security_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "Security_U3CPrivateImplementationDetailsU3E1486305137MethodDeclarations.h"
#include "Security_U3CPrivateImplementationDetailsU3E_U24Arr1568637719.h"
#include "Security_U3CPrivateImplementationDetailsU3E_U24Arr1568637719MethodDeclarations.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Asn1Node1770761751.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Asn1Node1770761751MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Collections_ArrayList4252133567MethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Int322071877448.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Asn1Util2059476207MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_IO_Stream3255436806MethodDeclarations.h"
#include "mscorlib_System_UInt642909196914.h"
#include "mscorlib_System_Object2689449295.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Oid113668572MethodDeclarations.h"
#include "mscorlib_System_IO_MemoryStream743994179MethodDeclarations.h"
#include "Security_LipingShare_LCLib_Asn1Processor_RelativeOi880150712MethodDeclarations.h"
#include "mscorlib_System_Text_UTF8Encoding111055448MethodDeclarations.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Oid113668572.h"
#include "Security_LipingShare_LCLib_Asn1Processor_RelativeOi880150712.h"
#include "mscorlib_System_Text_UTF8Encoding111055448.h"
#include "mscorlib_System_IO_MemoryStream743994179.h"
#include "mscorlib_System_Text_Encoding663144255MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding663144255.h"
#include "mscorlib_System_Int64909078037MethodDeclarations.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Asn1Parser914015216.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Asn1Parser914015216MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Asn1Util2059476207.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHel266230107MethodDeclarations.h"
#include "mscorlib_System_RuntimeFieldHandle2331729674.h"
#include "System_System_Collections_Specialized_StringDictio1070889667MethodDeclarations.h"
#include "System_System_Collections_Specialized_StringDictio1070889667.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "mscorlib_System_Exception1927440687MethodDeclarations.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_UInt642909196914MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_AppleInAp3271698749.h"
#include "Security_UnityEngine_Purchasing_Security_AppleInAp3271698749MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669.h"
#include "Security_UnityEngine_Purchasing_Security_AppleRece3991411794.h"
#include "Security_UnityEngine_Purchasing_Security_AppleRece3991411794MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_AppleVali3837389912.h"
#include "Security_UnityEngine_Purchasing_Security_AppleVali3837389912MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_X509Cert481809278MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_X509Cert481809278.h"
#include "Security_UnityEngine_Purchasing_Security_PKCS71974940522MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidSig488933488MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_PKCS71974940522.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidSig488933488.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidPK4123278833MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2640819881MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2640819881.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidPK4123278833.h"
#include "Security_ArrayTypes.h"
#include "Security_UnityEngine_Purchasing_Security_CrossPlatf305796431.h"
#include "Security_UnityEngine_Purchasing_Security_CrossPlatf305796431MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_GooglePla4061171767MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_GooglePla4061171767.h"
#include "Common_UnityEngine_Purchasing_MiniJson838727235MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidRe1032464292MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge309261261MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_MissingSt3547862692MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_GooglePla2643016893MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidBu3011288315MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable2148412300MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_StoreNotSu958190973MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge309261261.h"
#include "Security_UnityEngine_Purchasing_Security_GooglePla2643016893.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidRe1032464292.h"
#include "Security_UnityEngine_Purchasing_Security_MissingSt3547862692.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidBu3011288315.h"
#include "System_Core_System_Linq_Enumerable2148412300.h"
#include "Security_UnityEngine_Purchasing_Security_StoreNotSu958190973.h"
#include "Security_UnityEngine_Purchasing_Security_Distingui1881593989.h"
#include "Security_UnityEngine_Purchasing_Security_Distingui1881593989MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidX51630759105MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3986656710MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3986656710.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidX51630759105.h"
#include "Security_UnityEngine_Purchasing_Security_GooglePur2057805495.h"
#include "Security_UnityEngine_Purchasing_Security_RSAKey446464277MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_RSAKey446464277.h"
#include "mscorlib_System_DateTimeKind2186819611.h"
#include "mscorlib_System_Double4078015681.h"
#include "Security_UnityEngine_Purchasing_Security_GooglePur2057805495MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_IAPSecuri3038093501.h"
#include "Security_UnityEngine_Purchasing_Security_IAPSecuri3038093501MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidRS1674954323.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidRS1674954323MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidTi3933748955.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidTi3933748955MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_Obfuscato2878230988.h"
#include "Security_UnityEngine_Purchasing_Security_Obfuscato2878230988MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_Obfuscato1106656056MethodDeclarations.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3601859201MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_Obfuscato1106656056.h"
#include "System_Core_System_Func_2_gen3601859201.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3491469936.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4145897706.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3491469936MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4145897706MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_SignerInf4122348804MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_SignerInf4122348804.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3026199610.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3680627380.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3026199610MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3680627380MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_RSACryptoSer4229286967.h"
#include "mscorlib_System_Security_Cryptography_SHA1Managed7268864MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_HashAlgorith2624936259MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_RSACryptoSer4229286967MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_SHA1Managed7268864.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricAlg784058677MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricAlg784058677.h"
#include "Security_UnityEngine_Purchasing_Security_Unsupport2780725255MethodDeclarations.h"
#include "Security_UnityEngine_Purchasing_Security_Unsupport2780725255.h"

// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  ObjectU5BU5D_t3614634134* Enumerable_ToArray_TisIl2CppObject_m3301715283_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToArray_TisIl2CppObject_m3301715283(__this /* static, unused */, p0, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m3301715283_gshared)(__this /* static, unused */, p0, method)
// !!0[] System.Linq.Enumerable::ToArray<UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisAppleInAppPurchaseReceipt_t3271698749_m258157314(__this /* static, unused */, p0, method) ((  AppleInAppPurchaseReceiptU5BU5D_t579068208* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m3301715283_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Skip<System.Byte>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
extern "C"  Il2CppObject* Enumerable_Skip_TisByte_t3683104436_m3962905258_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, int32_t p1, const MethodInfo* method);
#define Enumerable_Skip_TisByte_t3683104436_m3962905258(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))Enumerable_Skip_TisByte_t3683104436_m3962905258_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Take<System.Byte>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
extern "C"  Il2CppObject* Enumerable_Take_TisByte_t3683104436_m3483797502_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, int32_t p1, const MethodInfo* method);
#define Enumerable_Take_TisByte_t3683104436_m3483797502(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))Enumerable_Take_TisByte_t3683104436_m3483797502_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] System.Linq.Enumerable::ToArray<System.Byte>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  ByteU5BU5D_t3397334013* Enumerable_ToArray_TisByte_t3683104436_m2323269811_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToArray_TisByte_t3683104436_m2323269811(__this /* static, unused */, p0, method) ((  ByteU5BU5D_t3397334013* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisByte_t3683104436_m2323269811_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Byte,System.Byte>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Enumerable_Select_TisByte_t3683104436_TisByte_t3683104436_m4080074214_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t3601859201 * p1, const MethodInfo* method);
#define Enumerable_Select_TisByte_t3683104436_TisByte_t3683104436_m4080074214(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3601859201 *, const MethodInfo*))Enumerable_Select_TisByte_t3683104436_TisByte_t3683104436_m4080074214_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType=32
extern "C" void U24ArrayTypeU3D32_t1568637719_marshal_pinvoke(const U24ArrayTypeU3D32_t1568637719& unmarshaled, U24ArrayTypeU3D32_t1568637719_marshaled_pinvoke& marshaled)
{
}
extern "C" void U24ArrayTypeU3D32_t1568637719_marshal_pinvoke_back(const U24ArrayTypeU3D32_t1568637719_marshaled_pinvoke& marshaled, U24ArrayTypeU3D32_t1568637719& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType=32
extern "C" void U24ArrayTypeU3D32_t1568637719_marshal_pinvoke_cleanup(U24ArrayTypeU3D32_t1568637719_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType=32
extern "C" void U24ArrayTypeU3D32_t1568637719_marshal_com(const U24ArrayTypeU3D32_t1568637719& unmarshaled, U24ArrayTypeU3D32_t1568637719_marshaled_com& marshaled)
{
}
extern "C" void U24ArrayTypeU3D32_t1568637719_marshal_com_back(const U24ArrayTypeU3D32_t1568637719_marshaled_com& marshaled, U24ArrayTypeU3D32_t1568637719& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType=32
extern "C" void U24ArrayTypeU3D32_t1568637719_marshal_com_cleanup(U24ArrayTypeU3D32_t1568637719_marshaled_com& marshaled)
{
}
// System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::.ctor(LipingShare.LCLib.Asn1Processor.Asn1Node,System.Int64)
extern Il2CppCodeGenString* _stringLiteral371857150;
extern const uint32_t Asn1Node__ctor_m2720905655_MetadataUsageId;
extern "C"  void Asn1Node__ctor_m2720905655 (Asn1Node_t1770761751 * __this, Asn1Node_t1770761751 * ___parentNode0, int64_t ___dataOffset1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Node__ctor_m2720905655_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_path_8(_stringLiteral371857150);
		__this->set_requireRecalculatePar_11((bool)1);
		__this->set_isIndefiniteLength_12((bool)0);
		__this->set_parseEncapsulatedData_13((bool)1);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Asn1Node_Init_m1357911443(__this, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_0 = ___parentNode0;
		NullCheck(L_0);
		int64_t L_1 = Asn1Node_get_Deepness_m684235038(L_0, /*hidden argument*/NULL);
		__this->set_deepness_7(((int64_t)((int64_t)L_1+(int64_t)(((int64_t)((int64_t)1))))));
		Asn1Node_t1770761751 * L_2 = ___parentNode0;
		__this->set_parentNode_10(L_2);
		int64_t L_3 = ___dataOffset1;
		__this->set_dataOffset_1(L_3);
		return;
	}
}
// System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::.ctor()
extern Il2CppCodeGenString* _stringLiteral371857150;
extern const uint32_t Asn1Node__ctor_m2386874033_MetadataUsageId;
extern "C"  void Asn1Node__ctor_m2386874033 (Asn1Node_t1770761751 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Node__ctor_m2386874033_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_path_8(_stringLiteral371857150);
		__this->set_requireRecalculatePar_11((bool)1);
		__this->set_isIndefiniteLength_12((bool)0);
		__this->set_parseEncapsulatedData_13((bool)1);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Asn1Node_Init_m1357911443(__this, /*hidden argument*/NULL);
		__this->set_dataOffset_1((((int64_t)((int64_t)0))));
		return;
	}
}
// System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::Init()
extern Il2CppClass* ArrayList_t4252133567_il2cpp_TypeInfo_var;
extern const uint32_t Asn1Node_Init_m1357911443_MetadataUsageId;
extern "C"  void Asn1Node_Init_m1357911443 (Asn1Node_t1770761751 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Node_Init_m1357911443_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t4252133567 * L_0 = (ArrayList_t4252133567 *)il2cpp_codegen_object_new(ArrayList_t4252133567_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4012174379(L_0, /*hidden argument*/NULL);
		__this->set_childNodeList_5(L_0);
		__this->set_data_4((ByteU5BU5D_t3397334013*)NULL);
		__this->set_dataLength_2((((int64_t)((int64_t)0))));
		__this->set_lengthFieldBytes_3((((int64_t)((int64_t)0))));
		__this->set_unusedBits_6(0);
		__this->set_tag_0(((int32_t)48));
		ArrayList_t4252133567 * L_1 = __this->get_childNodeList_5();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(31 /* System.Void System.Collections.ArrayList::Clear() */, L_1);
		__this->set_deepness_7((((int64_t)((int64_t)0))));
		__this->set_parentNode_10((Asn1Node_t1770761751 *)NULL);
		return;
	}
}
// System.String LipingShare.LCLib.Asn1Processor.Asn1Node::GetHexPrintingStr(LipingShare.LCLib.Asn1Processor.Asn1Node,System.String,System.String,System.Int32)
extern Il2CppClass* Asn1Util_t2059476207_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern Il2CppCodeGenString* _stringLiteral372029307;
extern Il2CppCodeGenString* _stringLiteral2162321587;
extern const uint32_t Asn1Node_GetHexPrintingStr_m134819188_MetadataUsageId;
extern "C"  String_t* Asn1Node_GetHexPrintingStr_m134819188 (Asn1Node_t1770761751 * __this, Asn1Node_t1770761751 * ___startNode0, String_t* ___baseLine1, String_t* ___lStr2, int32_t ___lineLen3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Node_GetHexPrintingStr_m134819188_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	{
		V_0 = _stringLiteral371857150;
		Asn1Node_t1770761751 * L_0 = ___startNode0;
		String_t* L_1 = Asn1Node_GetIndentStr_m4071159475(__this, L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		ByteU5BU5D_t3397334013* L_2 = __this->get_data_4();
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		String_t* L_3 = Asn1Util_ToHexString_m3932902392(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_2 = L_3;
		String_t* L_4 = V_2;
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m1606060069(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_6 = ___baseLine1;
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m1606060069(L_6, /*hidden argument*/NULL);
		String_t* L_8 = V_2;
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m1606060069(L_8, /*hidden argument*/NULL);
		int32_t L_10 = ___lineLen3;
		if ((((int32_t)((int32_t)((int32_t)L_7+(int32_t)L_9))) >= ((int32_t)L_10)))
		{
			goto IL_006d;
		}
	}
	{
		String_t* L_11 = V_0;
		V_3 = L_11;
		StringU5BU5D_t1642385972* L_12 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		String_t* L_13 = V_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 0);
		ArrayElementTypeCheck (L_12, L_13);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_13);
		StringU5BU5D_t1642385972* L_14 = L_12;
		String_t* L_15 = ___baseLine1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 1);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_15);
		StringU5BU5D_t1642385972* L_16 = L_14;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		ArrayElementTypeCheck (L_16, _stringLiteral372029307);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029307);
		StringU5BU5D_t1642385972* L_17 = L_16;
		String_t* L_18 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 3);
		ArrayElementTypeCheck (L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_18);
		StringU5BU5D_t1642385972* L_19 = L_17;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 4);
		ArrayElementTypeCheck (L_19, _stringLiteral372029307);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029307);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m626692867(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		V_0 = L_20;
		goto IL_0087;
	}

IL_006d:
	{
		String_t* L_21 = V_0;
		String_t* L_22 = ___baseLine1;
		String_t* L_23 = ___lStr2;
		String_t* L_24 = V_1;
		NullCheck(L_24);
		int32_t L_25 = String_get_Length_m1606060069(L_24, /*hidden argument*/NULL);
		int32_t L_26 = ___lineLen3;
		String_t* L_27 = V_2;
		String_t* L_28 = Asn1Node_FormatLineHexString_m3369492261(__this, L_23, L_25, L_26, L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m612901809(NULL /*static, unused*/, L_21, L_22, L_28, /*hidden argument*/NULL);
		V_0 = L_29;
	}

IL_0087:
	{
		goto IL_0097;
	}

IL_008d:
	{
		String_t* L_30 = V_0;
		String_t* L_31 = ___baseLine1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m2596409543(NULL /*static, unused*/, L_30, L_31, /*hidden argument*/NULL);
		V_0 = L_32;
	}

IL_0097:
	{
		String_t* L_33 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Concat_m2596409543(NULL /*static, unused*/, L_33, _stringLiteral2162321587, /*hidden argument*/NULL);
		V_4 = L_34;
		goto IL_00a9;
	}

IL_00a9:
	{
		String_t* L_35 = V_4;
		return L_35;
	}
}
// System.String LipingShare.LCLib.Asn1Processor.Asn1Node::FormatLineString(System.String,System.Int32,System.Int32,System.String)
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Asn1Util_t2059476207_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern Il2CppCodeGenString* _stringLiteral2162321587;
extern Il2CppCodeGenString* _stringLiteral372029307;
extern const uint32_t Asn1Node_FormatLineString_m3353585152_MetadataUsageId;
extern "C"  String_t* Asn1Node_FormatLineString_m3353585152 (Asn1Node_t1770761751 * __this, String_t* ___lStr0, int32_t ___indent1, int32_t ___lineLen2, String_t* ___msg3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Node_FormatLineString_m3353585152_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	{
		V_0 = _stringLiteral371857150;
		int32_t L_0 = ___indent1;
		___indent1 = ((int32_t)((int32_t)L_0+(int32_t)3));
		int32_t L_1 = ___lineLen2;
		int32_t L_2 = ___indent1;
		V_1 = ((int32_t)((int32_t)L_1-(int32_t)L_2));
		int32_t L_3 = ___indent1;
		V_2 = L_3;
		V_3 = 0;
		goto IL_00cd;
	}

IL_0019:
	{
		int32_t L_4 = V_3;
		int32_t L_5 = V_1;
		String_t* L_6 = ___msg3;
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m1606060069(L_6, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_4+(int32_t)L_5))) <= ((int32_t)L_7)))
		{
			goto IL_007f;
		}
	}
	{
		String_t* L_8 = V_0;
		V_4 = L_8;
		StringU5BU5D_t1642385972* L_9 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)7));
		String_t* L_10 = V_4;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_10);
		StringU5BU5D_t1642385972* L_11 = L_9;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
		ArrayElementTypeCheck (L_11, _stringLiteral2162321587);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2162321587);
		StringU5BU5D_t1642385972* L_12 = L_11;
		String_t* L_13 = ___lStr0;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, L_13);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_13);
		StringU5BU5D_t1642385972* L_14 = L_12;
		int32_t L_15 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		String_t* L_16 = Asn1Util_GenStr_m3970829795(NULL /*static, unused*/, L_15, ((int32_t)32), /*hidden argument*/NULL);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 3);
		ArrayElementTypeCheck (L_14, L_16);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_16);
		StringU5BU5D_t1642385972* L_17 = L_14;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 4);
		ArrayElementTypeCheck (L_17, _stringLiteral372029307);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029307);
		StringU5BU5D_t1642385972* L_18 = L_17;
		String_t* L_19 = ___msg3;
		int32_t L_20 = V_3;
		String_t* L_21 = ___msg3;
		NullCheck(L_21);
		int32_t L_22 = String_get_Length_m1606060069(L_21, /*hidden argument*/NULL);
		int32_t L_23 = V_3;
		NullCheck(L_19);
		String_t* L_24 = String_Substring_m12482732(L_19, L_20, ((int32_t)((int32_t)L_22-(int32_t)L_23)), /*hidden argument*/NULL);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 5);
		ArrayElementTypeCheck (L_18, L_24);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_24);
		StringU5BU5D_t1642385972* L_25 = L_18;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 6);
		ArrayElementTypeCheck (L_25, _stringLiteral372029307);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral372029307);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m626692867(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		V_0 = L_26;
		goto IL_00c8;
	}

IL_007f:
	{
		String_t* L_27 = V_0;
		V_4 = L_27;
		StringU5BU5D_t1642385972* L_28 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)7));
		String_t* L_29 = V_4;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 0);
		ArrayElementTypeCheck (L_28, L_29);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_29);
		StringU5BU5D_t1642385972* L_30 = L_28;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 1);
		ArrayElementTypeCheck (L_30, _stringLiteral2162321587);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2162321587);
		StringU5BU5D_t1642385972* L_31 = L_30;
		String_t* L_32 = ___lStr0;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 2);
		ArrayElementTypeCheck (L_31, L_32);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_32);
		StringU5BU5D_t1642385972* L_33 = L_31;
		int32_t L_34 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		String_t* L_35 = Asn1Util_GenStr_m3970829795(NULL /*static, unused*/, L_34, ((int32_t)32), /*hidden argument*/NULL);
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, 3);
		ArrayElementTypeCheck (L_33, L_35);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_35);
		StringU5BU5D_t1642385972* L_36 = L_33;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 4);
		ArrayElementTypeCheck (L_36, _stringLiteral372029307);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029307);
		StringU5BU5D_t1642385972* L_37 = L_36;
		String_t* L_38 = ___msg3;
		int32_t L_39 = V_3;
		int32_t L_40 = V_1;
		NullCheck(L_38);
		String_t* L_41 = String_Substring_m12482732(L_38, L_39, L_40, /*hidden argument*/NULL);
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, 5);
		ArrayElementTypeCheck (L_37, L_41);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_41);
		StringU5BU5D_t1642385972* L_42 = L_37;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 6);
		ArrayElementTypeCheck (L_42, _stringLiteral372029307);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral372029307);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_43 = String_Concat_m626692867(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		V_0 = L_43;
	}

IL_00c8:
	{
		int32_t L_44 = V_3;
		int32_t L_45 = V_1;
		V_3 = ((int32_t)((int32_t)L_44+(int32_t)L_45));
	}

IL_00cd:
	{
		int32_t L_46 = V_3;
		String_t* L_47 = ___msg3;
		NullCheck(L_47);
		int32_t L_48 = String_get_Length_m1606060069(L_47, /*hidden argument*/NULL);
		if ((((int32_t)L_46) < ((int32_t)L_48)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_49 = V_0;
		V_5 = L_49;
		goto IL_00e2;
	}

IL_00e2:
	{
		String_t* L_50 = V_5;
		return L_50;
	}
}
// System.String LipingShare.LCLib.Asn1Processor.Asn1Node::FormatLineHexString(System.String,System.Int32,System.Int32,System.String)
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Asn1Util_t2059476207_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern Il2CppCodeGenString* _stringLiteral2162321587;
extern const uint32_t Asn1Node_FormatLineHexString_m3369492261_MetadataUsageId;
extern "C"  String_t* Asn1Node_FormatLineHexString_m3369492261 (Asn1Node_t1770761751 * __this, String_t* ___lStr0, int32_t ___indent1, int32_t ___lineLen2, String_t* ___msg3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Node_FormatLineHexString_m3369492261_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	{
		V_0 = _stringLiteral371857150;
		int32_t L_0 = ___indent1;
		___indent1 = ((int32_t)((int32_t)L_0+(int32_t)3));
		int32_t L_1 = ___lineLen2;
		int32_t L_2 = ___indent1;
		V_1 = ((int32_t)((int32_t)L_1-(int32_t)L_2));
		int32_t L_3 = ___indent1;
		V_2 = L_3;
		V_3 = 0;
		goto IL_00ad;
	}

IL_0019:
	{
		int32_t L_4 = V_3;
		int32_t L_5 = V_1;
		String_t* L_6 = ___msg3;
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m1606060069(L_6, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_4+(int32_t)L_5))) <= ((int32_t)L_7)))
		{
			goto IL_006f;
		}
	}
	{
		String_t* L_8 = V_0;
		V_4 = L_8;
		StringU5BU5D_t1642385972* L_9 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		String_t* L_10 = V_4;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_10);
		StringU5BU5D_t1642385972* L_11 = L_9;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
		ArrayElementTypeCheck (L_11, _stringLiteral2162321587);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2162321587);
		StringU5BU5D_t1642385972* L_12 = L_11;
		String_t* L_13 = ___lStr0;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, L_13);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_13);
		StringU5BU5D_t1642385972* L_14 = L_12;
		int32_t L_15 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		String_t* L_16 = Asn1Util_GenStr_m3970829795(NULL /*static, unused*/, L_15, ((int32_t)32), /*hidden argument*/NULL);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 3);
		ArrayElementTypeCheck (L_14, L_16);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_16);
		StringU5BU5D_t1642385972* L_17 = L_14;
		String_t* L_18 = ___msg3;
		int32_t L_19 = V_3;
		String_t* L_20 = ___msg3;
		NullCheck(L_20);
		int32_t L_21 = String_get_Length_m1606060069(L_20, /*hidden argument*/NULL);
		int32_t L_22 = V_3;
		NullCheck(L_18);
		String_t* L_23 = String_Substring_m12482732(L_18, L_19, ((int32_t)((int32_t)L_21-(int32_t)L_22)), /*hidden argument*/NULL);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 4);
		ArrayElementTypeCheck (L_17, L_23);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_23);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = String_Concat_m626692867(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_0 = L_24;
		goto IL_00a8;
	}

IL_006f:
	{
		String_t* L_25 = V_0;
		V_4 = L_25;
		StringU5BU5D_t1642385972* L_26 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		String_t* L_27 = V_4;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 0);
		ArrayElementTypeCheck (L_26, L_27);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_27);
		StringU5BU5D_t1642385972* L_28 = L_26;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 1);
		ArrayElementTypeCheck (L_28, _stringLiteral2162321587);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2162321587);
		StringU5BU5D_t1642385972* L_29 = L_28;
		String_t* L_30 = ___lStr0;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 2);
		ArrayElementTypeCheck (L_29, L_30);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_30);
		StringU5BU5D_t1642385972* L_31 = L_29;
		int32_t L_32 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		String_t* L_33 = Asn1Util_GenStr_m3970829795(NULL /*static, unused*/, L_32, ((int32_t)32), /*hidden argument*/NULL);
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 3);
		ArrayElementTypeCheck (L_31, L_33);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_33);
		StringU5BU5D_t1642385972* L_34 = L_31;
		String_t* L_35 = ___msg3;
		int32_t L_36 = V_3;
		int32_t L_37 = V_1;
		NullCheck(L_35);
		String_t* L_38 = String_Substring_m12482732(L_35, L_36, L_37, /*hidden argument*/NULL);
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 4);
		ArrayElementTypeCheck (L_34, L_38);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_38);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Concat_m626692867(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		V_0 = L_39;
	}

IL_00a8:
	{
		int32_t L_40 = V_3;
		int32_t L_41 = V_1;
		V_3 = ((int32_t)((int32_t)L_40+(int32_t)L_41));
	}

IL_00ad:
	{
		int32_t L_42 = V_3;
		String_t* L_43 = ___msg3;
		NullCheck(L_43);
		int32_t L_44 = String_get_Length_m1606060069(L_43, /*hidden argument*/NULL);
		if ((((int32_t)L_42) < ((int32_t)L_44)))
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_45 = V_0;
		V_5 = L_45;
		goto IL_00c2;
	}

IL_00c2:
	{
		String_t* L_46 = V_5;
		return L_46;
	}
}
// System.Byte LipingShare.LCLib.Asn1Processor.Asn1Node::get_Tag()
extern "C"  uint8_t Asn1Node_get_Tag_m2005840874 (Asn1Node_t1770761751 * __this, const MethodInfo* method)
{
	uint8_t V_0 = 0x0;
	{
		uint8_t L_0 = __this->get_tag_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		uint8_t L_1 = V_0;
		return L_1;
	}
}
// System.Byte LipingShare.LCLib.Asn1Processor.Asn1Node::get_MaskedTag()
extern "C"  uint8_t Asn1Node_get_MaskedTag_m135294101 (Asn1Node_t1770761751 * __this, const MethodInfo* method)
{
	uint8_t V_0 = 0x0;
	{
		uint8_t L_0 = __this->get_tag_0();
		V_0 = (((int32_t)((uint8_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)31))))));
		goto IL_0011;
	}

IL_0011:
	{
		uint8_t L_1 = V_0;
		return L_1;
	}
}
// System.Boolean LipingShare.LCLib.Asn1Processor.Asn1Node::LoadData(System.IO.Stream)
extern "C"  bool Asn1Node_LoadData_m1298383144 (Asn1Node_t1770761751 * __this, Stream_t3255436806 * ___xdata0, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)0;
	}

IL_0003:
	try
	{ // begin try (depth: 1)
		Asn1Node_set_RequireRecalculatePar_m3884225938(__this, (bool)0, /*hidden argument*/NULL);
		Stream_t3255436806 * L_0 = ___xdata0;
		bool L_1 = Asn1Node_InternalLoadData_m220543567(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		V_1 = L_2;
		IL2CPP_LEAVE(0x2A, FINALLY_001a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001a;
	}

FINALLY_001a:
	{ // begin finally (depth: 1)
		Asn1Node_set_RequireRecalculatePar_m3884225938(__this, (bool)1, /*hidden argument*/NULL);
		Asn1Node_RecalculateTreePar_m2615014223(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(26)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(26)
	{
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_002a:
	{
		bool L_3 = V_1;
		return L_3;
	}
}
// System.Boolean LipingShare.LCLib.Asn1Processor.Asn1Node::SaveData(System.IO.Stream)
extern Il2CppClass* Asn1Util_t2059476207_il2cpp_TypeInfo_var;
extern const uint32_t Asn1Node_SaveData_m3078124111_MetadataUsageId;
extern "C"  bool Asn1Node_SaveData_m3078124111 (Asn1Node_t1770761751 * __this, Stream_t3255436806 * ___xdata0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Node_SaveData_m3078124111_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int64_t V_1 = 0;
	Asn1Node_t1770761751 * V_2 = NULL;
	int32_t V_3 = 0;
	bool V_4 = false;
	{
		V_0 = (bool)1;
		int64_t L_0 = Asn1Node_get_ChildNodeCount_m1123088750(__this, /*hidden argument*/NULL);
		V_1 = L_0;
		Stream_t3255436806 * L_1 = ___xdata0;
		uint8_t L_2 = __this->get_tag_0();
		NullCheck(L_1);
		VirtActionInvoker1< uint8_t >::Invoke(19 /* System.Void System.IO.Stream::WriteByte(System.Byte) */, L_1, L_2);
		Stream_t3255436806 * L_3 = ___xdata0;
		int64_t L_4 = __this->get_dataLength_2();
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		Asn1Util_DERLengthEncode_m3789574000(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		uint8_t L_5 = __this->get_tag_0();
		if ((!(((uint32_t)L_5) == ((uint32_t)3))))
		{
			goto IL_003d;
		}
	}
	{
		Stream_t3255436806 * L_6 = ___xdata0;
		uint8_t L_7 = __this->get_unusedBits_6();
		NullCheck(L_6);
		VirtActionInvoker1< uint8_t >::Invoke(19 /* System.Void System.IO.Stream::WriteByte(System.Byte) */, L_6, L_7);
	}

IL_003d:
	{
		int64_t L_8 = V_1;
		if ((!(((uint64_t)L_8) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_006e;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_9 = __this->get_data_4();
		if (!L_9)
		{
			goto IL_0068;
		}
	}
	{
		Stream_t3255436806 * L_10 = ___xdata0;
		ByteU5BU5D_t3397334013* L_11 = __this->get_data_4();
		ByteU5BU5D_t3397334013* L_12 = __this->get_data_4();
		NullCheck(L_12);
		NullCheck(L_10);
		VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_10, L_11, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))));
	}

IL_0068:
	{
		goto IL_0095;
	}

IL_006e:
	{
		V_3 = 0;
		goto IL_008c;
	}

IL_0076:
	{
		int32_t L_13 = V_3;
		Asn1Node_t1770761751 * L_14 = Asn1Node_GetChildNode_m4133223467(__this, L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		Asn1Node_t1770761751 * L_15 = V_2;
		Stream_t3255436806 * L_16 = ___xdata0;
		NullCheck(L_15);
		bool L_17 = Asn1Node_SaveData_m3078124111(L_15, L_16, /*hidden argument*/NULL);
		V_0 = L_17;
		int32_t L_18 = V_3;
		V_3 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_19 = V_3;
		int64_t L_20 = V_1;
		if ((((int64_t)(((int64_t)((int64_t)L_19)))) < ((int64_t)L_20)))
		{
			goto IL_0076;
		}
	}
	{
	}

IL_0095:
	{
		bool L_21 = V_0;
		V_4 = L_21;
		goto IL_009d;
	}

IL_009d:
	{
		bool L_22 = V_4;
		return L_22;
	}
}
// System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::ClearAll()
extern Il2CppClass* Asn1Node_t1770761751_il2cpp_TypeInfo_var;
extern const uint32_t Asn1Node_ClearAll_m2883531985_MetadataUsageId;
extern "C"  void Asn1Node_ClearAll_m2883531985 (Asn1Node_t1770761751 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Node_ClearAll_m2883531985_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Asn1Node_t1770761751 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		__this->set_data_4((ByteU5BU5D_t3397334013*)NULL);
		V_1 = 0;
		goto IL_002d;
	}

IL_000f:
	{
		ArrayList_t4252133567 * L_0 = __this->get_childNodeList_5();
		int32_t L_1 = V_1;
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, L_1);
		V_0 = ((Asn1Node_t1770761751 *)CastclassClass(L_2, Asn1Node_t1770761751_il2cpp_TypeInfo_var));
		Asn1Node_t1770761751 * L_3 = V_0;
		NullCheck(L_3);
		Asn1Node_ClearAll_m2883531985(L_3, /*hidden argument*/NULL);
		int32_t L_4 = V_1;
		V_1 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_5 = V_1;
		ArrayList_t4252133567 * L_6 = __this->get_childNodeList_5();
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_6);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_000f;
		}
	}
	{
		ArrayList_t4252133567 * L_8 = __this->get_childNodeList_5();
		NullCheck(L_8);
		VirtActionInvoker0::Invoke(31 /* System.Void System.Collections.ArrayList::Clear() */, L_8);
		Asn1Node_RecalculateTreePar_m2615014223(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::AddChild(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  void Asn1Node_AddChild_m134798510 (Asn1Node_t1770761751 * __this, Asn1Node_t1770761751 * ___xdata0, const MethodInfo* method)
{
	{
		ArrayList_t4252133567 * L_0 = __this->get_childNodeList_5();
		Asn1Node_t1770761751 * L_1 = ___xdata0;
		NullCheck(L_0);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_0, L_1);
		Asn1Node_RecalculateTreePar_m2615014223(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Node::get_ChildNodeCount()
extern "C"  int64_t Asn1Node_get_ChildNodeCount_m1123088750 (Asn1Node_t1770761751 * __this, const MethodInfo* method)
{
	int64_t V_0 = 0;
	{
		ArrayList_t4252133567 * L_0 = __this->get_childNodeList_5();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_0);
		V_0 = (((int64_t)((int64_t)L_1)));
		goto IL_0013;
	}

IL_0013:
	{
		int64_t L_2 = V_0;
		return L_2;
	}
}
// LipingShare.LCLib.Asn1Processor.Asn1Node LipingShare.LCLib.Asn1Processor.Asn1Node::GetChildNode(System.Int32)
extern Il2CppClass* Asn1Node_t1770761751_il2cpp_TypeInfo_var;
extern const uint32_t Asn1Node_GetChildNode_m4133223467_MetadataUsageId;
extern "C"  Asn1Node_t1770761751 * Asn1Node_GetChildNode_m4133223467 (Asn1Node_t1770761751 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Node_GetChildNode_m4133223467_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Asn1Node_t1770761751 * V_0 = NULL;
	Asn1Node_t1770761751 * V_1 = NULL;
	{
		V_0 = (Asn1Node_t1770761751 *)NULL;
		int32_t L_0 = ___index0;
		int64_t L_1 = Asn1Node_get_ChildNodeCount_m1123088750(__this, /*hidden argument*/NULL);
		if ((((int64_t)(((int64_t)((int64_t)L_0)))) >= ((int64_t)L_1)))
		{
			goto IL_0024;
		}
	}
	{
		ArrayList_t4252133567 * L_2 = __this->get_childNodeList_5();
		int32_t L_3 = ___index0;
		NullCheck(L_2);
		Il2CppObject * L_4 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_2, L_3);
		V_0 = ((Asn1Node_t1770761751 *)CastclassClass(L_4, Asn1Node_t1770761751_il2cpp_TypeInfo_var));
	}

IL_0024:
	{
		Asn1Node_t1770761751 * L_5 = V_0;
		V_1 = L_5;
		goto IL_002b;
	}

IL_002b:
	{
		Asn1Node_t1770761751 * L_6 = V_1;
		return L_6;
	}
}
// System.String LipingShare.LCLib.Asn1Processor.Asn1Node::get_TagName()
extern Il2CppClass* Asn1Util_t2059476207_il2cpp_TypeInfo_var;
extern const uint32_t Asn1Node_get_TagName_m1549199302_MetadataUsageId;
extern "C"  String_t* Asn1Node_get_TagName_m1549199302 (Asn1Node_t1770761751 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Node_get_TagName_m1549199302_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		uint8_t L_0 = __this->get_tag_0();
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		String_t* L_1 = Asn1Util_GetTagName_m2899215986(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
// LipingShare.LCLib.Asn1Processor.Asn1Node LipingShare.LCLib.Asn1Processor.Asn1Node::get_ParentNode()
extern "C"  Asn1Node_t1770761751 * Asn1Node_get_ParentNode_m4121820111 (Asn1Node_t1770761751 * __this, const MethodInfo* method)
{
	Asn1Node_t1770761751 * V_0 = NULL;
	{
		Asn1Node_t1770761751 * L_0 = __this->get_parentNode_10();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Asn1Node_t1770761751 * L_1 = V_0;
		return L_1;
	}
}
// System.String LipingShare.LCLib.Asn1Processor.Asn1Node::GetText(LipingShare.LCLib.Asn1Processor.Asn1Node,System.Int32)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t3683104436_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Asn1Util_t2059476207_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Oid_t113668572_il2cpp_TypeInfo_var;
extern Il2CppClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern Il2CppClass* RelativeOid_t880150712_il2cpp_TypeInfo_var;
extern Il2CppClass* UTF8Encoding_t111055448_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern Il2CppCodeGenString* _stringLiteral3047465420;
extern Il2CppCodeGenString* _stringLiteral2162321587;
extern Il2CppCodeGenString* _stringLiteral372029307;
extern Il2CppCodeGenString* _stringLiteral4090317072;
extern Il2CppCodeGenString* _stringLiteral713096914;
extern Il2CppCodeGenString* _stringLiteral178121469;
extern Il2CppCodeGenString* _stringLiteral1018099543;
extern Il2CppCodeGenString* _stringLiteral4040664675;
extern const uint32_t Asn1Node_GetText_m821606440_MetadataUsageId;
extern "C"  String_t* Asn1Node_GetText_m821606440 (Asn1Node_t1770761751 * __this, Asn1Node_t1770761751 * ___startNode0, int32_t ___lineLen1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Node_GetText_m821606440_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	uint8_t V_5 = 0x0;
	String_t* V_6 = NULL;
	Oid_t113668572 * V_7 = NULL;
	RelativeOid_t880150712 * V_8 = NULL;
	UTF8Encoding_t111055448 * V_9 = NULL;
	int64_t V_10 = 0;
	String_t* V_11 = NULL;
	{
		V_0 = _stringLiteral371857150;
		V_1 = _stringLiteral371857150;
		V_2 = _stringLiteral371857150;
		uint8_t L_0 = __this->get_tag_0();
		V_5 = L_0;
		uint8_t L_1 = V_5;
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)18))) == 0)
		{
			goto IL_0298;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)18))) == 1)
		{
			goto IL_0298;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)18))) == 2)
		{
			goto IL_0059;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)18))) == 3)
		{
			goto IL_0059;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)18))) == 4)
		{
			goto IL_0298;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)18))) == 5)
		{
			goto IL_0298;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)18))) == 6)
		{
			goto IL_0298;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)18))) == 7)
		{
			goto IL_0059;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)18))) == 8)
		{
			goto IL_0298;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)18))) == 9)
		{
			goto IL_0298;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)18))) == 10)
		{
			goto IL_0298;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)18))) == 11)
		{
			goto IL_0059;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)18))) == 12)
		{
			goto IL_0298;
		}
	}

IL_0059:
	{
		uint8_t L_2 = V_5;
		if (((int32_t)((int32_t)L_2-(int32_t)2)) == 0)
		{
			goto IL_0390;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)2)) == 1)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)2)) == 2)
		{
			goto IL_0076;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)2)) == 3)
		{
			goto IL_0076;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)2)) == 4)
		{
			goto IL_018f;
		}
	}

IL_0076:
	{
		uint8_t L_3 = V_5;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)12))))
		{
			goto IL_0298;
		}
	}
	{
		uint8_t L_4 = V_5;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)13))))
		{
			goto IL_0215;
		}
	}
	{
		goto IL_048a;
	}

IL_008d:
	{
		ObjectU5BU5D_t3614634134* L_5 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)6));
		int64_t L_6 = __this->get_dataOffset_1();
		int64_t L_7 = L_6;
		Il2CppObject * L_8 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_8);
		ObjectU5BU5D_t3614634134* L_9 = L_5;
		int64_t L_10 = __this->get_dataLength_2();
		int64_t L_11 = L_10;
		Il2CppObject * L_12 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = L_9;
		int64_t L_14 = __this->get_lengthFieldBytes_3();
		int64_t L_15 = L_14;
		Il2CppObject * L_16 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 2);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_16);
		ObjectU5BU5D_t3614634134* L_17 = L_13;
		Asn1Node_t1770761751 * L_18 = ___startNode0;
		String_t* L_19 = Asn1Node_GetIndentStr_m4071159475(__this, L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 3);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_19);
		ObjectU5BU5D_t3614634134* L_20 = L_17;
		String_t* L_21 = Asn1Node_get_TagName_m1549199302(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 4);
		ArrayElementTypeCheck (L_20, L_21);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_21);
		ObjectU5BU5D_t3614634134* L_22 = L_20;
		uint8_t L_23 = __this->get_unusedBits_6();
		uint8_t L_24 = L_23;
		Il2CppObject * L_25 = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &L_24);
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 5);
		ArrayElementTypeCheck (L_22, L_25);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_25);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Format_m1263743648(NULL /*static, unused*/, _stringLiteral3047465420, L_22, /*hidden argument*/NULL);
		V_1 = L_26;
		ByteU5BU5D_t3397334013* L_27 = __this->get_data_4();
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		String_t* L_28 = Asn1Util_ToHexString_m3932902392(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		V_2 = L_28;
		String_t* L_29 = V_1;
		NullCheck(L_29);
		int32_t L_30 = String_get_Length_m1606060069(L_29, /*hidden argument*/NULL);
		String_t* L_31 = V_2;
		NullCheck(L_31);
		int32_t L_32 = String_get_Length_m1606060069(L_31, /*hidden argument*/NULL);
		int32_t L_33 = ___lineLen1;
		if ((((int32_t)((int32_t)((int32_t)L_30+(int32_t)L_32))) >= ((int32_t)L_33)))
		{
			goto IL_015d;
		}
	}
	{
		String_t* L_34 = V_2;
		NullCheck(L_34);
		int32_t L_35 = String_get_Length_m1606060069(L_34, /*hidden argument*/NULL);
		if ((((int32_t)L_35) >= ((int32_t)1)))
		{
			goto IL_0129;
		}
	}
	{
		String_t* L_36 = V_0;
		String_t* L_37 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_38 = String_Concat_m612901809(NULL /*static, unused*/, L_36, L_37, _stringLiteral2162321587, /*hidden argument*/NULL);
		V_0 = L_38;
		goto IL_0157;
	}

IL_0129:
	{
		String_t* L_39 = V_0;
		V_6 = L_39;
		StringU5BU5D_t1642385972* L_40 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		String_t* L_41 = V_6;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 0);
		ArrayElementTypeCheck (L_40, L_41);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_41);
		StringU5BU5D_t1642385972* L_42 = L_40;
		String_t* L_43 = V_1;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 1);
		ArrayElementTypeCheck (L_42, L_43);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_43);
		StringU5BU5D_t1642385972* L_44 = L_42;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, 2);
		ArrayElementTypeCheck (L_44, _stringLiteral372029307);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029307);
		StringU5BU5D_t1642385972* L_45 = L_44;
		String_t* L_46 = V_2;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 3);
		ArrayElementTypeCheck (L_45, L_46);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_46);
		StringU5BU5D_t1642385972* L_47 = L_45;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, 4);
		ArrayElementTypeCheck (L_47, _stringLiteral4090317072);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral4090317072);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = String_Concat_m626692867(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		V_0 = L_48;
	}

IL_0157:
	{
		goto IL_018a;
	}

IL_015d:
	{
		String_t* L_49 = V_0;
		String_t* L_50 = V_1;
		Asn1Node_t1770761751 * L_51 = ___startNode0;
		String_t* L_52 = Asn1Node_GetIndentStr_m4071159475(__this, L_51, /*hidden argument*/NULL);
		NullCheck(L_52);
		int32_t L_53 = String_get_Length_m1606060069(L_52, /*hidden argument*/NULL);
		int32_t L_54 = ___lineLen1;
		String_t* L_55 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_56 = String_Concat_m2596409543(NULL /*static, unused*/, L_55, _stringLiteral2162321587, /*hidden argument*/NULL);
		String_t* L_57 = Asn1Node_FormatLineHexString_m3369492261(__this, _stringLiteral713096914, L_53, L_54, L_56, /*hidden argument*/NULL);
		String_t* L_58 = String_Concat_m612901809(NULL /*static, unused*/, L_49, L_50, L_57, /*hidden argument*/NULL);
		V_0 = L_58;
	}

IL_018a:
	{
		goto IL_05d2;
	}

IL_018f:
	{
		Oid_t113668572 * L_59 = (Oid_t113668572 *)il2cpp_codegen_object_new(Oid_t113668572_il2cpp_TypeInfo_var);
		Oid__ctor_m3384108428(L_59, /*hidden argument*/NULL);
		V_7 = L_59;
		Oid_t113668572 * L_60 = V_7;
		ByteU5BU5D_t3397334013* L_61 = __this->get_data_4();
		MemoryStream_t743994179 * L_62 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m4073175573(L_62, L_61, /*hidden argument*/NULL);
		NullCheck(L_60);
		String_t* L_63 = VirtFuncInvoker1< String_t*, Stream_t3255436806 * >::Invoke(4 /* System.String LipingShare.LCLib.Asn1Processor.Oid::Decode(System.IO.Stream) */, L_60, L_62);
		V_3 = L_63;
		Oid_t113668572 * L_64 = V_7;
		String_t* L_65 = V_3;
		NullCheck(L_64);
		String_t* L_66 = Oid_GetOidName_m1983955852(L_64, L_65, /*hidden argument*/NULL);
		V_4 = L_66;
		String_t* L_67 = V_0;
		ObjectU5BU5D_t3614634134* L_68 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)7));
		int64_t L_69 = __this->get_dataOffset_1();
		int64_t L_70 = L_69;
		Il2CppObject * L_71 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_70);
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, 0);
		ArrayElementTypeCheck (L_68, L_71);
		(L_68)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_71);
		ObjectU5BU5D_t3614634134* L_72 = L_68;
		int64_t L_73 = __this->get_dataLength_2();
		int64_t L_74 = L_73;
		Il2CppObject * L_75 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_74);
		NullCheck(L_72);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_72, 1);
		ArrayElementTypeCheck (L_72, L_75);
		(L_72)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_75);
		ObjectU5BU5D_t3614634134* L_76 = L_72;
		int64_t L_77 = __this->get_lengthFieldBytes_3();
		int64_t L_78 = L_77;
		Il2CppObject * L_79 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_78);
		NullCheck(L_76);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_76, 2);
		ArrayElementTypeCheck (L_76, L_79);
		(L_76)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_79);
		ObjectU5BU5D_t3614634134* L_80 = L_76;
		Asn1Node_t1770761751 * L_81 = ___startNode0;
		String_t* L_82 = Asn1Node_GetIndentStr_m4071159475(__this, L_81, /*hidden argument*/NULL);
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, 3);
		ArrayElementTypeCheck (L_80, L_82);
		(L_80)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_82);
		ObjectU5BU5D_t3614634134* L_83 = L_80;
		String_t* L_84 = Asn1Node_get_TagName_m1549199302(__this, /*hidden argument*/NULL);
		NullCheck(L_83);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_83, 4);
		ArrayElementTypeCheck (L_83, L_84);
		(L_83)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_84);
		ObjectU5BU5D_t3614634134* L_85 = L_83;
		String_t* L_86 = V_4;
		NullCheck(L_85);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_85, 5);
		ArrayElementTypeCheck (L_85, L_86);
		(L_85)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_86);
		ObjectU5BU5D_t3614634134* L_87 = L_85;
		String_t* L_88 = V_3;
		NullCheck(L_87);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_87, 6);
		ArrayElementTypeCheck (L_87, L_88);
		(L_87)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_88);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_89 = String_Format_m1263743648(NULL /*static, unused*/, _stringLiteral178121469, L_87, /*hidden argument*/NULL);
		String_t* L_90 = String_Concat_m2596409543(NULL /*static, unused*/, L_67, L_89, /*hidden argument*/NULL);
		V_0 = L_90;
		goto IL_05d2;
	}

IL_0215:
	{
		RelativeOid_t880150712 * L_91 = (RelativeOid_t880150712 *)il2cpp_codegen_object_new(RelativeOid_t880150712_il2cpp_TypeInfo_var);
		RelativeOid__ctor_m2864312816(L_91, /*hidden argument*/NULL);
		V_8 = L_91;
		RelativeOid_t880150712 * L_92 = V_8;
		ByteU5BU5D_t3397334013* L_93 = __this->get_data_4();
		MemoryStream_t743994179 * L_94 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m4073175573(L_94, L_93, /*hidden argument*/NULL);
		NullCheck(L_92);
		String_t* L_95 = VirtFuncInvoker1< String_t*, Stream_t3255436806 * >::Invoke(4 /* System.String LipingShare.LCLib.Asn1Processor.Oid::Decode(System.IO.Stream) */, L_92, L_94);
		V_3 = L_95;
		V_4 = _stringLiteral371857150;
		String_t* L_96 = V_0;
		ObjectU5BU5D_t3614634134* L_97 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)7));
		int64_t L_98 = __this->get_dataOffset_1();
		int64_t L_99 = L_98;
		Il2CppObject * L_100 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_99);
		NullCheck(L_97);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_97, 0);
		ArrayElementTypeCheck (L_97, L_100);
		(L_97)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_100);
		ObjectU5BU5D_t3614634134* L_101 = L_97;
		int64_t L_102 = __this->get_dataLength_2();
		int64_t L_103 = L_102;
		Il2CppObject * L_104 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_103);
		NullCheck(L_101);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_101, 1);
		ArrayElementTypeCheck (L_101, L_104);
		(L_101)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_104);
		ObjectU5BU5D_t3614634134* L_105 = L_101;
		int64_t L_106 = __this->get_lengthFieldBytes_3();
		int64_t L_107 = L_106;
		Il2CppObject * L_108 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_107);
		NullCheck(L_105);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_105, 2);
		ArrayElementTypeCheck (L_105, L_108);
		(L_105)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_108);
		ObjectU5BU5D_t3614634134* L_109 = L_105;
		Asn1Node_t1770761751 * L_110 = ___startNode0;
		String_t* L_111 = Asn1Node_GetIndentStr_m4071159475(__this, L_110, /*hidden argument*/NULL);
		NullCheck(L_109);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_109, 3);
		ArrayElementTypeCheck (L_109, L_111);
		(L_109)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_111);
		ObjectU5BU5D_t3614634134* L_112 = L_109;
		String_t* L_113 = Asn1Node_get_TagName_m1549199302(__this, /*hidden argument*/NULL);
		NullCheck(L_112);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_112, 4);
		ArrayElementTypeCheck (L_112, L_113);
		(L_112)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_113);
		ObjectU5BU5D_t3614634134* L_114 = L_112;
		String_t* L_115 = V_4;
		NullCheck(L_114);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_114, 5);
		ArrayElementTypeCheck (L_114, L_115);
		(L_114)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_115);
		ObjectU5BU5D_t3614634134* L_116 = L_114;
		String_t* L_117 = V_3;
		NullCheck(L_116);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_116, 6);
		ArrayElementTypeCheck (L_116, L_117);
		(L_116)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_117);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_118 = String_Format_m1263743648(NULL /*static, unused*/, _stringLiteral178121469, L_116, /*hidden argument*/NULL);
		String_t* L_119 = String_Concat_m2596409543(NULL /*static, unused*/, L_96, L_118, /*hidden argument*/NULL);
		V_0 = L_119;
		goto IL_05d2;
	}

IL_0298:
	{
		ObjectU5BU5D_t3614634134* L_120 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		int64_t L_121 = __this->get_dataOffset_1();
		int64_t L_122 = L_121;
		Il2CppObject * L_123 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_122);
		NullCheck(L_120);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_120, 0);
		ArrayElementTypeCheck (L_120, L_123);
		(L_120)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_123);
		ObjectU5BU5D_t3614634134* L_124 = L_120;
		int64_t L_125 = __this->get_dataLength_2();
		int64_t L_126 = L_125;
		Il2CppObject * L_127 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_126);
		NullCheck(L_124);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_124, 1);
		ArrayElementTypeCheck (L_124, L_127);
		(L_124)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_127);
		ObjectU5BU5D_t3614634134* L_128 = L_124;
		int64_t L_129 = __this->get_lengthFieldBytes_3();
		int64_t L_130 = L_129;
		Il2CppObject * L_131 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_130);
		NullCheck(L_128);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_128, 2);
		ArrayElementTypeCheck (L_128, L_131);
		(L_128)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_131);
		ObjectU5BU5D_t3614634134* L_132 = L_128;
		Asn1Node_t1770761751 * L_133 = ___startNode0;
		String_t* L_134 = Asn1Node_GetIndentStr_m4071159475(__this, L_133, /*hidden argument*/NULL);
		NullCheck(L_132);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_132, 3);
		ArrayElementTypeCheck (L_132, L_134);
		(L_132)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_134);
		ObjectU5BU5D_t3614634134* L_135 = L_132;
		String_t* L_136 = Asn1Node_get_TagName_m1549199302(__this, /*hidden argument*/NULL);
		NullCheck(L_135);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_135, 4);
		ArrayElementTypeCheck (L_135, L_136);
		(L_135)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_136);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_137 = String_Format_m1263743648(NULL /*static, unused*/, _stringLiteral1018099543, L_135, /*hidden argument*/NULL);
		V_1 = L_137;
		uint8_t L_138 = __this->get_tag_0();
		if ((!(((uint32_t)L_138) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_030f;
		}
	}
	{
		UTF8Encoding_t111055448 * L_139 = (UTF8Encoding_t111055448 *)il2cpp_codegen_object_new(UTF8Encoding_t111055448_il2cpp_TypeInfo_var);
		UTF8Encoding__ctor_m100325490(L_139, /*hidden argument*/NULL);
		V_9 = L_139;
		UTF8Encoding_t111055448 * L_140 = V_9;
		ByteU5BU5D_t3397334013* L_141 = __this->get_data_4();
		NullCheck(L_140);
		String_t* L_142 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_140, L_141);
		V_2 = L_142;
		goto IL_031d;
	}

IL_030f:
	{
		ByteU5BU5D_t3397334013* L_143 = __this->get_data_4();
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		String_t* L_144 = Asn1Util_BytesToString_m2403670248(NULL /*static, unused*/, L_143, /*hidden argument*/NULL);
		V_2 = L_144;
	}

IL_031d:
	{
		String_t* L_145 = V_1;
		NullCheck(L_145);
		int32_t L_146 = String_get_Length_m1606060069(L_145, /*hidden argument*/NULL);
		String_t* L_147 = V_2;
		NullCheck(L_147);
		int32_t L_148 = String_get_Length_m1606060069(L_147, /*hidden argument*/NULL);
		int32_t L_149 = ___lineLen1;
		if ((((int32_t)((int32_t)((int32_t)L_146+(int32_t)L_148))) >= ((int32_t)L_149)))
		{
			goto IL_0363;
		}
	}
	{
		String_t* L_150 = V_0;
		V_6 = L_150;
		StringU5BU5D_t1642385972* L_151 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		String_t* L_152 = V_6;
		NullCheck(L_151);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_151, 0);
		ArrayElementTypeCheck (L_151, L_152);
		(L_151)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_152);
		StringU5BU5D_t1642385972* L_153 = L_151;
		String_t* L_154 = V_1;
		NullCheck(L_153);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_153, 1);
		ArrayElementTypeCheck (L_153, L_154);
		(L_153)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_154);
		StringU5BU5D_t1642385972* L_155 = L_153;
		NullCheck(L_155);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_155, 2);
		ArrayElementTypeCheck (L_155, _stringLiteral372029307);
		(L_155)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029307);
		StringU5BU5D_t1642385972* L_156 = L_155;
		String_t* L_157 = V_2;
		NullCheck(L_156);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_156, 3);
		ArrayElementTypeCheck (L_156, L_157);
		(L_156)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_157);
		StringU5BU5D_t1642385972* L_158 = L_156;
		NullCheck(L_158);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_158, 4);
		ArrayElementTypeCheck (L_158, _stringLiteral4090317072);
		(L_158)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral4090317072);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_159 = String_Concat_m626692867(NULL /*static, unused*/, L_158, /*hidden argument*/NULL);
		V_0 = L_159;
		goto IL_038b;
	}

IL_0363:
	{
		String_t* L_160 = V_0;
		String_t* L_161 = V_1;
		Asn1Node_t1770761751 * L_162 = ___startNode0;
		String_t* L_163 = Asn1Node_GetIndentStr_m4071159475(__this, L_162, /*hidden argument*/NULL);
		NullCheck(L_163);
		int32_t L_164 = String_get_Length_m1606060069(L_163, /*hidden argument*/NULL);
		int32_t L_165 = ___lineLen1;
		String_t* L_166 = V_2;
		String_t* L_167 = Asn1Node_FormatLineString_m3353585152(__this, _stringLiteral713096914, L_164, L_165, L_166, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_168 = String_Concat_m1561703559(NULL /*static, unused*/, L_160, L_161, L_167, _stringLiteral2162321587, /*hidden argument*/NULL);
		V_0 = L_168;
	}

IL_038b:
	{
		goto IL_05d2;
	}

IL_0390:
	{
		ByteU5BU5D_t3397334013* L_169 = __this->get_data_4();
		if (!L_169)
		{
			goto IL_0420;
		}
	}
	{
		int64_t L_170 = __this->get_dataLength_2();
		if ((((int64_t)L_170) >= ((int64_t)(((int64_t)((int64_t)8))))))
		{
			goto IL_0420;
		}
	}
	{
		String_t* L_171 = V_0;
		ObjectU5BU5D_t3614634134* L_172 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)6));
		int64_t L_173 = __this->get_dataOffset_1();
		int64_t L_174 = L_173;
		Il2CppObject * L_175 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_174);
		NullCheck(L_172);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_172, 0);
		ArrayElementTypeCheck (L_172, L_175);
		(L_172)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_175);
		ObjectU5BU5D_t3614634134* L_176 = L_172;
		int64_t L_177 = __this->get_dataLength_2();
		int64_t L_178 = L_177;
		Il2CppObject * L_179 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_178);
		NullCheck(L_176);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_176, 1);
		ArrayElementTypeCheck (L_176, L_179);
		(L_176)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_179);
		ObjectU5BU5D_t3614634134* L_180 = L_176;
		int64_t L_181 = __this->get_lengthFieldBytes_3();
		int64_t L_182 = L_181;
		Il2CppObject * L_183 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_182);
		NullCheck(L_180);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_180, 2);
		ArrayElementTypeCheck (L_180, L_183);
		(L_180)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_183);
		ObjectU5BU5D_t3614634134* L_184 = L_180;
		Asn1Node_t1770761751 * L_185 = ___startNode0;
		String_t* L_186 = Asn1Node_GetIndentStr_m4071159475(__this, L_185, /*hidden argument*/NULL);
		NullCheck(L_184);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_184, 3);
		ArrayElementTypeCheck (L_184, L_186);
		(L_184)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_186);
		ObjectU5BU5D_t3614634134* L_187 = L_184;
		String_t* L_188 = Asn1Node_get_TagName_m1549199302(__this, /*hidden argument*/NULL);
		NullCheck(L_187);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_187, 4);
		ArrayElementTypeCheck (L_187, L_188);
		(L_187)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_188);
		ObjectU5BU5D_t3614634134* L_189 = L_187;
		ByteU5BU5D_t3397334013* L_190 = __this->get_data_4();
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		int64_t L_191 = Asn1Util_BytesToLong_m1963652187(NULL /*static, unused*/, L_190, /*hidden argument*/NULL);
		V_10 = L_191;
		String_t* L_192 = Int64_ToString_m689375889((&V_10), /*hidden argument*/NULL);
		NullCheck(L_189);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_189, 5);
		ArrayElementTypeCheck (L_189, L_192);
		(L_189)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_192);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_193 = String_Format_m1263743648(NULL /*static, unused*/, _stringLiteral4040664675, L_189, /*hidden argument*/NULL);
		String_t* L_194 = String_Concat_m2596409543(NULL /*static, unused*/, L_171, L_193, /*hidden argument*/NULL);
		V_0 = L_194;
		goto IL_0485;
	}

IL_0420:
	{
		ObjectU5BU5D_t3614634134* L_195 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		int64_t L_196 = __this->get_dataOffset_1();
		int64_t L_197 = L_196;
		Il2CppObject * L_198 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_197);
		NullCheck(L_195);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_195, 0);
		ArrayElementTypeCheck (L_195, L_198);
		(L_195)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_198);
		ObjectU5BU5D_t3614634134* L_199 = L_195;
		int64_t L_200 = __this->get_dataLength_2();
		int64_t L_201 = L_200;
		Il2CppObject * L_202 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_201);
		NullCheck(L_199);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_199, 1);
		ArrayElementTypeCheck (L_199, L_202);
		(L_199)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_202);
		ObjectU5BU5D_t3614634134* L_203 = L_199;
		int64_t L_204 = __this->get_lengthFieldBytes_3();
		int64_t L_205 = L_204;
		Il2CppObject * L_206 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_205);
		NullCheck(L_203);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_203, 2);
		ArrayElementTypeCheck (L_203, L_206);
		(L_203)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_206);
		ObjectU5BU5D_t3614634134* L_207 = L_203;
		Asn1Node_t1770761751 * L_208 = ___startNode0;
		String_t* L_209 = Asn1Node_GetIndentStr_m4071159475(__this, L_208, /*hidden argument*/NULL);
		NullCheck(L_207);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_207, 3);
		ArrayElementTypeCheck (L_207, L_209);
		(L_207)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_209);
		ObjectU5BU5D_t3614634134* L_210 = L_207;
		String_t* L_211 = Asn1Node_get_TagName_m1549199302(__this, /*hidden argument*/NULL);
		NullCheck(L_210);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_210, 4);
		ArrayElementTypeCheck (L_210, L_211);
		(L_210)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_211);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_212 = String_Format_m1263743648(NULL /*static, unused*/, _stringLiteral1018099543, L_210, /*hidden argument*/NULL);
		V_1 = L_212;
		String_t* L_213 = V_0;
		Asn1Node_t1770761751 * L_214 = ___startNode0;
		String_t* L_215 = V_1;
		int32_t L_216 = ___lineLen1;
		String_t* L_217 = Asn1Node_GetHexPrintingStr_m134819188(__this, L_214, L_215, _stringLiteral713096914, L_216, /*hidden argument*/NULL);
		String_t* L_218 = String_Concat_m2596409543(NULL /*static, unused*/, L_213, L_217, /*hidden argument*/NULL);
		V_0 = L_218;
	}

IL_0485:
	{
		goto IL_05d2;
	}

IL_048a:
	{
		uint8_t L_219 = __this->get_tag_0();
		if ((!(((uint32_t)((int32_t)((int32_t)L_219&(int32_t)((int32_t)31)))) == ((uint32_t)6))))
		{
			goto IL_0568;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_220 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		int64_t L_221 = __this->get_dataOffset_1();
		int64_t L_222 = L_221;
		Il2CppObject * L_223 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_222);
		NullCheck(L_220);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_220, 0);
		ArrayElementTypeCheck (L_220, L_223);
		(L_220)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_223);
		ObjectU5BU5D_t3614634134* L_224 = L_220;
		int64_t L_225 = __this->get_dataLength_2();
		int64_t L_226 = L_225;
		Il2CppObject * L_227 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_226);
		NullCheck(L_224);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_224, 1);
		ArrayElementTypeCheck (L_224, L_227);
		(L_224)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_227);
		ObjectU5BU5D_t3614634134* L_228 = L_224;
		int64_t L_229 = __this->get_lengthFieldBytes_3();
		int64_t L_230 = L_229;
		Il2CppObject * L_231 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_230);
		NullCheck(L_228);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_228, 2);
		ArrayElementTypeCheck (L_228, L_231);
		(L_228)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_231);
		ObjectU5BU5D_t3614634134* L_232 = L_228;
		Asn1Node_t1770761751 * L_233 = ___startNode0;
		String_t* L_234 = Asn1Node_GetIndentStr_m4071159475(__this, L_233, /*hidden argument*/NULL);
		NullCheck(L_232);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_232, 3);
		ArrayElementTypeCheck (L_232, L_234);
		(L_232)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_234);
		ObjectU5BU5D_t3614634134* L_235 = L_232;
		String_t* L_236 = Asn1Node_get_TagName_m1549199302(__this, /*hidden argument*/NULL);
		NullCheck(L_235);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_235, 4);
		ArrayElementTypeCheck (L_235, L_236);
		(L_235)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_236);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_237 = String_Format_m1263743648(NULL /*static, unused*/, _stringLiteral1018099543, L_235, /*hidden argument*/NULL);
		V_1 = L_237;
		ByteU5BU5D_t3397334013* L_238 = __this->get_data_4();
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		String_t* L_239 = Asn1Util_BytesToString_m2403670248(NULL /*static, unused*/, L_238, /*hidden argument*/NULL);
		V_2 = L_239;
		String_t* L_240 = V_1;
		NullCheck(L_240);
		int32_t L_241 = String_get_Length_m1606060069(L_240, /*hidden argument*/NULL);
		String_t* L_242 = V_2;
		NullCheck(L_242);
		int32_t L_243 = String_get_Length_m1606060069(L_242, /*hidden argument*/NULL);
		int32_t L_244 = ___lineLen1;
		if ((((int32_t)((int32_t)((int32_t)L_241+(int32_t)L_243))) >= ((int32_t)L_244)))
		{
			goto IL_053a;
		}
	}
	{
		String_t* L_245 = V_0;
		V_6 = L_245;
		StringU5BU5D_t1642385972* L_246 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		String_t* L_247 = V_6;
		NullCheck(L_246);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_246, 0);
		ArrayElementTypeCheck (L_246, L_247);
		(L_246)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_247);
		StringU5BU5D_t1642385972* L_248 = L_246;
		String_t* L_249 = V_1;
		NullCheck(L_248);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_248, 1);
		ArrayElementTypeCheck (L_248, L_249);
		(L_248)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_249);
		StringU5BU5D_t1642385972* L_250 = L_248;
		NullCheck(L_250);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_250, 2);
		ArrayElementTypeCheck (L_250, _stringLiteral372029307);
		(L_250)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029307);
		StringU5BU5D_t1642385972* L_251 = L_250;
		String_t* L_252 = V_2;
		NullCheck(L_251);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_251, 3);
		ArrayElementTypeCheck (L_251, L_252);
		(L_251)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_252);
		StringU5BU5D_t1642385972* L_253 = L_251;
		NullCheck(L_253);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_253, 4);
		ArrayElementTypeCheck (L_253, _stringLiteral4090317072);
		(L_253)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral4090317072);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_254 = String_Concat_m626692867(NULL /*static, unused*/, L_253, /*hidden argument*/NULL);
		V_0 = L_254;
		goto IL_0562;
	}

IL_053a:
	{
		String_t* L_255 = V_0;
		String_t* L_256 = V_1;
		Asn1Node_t1770761751 * L_257 = ___startNode0;
		String_t* L_258 = Asn1Node_GetIndentStr_m4071159475(__this, L_257, /*hidden argument*/NULL);
		NullCheck(L_258);
		int32_t L_259 = String_get_Length_m1606060069(L_258, /*hidden argument*/NULL);
		int32_t L_260 = ___lineLen1;
		String_t* L_261 = V_2;
		String_t* L_262 = Asn1Node_FormatLineString_m3353585152(__this, _stringLiteral713096914, L_259, L_260, L_261, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_263 = String_Concat_m1561703559(NULL /*static, unused*/, L_255, L_256, L_262, _stringLiteral2162321587, /*hidden argument*/NULL);
		V_0 = L_263;
	}

IL_0562:
	{
		goto IL_05cd;
	}

IL_0568:
	{
		ObjectU5BU5D_t3614634134* L_264 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		int64_t L_265 = __this->get_dataOffset_1();
		int64_t L_266 = L_265;
		Il2CppObject * L_267 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_266);
		NullCheck(L_264);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_264, 0);
		ArrayElementTypeCheck (L_264, L_267);
		(L_264)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_267);
		ObjectU5BU5D_t3614634134* L_268 = L_264;
		int64_t L_269 = __this->get_dataLength_2();
		int64_t L_270 = L_269;
		Il2CppObject * L_271 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_270);
		NullCheck(L_268);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_268, 1);
		ArrayElementTypeCheck (L_268, L_271);
		(L_268)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_271);
		ObjectU5BU5D_t3614634134* L_272 = L_268;
		int64_t L_273 = __this->get_lengthFieldBytes_3();
		int64_t L_274 = L_273;
		Il2CppObject * L_275 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_274);
		NullCheck(L_272);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_272, 2);
		ArrayElementTypeCheck (L_272, L_275);
		(L_272)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_275);
		ObjectU5BU5D_t3614634134* L_276 = L_272;
		Asn1Node_t1770761751 * L_277 = ___startNode0;
		String_t* L_278 = Asn1Node_GetIndentStr_m4071159475(__this, L_277, /*hidden argument*/NULL);
		NullCheck(L_276);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_276, 3);
		ArrayElementTypeCheck (L_276, L_278);
		(L_276)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_278);
		ObjectU5BU5D_t3614634134* L_279 = L_276;
		String_t* L_280 = Asn1Node_get_TagName_m1549199302(__this, /*hidden argument*/NULL);
		NullCheck(L_279);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_279, 4);
		ArrayElementTypeCheck (L_279, L_280);
		(L_279)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_280);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_281 = String_Format_m1263743648(NULL /*static, unused*/, _stringLiteral1018099543, L_279, /*hidden argument*/NULL);
		V_1 = L_281;
		String_t* L_282 = V_0;
		Asn1Node_t1770761751 * L_283 = ___startNode0;
		String_t* L_284 = V_1;
		int32_t L_285 = ___lineLen1;
		String_t* L_286 = Asn1Node_GetHexPrintingStr_m134819188(__this, L_283, L_284, _stringLiteral713096914, L_285, /*hidden argument*/NULL);
		String_t* L_287 = String_Concat_m2596409543(NULL /*static, unused*/, L_282, L_286, /*hidden argument*/NULL);
		V_0 = L_287;
	}

IL_05cd:
	{
		goto IL_05d2;
	}

IL_05d2:
	{
		ArrayList_t4252133567 * L_288 = __this->get_childNodeList_5();
		NullCheck(L_288);
		int32_t L_289 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_288);
		if ((((int32_t)L_289) < ((int32_t)0)))
		{
			goto IL_05f4;
		}
	}
	{
		String_t* L_290 = V_0;
		Asn1Node_t1770761751 * L_291 = ___startNode0;
		int32_t L_292 = ___lineLen1;
		String_t* L_293 = Asn1Node_GetListStr_m3438846266(__this, L_291, L_292, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_294 = String_Concat_m2596409543(NULL /*static, unused*/, L_290, L_293, /*hidden argument*/NULL);
		V_0 = L_294;
	}

IL_05f4:
	{
		String_t* L_295 = V_0;
		V_11 = L_295;
		goto IL_05fc;
	}

IL_05fc:
	{
		String_t* L_296 = V_11;
		return L_296;
	}
}
// System.String LipingShare.LCLib.Asn1Processor.Asn1Node::GetDataStr(System.Boolean)
extern Il2CppClass* Asn1Util_t2059476207_il2cpp_TypeInfo_var;
extern Il2CppClass* Oid_t113668572_il2cpp_TypeInfo_var;
extern Il2CppClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern Il2CppClass* RelativeOid_t880150712_il2cpp_TypeInfo_var;
extern Il2CppClass* UTF8Encoding_t111055448_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern const uint32_t Asn1Node_GetDataStr_m2973067886_MetadataUsageId;
extern "C"  String_t* Asn1Node_GetDataStr_m2973067886 (Asn1Node_t1770761751 * __this, bool ___pureHexMode0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Node_GetDataStr_m2973067886_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	uint8_t V_1 = 0x0;
	Oid_t113668572 * V_2 = NULL;
	RelativeOid_t880150712 * V_3 = NULL;
	UTF8Encoding_t111055448 * V_4 = NULL;
	String_t* V_5 = NULL;
	{
		V_0 = _stringLiteral371857150;
		bool L_0 = ___pureHexMode0;
		if (!L_0)
		{
			goto IL_0028;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_1 = __this->get_data_4();
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		String_t* L_2 = Asn1Util_ToHexString_m3932902392(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_3 = Asn1Util_FormatString_m1519920834(NULL /*static, unused*/, L_2, ((int32_t)32), 2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0173;
	}

IL_0028:
	{
		uint8_t L_4 = __this->get_tag_0();
		V_1 = L_4;
		uint8_t L_5 = V_1;
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)18))) == 0)
		{
			goto IL_00f1;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)18))) == 1)
		{
			goto IL_00f1;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)18))) == 2)
		{
			goto IL_006d;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)18))) == 3)
		{
			goto IL_006d;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)18))) == 4)
		{
			goto IL_00f1;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)18))) == 5)
		{
			goto IL_00f1;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)18))) == 6)
		{
			goto IL_00f1;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)18))) == 7)
		{
			goto IL_006d;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)18))) == 8)
		{
			goto IL_00f1;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)18))) == 9)
		{
			goto IL_00f1;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)18))) == 10)
		{
			goto IL_00f1;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)18))) == 11)
		{
			goto IL_006d;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)18))) == 12)
		{
			goto IL_00f1;
		}
	}

IL_006d:
	{
		uint8_t L_6 = V_1;
		if (((int32_t)((int32_t)L_6-(int32_t)2)) == 0)
		{
			goto IL_011c;
		}
		if (((int32_t)((int32_t)L_6-(int32_t)2)) == 1)
		{
			goto IL_009e;
		}
		if (((int32_t)((int32_t)L_6-(int32_t)2)) == 2)
		{
			goto IL_0089;
		}
		if (((int32_t)((int32_t)L_6-(int32_t)2)) == 3)
		{
			goto IL_0089;
		}
		if (((int32_t)((int32_t)L_6-(int32_t)2)) == 4)
		{
			goto IL_00b7;
		}
	}

IL_0089:
	{
		uint8_t L_7 = V_1;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)12))))
		{
			goto IL_0102;
		}
	}
	{
		uint8_t L_8 = V_1;
		if ((((int32_t)L_8) == ((int32_t)((int32_t)13))))
		{
			goto IL_00d4;
		}
	}
	{
		goto IL_0135;
	}

IL_009e:
	{
		ByteU5BU5D_t3397334013* L_9 = __this->get_data_4();
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		String_t* L_10 = Asn1Util_ToHexString_m3932902392(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		String_t* L_11 = Asn1Util_FormatString_m1519920834(NULL /*static, unused*/, L_10, ((int32_t)32), 2, /*hidden argument*/NULL);
		V_0 = L_11;
		goto IL_0172;
	}

IL_00b7:
	{
		Oid_t113668572 * L_12 = (Oid_t113668572 *)il2cpp_codegen_object_new(Oid_t113668572_il2cpp_TypeInfo_var);
		Oid__ctor_m3384108428(L_12, /*hidden argument*/NULL);
		V_2 = L_12;
		Oid_t113668572 * L_13 = V_2;
		ByteU5BU5D_t3397334013* L_14 = __this->get_data_4();
		MemoryStream_t743994179 * L_15 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m4073175573(L_15, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		String_t* L_16 = VirtFuncInvoker1< String_t*, Stream_t3255436806 * >::Invoke(4 /* System.String LipingShare.LCLib.Asn1Processor.Oid::Decode(System.IO.Stream) */, L_13, L_15);
		V_0 = L_16;
		goto IL_0172;
	}

IL_00d4:
	{
		RelativeOid_t880150712 * L_17 = (RelativeOid_t880150712 *)il2cpp_codegen_object_new(RelativeOid_t880150712_il2cpp_TypeInfo_var);
		RelativeOid__ctor_m2864312816(L_17, /*hidden argument*/NULL);
		V_3 = L_17;
		RelativeOid_t880150712 * L_18 = V_3;
		ByteU5BU5D_t3397334013* L_19 = __this->get_data_4();
		MemoryStream_t743994179 * L_20 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m4073175573(L_20, L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		String_t* L_21 = VirtFuncInvoker1< String_t*, Stream_t3255436806 * >::Invoke(4 /* System.String LipingShare.LCLib.Asn1Processor.Oid::Decode(System.IO.Stream) */, L_18, L_20);
		V_0 = L_21;
		goto IL_0172;
	}

IL_00f1:
	{
		ByteU5BU5D_t3397334013* L_22 = __this->get_data_4();
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		String_t* L_23 = Asn1Util_BytesToString_m2403670248(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		V_0 = L_23;
		goto IL_0172;
	}

IL_0102:
	{
		UTF8Encoding_t111055448 * L_24 = (UTF8Encoding_t111055448 *)il2cpp_codegen_object_new(UTF8Encoding_t111055448_il2cpp_TypeInfo_var);
		UTF8Encoding__ctor_m100325490(L_24, /*hidden argument*/NULL);
		V_4 = L_24;
		UTF8Encoding_t111055448 * L_25 = V_4;
		ByteU5BU5D_t3397334013* L_26 = __this->get_data_4();
		NullCheck(L_25);
		String_t* L_27 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_25, L_26);
		V_0 = L_27;
		goto IL_0172;
	}

IL_011c:
	{
		ByteU5BU5D_t3397334013* L_28 = __this->get_data_4();
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		String_t* L_29 = Asn1Util_ToHexString_m3932902392(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		String_t* L_30 = Asn1Util_FormatString_m1519920834(NULL /*static, unused*/, L_29, ((int32_t)32), 2, /*hidden argument*/NULL);
		V_0 = L_30;
		goto IL_0172;
	}

IL_0135:
	{
		uint8_t L_31 = __this->get_tag_0();
		if ((!(((uint32_t)((int32_t)((int32_t)L_31&(int32_t)((int32_t)31)))) == ((uint32_t)6))))
		{
			goto IL_0157;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_32 = __this->get_data_4();
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		String_t* L_33 = Asn1Util_BytesToString_m2403670248(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		V_0 = L_33;
		goto IL_016d;
	}

IL_0157:
	{
		ByteU5BU5D_t3397334013* L_34 = __this->get_data_4();
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		String_t* L_35 = Asn1Util_ToHexString_m3932902392(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		String_t* L_36 = Asn1Util_FormatString_m1519920834(NULL /*static, unused*/, L_35, ((int32_t)32), 2, /*hidden argument*/NULL);
		V_0 = L_36;
	}

IL_016d:
	{
		goto IL_0172;
	}

IL_0172:
	{
	}

IL_0173:
	{
		String_t* L_37 = V_0;
		V_5 = L_37;
		goto IL_017b;
	}

IL_017b:
	{
		String_t* L_38 = V_5;
		return L_38;
	}
}
// System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Node::get_DataLength()
extern "C"  int64_t Asn1Node_get_DataLength_m4095104165 (Asn1Node_t1770761751 * __this, const MethodInfo* method)
{
	int64_t V_0 = 0;
	{
		int64_t L_0 = __this->get_dataLength_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int64_t L_1 = V_0;
		return L_1;
	}
}
// System.Byte[] LipingShare.LCLib.Asn1Processor.Asn1Node::get_Data()
extern Il2CppClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern const uint32_t Asn1Node_get_Data_m1676178324_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* Asn1Node_get_Data_m1676178324 (Asn1Node_t1770761751 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Node_get_Data_m1676178324_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MemoryStream_t743994179 * V_0 = NULL;
	int64_t V_1 = 0;
	Asn1Node_t1770761751 * V_2 = NULL;
	int32_t V_3 = 0;
	ByteU5BU5D_t3397334013* V_4 = NULL;
	ByteU5BU5D_t3397334013* V_5 = NULL;
	{
		MemoryStream_t743994179 * L_0 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1043059966(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int64_t L_1 = Asn1Node_get_ChildNodeCount_m1123088750(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		int64_t L_2 = V_1;
		if ((!(((uint64_t)L_2) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_003f;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_3 = __this->get_data_4();
		if (!L_3)
		{
			goto IL_0039;
		}
	}
	{
		MemoryStream_t743994179 * L_4 = V_0;
		ByteU5BU5D_t3397334013* L_5 = __this->get_data_4();
		ByteU5BU5D_t3397334013* L_6 = __this->get_data_4();
		NullCheck(L_6);
		NullCheck(L_4);
		VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_4, L_5, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length)))));
	}

IL_0039:
	{
		goto IL_0066;
	}

IL_003f:
	{
		V_3 = 0;
		goto IL_005d;
	}

IL_0047:
	{
		int32_t L_7 = V_3;
		Asn1Node_t1770761751 * L_8 = Asn1Node_GetChildNode_m4133223467(__this, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		Asn1Node_t1770761751 * L_9 = V_2;
		MemoryStream_t743994179 * L_10 = V_0;
		NullCheck(L_9);
		Asn1Node_SaveData_m3078124111(L_9, L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_3;
		V_3 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_005d:
	{
		int32_t L_12 = V_3;
		int64_t L_13 = V_1;
		if ((((int64_t)(((int64_t)((int64_t)L_12)))) < ((int64_t)L_13)))
		{
			goto IL_0047;
		}
	}
	{
	}

IL_0066:
	{
		MemoryStream_t743994179 * L_14 = V_0;
		NullCheck(L_14);
		int64_t L_15 = VirtFuncInvoker0< int64_t >::Invoke(8 /* System.Int64 System.IO.Stream::get_Length() */, L_14);
		if ((int64_t)(L_15) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		V_4 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)(((intptr_t)L_15))));
		MemoryStream_t743994179 * L_16 = V_0;
		NullCheck(L_16);
		VirtActionInvoker1< int64_t >::Invoke(10 /* System.Void System.IO.Stream::set_Position(System.Int64) */, L_16, (((int64_t)((int64_t)0))));
		MemoryStream_t743994179 * L_17 = V_0;
		ByteU5BU5D_t3397334013* L_18 = V_4;
		MemoryStream_t743994179 * L_19 = V_0;
		NullCheck(L_19);
		int64_t L_20 = VirtFuncInvoker0< int64_t >::Invoke(8 /* System.Int64 System.IO.Stream::get_Length() */, L_19);
		NullCheck(L_17);
		VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(14 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_17, L_18, 0, (((int32_t)((int32_t)L_20))));
		MemoryStream_t743994179 * L_21 = V_0;
		NullCheck(L_21);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_21);
		ByteU5BU5D_t3397334013* L_22 = V_4;
		V_5 = L_22;
		goto IL_009c;
	}

IL_009c:
	{
		ByteU5BU5D_t3397334013* L_23 = V_5;
		return L_23;
	}
}
// System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Node::get_Deepness()
extern "C"  int64_t Asn1Node_get_Deepness_m684235038 (Asn1Node_t1770761751 * __this, const MethodInfo* method)
{
	int64_t V_0 = 0;
	{
		int64_t L_0 = __this->get_deepness_7();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int64_t L_1 = V_0;
		return L_1;
	}
}
// System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::set_RequireRecalculatePar(System.Boolean)
extern "C"  void Asn1Node_set_RequireRecalculatePar_m3884225938 (Asn1Node_t1770761751 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_requireRecalculatePar_11(L_0);
		return;
	}
}
// System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::RecalculateTreePar()
extern "C"  void Asn1Node_RecalculateTreePar_m2615014223 (Asn1Node_t1770761751 * __this, const MethodInfo* method)
{
	Asn1Node_t1770761751 * V_0 = NULL;
	int64_t V_1 = 0;
	{
		bool L_0 = __this->get_requireRecalculatePar_11();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		goto IL_005c;
	}

IL_0011:
	{
		V_0 = __this;
		goto IL_0021;
	}

IL_0018:
	{
		Asn1Node_t1770761751 * L_1 = V_0;
		NullCheck(L_1);
		Asn1Node_t1770761751 * L_2 = Asn1Node_get_ParentNode_m4121820111(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
	}

IL_0021:
	{
		Asn1Node_t1770761751 * L_3 = V_0;
		NullCheck(L_3);
		Asn1Node_t1770761751 * L_4 = Asn1Node_get_ParentNode_m4121820111(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0018;
		}
	}
	{
		Asn1Node_t1770761751 * L_5 = V_0;
		Asn1Node_ResetBranchDataLength_m2407129041(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_6 = V_0;
		NullCheck(L_6);
		L_6->set_dataOffset_1((((int64_t)((int64_t)0))));
		Asn1Node_t1770761751 * L_7 = V_0;
		NullCheck(L_7);
		L_7->set_deepness_7((((int64_t)((int64_t)0))));
		Asn1Node_t1770761751 * L_8 = V_0;
		NullCheck(L_8);
		int64_t L_9 = L_8->get_dataOffset_1();
		Asn1Node_t1770761751 * L_10 = V_0;
		NullCheck(L_10);
		int64_t L_11 = L_10->get_lengthFieldBytes_3();
		V_1 = ((int64_t)((int64_t)((int64_t)((int64_t)L_9+(int64_t)(((int64_t)((int64_t)1)))))+(int64_t)L_11));
		Asn1Node_t1770761751 * L_12 = V_0;
		int64_t L_13 = V_1;
		Asn1Node_ResetChildNodePar_m91912211(__this, L_12, L_13, /*hidden argument*/NULL);
	}

IL_005c:
	{
		return;
	}
}
// System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Node::ResetBranchDataLength(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  int64_t Asn1Node_ResetBranchDataLength_m2407129041 (Il2CppObject * __this /* static, unused */, Asn1Node_t1770761751 * ___node0, const MethodInfo* method)
{
	int64_t V_0 = 0;
	int64_t V_1 = 0;
	int32_t V_2 = 0;
	int64_t V_3 = 0;
	{
		V_0 = (((int64_t)((int64_t)0)));
		V_1 = (((int64_t)((int64_t)0)));
		Asn1Node_t1770761751 * L_0 = ___node0;
		NullCheck(L_0);
		int64_t L_1 = Asn1Node_get_ChildNodeCount_m1123088750(L_0, /*hidden argument*/NULL);
		if ((((int64_t)L_1) >= ((int64_t)(((int64_t)((int64_t)1))))))
		{
			goto IL_0032;
		}
	}
	{
		Asn1Node_t1770761751 * L_2 = ___node0;
		NullCheck(L_2);
		ByteU5BU5D_t3397334013* L_3 = L_2->get_data_4();
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		int64_t L_4 = V_1;
		Asn1Node_t1770761751 * L_5 = ___node0;
		NullCheck(L_5);
		ByteU5BU5D_t3397334013* L_6 = L_5->get_data_4();
		NullCheck(L_6);
		V_1 = ((int64_t)((int64_t)L_4+(int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length)))))))));
	}

IL_002c:
	{
		goto IL_005d;
	}

IL_0032:
	{
		V_2 = 0;
		goto IL_004f;
	}

IL_003a:
	{
		int64_t L_7 = V_1;
		Asn1Node_t1770761751 * L_8 = ___node0;
		int32_t L_9 = V_2;
		NullCheck(L_8);
		Asn1Node_t1770761751 * L_10 = Asn1Node_GetChildNode_m4133223467(L_8, L_9, /*hidden argument*/NULL);
		int64_t L_11 = Asn1Node_ResetBranchDataLength_m2407129041(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_1 = ((int64_t)((int64_t)L_7+(int64_t)L_11));
		int32_t L_12 = V_2;
		V_2 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_004f:
	{
		int32_t L_13 = V_2;
		Asn1Node_t1770761751 * L_14 = ___node0;
		NullCheck(L_14);
		int64_t L_15 = Asn1Node_get_ChildNodeCount_m1123088750(L_14, /*hidden argument*/NULL);
		if ((((int64_t)(((int64_t)((int64_t)L_13)))) < ((int64_t)L_15)))
		{
			goto IL_003a;
		}
	}
	{
	}

IL_005d:
	{
		Asn1Node_t1770761751 * L_16 = ___node0;
		int64_t L_17 = V_1;
		NullCheck(L_16);
		L_16->set_dataLength_2(L_17);
		Asn1Node_t1770761751 * L_18 = ___node0;
		NullCheck(L_18);
		uint8_t L_19 = L_18->get_tag_0();
		if ((!(((uint32_t)L_19) == ((uint32_t)3))))
		{
			goto IL_007f;
		}
	}
	{
		Asn1Node_t1770761751 * L_20 = ___node0;
		Asn1Node_t1770761751 * L_21 = L_20;
		NullCheck(L_21);
		int64_t L_22 = L_21->get_dataLength_2();
		NullCheck(L_21);
		L_21->set_dataLength_2(((int64_t)((int64_t)L_22+(int64_t)(((int64_t)((int64_t)1))))));
	}

IL_007f:
	{
		Asn1Node_t1770761751 * L_23 = ___node0;
		Asn1Node_ResetDataLengthFieldWidth_m9629946(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_24 = ___node0;
		NullCheck(L_24);
		int64_t L_25 = L_24->get_dataLength_2();
		Asn1Node_t1770761751 * L_26 = ___node0;
		NullCheck(L_26);
		int64_t L_27 = L_26->get_lengthFieldBytes_3();
		V_0 = ((int64_t)((int64_t)((int64_t)((int64_t)L_25+(int64_t)(((int64_t)((int64_t)1)))))+(int64_t)L_27));
		int64_t L_28 = V_0;
		V_3 = L_28;
		goto IL_009d;
	}

IL_009d:
	{
		int64_t L_29 = V_3;
		return L_29;
	}
}
// System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::ResetDataLengthFieldWidth(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern Il2CppClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern Il2CppClass* Asn1Util_t2059476207_il2cpp_TypeInfo_var;
extern const uint32_t Asn1Node_ResetDataLengthFieldWidth_m9629946_MetadataUsageId;
extern "C"  void Asn1Node_ResetDataLengthFieldWidth_m9629946 (Il2CppObject * __this /* static, unused */, Asn1Node_t1770761751 * ___node0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Node_ResetDataLengthFieldWidth_m9629946_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MemoryStream_t743994179 * V_0 = NULL;
	{
		MemoryStream_t743994179 * L_0 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1043059966(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		MemoryStream_t743994179 * L_1 = V_0;
		Asn1Node_t1770761751 * L_2 = ___node0;
		NullCheck(L_2);
		int64_t L_3 = L_2->get_dataLength_2();
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		Asn1Util_DERLengthEncode_m3789574000(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_4 = ___node0;
		MemoryStream_t743994179 * L_5 = V_0;
		NullCheck(L_5);
		int64_t L_6 = VirtFuncInvoker0< int64_t >::Invoke(8 /* System.Int64 System.IO.Stream::get_Length() */, L_5);
		NullCheck(L_4);
		L_4->set_lengthFieldBytes_3(L_6);
		MemoryStream_t743994179 * L_7 = V_0;
		NullCheck(L_7);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_7);
		return;
	}
}
// System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::ResetChildNodePar(LipingShare.LCLib.Asn1Processor.Asn1Node,System.Int64)
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Asn1Node_ResetChildNodePar_m91912211_MetadataUsageId;
extern "C"  void Asn1Node_ResetChildNodePar_m91912211 (Asn1Node_t1770761751 * __this, Asn1Node_t1770761751 * ___xNode0, int64_t ___subOffset1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Node_ResetChildNodePar_m91912211_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Asn1Node_t1770761751 * V_1 = NULL;
	{
		Asn1Node_t1770761751 * L_0 = ___xNode0;
		NullCheck(L_0);
		uint8_t L_1 = L_0->get_tag_0();
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0015;
		}
	}
	{
		int64_t L_2 = ___subOffset1;
		___subOffset1 = ((int64_t)((int64_t)L_2+(int64_t)(((int64_t)((int64_t)1)))));
	}

IL_0015:
	{
		V_0 = 0;
		goto IL_008b;
	}

IL_001c:
	{
		Asn1Node_t1770761751 * L_3 = ___xNode0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Asn1Node_t1770761751 * L_5 = Asn1Node_GetChildNode_m4133223467(L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Asn1Node_t1770761751 * L_6 = V_1;
		Asn1Node_t1770761751 * L_7 = ___xNode0;
		NullCheck(L_6);
		L_6->set_parentNode_10(L_7);
		Asn1Node_t1770761751 * L_8 = V_1;
		int64_t L_9 = ___subOffset1;
		NullCheck(L_8);
		L_8->set_dataOffset_1(L_9);
		Asn1Node_t1770761751 * L_10 = V_1;
		Asn1Node_t1770761751 * L_11 = ___xNode0;
		NullCheck(L_11);
		int64_t L_12 = L_11->get_deepness_7();
		NullCheck(L_10);
		L_10->set_deepness_7(((int64_t)((int64_t)L_12+(int64_t)(((int64_t)((int64_t)1))))));
		Asn1Node_t1770761751 * L_13 = V_1;
		Asn1Node_t1770761751 * L_14 = ___xNode0;
		NullCheck(L_14);
		String_t* L_15 = L_14->get_path_8();
		Il2CppChar L_16 = ((Il2CppChar)((int32_t)47));
		Il2CppObject * L_17 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_16);
		String_t* L_18 = Int32_ToString_m2960866144((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m2000667605(NULL /*static, unused*/, L_15, L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_13);
		L_13->set_path_8(L_19);
		int64_t L_20 = ___subOffset1;
		Asn1Node_t1770761751 * L_21 = V_1;
		NullCheck(L_21);
		int64_t L_22 = L_21->get_lengthFieldBytes_3();
		___subOffset1 = ((int64_t)((int64_t)L_20+(int64_t)((int64_t)((int64_t)(((int64_t)((int64_t)1)))+(int64_t)L_22))));
		Asn1Node_t1770761751 * L_23 = V_1;
		int64_t L_24 = ___subOffset1;
		Asn1Node_ResetChildNodePar_m91912211(__this, L_23, L_24, /*hidden argument*/NULL);
		int64_t L_25 = ___subOffset1;
		Asn1Node_t1770761751 * L_26 = V_1;
		NullCheck(L_26);
		int64_t L_27 = L_26->get_dataLength_2();
		___subOffset1 = ((int64_t)((int64_t)L_25+(int64_t)L_27));
		int32_t L_28 = V_0;
		V_0 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_008b:
	{
		int32_t L_29 = V_0;
		Asn1Node_t1770761751 * L_30 = ___xNode0;
		NullCheck(L_30);
		int64_t L_31 = Asn1Node_get_ChildNodeCount_m1123088750(L_30, /*hidden argument*/NULL);
		if ((((int64_t)(((int64_t)((int64_t)L_29)))) < ((int64_t)L_31)))
		{
			goto IL_001c;
		}
	}
	{
		return;
	}
}
// System.String LipingShare.LCLib.Asn1Processor.Asn1Node::GetListStr(LipingShare.LCLib.Asn1Processor.Asn1Node,System.Int32)
extern Il2CppClass* Asn1Node_t1770761751_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern const uint32_t Asn1Node_GetListStr_m3438846266_MetadataUsageId;
extern "C"  String_t* Asn1Node_GetListStr_m3438846266 (Asn1Node_t1770761751 * __this, Asn1Node_t1770761751 * ___startNode0, int32_t ___lineLen1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Node_GetListStr_m3438846266_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	Asn1Node_t1770761751 * V_2 = NULL;
	String_t* V_3 = NULL;
	{
		V_0 = _stringLiteral371857150;
		V_1 = 0;
		goto IL_0035;
	}

IL_000e:
	{
		ArrayList_t4252133567 * L_0 = __this->get_childNodeList_5();
		int32_t L_1 = V_1;
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, L_1);
		V_2 = ((Asn1Node_t1770761751 *)CastclassClass(L_2, Asn1Node_t1770761751_il2cpp_TypeInfo_var));
		String_t* L_3 = V_0;
		Asn1Node_t1770761751 * L_4 = V_2;
		Asn1Node_t1770761751 * L_5 = ___startNode0;
		int32_t L_6 = ___lineLen1;
		NullCheck(L_4);
		String_t* L_7 = Asn1Node_GetText_m821606440(L_4, L_5, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m2596409543(NULL /*static, unused*/, L_3, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0035:
	{
		int32_t L_10 = V_1;
		ArrayList_t4252133567 * L_11 = __this->get_childNodeList_5();
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_11);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_000e;
		}
	}
	{
		String_t* L_13 = V_0;
		V_3 = L_13;
		goto IL_004d;
	}

IL_004d:
	{
		String_t* L_14 = V_3;
		return L_14;
	}
}
// System.String LipingShare.LCLib.Asn1Processor.Asn1Node::GetIndentStr(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern Il2CppCodeGenString* _stringLiteral816986750;
extern const uint32_t Asn1Node_GetIndentStr_m4071159475_MetadataUsageId;
extern "C"  String_t* Asn1Node_GetIndentStr_m4071159475 (Asn1Node_t1770761751 * __this, Asn1Node_t1770761751 * ___startNode0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Node_GetIndentStr_m4071159475_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int64_t V_1 = 0;
	int64_t V_2 = 0;
	String_t* V_3 = NULL;
	{
		V_0 = _stringLiteral371857150;
		V_1 = (((int64_t)((int64_t)0)));
		Asn1Node_t1770761751 * L_0 = ___startNode0;
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		Asn1Node_t1770761751 * L_1 = ___startNode0;
		NullCheck(L_1);
		int64_t L_2 = Asn1Node_get_Deepness_m684235038(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
	}

IL_0019:
	{
		V_2 = (((int64_t)((int64_t)0)));
		goto IL_0034;
	}

IL_0021:
	{
		String_t* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m2596409543(NULL /*static, unused*/, L_3, _stringLiteral816986750, /*hidden argument*/NULL);
		V_0 = L_4;
		int64_t L_5 = V_2;
		V_2 = ((int64_t)((int64_t)L_5+(int64_t)(((int64_t)((int64_t)1)))));
	}

IL_0034:
	{
		int64_t L_6 = V_2;
		int64_t L_7 = __this->get_deepness_7();
		int64_t L_8 = V_1;
		if ((((int64_t)L_6) < ((int64_t)((int64_t)((int64_t)L_7-(int64_t)L_8)))))
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_9 = V_0;
		V_3 = L_9;
		goto IL_0049;
	}

IL_0049:
	{
		String_t* L_10 = V_3;
		return L_10;
	}
}
// System.Boolean LipingShare.LCLib.Asn1Processor.Asn1Node::GeneralDecode(System.IO.Stream)
extern Il2CppClass* Asn1Util_t2059476207_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern const uint32_t Asn1Node_GeneralDecode_m3857333286_MetadataUsageId;
extern "C"  bool Asn1Node_GeneralDecode_m3857333286 (Asn1Node_t1770761751 * __this, Stream_t3255436806 * ___xdata0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Node_GeneralDecode_m3857333286_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int64_t V_1 = 0;
	int64_t V_2 = 0;
	int64_t V_3 = 0;
	bool V_4 = false;
	{
		V_0 = (bool)0;
		Stream_t3255436806 * L_0 = ___xdata0;
		NullCheck(L_0);
		int64_t L_1 = VirtFuncInvoker0< int64_t >::Invoke(8 /* System.Int64 System.IO.Stream::get_Length() */, L_0);
		Stream_t3255436806 * L_2 = ___xdata0;
		NullCheck(L_2);
		int64_t L_3 = VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.Stream::get_Position() */, L_2);
		V_1 = ((int64_t)((int64_t)L_1-(int64_t)L_3));
		Stream_t3255436806 * L_4 = ___xdata0;
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.Stream::ReadByte() */, L_4);
		__this->set_tag_0((((int32_t)((uint8_t)L_5))));
		Stream_t3255436806 * L_6 = ___xdata0;
		NullCheck(L_6);
		int64_t L_7 = VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.Stream::get_Position() */, L_6);
		V_2 = L_7;
		Stream_t3255436806 * L_8 = ___xdata0;
		bool* L_9 = __this->get_address_of_isIndefiniteLength_12();
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		int64_t L_10 = Asn1Util_DerLengthDecode_m23376285(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		__this->set_dataLength_2(L_10);
		int64_t L_11 = __this->get_dataLength_2();
		if ((((int64_t)L_11) >= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_004c;
		}
	}
	{
		bool L_12 = V_0;
		V_4 = L_12;
		goto IL_0157;
	}

IL_004c:
	{
		Stream_t3255436806 * L_13 = ___xdata0;
		NullCheck(L_13);
		int64_t L_14 = VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.Stream::get_Position() */, L_13);
		V_3 = L_14;
		int64_t L_15 = V_3;
		int64_t L_16 = V_2;
		__this->set_lengthFieldBytes_3(((int64_t)((int64_t)L_15-(int64_t)L_16)));
		int64_t L_17 = V_1;
		int64_t L_18 = __this->get_dataLength_2();
		int64_t L_19 = __this->get_lengthFieldBytes_3();
		if ((((int64_t)L_17) >= ((int64_t)((int64_t)((int64_t)((int64_t)((int64_t)L_18+(int64_t)(((int64_t)((int64_t)1)))))+(int64_t)L_19)))))
		{
			goto IL_007b;
		}
	}
	{
		bool L_20 = V_0;
		V_4 = L_20;
		goto IL_0157;
	}

IL_007b:
	{
		Asn1Node_t1770761751 * L_21 = Asn1Node_get_ParentNode_m4121820111(__this, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0099;
		}
	}
	{
		Asn1Node_t1770761751 * L_22 = Asn1Node_get_ParentNode_m4121820111(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		uint8_t L_23 = L_22->get_tag_0();
		if (((int32_t)((int32_t)L_23&(int32_t)((int32_t)32))))
		{
			goto IL_00c2;
		}
	}

IL_0099:
	{
		uint8_t L_24 = __this->get_tag_0();
		if ((((int32_t)((int32_t)((int32_t)L_24&(int32_t)((int32_t)31)))) <= ((int32_t)0)))
		{
			goto IL_00b9;
		}
	}
	{
		uint8_t L_25 = __this->get_tag_0();
		if ((((int32_t)((int32_t)((int32_t)L_25&(int32_t)((int32_t)31)))) <= ((int32_t)((int32_t)30))))
		{
			goto IL_00c1;
		}
	}

IL_00b9:
	{
		bool L_26 = V_0;
		V_4 = L_26;
		goto IL_0157;
	}

IL_00c1:
	{
	}

IL_00c2:
	{
		uint8_t L_27 = __this->get_tag_0();
		if ((!(((uint32_t)L_27) == ((uint32_t)3))))
		{
			goto IL_0124;
		}
	}
	{
		int64_t L_28 = __this->get_dataLength_2();
		if ((((int64_t)L_28) >= ((int64_t)(((int64_t)((int64_t)1))))))
		{
			goto IL_00e4;
		}
	}
	{
		bool L_29 = V_0;
		V_4 = L_29;
		goto IL_0157;
	}

IL_00e4:
	{
		Stream_t3255436806 * L_30 = ___xdata0;
		NullCheck(L_30);
		int32_t L_31 = VirtFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.Stream::ReadByte() */, L_30);
		__this->set_unusedBits_6((((int32_t)((uint8_t)L_31))));
		int64_t L_32 = __this->get_dataLength_2();
		if ((int64_t)(((int64_t)((int64_t)L_32-(int64_t)(((int64_t)((int64_t)1)))))) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		__this->set_data_4(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)(((intptr_t)((int64_t)((int64_t)L_32-(int64_t)(((int64_t)((int64_t)1))))))))));
		Stream_t3255436806 * L_33 = ___xdata0;
		ByteU5BU5D_t3397334013* L_34 = __this->get_data_4();
		int64_t L_35 = __this->get_dataLength_2();
		NullCheck(L_33);
		VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(14 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_33, L_34, 0, (((int32_t)((int32_t)((int64_t)((int64_t)L_35-(int64_t)(((int64_t)((int64_t)1)))))))));
		goto IL_014d;
	}

IL_0124:
	{
		int64_t L_36 = __this->get_dataLength_2();
		if ((int64_t)(L_36) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		__this->set_data_4(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)(((intptr_t)L_36)))));
		Stream_t3255436806 * L_37 = ___xdata0;
		ByteU5BU5D_t3397334013* L_38 = __this->get_data_4();
		int64_t L_39 = __this->get_dataLength_2();
		NullCheck(L_37);
		VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(14 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_37, L_38, 0, (((int32_t)((int32_t)L_39))));
	}

IL_014d:
	{
		V_0 = (bool)1;
		bool L_40 = V_0;
		V_4 = L_40;
		goto IL_0157;
	}

IL_0157:
	{
		bool L_41 = V_4;
		return L_41;
	}
}
// System.Boolean LipingShare.LCLib.Asn1Processor.Asn1Node::ListDecode(System.IO.Stream)
extern Il2CppClass* Asn1Util_t2059476207_il2cpp_TypeInfo_var;
extern Il2CppClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* Asn1Node_t1770761751_il2cpp_TypeInfo_var;
extern const uint32_t Asn1Node_ListDecode_m89847718_MetadataUsageId;
extern "C"  bool Asn1Node_ListDecode_m89847718 (Asn1Node_t1770761751 * __this, Stream_t3255436806 * ___xdata0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Node_ListDecode_m89847718_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int64_t V_1 = 0;
	int64_t V_2 = 0;
	int64_t V_3 = 0;
	int64_t V_4 = 0;
	int64_t V_5 = 0;
	bool V_6 = false;
	Stream_t3255436806 * V_7 = NULL;
	ByteU5BU5D_t3397334013* V_8 = NULL;
	Asn1Node_t1770761751 * V_9 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)0;
		Stream_t3255436806 * L_0 = ___xdata0;
		NullCheck(L_0);
		int64_t L_1 = VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.Stream::get_Position() */, L_0);
		V_1 = L_1;
	}

IL_000a:
	try
	{ // begin try (depth: 1)
		{
			Stream_t3255436806 * L_2 = ___xdata0;
			NullCheck(L_2);
			int64_t L_3 = VirtFuncInvoker0< int64_t >::Invoke(8 /* System.Int64 System.IO.Stream::get_Length() */, L_2);
			Stream_t3255436806 * L_4 = ___xdata0;
			NullCheck(L_4);
			int64_t L_5 = VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.Stream::get_Position() */, L_4);
			V_2 = ((int64_t)((int64_t)L_3-(int64_t)L_5));
			Stream_t3255436806 * L_6 = ___xdata0;
			NullCheck(L_6);
			int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.Stream::ReadByte() */, L_6);
			__this->set_tag_0((((int32_t)((uint8_t)L_7))));
			Stream_t3255436806 * L_8 = ___xdata0;
			NullCheck(L_8);
			int64_t L_9 = VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.Stream::get_Position() */, L_8);
			V_3 = L_9;
			Stream_t3255436806 * L_10 = ___xdata0;
			bool* L_11 = __this->get_address_of_isIndefiniteLength_12();
			IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
			int64_t L_12 = Asn1Util_DerLengthDecode_m23376285(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
			__this->set_dataLength_2(L_12);
			int64_t L_13 = __this->get_dataLength_2();
			if ((((int64_t)L_13) < ((int64_t)(((int64_t)((int64_t)0))))))
			{
				goto IL_0058;
			}
		}

IL_004c:
		{
			int64_t L_14 = V_2;
			int64_t L_15 = __this->get_dataLength_2();
			if ((((int64_t)L_14) >= ((int64_t)L_15)))
			{
				goto IL_0061;
			}
		}

IL_0058:
		{
			bool L_16 = V_0;
			V_6 = L_16;
			IL2CPP_LEAVE(0x1BB, FINALLY_019b);
		}

IL_0061:
		{
			Stream_t3255436806 * L_17 = ___xdata0;
			NullCheck(L_17);
			int64_t L_18 = VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.Stream::get_Position() */, L_17);
			V_4 = L_18;
			int64_t L_19 = V_4;
			int64_t L_20 = V_3;
			__this->set_lengthFieldBytes_3(((int64_t)((int64_t)L_19-(int64_t)L_20)));
			int64_t L_21 = __this->get_dataOffset_1();
			int64_t L_22 = __this->get_lengthFieldBytes_3();
			V_5 = ((int64_t)((int64_t)((int64_t)((int64_t)L_21+(int64_t)(((int64_t)((int64_t)1)))))+(int64_t)L_22));
			uint8_t L_23 = __this->get_tag_0();
			if ((!(((uint32_t)L_23) == ((uint32_t)3))))
			{
				goto IL_00b6;
			}
		}

IL_0091:
		{
			Stream_t3255436806 * L_24 = ___xdata0;
			NullCheck(L_24);
			int32_t L_25 = VirtFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.Stream::ReadByte() */, L_24);
			__this->set_unusedBits_6((((int32_t)((uint8_t)L_25))));
			int64_t L_26 = __this->get_dataLength_2();
			__this->set_dataLength_2(((int64_t)((int64_t)L_26-(int64_t)(((int64_t)((int64_t)1))))));
			int64_t L_27 = V_5;
			V_5 = ((int64_t)((int64_t)L_27+(int64_t)(((int64_t)((int64_t)1)))));
		}

IL_00b6:
		{
			int64_t L_28 = __this->get_dataLength_2();
			if ((((int64_t)L_28) > ((int64_t)(((int64_t)((int64_t)0))))))
			{
				goto IL_00cb;
			}
		}

IL_00c3:
		{
			bool L_29 = V_0;
			V_6 = L_29;
			IL2CPP_LEAVE(0x1BB, FINALLY_019b);
		}

IL_00cb:
		{
			int64_t L_30 = __this->get_dataLength_2();
			MemoryStream_t743994179 * L_31 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
			MemoryStream__ctor_m1489366847(L_31, (((int32_t)((int32_t)L_30))), /*hidden argument*/NULL);
			V_7 = L_31;
			int64_t L_32 = __this->get_dataLength_2();
			if ((int64_t)(L_32) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
			V_8 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)(((intptr_t)L_32))));
			Stream_t3255436806 * L_33 = ___xdata0;
			ByteU5BU5D_t3397334013* L_34 = V_8;
			int64_t L_35 = __this->get_dataLength_2();
			NullCheck(L_33);
			VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(14 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_33, L_34, 0, (((int32_t)((int32_t)L_35))));
			uint8_t L_36 = __this->get_tag_0();
			if ((!(((uint32_t)L_36) == ((uint32_t)3))))
			{
				goto IL_0113;
			}
		}

IL_0104:
		{
			int64_t L_37 = __this->get_dataLength_2();
			__this->set_dataLength_2(((int64_t)((int64_t)L_37+(int64_t)(((int64_t)((int64_t)1))))));
		}

IL_0113:
		{
			Stream_t3255436806 * L_38 = V_7;
			ByteU5BU5D_t3397334013* L_39 = V_8;
			ByteU5BU5D_t3397334013* L_40 = V_8;
			NullCheck(L_40);
			NullCheck(L_38);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_38, L_39, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_40)->max_length)))));
			Stream_t3255436806 * L_41 = V_7;
			NullCheck(L_41);
			VirtActionInvoker1< int64_t >::Invoke(10 /* System.Void System.IO.Stream::set_Position(System.Int64) */, L_41, (((int64_t)((int64_t)0))));
			goto IL_0180;
		}

IL_012f:
		{
			int64_t L_42 = V_5;
			Asn1Node_t1770761751 * L_43 = (Asn1Node_t1770761751 *)il2cpp_codegen_object_new(Asn1Node_t1770761751_il2cpp_TypeInfo_var);
			Asn1Node__ctor_m2720905655(L_43, __this, L_42, /*hidden argument*/NULL);
			V_9 = L_43;
			Asn1Node_t1770761751 * L_44 = V_9;
			bool L_45 = __this->get_parseEncapsulatedData_13();
			NullCheck(L_44);
			L_44->set_parseEncapsulatedData_13(L_45);
			Stream_t3255436806 * L_46 = V_7;
			NullCheck(L_46);
			int64_t L_47 = VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.Stream::get_Position() */, L_46);
			V_3 = L_47;
			Asn1Node_t1770761751 * L_48 = V_9;
			Stream_t3255436806 * L_49 = V_7;
			NullCheck(L_48);
			bool L_50 = Asn1Node_InternalLoadData_m220543567(L_48, L_49, /*hidden argument*/NULL);
			if (L_50)
			{
				goto IL_0165;
			}
		}

IL_015d:
		{
			bool L_51 = V_0;
			V_6 = L_51;
			IL2CPP_LEAVE(0x1BB, FINALLY_019b);
		}

IL_0165:
		{
			Asn1Node_t1770761751 * L_52 = V_9;
			Asn1Node_AddChild_m134798510(__this, L_52, /*hidden argument*/NULL);
			Stream_t3255436806 * L_53 = V_7;
			NullCheck(L_53);
			int64_t L_54 = VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.Stream::get_Position() */, L_53);
			V_4 = L_54;
			int64_t L_55 = V_5;
			int64_t L_56 = V_4;
			int64_t L_57 = V_3;
			V_5 = ((int64_t)((int64_t)L_55+(int64_t)((int64_t)((int64_t)L_56-(int64_t)L_57))));
		}

IL_0180:
		{
			Stream_t3255436806 * L_58 = V_7;
			NullCheck(L_58);
			int64_t L_59 = VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.Stream::get_Position() */, L_58);
			Stream_t3255436806 * L_60 = V_7;
			NullCheck(L_60);
			int64_t L_61 = VirtFuncInvoker0< int64_t >::Invoke(8 /* System.Int64 System.IO.Stream::get_Length() */, L_60);
			if ((((int64_t)L_59) < ((int64_t)L_61)))
			{
				goto IL_012f;
			}
		}

IL_0193:
		{
			V_0 = (bool)1;
			IL2CPP_LEAVE(0x1B3, FINALLY_019b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_019b;
	}

FINALLY_019b:
	{ // begin finally (depth: 1)
		{
			bool L_62 = V_0;
			if (L_62)
			{
				goto IL_01b1;
			}
		}

IL_01a2:
		{
			Stream_t3255436806 * L_63 = ___xdata0;
			int64_t L_64 = V_1;
			NullCheck(L_63);
			VirtActionInvoker1< int64_t >::Invoke(10 /* System.Void System.IO.Stream::set_Position(System.Int64) */, L_63, L_64);
			Asn1Node_ClearAll_m2883531985(__this, /*hidden argument*/NULL);
		}

IL_01b1:
		{
			IL2CPP_END_FINALLY(411)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(411)
	{
		IL2CPP_JUMP_TBL(0x1BB, IL_01bb)
		IL2CPP_JUMP_TBL(0x1B3, IL_01b3)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_01b3:
	{
		bool L_65 = V_0;
		V_6 = L_65;
		goto IL_01bb;
	}

IL_01bb:
	{
		bool L_66 = V_6;
		return L_66;
	}
}
// System.Boolean LipingShare.LCLib.Asn1Processor.Asn1Node::InternalLoadData(System.IO.Stream)
extern "C"  bool Asn1Node_InternalLoadData_m220543567 (Asn1Node_t1770761751 * __this, Stream_t3255436806 * ___xdata0, const MethodInfo* method)
{
	bool V_0 = false;
	uint8_t V_1 = 0x0;
	int64_t V_2 = 0;
	int32_t V_3 = 0;
	bool V_4 = false;
	{
		V_0 = (bool)1;
		Asn1Node_ClearAll_m2883531985(__this, /*hidden argument*/NULL);
		Stream_t3255436806 * L_0 = ___xdata0;
		NullCheck(L_0);
		int64_t L_1 = VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.Stream::get_Position() */, L_0);
		V_2 = L_1;
		Stream_t3255436806 * L_2 = ___xdata0;
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.Stream::ReadByte() */, L_2);
		V_1 = (((int32_t)((uint8_t)L_3)));
		Stream_t3255436806 * L_4 = ___xdata0;
		int64_t L_5 = V_2;
		NullCheck(L_4);
		VirtActionInvoker1< int64_t >::Invoke(10 /* System.Void System.IO.Stream::set_Position(System.Int64) */, L_4, L_5);
		uint8_t L_6 = V_1;
		V_3 = ((int32_t)((int32_t)L_6&(int32_t)((int32_t)31)));
		uint8_t L_7 = V_1;
		if (((int32_t)((int32_t)L_7&(int32_t)((int32_t)32))))
		{
			goto IL_00ad;
		}
	}
	{
		bool L_8 = __this->get_parseEncapsulatedData_13();
		if (!L_8)
		{
			goto IL_00d2;
		}
	}
	{
		int32_t L_9 = V_3;
		if ((((int32_t)L_9) == ((int32_t)3)))
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_10 = V_3;
		if ((((int32_t)L_10) == ((int32_t)8)))
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_11 = V_3;
		if ((((int32_t)L_11) == ((int32_t)((int32_t)27))))
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_12 = V_3;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)24))))
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_13 = V_3;
		if ((((int32_t)L_13) == ((int32_t)((int32_t)25))))
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_14 = V_3;
		if ((((int32_t)L_14) == ((int32_t)((int32_t)22))))
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_15 = V_3;
		if ((((int32_t)L_15) == ((int32_t)4)))
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_16 = V_3;
		if ((((int32_t)L_16) == ((int32_t)((int32_t)19))))
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_17 = V_3;
		if ((((int32_t)L_17) == ((int32_t)((int32_t)16))))
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_18 = V_3;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)17))))
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_19 = V_3;
		if ((((int32_t)L_19) == ((int32_t)((int32_t)20))))
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_20 = V_3;
		if ((((int32_t)L_20) == ((int32_t)((int32_t)28))))
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_21 = V_3;
		if ((((int32_t)L_21) == ((int32_t)((int32_t)12))))
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_22 = V_3;
		if ((((int32_t)L_22) == ((int32_t)((int32_t)21))))
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_23 = V_3;
		if ((!(((uint32_t)L_23) == ((uint32_t)((int32_t)26)))))
		{
			goto IL_00d2;
		}
	}

IL_00ad:
	{
		Stream_t3255436806 * L_24 = ___xdata0;
		bool L_25 = Asn1Node_ListDecode_m89847718(__this, L_24, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_00cc;
		}
	}
	{
		Stream_t3255436806 * L_26 = ___xdata0;
		bool L_27 = Asn1Node_GeneralDecode_m3857333286(__this, L_26, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_00cb;
		}
	}
	{
		V_0 = (bool)0;
	}

IL_00cb:
	{
	}

IL_00cc:
	{
		goto IL_00e2;
	}

IL_00d2:
	{
		Stream_t3255436806 * L_28 = ___xdata0;
		bool L_29 = Asn1Node_GeneralDecode_m3857333286(__this, L_28, /*hidden argument*/NULL);
		if (L_29)
		{
			goto IL_00e1;
		}
	}
	{
		V_0 = (bool)0;
	}

IL_00e1:
	{
	}

IL_00e2:
	{
		bool L_30 = V_0;
		V_4 = L_30;
		goto IL_00ea;
	}

IL_00ea:
	{
		bool L_31 = V_4;
		return L_31;
	}
}
// System.Void LipingShare.LCLib.Asn1Processor.Asn1Parser::.ctor()
extern Il2CppClass* Asn1Node_t1770761751_il2cpp_TypeInfo_var;
extern const uint32_t Asn1Parser__ctor_m2929298044_MetadataUsageId;
extern "C"  void Asn1Parser__ctor_m2929298044 (Asn1Parser_t914015216 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Parser__ctor_m2929298044_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Asn1Node_t1770761751 * L_0 = (Asn1Node_t1770761751 *)il2cpp_codegen_object_new(Asn1Node_t1770761751_il2cpp_TypeInfo_var);
		Asn1Node__ctor_m2386874033(L_0, /*hidden argument*/NULL);
		__this->set_rootNode_1(L_0);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LipingShare.LCLib.Asn1Processor.Asn1Parser::LoadData(System.IO.Stream)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1957348142;
extern const uint32_t Asn1Parser_LoadData_m2612632163_MetadataUsageId;
extern "C"  void Asn1Parser_LoadData_m2612632163 (Asn1Parser_t914015216 * __this, Stream_t3255436806 * ___stream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Parser_LoadData_m2612632163_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Stream_t3255436806 * L_0 = ___stream0;
		NullCheck(L_0);
		VirtActionInvoker1< int64_t >::Invoke(10 /* System.Void System.IO.Stream::set_Position(System.Int64) */, L_0, (((int64_t)((int64_t)0))));
		Asn1Node_t1770761751 * L_1 = __this->get_rootNode_1();
		Stream_t3255436806 * L_2 = ___stream0;
		NullCheck(L_1);
		bool L_3 = Asn1Node_LoadData_m1298383144(L_1, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0026;
		}
	}
	{
		ArgumentException_t3259014390 * L_4 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_4, _stringLiteral1957348142, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0026:
	{
		Stream_t3255436806 * L_5 = ___stream0;
		NullCheck(L_5);
		int64_t L_6 = VirtFuncInvoker0< int64_t >::Invoke(8 /* System.Int64 System.IO.Stream::get_Length() */, L_5);
		if ((int64_t)(L_6) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		__this->set_rawData_0(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)(((intptr_t)L_6)))));
		Stream_t3255436806 * L_7 = ___stream0;
		NullCheck(L_7);
		VirtActionInvoker1< int64_t >::Invoke(10 /* System.Void System.IO.Stream::set_Position(System.Int64) */, L_7, (((int64_t)((int64_t)0))));
		Stream_t3255436806 * L_8 = ___stream0;
		ByteU5BU5D_t3397334013* L_9 = __this->get_rawData_0();
		ByteU5BU5D_t3397334013* L_10 = __this->get_rawData_0();
		NullCheck(L_10);
		NullCheck(L_8);
		VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(14 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_8, L_9, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))));
		return;
	}
}
// LipingShare.LCLib.Asn1Processor.Asn1Node LipingShare.LCLib.Asn1Processor.Asn1Parser::get_RootNode()
extern "C"  Asn1Node_t1770761751 * Asn1Parser_get_RootNode_m542146928 (Asn1Parser_t914015216 * __this, const MethodInfo* method)
{
	Asn1Node_t1770761751 * V_0 = NULL;
	{
		Asn1Node_t1770761751 * L_0 = __this->get_rootNode_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Asn1Node_t1770761751 * L_1 = V_0;
		return L_1;
	}
}
// System.String LipingShare.LCLib.Asn1Processor.Asn1Parser::GetNodeTextHeader(System.Int32)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Asn1Util_t2059476207_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3803946940;
extern Il2CppCodeGenString* _stringLiteral67058950;
extern Il2CppCodeGenString* _stringLiteral2162321587;
extern const uint32_t Asn1Parser_GetNodeTextHeader_m3481486116_MetadataUsageId;
extern "C"  String_t* Asn1Parser_GetNodeTextHeader_m3481486116 (Il2CppObject * __this /* static, unused */, int32_t ___lineLen0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Parser_GetNodeTextHeader_m3481486116_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = String_Format_m1263743648(NULL /*static, unused*/, _stringLiteral3803946940, ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		int32_t L_2 = ___lineLen0;
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		String_t* L_3 = Asn1Util_GenStr_m3970829795(NULL /*static, unused*/, ((int32_t)((int32_t)L_2+(int32_t)((int32_t)10))), ((int32_t)61), /*hidden argument*/NULL);
		String_t* L_4 = String_Concat_m1561703559(NULL /*static, unused*/, L_1, _stringLiteral67058950, L_3, _stringLiteral2162321587, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		V_1 = L_5;
		goto IL_0035;
	}

IL_0035:
	{
		String_t* L_6 = V_1;
		return L_6;
	}
}
// System.String LipingShare.LCLib.Asn1Processor.Asn1Parser::ToString()
extern "C"  String_t* Asn1Parser_ToString_m3361830265 (Asn1Parser_t914015216 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		Asn1Node_t1770761751 * L_0 = __this->get_rootNode_1();
		String_t* L_1 = Asn1Parser_GetNodeText_m742522949(NULL /*static, unused*/, L_0, ((int32_t)100), /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0014;
	}

IL_0014:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
// System.String LipingShare.LCLib.Asn1Processor.Asn1Parser::GetNodeText(LipingShare.LCLib.Asn1Processor.Asn1Node,System.Int32)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Asn1Parser_GetNodeText_m742522949_MetadataUsageId;
extern "C"  String_t* Asn1Parser_GetNodeText_m742522949 (Il2CppObject * __this /* static, unused */, Asn1Node_t1770761751 * ___node0, int32_t ___lineLen1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Parser_GetNodeText_m742522949_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		int32_t L_0 = ___lineLen1;
		String_t* L_1 = Asn1Parser_GetNodeTextHeader_m3481486116(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		Asn1Node_t1770761751 * L_3 = ___node0;
		Asn1Node_t1770761751 * L_4 = ___node0;
		int32_t L_5 = ___lineLen1;
		NullCheck(L_3);
		String_t* L_6 = Asn1Node_GetText_m821606440(L_3, L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m2596409543(NULL /*static, unused*/, L_2, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		String_t* L_8 = V_0;
		V_1 = L_8;
		goto IL_001e;
	}

IL_001e:
	{
		String_t* L_9 = V_1;
		return L_9;
	}
}
// System.String LipingShare.LCLib.Asn1Processor.Asn1Util::FormatString(System.String,System.Int32,System.Int32)
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern const uint32_t Asn1Util_FormatString_m1519920834_MetadataUsageId;
extern "C"  String_t* Asn1Util_FormatString_m1519920834 (Il2CppObject * __this /* static, unused */, String_t* ___inStr0, int32_t ___lineLen1, int32_t ___groupLen2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Util_FormatString_m1519920834_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CharU5BU5D_t1328083999* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	{
		String_t* L_0 = ___inStr0;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1606060069(L_0, /*hidden argument*/NULL);
		V_0 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_1*(int32_t)2))));
		V_2 = 0;
		V_3 = 0;
		V_4 = 0;
		V_1 = 0;
		goto IL_0075;
	}

IL_001d:
	{
		CharU5BU5D_t1328083999* L_2 = V_0;
		int32_t L_3 = V_2;
		int32_t L_4 = L_3;
		V_2 = ((int32_t)((int32_t)L_4+(int32_t)1));
		String_t* L_5 = ___inStr0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		Il2CppChar L_7 = String_get_Chars_m4230566705(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_4);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (Il2CppChar)L_7);
		int32_t L_8 = V_4;
		V_4 = ((int32_t)((int32_t)L_8+(int32_t)1));
		int32_t L_9 = V_3;
		V_3 = ((int32_t)((int32_t)L_9+(int32_t)1));
		int32_t L_10 = V_4;
		int32_t L_11 = ___groupLen2;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0053;
		}
	}
	{
		int32_t L_12 = ___groupLen2;
		if ((((int32_t)L_12) <= ((int32_t)0)))
		{
			goto IL_0053;
		}
	}
	{
		CharU5BU5D_t1328083999* L_13 = V_0;
		int32_t L_14 = V_2;
		int32_t L_15 = L_14;
		V_2 = ((int32_t)((int32_t)L_15+(int32_t)1));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_15);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (Il2CppChar)((int32_t)32));
		V_4 = 0;
	}

IL_0053:
	{
		int32_t L_16 = V_3;
		int32_t L_17 = ___lineLen1;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0070;
		}
	}
	{
		CharU5BU5D_t1328083999* L_18 = V_0;
		int32_t L_19 = V_2;
		int32_t L_20 = L_19;
		V_2 = ((int32_t)((int32_t)L_20+(int32_t)1));
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_20);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_20), (Il2CppChar)((int32_t)13));
		CharU5BU5D_t1328083999* L_21 = V_0;
		int32_t L_22 = V_2;
		int32_t L_23 = L_22;
		V_2 = ((int32_t)((int32_t)L_23+(int32_t)1));
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_23);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(L_23), (Il2CppChar)((int32_t)10));
		V_3 = 0;
	}

IL_0070:
	{
		int32_t L_24 = V_1;
		V_1 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_0075:
	{
		int32_t L_25 = V_1;
		String_t* L_26 = ___inStr0;
		NullCheck(L_26);
		int32_t L_27 = String_get_Length_m1606060069(L_26, /*hidden argument*/NULL);
		if ((((int32_t)L_25) < ((int32_t)L_27)))
		{
			goto IL_001d;
		}
	}
	{
		CharU5BU5D_t1328083999* L_28 = V_0;
		String_t* L_29 = String_CreateString_m3818307083(NULL, L_28, /*hidden argument*/NULL);
		V_5 = L_29;
		String_t* L_30 = V_5;
		NullCheck(L_30);
		String_t* L_31 = String_TrimEnd_m3153143011(L_30, ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1)), /*hidden argument*/NULL);
		V_5 = L_31;
		String_t* L_32 = V_5;
		CharU5BU5D_t1328083999* L_33 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, 0);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)10));
		NullCheck(L_32);
		String_t* L_34 = String_TrimEnd_m3153143011(L_32, L_33, /*hidden argument*/NULL);
		V_5 = L_34;
		String_t* L_35 = V_5;
		CharU5BU5D_t1328083999* L_36 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 0);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)13));
		NullCheck(L_35);
		String_t* L_37 = String_TrimEnd_m3153143011(L_35, L_36, /*hidden argument*/NULL);
		V_5 = L_37;
		String_t* L_38 = V_5;
		V_6 = L_38;
		goto IL_00c9;
	}

IL_00c9:
	{
		String_t* L_39 = V_6;
		return L_39;
	}
}
// System.String LipingShare.LCLib.Asn1Processor.Asn1Util::GenStr(System.Int32,System.Char)
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern const uint32_t Asn1Util_GenStr_m3970829795_MetadataUsageId;
extern "C"  String_t* Asn1Util_GenStr_m3970829795 (Il2CppObject * __this /* static, unused */, int32_t ___len0, Il2CppChar ___xch1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Util_GenStr_m3970829795_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CharU5BU5D_t1328083999* V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	{
		int32_t L_0 = ___len0;
		V_0 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)L_0));
		V_1 = 0;
		goto IL_0019;
	}

IL_000f:
	{
		CharU5BU5D_t1328083999* L_1 = V_0;
		int32_t L_2 = V_1;
		Il2CppChar L_3 = ___xch1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (Il2CppChar)L_3);
		int32_t L_4 = V_1;
		V_1 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0019:
	{
		int32_t L_5 = V_1;
		int32_t L_6 = ___len0;
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_000f;
		}
	}
	{
		CharU5BU5D_t1328083999* L_7 = V_0;
		String_t* L_8 = String_CreateString_m3818307083(NULL, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		goto IL_002c;
	}

IL_002c:
	{
		String_t* L_9 = V_2;
		return L_9;
	}
}
// System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Util::BytesToLong(System.Byte[])
extern "C"  int64_t Asn1Util_BytesToLong_m1963652187 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method)
{
	int64_t V_0 = 0;
	int32_t V_1 = 0;
	int64_t V_2 = 0;
	{
		V_0 = (((int64_t)((int64_t)0)));
		V_1 = 0;
		goto IL_001a;
	}

IL_000b:
	{
		int64_t L_0 = V_0;
		ByteU5BU5D_t3397334013* L_1 = ___bytes0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		uint8_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = ((int64_t)((int64_t)((int64_t)((int64_t)L_0<<(int32_t)8))|(int64_t)(((int64_t)((int64_t)L_4)))));
		int32_t L_5 = V_1;
		V_1 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001a:
	{
		int32_t L_6 = V_1;
		ByteU5BU5D_t3397334013* L_7 = ___bytes0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_000b;
		}
	}
	{
		int64_t L_8 = V_0;
		V_2 = L_8;
		goto IL_002a;
	}

IL_002a:
	{
		int64_t L_9 = V_2;
		return L_9;
	}
}
// System.String LipingShare.LCLib.Asn1Processor.Asn1Util::BytesToString(System.Byte[])
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern const uint32_t Asn1Util_BytesToString_m2403670248_MetadataUsageId;
extern "C"  String_t* Asn1Util_BytesToString_m2403670248 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Util_BytesToString_m2403670248_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	CharU5BU5D_t1328083999* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		V_0 = _stringLiteral371857150;
		ByteU5BU5D_t3397334013* L_0 = ___bytes0;
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_1 = ___bytes0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) >= ((int32_t)1)))
		{
			goto IL_001d;
		}
	}

IL_0016:
	{
		String_t* L_2 = V_0;
		V_1 = L_2;
		goto IL_0071;
	}

IL_001d:
	{
		ByteU5BU5D_t3397334013* L_3 = ___bytes0;
		NullCheck(L_3);
		V_2 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))));
		V_3 = 0;
		V_4 = 0;
		goto IL_004d;
	}

IL_0030:
	{
		ByteU5BU5D_t3397334013* L_4 = ___bytes0;
		int32_t L_5 = V_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		uint8_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		if (!L_7)
		{
			goto IL_0048;
		}
	}
	{
		CharU5BU5D_t1328083999* L_8 = V_2;
		int32_t L_9 = V_4;
		int32_t L_10 = L_9;
		V_4 = ((int32_t)((int32_t)L_10+(int32_t)1));
		ByteU5BU5D_t3397334013* L_11 = ___bytes0;
		int32_t L_12 = V_3;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		uint8_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Il2CppChar)(((int32_t)((uint16_t)L_14))));
	}

IL_0048:
	{
		int32_t L_15 = V_3;
		V_3 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_16 = V_3;
		ByteU5BU5D_t3397334013* L_17 = ___bytes0;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length)))))))
		{
			goto IL_0030;
		}
	}
	{
		CharU5BU5D_t1328083999* L_18 = V_2;
		String_t* L_19 = String_CreateString_m3818307083(NULL, L_18, /*hidden argument*/NULL);
		V_0 = L_19;
		String_t* L_20 = V_0;
		NullCheck(L_20);
		String_t* L_21 = String_TrimEnd_m3153143011(L_20, ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1)), /*hidden argument*/NULL);
		V_0 = L_21;
		String_t* L_22 = V_0;
		V_1 = L_22;
		goto IL_0071;
	}

IL_0071:
	{
		String_t* L_23 = V_1;
		return L_23;
	}
}
// System.String LipingShare.LCLib.Asn1Processor.Asn1Util::ToHexString(System.Byte[])
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Asn1Util_t2059476207_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern const uint32_t Asn1Util_ToHexString_m3932902392_MetadataUsageId;
extern "C"  String_t* Asn1Util_ToHexString_m3932902392 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Util_ToHexString_m3932902392_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	CharU5BU5D_t1328083999* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		ByteU5BU5D_t3397334013* L_0 = ___bytes0;
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		V_0 = _stringLiteral371857150;
		goto IL_0062;
	}

IL_0012:
	{
		ByteU5BU5D_t3397334013* L_1 = ___bytes0;
		NullCheck(L_1);
		V_1 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))*(int32_t)2))));
		V_3 = 0;
		goto IL_004d;
	}

IL_0024:
	{
		ByteU5BU5D_t3397334013* L_2 = ___bytes0;
		int32_t L_3 = V_3;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		uint8_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_2 = L_5;
		CharU5BU5D_t1328083999* L_6 = V_1;
		int32_t L_7 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		CharU5BU5D_t1328083999* L_8 = ((Asn1Util_t2059476207_StaticFields*)Asn1Util_t2059476207_il2cpp_TypeInfo_var->static_fields)->get_hexDigits_0();
		int32_t L_9 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)L_9>>(int32_t)4)));
		int32_t L_10 = ((int32_t)((int32_t)L_9>>(int32_t)4));
		uint16_t L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, ((int32_t)((int32_t)L_7*(int32_t)2)));
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_7*(int32_t)2))), (Il2CppChar)L_11);
		CharU5BU5D_t1328083999* L_12 = V_1;
		int32_t L_13 = V_3;
		CharU5BU5D_t1328083999* L_14 = ((Asn1Util_t2059476207_StaticFields*)Asn1Util_t2059476207_il2cpp_TypeInfo_var->static_fields)->get_hexDigits_0();
		int32_t L_15 = V_2;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, ((int32_t)((int32_t)L_15&(int32_t)((int32_t)15))));
		int32_t L_16 = ((int32_t)((int32_t)L_15&(int32_t)((int32_t)15)));
		uint16_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, ((int32_t)((int32_t)((int32_t)((int32_t)L_13*(int32_t)2))+(int32_t)1)));
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)L_13*(int32_t)2))+(int32_t)1))), (Il2CppChar)L_17);
		int32_t L_18 = V_3;
		V_3 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_19 = V_3;
		ByteU5BU5D_t3397334013* L_20 = ___bytes0;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		CharU5BU5D_t1328083999* L_21 = V_1;
		String_t* L_22 = String_CreateString_m3818307083(NULL, L_21, /*hidden argument*/NULL);
		V_0 = L_22;
		goto IL_0062;
	}

IL_0062:
	{
		String_t* L_23 = V_0;
		return L_23;
	}
}
// System.Int32 LipingShare.LCLib.Asn1Processor.Asn1Util::BytePrecision(System.UInt64)
extern "C"  int32_t Asn1Util_BytePrecision_m2588186404 (Il2CppObject * __this /* static, unused */, uint64_t ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 4;
		goto IL_0022;
	}

IL_0008:
	{
		uint64_t L_0 = ___value0;
		int32_t L_1 = V_0;
		if ((((int64_t)((int64_t)((uint64_t)L_0>>((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1-(int32_t)1))*(int32_t)8))&(int32_t)((int32_t)63)))))) == ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_001e;
		}
	}
	{
		goto IL_0029;
	}

IL_001e:
	{
		int32_t L_2 = V_0;
		V_0 = ((int32_t)((int32_t)L_2-(int32_t)1));
	}

IL_0022:
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) > ((int32_t)0)))
		{
			goto IL_0008;
		}
	}

IL_0029:
	{
		int32_t L_4 = V_0;
		V_1 = L_4;
		goto IL_0030;
	}

IL_0030:
	{
		int32_t L_5 = V_1;
		return L_5;
	}
}
// System.Int32 LipingShare.LCLib.Asn1Processor.Asn1Util::DERLengthEncode(System.IO.Stream,System.UInt64)
extern Il2CppClass* Asn1Util_t2059476207_il2cpp_TypeInfo_var;
extern const uint32_t Asn1Util_DERLengthEncode_m3789574000_MetadataUsageId;
extern "C"  int32_t Asn1Util_DERLengthEncode_m3789574000 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___xdata0, uint64_t ___length1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Util_DERLengthEncode_m3789574000_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		uint64_t L_0 = ___length1;
		if ((!(((uint64_t)L_0) <= ((uint64_t)(((int64_t)((int64_t)((int32_t)127))))))))
		{
			goto IL_001f;
		}
	}
	{
		Stream_t3255436806 * L_1 = ___xdata0;
		uint64_t L_2 = ___length1;
		NullCheck(L_1);
		VirtActionInvoker1< uint8_t >::Invoke(19 /* System.Void System.IO.Stream::WriteByte(System.Byte) */, L_1, (((int32_t)((uint8_t)L_2))));
		int32_t L_3 = V_0;
		V_0 = ((int32_t)((int32_t)L_3+(int32_t)1));
		goto IL_0066;
	}

IL_001f:
	{
		Stream_t3255436806 * L_4 = ___xdata0;
		uint64_t L_5 = ___length1;
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		int32_t L_6 = Asn1Util_BytePrecision_m2588186404(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		VirtActionInvoker1< uint8_t >::Invoke(19 /* System.Void System.IO.Stream::WriteByte(System.Byte) */, L_4, (((int32_t)((uint8_t)((int32_t)((int32_t)L_6|(int32_t)((int32_t)128)))))));
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
		uint64_t L_8 = ___length1;
		int32_t L_9 = Asn1Util_BytePrecision_m2588186404(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		goto IL_005e;
	}

IL_0043:
	{
		Stream_t3255436806 * L_10 = ___xdata0;
		uint64_t L_11 = ___length1;
		int32_t L_12 = V_1;
		NullCheck(L_10);
		VirtActionInvoker1< uint8_t >::Invoke(19 /* System.Void System.IO.Stream::WriteByte(System.Byte) */, L_10, (((int32_t)((uint8_t)((int64_t)((uint64_t)L_11>>((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_12-(int32_t)1))*(int32_t)8))&(int32_t)((int32_t)63)))))))));
		int32_t L_13 = V_0;
		V_0 = ((int32_t)((int32_t)L_13+(int32_t)1));
		int32_t L_14 = V_1;
		V_1 = ((int32_t)((int32_t)L_14-(int32_t)1));
	}

IL_005e:
	{
		int32_t L_15 = V_1;
		if ((((int32_t)L_15) > ((int32_t)0)))
		{
			goto IL_0043;
		}
	}
	{
	}

IL_0066:
	{
		int32_t L_16 = V_0;
		V_2 = L_16;
		goto IL_006d;
	}

IL_006d:
	{
		int32_t L_17 = V_2;
		return L_17;
	}
}
// System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Util::DerLengthDecode(System.IO.Stream,System.Boolean&)
extern "C"  int64_t Asn1Util_DerLengthDecode_m23376285 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___bt0, bool* ___isIndefiniteLength1, const MethodInfo* method)
{
	int64_t V_0 = 0;
	uint8_t V_1 = 0x0;
	int64_t V_2 = 0;
	int64_t V_3 = 0;
	{
		bool* L_0 = ___isIndefiniteLength1;
		*((int8_t*)(L_0)) = (int8_t)0;
		V_0 = (((int64_t)((int64_t)0)));
		Stream_t3255436806 * L_1 = ___bt0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.Stream::ReadByte() */, L_1);
		V_1 = (((int32_t)((uint8_t)L_2)));
		uint8_t L_3 = V_1;
		if (((int32_t)((int32_t)L_3&(int32_t)((int32_t)128))))
		{
			goto IL_0025;
		}
	}
	{
		uint8_t L_4 = V_1;
		V_0 = (((int64_t)((int64_t)L_4)));
		goto IL_007c;
	}

IL_0025:
	{
		uint8_t L_5 = V_1;
		V_2 = (((int64_t)((int64_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)127))))));
		int64_t L_6 = V_2;
		if ((!(((uint64_t)L_6) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_0041;
		}
	}
	{
		bool* L_7 = ___isIndefiniteLength1;
		*((int8_t*)(L_7)) = (int8_t)1;
		V_3 = (((int64_t)((int64_t)((int32_t)-2))));
		goto IL_0083;
	}

IL_0041:
	{
		V_0 = (((int64_t)((int64_t)0)));
		goto IL_006e;
	}

IL_0049:
	{
		int64_t L_8 = V_0;
		if ((((int64_t)((int64_t)((int64_t)L_8>>(int32_t)((int32_t)24)))) <= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_005e;
		}
	}
	{
		V_3 = (((int64_t)((int64_t)(-1))));
		goto IL_0083;
	}

IL_005e:
	{
		Stream_t3255436806 * L_9 = ___bt0;
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.Stream::ReadByte() */, L_9);
		V_1 = (((int32_t)((uint8_t)L_10)));
		int64_t L_11 = V_0;
		uint8_t L_12 = V_1;
		V_0 = ((int64_t)((int64_t)((int64_t)((int64_t)L_11<<(int32_t)8))|(int64_t)(((int64_t)((int64_t)L_12)))));
	}

IL_006e:
	{
		int64_t L_13 = V_2;
		int64_t L_14 = L_13;
		V_2 = ((int64_t)((int64_t)L_14-(int64_t)(((int64_t)((int64_t)1)))));
		if ((((int64_t)L_14) > ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0049;
		}
	}
	{
	}

IL_007c:
	{
		int64_t L_15 = V_0;
		V_3 = L_15;
		goto IL_0083;
	}

IL_0083:
	{
		int64_t L_16 = V_3;
		return L_16;
	}
}
// System.String LipingShare.LCLib.Asn1Processor.Asn1Util::GetTagName(System.Byte)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern Il2CppCodeGenString* _stringLiteral4095345127;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern Il2CppCodeGenString* _stringLiteral1366820792;
extern Il2CppCodeGenString* _stringLiteral1741713465;
extern Il2CppCodeGenString* _stringLiteral1759529458;
extern Il2CppCodeGenString* _stringLiteral3918829649;
extern Il2CppCodeGenString* _stringLiteral1583067846;
extern Il2CppCodeGenString* _stringLiteral1481211364;
extern Il2CppCodeGenString* _stringLiteral2965372490;
extern Il2CppCodeGenString* _stringLiteral299294434;
extern Il2CppCodeGenString* _stringLiteral3390779443;
extern Il2CppCodeGenString* _stringLiteral2871608316;
extern Il2CppCodeGenString* _stringLiteral733421318;
extern Il2CppCodeGenString* _stringLiteral3803580443;
extern Il2CppCodeGenString* _stringLiteral1643924649;
extern Il2CppCodeGenString* _stringLiteral1132179826;
extern Il2CppCodeGenString* _stringLiteral2653595592;
extern Il2CppCodeGenString* _stringLiteral1596761610;
extern Il2CppCodeGenString* _stringLiteral2919529207;
extern Il2CppCodeGenString* _stringLiteral1596708418;
extern Il2CppCodeGenString* _stringLiteral2989205142;
extern Il2CppCodeGenString* _stringLiteral3428014470;
extern Il2CppCodeGenString* _stringLiteral2826649596;
extern Il2CppCodeGenString* _stringLiteral986666689;
extern Il2CppCodeGenString* _stringLiteral427713132;
extern Il2CppCodeGenString* _stringLiteral3978946587;
extern Il2CppCodeGenString* _stringLiteral3913598761;
extern Il2CppCodeGenString* _stringLiteral1855361963;
extern Il2CppCodeGenString* _stringLiteral3932476711;
extern Il2CppCodeGenString* _stringLiteral2540554515;
extern Il2CppCodeGenString* _stringLiteral1152437552;
extern Il2CppCodeGenString* _stringLiteral3827221042;
extern Il2CppCodeGenString* _stringLiteral2715145864;
extern const uint32_t Asn1Util_GetTagName_m2899215986_MetadataUsageId;
extern "C"  String_t* Asn1Util_GetTagName_m2899215986 (Il2CppObject * __this /* static, unused */, uint8_t ___tag0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Util_GetTagName_m2899215986_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	String_t* V_8 = NULL;
	{
		V_0 = _stringLiteral371857150;
		uint8_t L_0 = ___tag0;
		if (!((int32_t)((int32_t)L_0&(int32_t)((int32_t)192))))
		{
			goto IL_011e;
		}
	}
	{
		uint8_t L_1 = ___tag0;
		V_1 = ((int32_t)((int32_t)L_1&(int32_t)((int32_t)192)));
		int32_t L_2 = V_1;
		if (!L_2)
		{
			goto IL_00ef;
		}
	}
	{
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)32))))
		{
			goto IL_00c6;
		}
	}
	{
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)64))))
		{
			goto IL_0075;
		}
	}
	{
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)128))))
		{
			goto IL_004d;
		}
	}
	{
		int32_t L_6 = V_1;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)192))))
		{
			goto IL_009d;
		}
	}
	{
		goto IL_0118;
	}

IL_004d:
	{
		String_t* L_7 = V_0;
		uint8_t L_8 = ___tag0;
		V_2 = ((int32_t)((int32_t)L_8&(int32_t)((int32_t)31)));
		String_t* L_9 = Int32_ToString_m2960866144((&V_2), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1561703559(NULL /*static, unused*/, L_7, _stringLiteral4095345127, L_9, _stringLiteral372029317, /*hidden argument*/NULL);
		V_0 = L_10;
		goto IL_0118;
	}

IL_0075:
	{
		String_t* L_11 = V_0;
		uint8_t L_12 = ___tag0;
		V_3 = ((int32_t)((int32_t)L_12&(int32_t)((int32_t)31)));
		String_t* L_13 = Int32_ToString_m2960866144((&V_3), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m1561703559(NULL /*static, unused*/, L_11, _stringLiteral1366820792, L_13, _stringLiteral372029317, /*hidden argument*/NULL);
		V_0 = L_14;
		goto IL_0118;
	}

IL_009d:
	{
		String_t* L_15 = V_0;
		uint8_t L_16 = ___tag0;
		V_4 = ((int32_t)((int32_t)L_16&(int32_t)((int32_t)31)));
		String_t* L_17 = Int32_ToString_m2960866144((&V_4), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m1561703559(NULL /*static, unused*/, L_15, _stringLiteral1741713465, L_17, _stringLiteral372029317, /*hidden argument*/NULL);
		V_0 = L_18;
		goto IL_0118;
	}

IL_00c6:
	{
		String_t* L_19 = V_0;
		uint8_t L_20 = ___tag0;
		V_5 = ((int32_t)((int32_t)L_20&(int32_t)((int32_t)31)));
		String_t* L_21 = Int32_ToString_m2960866144((&V_5), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m1561703559(NULL /*static, unused*/, L_19, _stringLiteral1759529458, L_21, _stringLiteral372029317, /*hidden argument*/NULL);
		V_0 = L_22;
		goto IL_0118;
	}

IL_00ef:
	{
		String_t* L_23 = V_0;
		uint8_t L_24 = ___tag0;
		V_6 = ((int32_t)((int32_t)L_24&(int32_t)((int32_t)31)));
		String_t* L_25 = Int32_ToString_m2960866144((&V_6), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m1561703559(NULL /*static, unused*/, L_23, _stringLiteral3918829649, L_25, _stringLiteral372029317, /*hidden argument*/NULL);
		V_0 = L_26;
		goto IL_0118;
	}

IL_0118:
	{
		goto IL_0377;
	}

IL_011e:
	{
		uint8_t L_27 = ___tag0;
		V_7 = ((int32_t)((int32_t)L_27&(int32_t)((int32_t)31)));
		int32_t L_28 = V_7;
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 0)
		{
			goto IL_01ab;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 1)
		{
			goto IL_01bc;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 2)
		{
			goto IL_01cd;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 3)
		{
			goto IL_01de;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 4)
		{
			goto IL_01ef;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 5)
		{
			goto IL_0200;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 6)
		{
			goto IL_0211;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 7)
		{
			goto IL_0233;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 8)
		{
			goto IL_0244;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 9)
		{
			goto IL_0255;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 10)
		{
			goto IL_0365;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 11)
		{
			goto IL_0266;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 12)
		{
			goto IL_0222;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 13)
		{
			goto IL_0365;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 14)
		{
			goto IL_0365;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 15)
		{
			goto IL_0277;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 16)
		{
			goto IL_0288;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 17)
		{
			goto IL_0299;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 18)
		{
			goto IL_02aa;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 19)
		{
			goto IL_02bb;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 20)
		{
			goto IL_02cc;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 21)
		{
			goto IL_02dd;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 22)
		{
			goto IL_02ee;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 23)
		{
			goto IL_02ff;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 24)
		{
			goto IL_0310;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 25)
		{
			goto IL_0321;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 26)
		{
			goto IL_0332;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 27)
		{
			goto IL_0343;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 28)
		{
			goto IL_0365;
		}
		if (((int32_t)((int32_t)L_28-(int32_t)1)) == 29)
		{
			goto IL_0354;
		}
	}
	{
		goto IL_0365;
	}

IL_01ab:
	{
		String_t* L_29 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m2596409543(NULL /*static, unused*/, L_29, _stringLiteral1583067846, /*hidden argument*/NULL);
		V_0 = L_30;
		goto IL_0376;
	}

IL_01bc:
	{
		String_t* L_31 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m2596409543(NULL /*static, unused*/, L_31, _stringLiteral1481211364, /*hidden argument*/NULL);
		V_0 = L_32;
		goto IL_0376;
	}

IL_01cd:
	{
		String_t* L_33 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Concat_m2596409543(NULL /*static, unused*/, L_33, _stringLiteral2965372490, /*hidden argument*/NULL);
		V_0 = L_34;
		goto IL_0376;
	}

IL_01de:
	{
		String_t* L_35 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_36 = String_Concat_m2596409543(NULL /*static, unused*/, L_35, _stringLiteral299294434, /*hidden argument*/NULL);
		V_0 = L_36;
		goto IL_0376;
	}

IL_01ef:
	{
		String_t* L_37 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_38 = String_Concat_m2596409543(NULL /*static, unused*/, L_37, _stringLiteral3390779443, /*hidden argument*/NULL);
		V_0 = L_38;
		goto IL_0376;
	}

IL_0200:
	{
		String_t* L_39 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_40 = String_Concat_m2596409543(NULL /*static, unused*/, L_39, _stringLiteral2871608316, /*hidden argument*/NULL);
		V_0 = L_40;
		goto IL_0376;
	}

IL_0211:
	{
		String_t* L_41 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_42 = String_Concat_m2596409543(NULL /*static, unused*/, L_41, _stringLiteral733421318, /*hidden argument*/NULL);
		V_0 = L_42;
		goto IL_0376;
	}

IL_0222:
	{
		String_t* L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = String_Concat_m2596409543(NULL /*static, unused*/, L_43, _stringLiteral3803580443, /*hidden argument*/NULL);
		V_0 = L_44;
		goto IL_0376;
	}

IL_0233:
	{
		String_t* L_45 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_46 = String_Concat_m2596409543(NULL /*static, unused*/, L_45, _stringLiteral1643924649, /*hidden argument*/NULL);
		V_0 = L_46;
		goto IL_0376;
	}

IL_0244:
	{
		String_t* L_47 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = String_Concat_m2596409543(NULL /*static, unused*/, L_47, _stringLiteral1132179826, /*hidden argument*/NULL);
		V_0 = L_48;
		goto IL_0376;
	}

IL_0255:
	{
		String_t* L_49 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_50 = String_Concat_m2596409543(NULL /*static, unused*/, L_49, _stringLiteral2653595592, /*hidden argument*/NULL);
		V_0 = L_50;
		goto IL_0376;
	}

IL_0266:
	{
		String_t* L_51 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_52 = String_Concat_m2596409543(NULL /*static, unused*/, L_51, _stringLiteral1596761610, /*hidden argument*/NULL);
		V_0 = L_52;
		goto IL_0376;
	}

IL_0277:
	{
		String_t* L_53 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_54 = String_Concat_m2596409543(NULL /*static, unused*/, L_53, _stringLiteral2919529207, /*hidden argument*/NULL);
		V_0 = L_54;
		goto IL_0376;
	}

IL_0288:
	{
		String_t* L_55 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_56 = String_Concat_m2596409543(NULL /*static, unused*/, L_55, _stringLiteral1596708418, /*hidden argument*/NULL);
		V_0 = L_56;
		goto IL_0376;
	}

IL_0299:
	{
		String_t* L_57 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_58 = String_Concat_m2596409543(NULL /*static, unused*/, L_57, _stringLiteral2989205142, /*hidden argument*/NULL);
		V_0 = L_58;
		goto IL_0376;
	}

IL_02aa:
	{
		String_t* L_59 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_60 = String_Concat_m2596409543(NULL /*static, unused*/, L_59, _stringLiteral3428014470, /*hidden argument*/NULL);
		V_0 = L_60;
		goto IL_0376;
	}

IL_02bb:
	{
		String_t* L_61 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_62 = String_Concat_m2596409543(NULL /*static, unused*/, L_61, _stringLiteral2826649596, /*hidden argument*/NULL);
		V_0 = L_62;
		goto IL_0376;
	}

IL_02cc:
	{
		String_t* L_63 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_64 = String_Concat_m2596409543(NULL /*static, unused*/, L_63, _stringLiteral986666689, /*hidden argument*/NULL);
		V_0 = L_64;
		goto IL_0376;
	}

IL_02dd:
	{
		String_t* L_65 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_66 = String_Concat_m2596409543(NULL /*static, unused*/, L_65, _stringLiteral427713132, /*hidden argument*/NULL);
		V_0 = L_66;
		goto IL_0376;
	}

IL_02ee:
	{
		String_t* L_67 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_68 = String_Concat_m2596409543(NULL /*static, unused*/, L_67, _stringLiteral3978946587, /*hidden argument*/NULL);
		V_0 = L_68;
		goto IL_0376;
	}

IL_02ff:
	{
		String_t* L_69 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_70 = String_Concat_m2596409543(NULL /*static, unused*/, L_69, _stringLiteral3913598761, /*hidden argument*/NULL);
		V_0 = L_70;
		goto IL_0376;
	}

IL_0310:
	{
		String_t* L_71 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_72 = String_Concat_m2596409543(NULL /*static, unused*/, L_71, _stringLiteral1855361963, /*hidden argument*/NULL);
		V_0 = L_72;
		goto IL_0376;
	}

IL_0321:
	{
		String_t* L_73 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_74 = String_Concat_m2596409543(NULL /*static, unused*/, L_73, _stringLiteral3932476711, /*hidden argument*/NULL);
		V_0 = L_74;
		goto IL_0376;
	}

IL_0332:
	{
		String_t* L_75 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_76 = String_Concat_m2596409543(NULL /*static, unused*/, L_75, _stringLiteral2540554515, /*hidden argument*/NULL);
		V_0 = L_76;
		goto IL_0376;
	}

IL_0343:
	{
		String_t* L_77 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_78 = String_Concat_m2596409543(NULL /*static, unused*/, L_77, _stringLiteral1152437552, /*hidden argument*/NULL);
		V_0 = L_78;
		goto IL_0376;
	}

IL_0354:
	{
		String_t* L_79 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_80 = String_Concat_m2596409543(NULL /*static, unused*/, L_79, _stringLiteral3827221042, /*hidden argument*/NULL);
		V_0 = L_80;
		goto IL_0376;
	}

IL_0365:
	{
		String_t* L_81 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_82 = String_Concat_m2596409543(NULL /*static, unused*/, L_81, _stringLiteral2715145864, /*hidden argument*/NULL);
		V_0 = L_82;
		goto IL_0376;
	}

IL_0376:
	{
	}

IL_0377:
	{
		String_t* L_83 = V_0;
		V_8 = L_83;
		goto IL_037f;
	}

IL_037f:
	{
		String_t* L_84 = V_8;
		return L_84;
	}
}
// System.Void LipingShare.LCLib.Asn1Processor.Asn1Util::.cctor()
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Asn1Util_t2059476207_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305142____U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_0_FieldInfo_var;
extern const uint32_t Asn1Util__cctor_m4280155068_MetadataUsageId;
extern "C"  void Asn1Util__cctor_m4280155068 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Asn1Util__cctor_m4280155068_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CharU5BU5D_t1328083999* L_0 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305142____U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_0_FieldInfo_var), /*hidden argument*/NULL);
		((Asn1Util_t2059476207_StaticFields*)Asn1Util_t2059476207_il2cpp_TypeInfo_var->static_fields)->set_hexDigits_0(L_0);
		return;
	}
}
// System.Void LipingShare.LCLib.Asn1Processor.Oid::.ctor()
extern "C"  void Oid__ctor_m3384108428 (Oid_t113668572 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String LipingShare.LCLib.Asn1Processor.Oid::GetOidName(System.String)
extern Il2CppClass* Oid_t113668572_il2cpp_TypeInfo_var;
extern Il2CppClass* StringDictionary_t1070889667_il2cpp_TypeInfo_var;
extern const uint32_t Oid_GetOidName_m1983955852_MetadataUsageId;
extern "C"  String_t* Oid_GetOidName_m1983955852 (Oid_t113668572 * __this, String_t* ___inOidStr0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Oid_GetOidName_m1983955852_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Oid_t113668572_il2cpp_TypeInfo_var);
		StringDictionary_t1070889667 * L_0 = ((Oid_t113668572_StaticFields*)Oid_t113668572_il2cpp_TypeInfo_var->static_fields)->get_oidDictionary_0();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		StringDictionary_t1070889667 * L_1 = (StringDictionary_t1070889667 *)il2cpp_codegen_object_new(StringDictionary_t1070889667_il2cpp_TypeInfo_var);
		StringDictionary__ctor_m270184480(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Oid_t113668572_il2cpp_TypeInfo_var);
		((Oid_t113668572_StaticFields*)Oid_t113668572_il2cpp_TypeInfo_var->static_fields)->set_oidDictionary_0(L_1);
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Oid_t113668572_il2cpp_TypeInfo_var);
		StringDictionary_t1070889667 * L_2 = ((Oid_t113668572_StaticFields*)Oid_t113668572_il2cpp_TypeInfo_var->static_fields)->get_oidDictionary_0();
		String_t* L_3 = ___inOidStr0;
		NullCheck(L_2);
		String_t* L_4 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(5 /* System.String System.Collections.Specialized.StringDictionary::get_Item(System.String) */, L_2, L_3);
		V_0 = L_4;
		goto IL_0028;
	}

IL_0028:
	{
		String_t* L_5 = V_0;
		return L_5;
	}
}
// System.String LipingShare.LCLib.Asn1Processor.Oid::Decode(System.Byte[])
extern Il2CppClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern const uint32_t Oid_Decode_m2891545618_MetadataUsageId;
extern "C"  String_t* Oid_Decode_m2891545618 (Oid_t113668572 * __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Oid_Decode_m2891545618_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MemoryStream_t743994179 * V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	{
		ByteU5BU5D_t3397334013* L_0 = ___data0;
		MemoryStream_t743994179 * L_1 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m4073175573(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		MemoryStream_t743994179 * L_2 = V_0;
		NullCheck(L_2);
		VirtActionInvoker1< int64_t >::Invoke(10 /* System.Void System.IO.Stream::set_Position(System.Int64) */, L_2, (((int64_t)((int64_t)0))));
		MemoryStream_t743994179 * L_3 = V_0;
		String_t* L_4 = VirtFuncInvoker1< String_t*, Stream_t3255436806 * >::Invoke(4 /* System.String LipingShare.LCLib.Asn1Processor.Oid::Decode(System.IO.Stream) */, __this, L_3);
		V_1 = L_4;
		MemoryStream_t743994179 * L_5 = V_0;
		NullCheck(L_5);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_5);
		String_t* L_6 = V_1;
		V_2 = L_6;
		goto IL_0025;
	}

IL_0025:
	{
		String_t* L_7 = V_2;
		return L_7;
	}
}
// System.String LipingShare.LCLib.Asn1Processor.Oid::Decode(System.IO.Stream)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern Il2CppCodeGenString* _stringLiteral372029316;
extern Il2CppCodeGenString* _stringLiteral3026178547;
extern const uint32_t Oid_Decode_m2891299360_MetadataUsageId;
extern "C"  String_t* Oid_Decode_m2891299360 (Oid_t113668572 * __this, Stream_t3255436806 * ___bt0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Oid_Decode_m2891299360_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	uint8_t V_1 = 0x0;
	uint64_t V_2 = 0;
	Exception_t1927440687 * V_3 = NULL;
	String_t* V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = _stringLiteral371857150;
		V_2 = (((int64_t)((int64_t)0)));
		Stream_t3255436806 * L_0 = ___bt0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.Stream::ReadByte() */, L_0);
		V_1 = (((int32_t)((uint8_t)L_1)));
		String_t* L_2 = V_0;
		uint8_t L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		String_t* L_4 = Convert_ToString_m2425590390(NULL /*static, unused*/, ((int32_t)((int32_t)L_3/(int32_t)((int32_t)40))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m2596409543(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		String_t* L_6 = V_0;
		uint8_t L_7 = V_1;
		String_t* L_8 = Convert_ToString_m2425590390(NULL /*static, unused*/, ((int32_t)((int32_t)L_7%(int32_t)((int32_t)40))), /*hidden argument*/NULL);
		String_t* L_9 = String_Concat_m612901809(NULL /*static, unused*/, L_6, _stringLiteral372029316, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0080;
	}

IL_003c:
	{
	}

IL_003d:
	try
	{ // begin try (depth: 1)
		Stream_t3255436806 * L_10 = ___bt0;
		Oid_DecodeValue_m1983536933(__this, L_10, (&V_2), /*hidden argument*/NULL);
		String_t* L_11 = V_0;
		String_t* L_12 = UInt64_ToString_m446228920((&V_2), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m612901809(NULL /*static, unused*/, L_11, _stringLiteral372029316, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		goto IL_007f;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0067;
		throw e;
	}

CATCH_0067:
	{ // begin catch(System.Exception)
		V_3 = ((Exception_t1927440687 *)__exception_local);
		Exception_t1927440687 * L_14 = V_3;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3026178547, L_15, /*hidden argument*/NULL);
		Exception_t1927440687 * L_17 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(L_17, L_16, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
	} // end catch (depth: 1)

IL_007f:
	{
	}

IL_0080:
	{
		Stream_t3255436806 * L_18 = ___bt0;
		NullCheck(L_18);
		int64_t L_19 = VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.Stream::get_Position() */, L_18);
		Stream_t3255436806 * L_20 = ___bt0;
		NullCheck(L_20);
		int64_t L_21 = VirtFuncInvoker0< int64_t >::Invoke(8 /* System.Int64 System.IO.Stream::get_Length() */, L_20);
		if ((((int64_t)L_19) < ((int64_t)L_21)))
		{
			goto IL_003c;
		}
	}
	{
		String_t* L_22 = V_0;
		V_4 = L_22;
		goto IL_0099;
	}

IL_0099:
	{
		String_t* L_23 = V_4;
		return L_23;
	}
}
// System.Int32 LipingShare.LCLib.Asn1Processor.Oid::DecodeValue(System.IO.Stream,System.UInt64&)
extern "C"  int32_t Oid_DecodeValue_m1983536933 (Oid_t113668572 * __this, Stream_t3255436806 * ___bt0, uint64_t* ___v1, const MethodInfo* method)
{
	uint8_t V_0 = 0x0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_1 = 0;
		uint64_t* L_0 = ___v1;
		*((int64_t*)(L_0)) = (int64_t)(((int64_t)((int64_t)0)));
	}

IL_0007:
	{
		Stream_t3255436806 * L_1 = ___bt0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.Stream::ReadByte() */, L_1);
		V_0 = (((int32_t)((uint8_t)L_2)));
		int32_t L_3 = V_1;
		V_1 = ((int32_t)((int32_t)L_3+(int32_t)1));
		uint64_t* L_4 = ___v1;
		uint64_t* L_5 = ___v1;
		*((int64_t*)(L_4)) = (int64_t)((int64_t)((int64_t)(*((int64_t*)L_5))<<(int32_t)7));
		uint64_t* L_6 = ___v1;
		uint64_t* L_7 = ___v1;
		uint8_t L_8 = V_0;
		*((int64_t*)(L_6)) = (int64_t)((int64_t)((int64_t)(*((int64_t*)L_7))+(int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)L_8&(int32_t)((int32_t)127))))))));
		uint8_t L_9 = V_0;
		if (((int32_t)((int32_t)L_9&(int32_t)((int32_t)128))))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_10 = V_1;
		V_2 = L_10;
		goto IL_003e;
	}

IL_0038:
	{
		goto IL_0007;
	}

IL_003e:
	{
		int32_t L_11 = V_2;
		return L_11;
	}
}
// System.Void LipingShare.LCLib.Asn1Processor.Oid::.cctor()
extern Il2CppClass* Oid_t113668572_il2cpp_TypeInfo_var;
extern const uint32_t Oid__cctor_m4076678605_MetadataUsageId;
extern "C"  void Oid__cctor_m4076678605 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Oid__cctor_m4076678605_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((Oid_t113668572_StaticFields*)Oid_t113668572_il2cpp_TypeInfo_var->static_fields)->set_oidDictionary_0((StringDictionary_t1070889667 *)NULL);
		return;
	}
}
// System.Void LipingShare.LCLib.Asn1Processor.RelativeOid::.ctor()
extern Il2CppClass* Oid_t113668572_il2cpp_TypeInfo_var;
extern const uint32_t RelativeOid__ctor_m2864312816_MetadataUsageId;
extern "C"  void RelativeOid__ctor_m2864312816 (RelativeOid_t880150712 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RelativeOid__ctor_m2864312816_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Oid_t113668572_il2cpp_TypeInfo_var);
		Oid__ctor_m3384108428(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String LipingShare.LCLib.Asn1Processor.RelativeOid::Decode(System.IO.Stream)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern Il2CppCodeGenString* _stringLiteral372029316;
extern Il2CppCodeGenString* _stringLiteral3026178547;
extern const uint32_t RelativeOid_Decode_m1265304508_MetadataUsageId;
extern "C"  String_t* RelativeOid_Decode_m1265304508 (RelativeOid_t880150712 * __this, Stream_t3255436806 * ___bt0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RelativeOid_Decode_m1265304508_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	uint64_t V_1 = 0;
	bool V_2 = false;
	Exception_t1927440687 * V_3 = NULL;
	String_t* V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = _stringLiteral371857150;
		V_1 = (((int64_t)((int64_t)0)));
		V_2 = (bool)1;
		goto IL_0074;
	}

IL_0011:
	{
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			Stream_t3255436806 * L_0 = ___bt0;
			Oid_DecodeValue_m1983536933(__this, L_0, (&V_1), /*hidden argument*/NULL);
			bool L_1 = V_2;
			if (!L_1)
			{
				goto IL_003a;
			}
		}

IL_0023:
		{
			String_t* L_2 = UInt64_ToString_m446228920((&V_1), /*hidden argument*/NULL);
			V_0 = L_2;
			V_2 = (bool)0;
			goto IL_0055;
		}

IL_003a:
		{
			String_t* L_3 = V_0;
			String_t* L_4 = UInt64_ToString_m446228920((&V_1), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_5 = String_Concat_m612901809(NULL /*static, unused*/, L_3, _stringLiteral372029316, L_4, /*hidden argument*/NULL);
			V_0 = L_5;
		}

IL_0055:
		{
			goto IL_0073;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005b;
		throw e;
	}

CATCH_005b:
	{ // begin catch(System.Exception)
		V_3 = ((Exception_t1927440687 *)__exception_local);
		Exception_t1927440687 * L_6 = V_3;
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3026178547, L_7, /*hidden argument*/NULL);
		Exception_t1927440687 * L_9 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(L_9, L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	} // end catch (depth: 1)

IL_0073:
	{
	}

IL_0074:
	{
		Stream_t3255436806 * L_10 = ___bt0;
		NullCheck(L_10);
		int64_t L_11 = VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.Stream::get_Position() */, L_10);
		Stream_t3255436806 * L_12 = ___bt0;
		NullCheck(L_12);
		int64_t L_13 = VirtFuncInvoker0< int64_t >::Invoke(8 /* System.Int64 System.IO.Stream::get_Length() */, L_12);
		if ((((int64_t)L_11) < ((int64_t)L_13)))
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_14 = V_0;
		V_4 = L_14;
		goto IL_008d;
	}

IL_008d:
	{
		String_t* L_15 = V_4;
		return L_15;
	}
}
// System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::.ctor()
extern "C"  void AppleInAppPurchaseReceipt__ctor_m1941626255 (AppleInAppPurchaseReceipt_t3271698749 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::get_quantity()
extern "C"  int32_t AppleInAppPurchaseReceipt_get_quantity_m2899296427 (AppleInAppPurchaseReceipt_t3271698749 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CquantityU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_quantity(System.Int32)
extern "C"  void AppleInAppPurchaseReceipt_set_quantity_m347140636 (AppleInAppPurchaseReceipt_t3271698749 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CquantityU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::get_productID()
extern "C"  String_t* AppleInAppPurchaseReceipt_get_productID_m152993489 (AppleInAppPurchaseReceipt_t3271698749 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CproductIDU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_productID(System.String)
extern "C"  void AppleInAppPurchaseReceipt_set_productID_m2720522190 (AppleInAppPurchaseReceipt_t3271698749 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CproductIDU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::get_transactionID()
extern "C"  String_t* AppleInAppPurchaseReceipt_get_transactionID_m677217978 (AppleInAppPurchaseReceipt_t3271698749 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CtransactionIDU3Ek__BackingField_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_transactionID(System.String)
extern "C"  void AppleInAppPurchaseReceipt_set_transactionID_m3165163845 (AppleInAppPurchaseReceipt_t3271698749 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CtransactionIDU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::get_originalTransactionIdentifier()
extern "C"  String_t* AppleInAppPurchaseReceipt_get_originalTransactionIdentifier_m795053829 (AppleInAppPurchaseReceipt_t3271698749 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CoriginalTransactionIdentifierU3Ek__BackingField_3();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_originalTransactionIdentifier(System.String)
extern "C"  void AppleInAppPurchaseReceipt_set_originalTransactionIdentifier_m2823063774 (AppleInAppPurchaseReceipt_t3271698749 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CoriginalTransactionIdentifierU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.DateTime UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::get_purchaseDate()
extern "C"  DateTime_t693205669  AppleInAppPurchaseReceipt_get_purchaseDate_m1052795456 (AppleInAppPurchaseReceipt_t3271698749 * __this, const MethodInfo* method)
{
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DateTime_t693205669  L_0 = __this->get_U3CpurchaseDateU3Ek__BackingField_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		DateTime_t693205669  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_purchaseDate(System.DateTime)
extern "C"  void AppleInAppPurchaseReceipt_set_purchaseDate_m1082187471 (AppleInAppPurchaseReceipt_t3271698749 * __this, DateTime_t693205669  ___value0, const MethodInfo* method)
{
	{
		DateTime_t693205669  L_0 = ___value0;
		__this->set_U3CpurchaseDateU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_originalPurchaseDate(System.DateTime)
extern "C"  void AppleInAppPurchaseReceipt_set_originalPurchaseDate_m1920060578 (AppleInAppPurchaseReceipt_t3271698749 * __this, DateTime_t693205669  ___value0, const MethodInfo* method)
{
	{
		DateTime_t693205669  L_0 = ___value0;
		__this->set_U3CoriginalPurchaseDateU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.DateTime UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::get_subscriptionExpirationDate()
extern "C"  DateTime_t693205669  AppleInAppPurchaseReceipt_get_subscriptionExpirationDate_m3742016707 (AppleInAppPurchaseReceipt_t3271698749 * __this, const MethodInfo* method)
{
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DateTime_t693205669  L_0 = __this->get_U3CsubscriptionExpirationDateU3Ek__BackingField_6();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		DateTime_t693205669  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_subscriptionExpirationDate(System.DateTime)
extern "C"  void AppleInAppPurchaseReceipt_set_subscriptionExpirationDate_m2975097394 (AppleInAppPurchaseReceipt_t3271698749 * __this, DateTime_t693205669  ___value0, const MethodInfo* method)
{
	{
		DateTime_t693205669  L_0 = ___value0;
		__this->set_U3CsubscriptionExpirationDateU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.DateTime UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::get_cancellationDate()
extern "C"  DateTime_t693205669  AppleInAppPurchaseReceipt_get_cancellationDate_m4106835304 (AppleInAppPurchaseReceipt_t3271698749 * __this, const MethodInfo* method)
{
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DateTime_t693205669  L_0 = __this->get_U3CcancellationDateU3Ek__BackingField_7();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		DateTime_t693205669  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_cancellationDate(System.DateTime)
extern "C"  void AppleInAppPurchaseReceipt_set_cancellationDate_m2514312931 (AppleInAppPurchaseReceipt_t3271698749 * __this, DateTime_t693205669  ___value0, const MethodInfo* method)
{
	{
		DateTime_t693205669  L_0 = ___value0;
		__this->set_U3CcancellationDateU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.AppleReceipt::.ctor()
extern "C"  void AppleReceipt__ctor_m4128629250 (AppleReceipt_t3991411794 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Purchasing.Security.AppleReceipt::get_bundleID()
extern "C"  String_t* AppleReceipt_get_bundleID_m1725040983 (AppleReceipt_t3991411794 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CbundleIDU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.AppleReceipt::set_bundleID(System.String)
extern "C"  void AppleReceipt_set_bundleID_m1577937306 (AppleReceipt_t3991411794 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CbundleIDU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.AppleReceipt::set_appVersion(System.String)
extern "C"  void AppleReceipt_set_appVersion_m3571446326 (AppleReceipt_t3991411794 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CappVersionU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.AppleReceipt::set_opaque(System.Byte[])
extern "C"  void AppleReceipt_set_opaque_m2408428135 (AppleReceipt_t3991411794 * __this, ByteU5BU5D_t3397334013* ___value0, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = ___value0;
		__this->set_U3CopaqueU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.AppleReceipt::set_hash(System.Byte[])
extern "C"  void AppleReceipt_set_hash_m1608210220 (AppleReceipt_t3991411794 * __this, ByteU5BU5D_t3397334013* ___value0, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = ___value0;
		__this->set_U3ChashU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.AppleReceipt::set_originalApplicationVersion(System.String)
extern "C"  void AppleReceipt_set_originalApplicationVersion_m3318704410 (AppleReceipt_t3991411794 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CoriginalApplicationVersionU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.DateTime UnityEngine.Purchasing.Security.AppleReceipt::get_receiptCreationDate()
extern "C"  DateTime_t693205669  AppleReceipt_get_receiptCreationDate_m4060965251 (AppleReceipt_t3991411794 * __this, const MethodInfo* method)
{
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DateTime_t693205669  L_0 = __this->get_U3CreceiptCreationDateU3Ek__BackingField_5();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		DateTime_t693205669  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.AppleReceipt::set_receiptCreationDate(System.DateTime)
extern "C"  void AppleReceipt_set_receiptCreationDate_m3678109238 (AppleReceipt_t3991411794 * __this, DateTime_t693205669  ___value0, const MethodInfo* method)
{
	{
		DateTime_t693205669  L_0 = ___value0;
		__this->set_U3CreceiptCreationDateU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.AppleValidator::.ctor(System.Byte[])
extern Il2CppClass* X509Cert_t481809278_il2cpp_TypeInfo_var;
extern const uint32_t AppleValidator__ctor_m1455195731_MetadataUsageId;
extern "C"  void AppleValidator__ctor_m1455195731 (AppleValidator_t3837389912 * __this, ByteU5BU5D_t3397334013* ___appleRootCertificate0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AppleValidator__ctor_m1455195731_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_0 = ___appleRootCertificate0;
		X509Cert_t481809278 * L_1 = (X509Cert_t481809278 *)il2cpp_codegen_object_new(X509Cert_t481809278_il2cpp_TypeInfo_var);
		X509Cert__ctor_m3040169225(L_1, L_0, /*hidden argument*/NULL);
		__this->set_cert_0(L_1);
		return;
	}
}
// UnityEngine.Purchasing.Security.AppleReceipt UnityEngine.Purchasing.Security.AppleValidator::Validate(System.Byte[])
extern Il2CppClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern Il2CppClass* Asn1Parser_t914015216_il2cpp_TypeInfo_var;
extern Il2CppClass* PKCS7_t1974940522_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidSignatureException_t488933488_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t AppleValidator_Validate_m6433925_MetadataUsageId;
extern "C"  AppleReceipt_t3991411794 * AppleValidator_Validate_m6433925 (AppleValidator_t3837389912 * __this, ByteU5BU5D_t3397334013* ___receiptData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AppleValidator_Validate_m6433925_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MemoryStream_t743994179 * V_0 = NULL;
	Asn1Parser_t914015216 * V_1 = NULL;
	PKCS7_t1974940522 * V_2 = NULL;
	AppleReceipt_t3991411794 * V_3 = NULL;
	AppleReceipt_t3991411794 * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ByteU5BU5D_t3397334013* L_0 = ___receiptData0;
		MemoryStream_t743994179 * L_1 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m4073175573(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			Asn1Parser_t914015216 * L_2 = (Asn1Parser_t914015216 *)il2cpp_codegen_object_new(Asn1Parser_t914015216_il2cpp_TypeInfo_var);
			Asn1Parser__ctor_m2929298044(L_2, /*hidden argument*/NULL);
			V_1 = L_2;
			Asn1Parser_t914015216 * L_3 = V_1;
			MemoryStream_t743994179 * L_4 = V_0;
			NullCheck(L_3);
			Asn1Parser_LoadData_m2612632163(L_3, L_4, /*hidden argument*/NULL);
			Asn1Parser_t914015216 * L_5 = V_1;
			NullCheck(L_5);
			Asn1Node_t1770761751 * L_6 = Asn1Parser_get_RootNode_m542146928(L_5, /*hidden argument*/NULL);
			PKCS7_t1974940522 * L_7 = (PKCS7_t1974940522 *)il2cpp_codegen_object_new(PKCS7_t1974940522_il2cpp_TypeInfo_var);
			PKCS7__ctor_m1019290992(L_7, L_6, /*hidden argument*/NULL);
			V_2 = L_7;
			PKCS7_t1974940522 * L_8 = V_2;
			NullCheck(L_8);
			Asn1Node_t1770761751 * L_9 = PKCS7_get_data_m139975642(L_8, /*hidden argument*/NULL);
			AppleReceipt_t3991411794 * L_10 = AppleValidator_ParseReceipt_m3945723525(__this, L_9, /*hidden argument*/NULL);
			V_3 = L_10;
			PKCS7_t1974940522 * L_11 = V_2;
			X509Cert_t481809278 * L_12 = __this->get_cert_0();
			AppleReceipt_t3991411794 * L_13 = V_3;
			NullCheck(L_13);
			DateTime_t693205669  L_14 = AppleReceipt_get_receiptCreationDate_m4060965251(L_13, /*hidden argument*/NULL);
			NullCheck(L_11);
			bool L_15 = PKCS7_Verify_m738735600(L_11, L_12, L_14, /*hidden argument*/NULL);
			if (L_15)
			{
				goto IL_004d;
			}
		}

IL_0046:
		{
			InvalidSignatureException_t488933488 * L_16 = (InvalidSignatureException_t488933488 *)il2cpp_codegen_object_new(InvalidSignatureException_t488933488_il2cpp_TypeInfo_var);
			InvalidSignatureException__ctor_m883303240(L_16, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
		}

IL_004d:
		{
			AppleReceipt_t3991411794 * L_17 = V_3;
			V_4 = L_17;
			IL2CPP_LEAVE(0x62, FINALLY_0055);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0055;
	}

FINALLY_0055:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t743994179 * L_18 = V_0;
			if (!L_18)
			{
				goto IL_0061;
			}
		}

IL_005b:
		{
			MemoryStream_t743994179 * L_19 = V_0;
			NullCheck(L_19);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_19);
		}

IL_0061:
		{
			IL2CPP_END_FINALLY(85)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(85)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0062:
	{
		AppleReceipt_t3991411794 * L_20 = V_4;
		return L_20;
	}
}
// UnityEngine.Purchasing.Security.AppleReceipt UnityEngine.Purchasing.Security.AppleValidator::ParseReceipt(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern Il2CppClass* InvalidPKCS7Data_t4123278833_il2cpp_TypeInfo_var;
extern Il2CppClass* AppleReceipt_t3991411794_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2640819881_il2cpp_TypeInfo_var;
extern Il2CppClass* Asn1Util_t2059476207_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3642515373_MethodInfo_var;
extern const MethodInfo* List_1_Add_m69113193_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m4187629099_MethodInfo_var;
extern const uint32_t AppleValidator_ParseReceipt_m3945723525_MetadataUsageId;
extern "C"  AppleReceipt_t3991411794 * AppleValidator_ParseReceipt_m3945723525 (AppleValidator_t3837389912 * __this, Asn1Node_t1770761751 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AppleValidator_ParseReceipt_m3945723525_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Asn1Node_t1770761751 * V_0 = NULL;
	AppleReceipt_t3991411794 * V_1 = NULL;
	List_1_t2640819881 * V_2 = NULL;
	int32_t V_3 = 0;
	Asn1Node_t1770761751 * V_4 = NULL;
	int64_t V_5 = 0;
	Asn1Node_t1770761751 * V_6 = NULL;
	String_t* V_7 = NULL;
	DateTime_t693205669  V_8;
	memset(&V_8, 0, sizeof(V_8));
	AppleReceipt_t3991411794 * V_9 = NULL;
	{
		Asn1Node_t1770761751 * L_0 = ___data0;
		NullCheck(L_0);
		int64_t L_1 = Asn1Node_get_ChildNodeCount_m1123088750(L_0, /*hidden argument*/NULL);
		if ((((int64_t)L_1) == ((int64_t)(((int64_t)((int64_t)1))))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidPKCS7Data_t4123278833 * L_2 = (InvalidPKCS7Data_t4123278833 *)il2cpp_codegen_object_new(InvalidPKCS7Data_t4123278833_il2cpp_TypeInfo_var);
		InvalidPKCS7Data__ctor_m1217025319(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0015:
	{
		Asn1Node_t1770761751 * L_3 = ___data0;
		NullCheck(L_3);
		Asn1Node_t1770761751 * L_4 = Asn1Node_GetChildNode_m4133223467(L_3, 0, /*hidden argument*/NULL);
		V_0 = L_4;
		AppleReceipt_t3991411794 * L_5 = (AppleReceipt_t3991411794 *)il2cpp_codegen_object_new(AppleReceipt_t3991411794_il2cpp_TypeInfo_var);
		AppleReceipt__ctor_m4128629250(L_5, /*hidden argument*/NULL);
		V_1 = L_5;
		List_1_t2640819881 * L_6 = (List_1_t2640819881 *)il2cpp_codegen_object_new(List_1_t2640819881_il2cpp_TypeInfo_var);
		List_1__ctor_m3642515373(L_6, /*hidden argument*/List_1__ctor_m3642515373_MethodInfo_var);
		V_2 = L_6;
		V_3 = 0;
		goto IL_01ac;
	}

IL_0030:
	{
		Asn1Node_t1770761751 * L_7 = V_0;
		int32_t L_8 = V_3;
		NullCheck(L_7);
		Asn1Node_t1770761751 * L_9 = Asn1Node_GetChildNode_m4133223467(L_7, L_8, /*hidden argument*/NULL);
		V_4 = L_9;
		Asn1Node_t1770761751 * L_10 = V_4;
		NullCheck(L_10);
		int64_t L_11 = Asn1Node_get_ChildNodeCount_m1123088750(L_10, /*hidden argument*/NULL);
		if ((!(((uint64_t)L_11) == ((uint64_t)(((int64_t)((int64_t)3)))))))
		{
			goto IL_01a7;
		}
	}
	{
		Asn1Node_t1770761751 * L_12 = V_4;
		NullCheck(L_12);
		Asn1Node_t1770761751 * L_13 = Asn1Node_GetChildNode_m4133223467(L_12, 0, /*hidden argument*/NULL);
		NullCheck(L_13);
		ByteU5BU5D_t3397334013* L_14 = Asn1Node_get_Data_m1676178324(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		int64_t L_15 = Asn1Util_BytesToLong_m1963652187(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		V_5 = L_15;
		Asn1Node_t1770761751 * L_16 = V_4;
		NullCheck(L_16);
		Asn1Node_t1770761751 * L_17 = Asn1Node_GetChildNode_m4133223467(L_16, 2, /*hidden argument*/NULL);
		V_6 = L_17;
		int64_t L_18 = V_5;
		if ((((int64_t)L_18) < ((int64_t)(((int64_t)((int64_t)2))))))
		{
			goto IL_0094;
		}
	}
	{
		int64_t L_19 = V_5;
		if ((((int64_t)L_19) > ((int64_t)(((int64_t)((int64_t)5))))))
		{
			goto IL_0094;
		}
	}
	{
		int64_t L_20 = V_5;
		if ((((int32_t)((int32_t)((int64_t)((int64_t)L_20-(int64_t)(((int64_t)((int64_t)2)))))))) == 0)
		{
			goto IL_00cf;
		}
		if ((((int32_t)((int32_t)((int64_t)((int64_t)L_20-(int64_t)(((int64_t)((int64_t)2)))))))) == 1)
		{
			goto IL_00f1;
		}
		if ((((int32_t)((int32_t)((int64_t)((int64_t)L_20-(int64_t)(((int64_t)((int64_t)2)))))))) == 2)
		{
			goto IL_0113;
		}
		if ((((int32_t)((int32_t)((int64_t)((int64_t)L_20-(int64_t)(((int64_t)((int64_t)2)))))))) == 3)
		{
			goto IL_0125;
		}
	}

IL_0094:
	{
		int64_t L_21 = V_5;
		if ((((int64_t)L_21) < ((int64_t)(((int64_t)((int64_t)((int32_t)17)))))))
		{
			goto IL_00c0;
		}
	}
	{
		int64_t L_22 = V_5;
		if ((((int64_t)L_22) > ((int64_t)(((int64_t)((int64_t)((int32_t)19)))))))
		{
			goto IL_00c0;
		}
	}
	{
		int64_t L_23 = V_5;
		if ((((int32_t)((int32_t)((int64_t)((int64_t)L_23-(int64_t)(((int64_t)((int64_t)((int32_t)17))))))))) == 0)
		{
			goto IL_016b;
		}
		if ((((int32_t)((int32_t)((int64_t)((int64_t)L_23-(int64_t)(((int64_t)((int64_t)((int32_t)17))))))))) == 1)
		{
			goto IL_00c0;
		}
		if ((((int32_t)((int32_t)((int64_t)((int64_t)L_23-(int64_t)(((int64_t)((int64_t)((int32_t)17))))))))) == 2)
		{
			goto IL_0184;
		}
	}

IL_00c0:
	{
		int64_t L_24 = V_5;
		if ((((int64_t)L_24) == ((int64_t)(((int64_t)((int64_t)((int32_t)12)))))))
		{
			goto IL_0137;
		}
	}
	{
		goto IL_01a6;
	}

IL_00cf:
	{
		AppleReceipt_t3991411794 * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_26 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_27 = V_6;
		NullCheck(L_27);
		Asn1Node_t1770761751 * L_28 = Asn1Node_GetChildNode_m4133223467(L_27, 0, /*hidden argument*/NULL);
		NullCheck(L_28);
		ByteU5BU5D_t3397334013* L_29 = Asn1Node_get_Data_m1676178324(L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		String_t* L_30 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_26, L_29);
		NullCheck(L_25);
		AppleReceipt_set_bundleID_m1577937306(L_25, L_30, /*hidden argument*/NULL);
		goto IL_01a6;
	}

IL_00f1:
	{
		AppleReceipt_t3991411794 * L_31 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_32 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_33 = V_6;
		NullCheck(L_33);
		Asn1Node_t1770761751 * L_34 = Asn1Node_GetChildNode_m4133223467(L_33, 0, /*hidden argument*/NULL);
		NullCheck(L_34);
		ByteU5BU5D_t3397334013* L_35 = Asn1Node_get_Data_m1676178324(L_34, /*hidden argument*/NULL);
		NullCheck(L_32);
		String_t* L_36 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_32, L_35);
		NullCheck(L_31);
		AppleReceipt_set_appVersion_m3571446326(L_31, L_36, /*hidden argument*/NULL);
		goto IL_01a6;
	}

IL_0113:
	{
		AppleReceipt_t3991411794 * L_37 = V_1;
		Asn1Node_t1770761751 * L_38 = V_6;
		NullCheck(L_38);
		ByteU5BU5D_t3397334013* L_39 = Asn1Node_get_Data_m1676178324(L_38, /*hidden argument*/NULL);
		NullCheck(L_37);
		AppleReceipt_set_opaque_m2408428135(L_37, L_39, /*hidden argument*/NULL);
		goto IL_01a6;
	}

IL_0125:
	{
		AppleReceipt_t3991411794 * L_40 = V_1;
		Asn1Node_t1770761751 * L_41 = V_6;
		NullCheck(L_41);
		ByteU5BU5D_t3397334013* L_42 = Asn1Node_get_Data_m1676178324(L_41, /*hidden argument*/NULL);
		NullCheck(L_40);
		AppleReceipt_set_hash_m1608210220(L_40, L_42, /*hidden argument*/NULL);
		goto IL_01a6;
	}

IL_0137:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_43 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_44 = V_6;
		NullCheck(L_44);
		Asn1Node_t1770761751 * L_45 = Asn1Node_GetChildNode_m4133223467(L_44, 0, /*hidden argument*/NULL);
		NullCheck(L_45);
		ByteU5BU5D_t3397334013* L_46 = Asn1Node_get_Data_m1676178324(L_45, /*hidden argument*/NULL);
		NullCheck(L_43);
		String_t* L_47 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_43, L_46);
		V_7 = L_47;
		AppleReceipt_t3991411794 * L_48 = V_1;
		String_t* L_49 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_50 = DateTime_Parse_m1142721566(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		V_8 = L_50;
		DateTime_t693205669  L_51 = DateTime_ToUniversalTime_m1815024752((&V_8), /*hidden argument*/NULL);
		NullCheck(L_48);
		AppleReceipt_set_receiptCreationDate_m3678109238(L_48, L_51, /*hidden argument*/NULL);
		goto IL_01a6;
	}

IL_016b:
	{
		List_1_t2640819881 * L_52 = V_2;
		Asn1Node_t1770761751 * L_53 = V_6;
		NullCheck(L_53);
		Asn1Node_t1770761751 * L_54 = Asn1Node_GetChildNode_m4133223467(L_53, 0, /*hidden argument*/NULL);
		AppleInAppPurchaseReceipt_t3271698749 * L_55 = AppleValidator_ParseInAppReceipt_m3138849002(__this, L_54, /*hidden argument*/NULL);
		NullCheck(L_52);
		List_1_Add_m69113193(L_52, L_55, /*hidden argument*/List_1_Add_m69113193_MethodInfo_var);
		goto IL_01a6;
	}

IL_0184:
	{
		AppleReceipt_t3991411794 * L_56 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_57 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_58 = V_6;
		NullCheck(L_58);
		Asn1Node_t1770761751 * L_59 = Asn1Node_GetChildNode_m4133223467(L_58, 0, /*hidden argument*/NULL);
		NullCheck(L_59);
		ByteU5BU5D_t3397334013* L_60 = Asn1Node_get_Data_m1676178324(L_59, /*hidden argument*/NULL);
		NullCheck(L_57);
		String_t* L_61 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_57, L_60);
		NullCheck(L_56);
		AppleReceipt_set_originalApplicationVersion_m3318704410(L_56, L_61, /*hidden argument*/NULL);
		goto IL_01a6;
	}

IL_01a6:
	{
	}

IL_01a7:
	{
		int32_t L_62 = V_3;
		V_3 = ((int32_t)((int32_t)L_62+(int32_t)1));
	}

IL_01ac:
	{
		int32_t L_63 = V_3;
		Asn1Node_t1770761751 * L_64 = V_0;
		NullCheck(L_64);
		int64_t L_65 = Asn1Node_get_ChildNodeCount_m1123088750(L_64, /*hidden argument*/NULL);
		if ((((int64_t)(((int64_t)((int64_t)L_63)))) < ((int64_t)L_65)))
		{
			goto IL_0030;
		}
	}
	{
		AppleReceipt_t3991411794 * L_66 = V_1;
		List_1_t2640819881 * L_67 = V_2;
		NullCheck(L_67);
		AppleInAppPurchaseReceiptU5BU5D_t579068208* L_68 = List_1_ToArray_m4187629099(L_67, /*hidden argument*/List_1_ToArray_m4187629099_MethodInfo_var);
		NullCheck(L_66);
		L_66->set_inAppPurchaseReceipts_6(L_68);
		AppleReceipt_t3991411794 * L_69 = V_1;
		V_9 = L_69;
		goto IL_01cd;
	}

IL_01cd:
	{
		AppleReceipt_t3991411794 * L_70 = V_9;
		return L_70;
	}
}
// UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt UnityEngine.Purchasing.Security.AppleValidator::ParseInAppReceipt(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern Il2CppClass* AppleInAppPurchaseReceipt_t3271698749_il2cpp_TypeInfo_var;
extern Il2CppClass* Asn1Util_t2059476207_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern const uint32_t AppleValidator_ParseInAppReceipt_m3138849002_MetadataUsageId;
extern "C"  AppleInAppPurchaseReceipt_t3271698749 * AppleValidator_ParseInAppReceipt_m3138849002 (AppleValidator_t3837389912 * __this, Asn1Node_t1770761751 * ___inApp0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AppleValidator_ParseInAppReceipt_m3138849002_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AppleInAppPurchaseReceipt_t3271698749 * V_0 = NULL;
	int32_t V_1 = 0;
	Asn1Node_t1770761751 * V_2 = NULL;
	int64_t V_3 = 0;
	Asn1Node_t1770761751 * V_4 = NULL;
	AppleInAppPurchaseReceipt_t3271698749 * V_5 = NULL;
	{
		AppleInAppPurchaseReceipt_t3271698749 * L_0 = (AppleInAppPurchaseReceipt_t3271698749 *)il2cpp_codegen_object_new(AppleInAppPurchaseReceipt_t3271698749_il2cpp_TypeInfo_var);
		AppleInAppPurchaseReceipt__ctor_m1941626255(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_016d;
	}

IL_000e:
	{
		Asn1Node_t1770761751 * L_1 = ___inApp0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		Asn1Node_t1770761751 * L_3 = Asn1Node_GetChildNode_m4133223467(L_1, L_2, /*hidden argument*/NULL);
		V_2 = L_3;
		Asn1Node_t1770761751 * L_4 = V_2;
		NullCheck(L_4);
		int64_t L_5 = Asn1Node_get_ChildNodeCount_m1123088750(L_4, /*hidden argument*/NULL);
		if ((!(((uint64_t)L_5) == ((uint64_t)(((int64_t)((int64_t)3)))))))
		{
			goto IL_0168;
		}
	}
	{
		Asn1Node_t1770761751 * L_6 = V_2;
		NullCheck(L_6);
		Asn1Node_t1770761751 * L_7 = Asn1Node_GetChildNode_m4133223467(L_6, 0, /*hidden argument*/NULL);
		NullCheck(L_7);
		ByteU5BU5D_t3397334013* L_8 = Asn1Node_get_Data_m1676178324(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		int64_t L_9 = Asn1Util_BytesToLong_m1963652187(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		Asn1Node_t1770761751 * L_10 = V_2;
		NullCheck(L_10);
		Asn1Node_t1770761751 * L_11 = Asn1Node_GetChildNode_m4133223467(L_10, 2, /*hidden argument*/NULL);
		V_4 = L_11;
		int64_t L_12 = V_3;
		if ((((int64_t)L_12) < ((int64_t)(((int64_t)((int64_t)((int32_t)1701)))))))
		{
			goto IL_0167;
		}
	}
	{
		int64_t L_13 = V_3;
		if ((((int64_t)L_13) > ((int64_t)(((int64_t)((int64_t)((int32_t)1712)))))))
		{
			goto IL_0167;
		}
	}
	{
		int64_t L_14 = V_3;
		if ((((int32_t)((int32_t)((int64_t)((int64_t)L_14-(int64_t)(((int64_t)((int64_t)((int32_t)1701))))))))) == 0)
		{
			goto IL_009b;
		}
		if ((((int32_t)((int32_t)((int64_t)((int64_t)L_14-(int64_t)(((int64_t)((int64_t)((int32_t)1701))))))))) == 1)
		{
			goto IL_00b9;
		}
		if ((((int32_t)((int32_t)((int64_t)((int64_t)L_14-(int64_t)(((int64_t)((int64_t)((int32_t)1701))))))))) == 2)
		{
			goto IL_00db;
		}
		if ((((int32_t)((int32_t)((int64_t)((int64_t)L_14-(int64_t)(((int64_t)((int64_t)((int32_t)1701))))))))) == 3)
		{
			goto IL_011f;
		}
		if ((((int32_t)((int32_t)((int64_t)((int64_t)L_14-(int64_t)(((int64_t)((int64_t)((int32_t)1701))))))))) == 4)
		{
			goto IL_00fd;
		}
		if ((((int32_t)((int32_t)((int64_t)((int64_t)L_14-(int64_t)(((int64_t)((int64_t)((int32_t)1701))))))))) == 5)
		{
			goto IL_0131;
		}
		if ((((int32_t)((int32_t)((int64_t)((int64_t)L_14-(int64_t)(((int64_t)((int64_t)((int32_t)1701))))))))) == 6)
		{
			goto IL_0167;
		}
		if ((((int32_t)((int32_t)((int64_t)((int64_t)L_14-(int64_t)(((int64_t)((int64_t)((int32_t)1701))))))))) == 7)
		{
			goto IL_0143;
		}
		if ((((int32_t)((int32_t)((int64_t)((int64_t)L_14-(int64_t)(((int64_t)((int64_t)((int32_t)1701))))))))) == 8)
		{
			goto IL_0167;
		}
		if ((((int32_t)((int32_t)((int64_t)((int64_t)L_14-(int64_t)(((int64_t)((int64_t)((int32_t)1701))))))))) == 9)
		{
			goto IL_0167;
		}
		if ((((int32_t)((int32_t)((int64_t)((int64_t)L_14-(int64_t)(((int64_t)((int64_t)((int32_t)1701))))))))) == 10)
		{
			goto IL_0167;
		}
		if ((((int32_t)((int32_t)((int64_t)((int64_t)L_14-(int64_t)(((int64_t)((int64_t)((int32_t)1701))))))))) == 11)
		{
			goto IL_0155;
		}
	}
	{
		goto IL_0167;
	}

IL_009b:
	{
		AppleInAppPurchaseReceipt_t3271698749 * L_15 = V_0;
		Asn1Node_t1770761751 * L_16 = V_4;
		NullCheck(L_16);
		Asn1Node_t1770761751 * L_17 = Asn1Node_GetChildNode_m4133223467(L_16, 0, /*hidden argument*/NULL);
		NullCheck(L_17);
		ByteU5BU5D_t3397334013* L_18 = Asn1Node_get_Data_m1676178324(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		int64_t L_19 = Asn1Util_BytesToLong_m1963652187(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		NullCheck(L_15);
		AppleInAppPurchaseReceipt_set_quantity_m347140636(L_15, (((int32_t)((int32_t)L_19))), /*hidden argument*/NULL);
		goto IL_0167;
	}

IL_00b9:
	{
		AppleInAppPurchaseReceipt_t3271698749 * L_20 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_21 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_22 = V_4;
		NullCheck(L_22);
		Asn1Node_t1770761751 * L_23 = Asn1Node_GetChildNode_m4133223467(L_22, 0, /*hidden argument*/NULL);
		NullCheck(L_23);
		ByteU5BU5D_t3397334013* L_24 = Asn1Node_get_Data_m1676178324(L_23, /*hidden argument*/NULL);
		NullCheck(L_21);
		String_t* L_25 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_21, L_24);
		NullCheck(L_20);
		AppleInAppPurchaseReceipt_set_productID_m2720522190(L_20, L_25, /*hidden argument*/NULL);
		goto IL_0167;
	}

IL_00db:
	{
		AppleInAppPurchaseReceipt_t3271698749 * L_26 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_27 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_28 = V_4;
		NullCheck(L_28);
		Asn1Node_t1770761751 * L_29 = Asn1Node_GetChildNode_m4133223467(L_28, 0, /*hidden argument*/NULL);
		NullCheck(L_29);
		ByteU5BU5D_t3397334013* L_30 = Asn1Node_get_Data_m1676178324(L_29, /*hidden argument*/NULL);
		NullCheck(L_27);
		String_t* L_31 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_27, L_30);
		NullCheck(L_26);
		AppleInAppPurchaseReceipt_set_transactionID_m3165163845(L_26, L_31, /*hidden argument*/NULL);
		goto IL_0167;
	}

IL_00fd:
	{
		AppleInAppPurchaseReceipt_t3271698749 * L_32 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_33 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_34 = V_4;
		NullCheck(L_34);
		Asn1Node_t1770761751 * L_35 = Asn1Node_GetChildNode_m4133223467(L_34, 0, /*hidden argument*/NULL);
		NullCheck(L_35);
		ByteU5BU5D_t3397334013* L_36 = Asn1Node_get_Data_m1676178324(L_35, /*hidden argument*/NULL);
		NullCheck(L_33);
		String_t* L_37 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_33, L_36);
		NullCheck(L_32);
		AppleInAppPurchaseReceipt_set_originalTransactionIdentifier_m2823063774(L_32, L_37, /*hidden argument*/NULL);
		goto IL_0167;
	}

IL_011f:
	{
		AppleInAppPurchaseReceipt_t3271698749 * L_38 = V_0;
		Asn1Node_t1770761751 * L_39 = V_4;
		DateTime_t693205669  L_40 = AppleValidator_TryParseDateTimeNode_m190299298(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		NullCheck(L_38);
		AppleInAppPurchaseReceipt_set_purchaseDate_m1082187471(L_38, L_40, /*hidden argument*/NULL);
		goto IL_0167;
	}

IL_0131:
	{
		AppleInAppPurchaseReceipt_t3271698749 * L_41 = V_0;
		Asn1Node_t1770761751 * L_42 = V_4;
		DateTime_t693205669  L_43 = AppleValidator_TryParseDateTimeNode_m190299298(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		NullCheck(L_41);
		AppleInAppPurchaseReceipt_set_originalPurchaseDate_m1920060578(L_41, L_43, /*hidden argument*/NULL);
		goto IL_0167;
	}

IL_0143:
	{
		AppleInAppPurchaseReceipt_t3271698749 * L_44 = V_0;
		Asn1Node_t1770761751 * L_45 = V_4;
		DateTime_t693205669  L_46 = AppleValidator_TryParseDateTimeNode_m190299298(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		NullCheck(L_44);
		AppleInAppPurchaseReceipt_set_subscriptionExpirationDate_m2975097394(L_44, L_46, /*hidden argument*/NULL);
		goto IL_0167;
	}

IL_0155:
	{
		AppleInAppPurchaseReceipt_t3271698749 * L_47 = V_0;
		Asn1Node_t1770761751 * L_48 = V_4;
		DateTime_t693205669  L_49 = AppleValidator_TryParseDateTimeNode_m190299298(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		NullCheck(L_47);
		AppleInAppPurchaseReceipt_set_cancellationDate_m2514312931(L_47, L_49, /*hidden argument*/NULL);
		goto IL_0167;
	}

IL_0167:
	{
	}

IL_0168:
	{
		int32_t L_50 = V_1;
		V_1 = ((int32_t)((int32_t)L_50+(int32_t)1));
	}

IL_016d:
	{
		int32_t L_51 = V_1;
		Asn1Node_t1770761751 * L_52 = ___inApp0;
		NullCheck(L_52);
		int64_t L_53 = Asn1Node_get_ChildNodeCount_m1123088750(L_52, /*hidden argument*/NULL);
		if ((((int64_t)(((int64_t)((int64_t)L_51)))) < ((int64_t)L_53)))
		{
			goto IL_000e;
		}
	}
	{
		AppleInAppPurchaseReceipt_t3271698749 * L_54 = V_0;
		V_5 = L_54;
		goto IL_0182;
	}

IL_0182:
	{
		AppleInAppPurchaseReceipt_t3271698749 * L_55 = V_5;
		return L_55;
	}
}
// System.DateTime UnityEngine.Purchasing.Security.AppleValidator::TryParseDateTimeNode(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern const uint32_t AppleValidator_TryParseDateTimeNode_m190299298_MetadataUsageId;
extern "C"  DateTime_t693205669  AppleValidator_TryParseDateTimeNode_m190299298 (Il2CppObject * __this /* static, unused */, Asn1Node_t1770761751 * ___node0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AppleValidator_TryParseDateTimeNode_m190299298_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	DateTime_t693205669  V_1;
	memset(&V_1, 0, sizeof(V_1));
	DateTime_t693205669  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_0 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_1 = ___node0;
		NullCheck(L_1);
		Asn1Node_t1770761751 * L_2 = Asn1Node_GetChildNode_m4133223467(L_1, 0, /*hidden argument*/NULL);
		NullCheck(L_2);
		ByteU5BU5D_t3397334013* L_3 = Asn1Node_get_Data_m1676178324(L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_4 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_0, L_3);
		V_0 = L_4;
		String_t* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_8 = DateTime_Parse_m1142721566(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		DateTime_t693205669  L_9 = DateTime_ToUniversalTime_m1815024752((&V_1), /*hidden argument*/NULL);
		V_2 = L_9;
		goto IL_0043;
	}

IL_0038:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_10 = ((DateTime_t693205669_StaticFields*)DateTime_t693205669_il2cpp_TypeInfo_var->static_fields)->get_MinValue_13();
		V_2 = L_10;
		goto IL_0043;
	}

IL_0043:
	{
		DateTime_t693205669  L_11 = V_2;
		return L_11;
	}
}
// System.Void UnityEngine.Purchasing.Security.CrossPlatformValidator::.ctor(System.Byte[],System.Byte[],System.String)
extern "C"  void CrossPlatformValidator__ctor_m2169949003 (CrossPlatformValidator_t305796431 * __this, ByteU5BU5D_t3397334013* ___googlePublicKey0, ByteU5BU5D_t3397334013* ___appleRootCert1, String_t* ___appBundleId2, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = ___googlePublicKey0;
		ByteU5BU5D_t3397334013* L_1 = ___appleRootCert1;
		String_t* L_2 = ___appBundleId2;
		String_t* L_3 = ___appBundleId2;
		CrossPlatformValidator__ctor_m2046766649(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.CrossPlatformValidator::.ctor(System.Byte[],System.Byte[],System.String,System.String)
extern Il2CppClass* GooglePlayValidator_t4061171767_il2cpp_TypeInfo_var;
extern Il2CppClass* AppleValidator_t3837389912_il2cpp_TypeInfo_var;
extern const uint32_t CrossPlatformValidator__ctor_m2046766649_MetadataUsageId;
extern "C"  void CrossPlatformValidator__ctor_m2046766649 (CrossPlatformValidator_t305796431 * __this, ByteU5BU5D_t3397334013* ___googlePublicKey0, ByteU5BU5D_t3397334013* ___appleRootCert1, String_t* ___googleBundleId2, String_t* ___appleBundleId3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CrossPlatformValidator__ctor_m2046766649_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_0 = ___googlePublicKey0;
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_1 = ___googlePublicKey0;
		GooglePlayValidator_t4061171767 * L_2 = (GooglePlayValidator_t4061171767 *)il2cpp_codegen_object_new(GooglePlayValidator_t4061171767_il2cpp_TypeInfo_var);
		GooglePlayValidator__ctor_m2387657698(L_2, L_1, /*hidden argument*/NULL);
		__this->set_google_0(L_2);
	}

IL_001b:
	{
		ByteU5BU5D_t3397334013* L_3 = ___appleRootCert1;
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_4 = ___appleRootCert1;
		AppleValidator_t3837389912 * L_5 = (AppleValidator_t3837389912 *)il2cpp_codegen_object_new(AppleValidator_t3837389912_il2cpp_TypeInfo_var);
		AppleValidator__ctor_m1455195731(L_5, L_4, /*hidden argument*/NULL);
		__this->set_apple_1(L_5);
	}

IL_002f:
	{
		String_t* L_6 = ___googleBundleId2;
		__this->set_googleBundleId_2(L_6);
		String_t* L_7 = ___appleBundleId3;
		__this->set_appleBundleId_3(L_7);
		return;
	}
}
// UnityEngine.Purchasing.Security.IPurchaseReceipt[] UnityEngine.Purchasing.Security.CrossPlatformValidator::Validate(System.String)
extern Il2CppClass* MiniJson_t838727235_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t309261261_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidReceiptDataException_t1032464292_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MissingStoreSecretException_t3547862692_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidBundleIdException_t3011288315_il2cpp_TypeInfo_var;
extern Il2CppClass* IPurchaseReceiptU5BU5D_t988864285_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* StoreNotSupportedException_t958190973_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m464793699_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisAppleInAppPurchaseReceipt_t3271698749_m258157314_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2858757963;
extern Il2CppCodeGenString* _stringLiteral2547134676;
extern Il2CppCodeGenString* _stringLiteral1188405057;
extern Il2CppCodeGenString* _stringLiteral1570961022;
extern Il2CppCodeGenString* _stringLiteral2700534657;
extern Il2CppCodeGenString* _stringLiteral843548423;
extern Il2CppCodeGenString* _stringLiteral2704910744;
extern Il2CppCodeGenString* _stringLiteral3867530100;
extern Il2CppCodeGenString* _stringLiteral2143564527;
extern Il2CppCodeGenString* _stringLiteral3351902862;
extern const uint32_t CrossPlatformValidator_Validate_m4256704193_MetadataUsageId;
extern "C"  IPurchaseReceiptU5BU5D_t988864285* CrossPlatformValidator_Validate_m4256704193 (CrossPlatformValidator_t305796431 * __this, String_t* ___unityIAPReceipt0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CrossPlatformValidator_Validate_m4256704193_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t309261261 * V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	Dictionary_2_t309261261 * V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	GooglePlayReceipt_t2643016893 * V_6 = NULL;
	IPurchaseReceiptU5BU5D_t988864285* V_7 = NULL;
	AppleReceipt_t3991411794 * V_8 = NULL;
	{
		String_t* L_0 = ___unityIAPReceipt0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJson_t838727235_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = MiniJson_JsonDecode_m1067224953(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((Dictionary_2_t309261261 *)CastclassClass(L_1, Dictionary_2_t309261261_il2cpp_TypeInfo_var));
		Dictionary_2_t309261261 * L_2 = V_0;
		if (L_2)
		{
			goto IL_001a;
		}
	}
	{
		InvalidReceiptDataException_t1032464292 * L_3 = (InvalidReceiptDataException_t1032464292 *)il2cpp_codegen_object_new(InvalidReceiptDataException_t1032464292_il2cpp_TypeInfo_var);
		InvalidReceiptDataException__ctor_m2773429078(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001a:
	{
		Dictionary_2_t309261261 * L_4 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_5 = Dictionary_2_get_Item_m464793699(L_4, _stringLiteral2858757963, /*hidden argument*/Dictionary_2_get_Item_m464793699_MethodInfo_var);
		V_1 = ((String_t*)CastclassSealed(L_5, String_t_il2cpp_TypeInfo_var));
		Dictionary_2_t309261261 * L_6 = V_0;
		NullCheck(L_6);
		Il2CppObject * L_7 = Dictionary_2_get_Item_m464793699(L_6, _stringLiteral2547134676, /*hidden argument*/Dictionary_2_get_Item_m464793699_MethodInfo_var);
		V_2 = ((String_t*)CastclassSealed(L_7, String_t_il2cpp_TypeInfo_var));
		String_t* L_8 = V_1;
		if (!L_8)
		{
			goto IL_015a;
		}
	}
	{
		String_t* L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_9, _stringLiteral1188405057, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0077;
		}
	}
	{
		String_t* L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_11, _stringLiteral1570961022, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_00ff;
		}
	}
	{
		String_t* L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_14 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_13, _stringLiteral2700534657, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_00ff;
		}
	}
	{
		goto IL_015a;
	}

IL_0077:
	{
		GooglePlayValidator_t4061171767 * L_15 = __this->get_google_0();
		if (L_15)
		{
			goto IL_008e;
		}
	}
	{
		MissingStoreSecretException_t3547862692 * L_16 = (MissingStoreSecretException_t3547862692 *)il2cpp_codegen_object_new(MissingStoreSecretException_t3547862692_il2cpp_TypeInfo_var);
		MissingStoreSecretException__ctor_m800783456(L_16, _stringLiteral843548423, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_008e:
	{
		String_t* L_17 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJson_t838727235_il2cpp_TypeInfo_var);
		Il2CppObject * L_18 = MiniJson_JsonDecode_m1067224953(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_3 = ((Dictionary_2_t309261261 *)CastclassClass(L_18, Dictionary_2_t309261261_il2cpp_TypeInfo_var));
		Dictionary_2_t309261261 * L_19 = V_3;
		NullCheck(L_19);
		Il2CppObject * L_20 = Dictionary_2_get_Item_m464793699(L_19, _stringLiteral2704910744, /*hidden argument*/Dictionary_2_get_Item_m464793699_MethodInfo_var);
		V_4 = ((String_t*)CastclassSealed(L_20, String_t_il2cpp_TypeInfo_var));
		Dictionary_2_t309261261 * L_21 = V_3;
		NullCheck(L_21);
		Il2CppObject * L_22 = Dictionary_2_get_Item_m464793699(L_21, _stringLiteral3867530100, /*hidden argument*/Dictionary_2_get_Item_m464793699_MethodInfo_var);
		V_5 = ((String_t*)CastclassSealed(L_22, String_t_il2cpp_TypeInfo_var));
		GooglePlayValidator_t4061171767 * L_23 = __this->get_google_0();
		String_t* L_24 = V_4;
		String_t* L_25 = V_5;
		NullCheck(L_23);
		GooglePlayReceipt_t2643016893 * L_26 = GooglePlayValidator_Validate_m56002928(L_23, L_24, L_25, /*hidden argument*/NULL);
		V_6 = L_26;
		String_t* L_27 = __this->get_googleBundleId_2();
		GooglePlayReceipt_t2643016893 * L_28 = V_6;
		NullCheck(L_28);
		String_t* L_29 = GooglePlayReceipt_get_packageName_m2144183668(L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		bool L_30 = String_Equals_m2633592423(L_27, L_29, /*hidden argument*/NULL);
		if (L_30)
		{
			goto IL_00ed;
		}
	}
	{
		InvalidBundleIdException_t3011288315 * L_31 = (InvalidBundleIdException_t3011288315 *)il2cpp_codegen_object_new(InvalidBundleIdException_t3011288315_il2cpp_TypeInfo_var);
		InvalidBundleIdException__ctor_m4044292885(L_31, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_31);
	}

IL_00ed:
	{
		IPurchaseReceiptU5BU5D_t988864285* L_32 = ((IPurchaseReceiptU5BU5D_t988864285*)SZArrayNew(IPurchaseReceiptU5BU5D_t988864285_il2cpp_TypeInfo_var, (uint32_t)1));
		GooglePlayReceipt_t2643016893 * L_33 = V_6;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 0);
		ArrayElementTypeCheck (L_32, L_33);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_33);
		V_7 = L_32;
		goto IL_016b;
	}

IL_00ff:
	{
		AppleValidator_t3837389912 * L_34 = __this->get_apple_1();
		if (L_34)
		{
			goto IL_0116;
		}
	}
	{
		MissingStoreSecretException_t3547862692 * L_35 = (MissingStoreSecretException_t3547862692 *)il2cpp_codegen_object_new(MissingStoreSecretException_t3547862692_il2cpp_TypeInfo_var);
		MissingStoreSecretException__ctor_m800783456(L_35, _stringLiteral2143564527, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_35);
	}

IL_0116:
	{
		AppleValidator_t3837389912 * L_36 = __this->get_apple_1();
		String_t* L_37 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_38 = Convert_FromBase64String_m3629466114(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		NullCheck(L_36);
		AppleReceipt_t3991411794 * L_39 = AppleValidator_Validate_m6433925(L_36, L_38, /*hidden argument*/NULL);
		V_8 = L_39;
		String_t* L_40 = __this->get_appleBundleId_3();
		AppleReceipt_t3991411794 * L_41 = V_8;
		NullCheck(L_41);
		String_t* L_42 = AppleReceipt_get_bundleID_m1725040983(L_41, /*hidden argument*/NULL);
		NullCheck(L_40);
		bool L_43 = String_Equals_m2633592423(L_40, L_42, /*hidden argument*/NULL);
		if (L_43)
		{
			goto IL_0147;
		}
	}
	{
		InvalidBundleIdException_t3011288315 * L_44 = (InvalidBundleIdException_t3011288315 *)il2cpp_codegen_object_new(InvalidBundleIdException_t3011288315_il2cpp_TypeInfo_var);
		InvalidBundleIdException__ctor_m4044292885(L_44, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_44);
	}

IL_0147:
	{
		AppleReceipt_t3991411794 * L_45 = V_8;
		NullCheck(L_45);
		AppleInAppPurchaseReceiptU5BU5D_t579068208* L_46 = L_45->get_inAppPurchaseReceipts_6();
		AppleInAppPurchaseReceiptU5BU5D_t579068208* L_47 = Enumerable_ToArray_TisAppleInAppPurchaseReceipt_t3271698749_m258157314(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_46, /*hidden argument*/Enumerable_ToArray_TisAppleInAppPurchaseReceipt_t3271698749_m258157314_MethodInfo_var);
		V_7 = (IPurchaseReceiptU5BU5D_t988864285*)L_47;
		goto IL_016b;
	}

IL_015a:
	{
		String_t* L_48 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_49 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3351902862, L_48, /*hidden argument*/NULL);
		StoreNotSupportedException_t958190973 * L_50 = (StoreNotSupportedException_t958190973 *)il2cpp_codegen_object_new(StoreNotSupportedException_t958190973_il2cpp_TypeInfo_var);
		StoreNotSupportedException__ctor_m4169319163(L_50, L_49, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_50);
	}

IL_016b:
	{
		IPurchaseReceiptU5BU5D_t988864285* L_51 = V_7;
		return L_51;
	}
}
// System.Void UnityEngine.Purchasing.Security.DistinguishedName::.ctor(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern Il2CppClass* InvalidX509Data_t1630759105_il2cpp_TypeInfo_var;
extern Il2CppClass* Oid_t113668572_il2cpp_TypeInfo_var;
extern Il2CppClass* UTF8Encoding_t111055448_il2cpp_TypeInfo_var;
extern Il2CppClass* DistinguishedName_t1881593989_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t3986656710_il2cpp_TypeInfo_var;
extern Il2CppClass* Asn1Util_t2059476207_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2118310873_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m1209957957_MethodInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m2977303364_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3983620311;
extern Il2CppCodeGenString* _stringLiteral998159726;
extern Il2CppCodeGenString* _stringLiteral3727043081;
extern Il2CppCodeGenString* _stringLiteral3983620316;
extern Il2CppCodeGenString* _stringLiteral3983620314;
extern Il2CppCodeGenString* _stringLiteral4130327603;
extern Il2CppCodeGenString* _stringLiteral3983620325;
extern const uint32_t DistinguishedName__ctor_m3658482605_MetadataUsageId;
extern "C"  void DistinguishedName__ctor_m3658482605 (DistinguishedName_t1881593989 * __this, Asn1Node_t1770761751 * ___n0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DistinguishedName__ctor_m3658482605_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Asn1Node_t1770761751 * V_1 = NULL;
	Asn1Node_t1770761751 * V_2 = NULL;
	Asn1Node_t1770761751 * V_3 = NULL;
	Oid_t113668572 * V_4 = NULL;
	String_t* V_5 = NULL;
	UTF8Encoding_t111055448 * V_6 = NULL;
	Dictionary_2_t3986656710 * V_7 = NULL;
	int32_t V_8 = 0;
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_0 = ___n0;
		NullCheck(L_0);
		uint8_t L_1 = Asn1Node_get_MaskedTag_m135294101(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_0236;
		}
	}
	{
		V_0 = 0;
		goto IL_0228;
	}

IL_001c:
	{
		Asn1Node_t1770761751 * L_2 = ___n0;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Asn1Node_t1770761751 * L_4 = Asn1Node_GetChildNode_m4133223467(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		Asn1Node_t1770761751 * L_5 = V_1;
		NullCheck(L_5);
		uint8_t L_6 = Asn1Node_get_MaskedTag_m135294101(L_5, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)17)))))
		{
			goto IL_003f;
		}
	}
	{
		Asn1Node_t1770761751 * L_7 = V_1;
		NullCheck(L_7);
		int64_t L_8 = Asn1Node_get_ChildNodeCount_m1123088750(L_7, /*hidden argument*/NULL);
		if ((((int64_t)L_8) == ((int64_t)(((int64_t)((int64_t)1))))))
		{
			goto IL_0045;
		}
	}

IL_003f:
	{
		InvalidX509Data_t1630759105 * L_9 = (InvalidX509Data_t1630759105 *)il2cpp_codegen_object_new(InvalidX509Data_t1630759105_il2cpp_TypeInfo_var);
		InvalidX509Data__ctor_m1976349147(L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0045:
	{
		Asn1Node_t1770761751 * L_10 = V_1;
		NullCheck(L_10);
		Asn1Node_t1770761751 * L_11 = Asn1Node_GetChildNode_m4133223467(L_10, 0, /*hidden argument*/NULL);
		V_1 = L_11;
		Asn1Node_t1770761751 * L_12 = V_1;
		NullCheck(L_12);
		uint8_t L_13 = Asn1Node_get_MaskedTag_m135294101(L_12, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_0067;
		}
	}
	{
		Asn1Node_t1770761751 * L_14 = V_1;
		NullCheck(L_14);
		int64_t L_15 = Asn1Node_get_ChildNodeCount_m1123088750(L_14, /*hidden argument*/NULL);
		if ((((int64_t)L_15) == ((int64_t)(((int64_t)((int64_t)2))))))
		{
			goto IL_006d;
		}
	}

IL_0067:
	{
		InvalidX509Data_t1630759105 * L_16 = (InvalidX509Data_t1630759105 *)il2cpp_codegen_object_new(InvalidX509Data_t1630759105_il2cpp_TypeInfo_var);
		InvalidX509Data__ctor_m1976349147(L_16, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_006d:
	{
		Asn1Node_t1770761751 * L_17 = V_1;
		NullCheck(L_17);
		Asn1Node_t1770761751 * L_18 = Asn1Node_GetChildNode_m4133223467(L_17, 0, /*hidden argument*/NULL);
		V_2 = L_18;
		Asn1Node_t1770761751 * L_19 = V_1;
		NullCheck(L_19);
		Asn1Node_t1770761751 * L_20 = Asn1Node_GetChildNode_m4133223467(L_19, 1, /*hidden argument*/NULL);
		V_3 = L_20;
		Asn1Node_t1770761751 * L_21 = V_2;
		NullCheck(L_21);
		uint8_t L_22 = Asn1Node_get_MaskedTag_m135294101(L_21, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_22) == ((uint32_t)6))))
		{
			goto IL_00a3;
		}
	}
	{
		Asn1Node_t1770761751 * L_23 = V_3;
		NullCheck(L_23);
		uint8_t L_24 = Asn1Node_get_MaskedTag_m135294101(L_23, /*hidden argument*/NULL);
		if ((((int32_t)L_24) == ((int32_t)((int32_t)19))))
		{
			goto IL_00aa;
		}
	}
	{
		Asn1Node_t1770761751 * L_25 = V_3;
		NullCheck(L_25);
		uint8_t L_26 = Asn1Node_get_MaskedTag_m135294101(L_25, /*hidden argument*/NULL);
		if ((((int32_t)L_26) == ((int32_t)((int32_t)12))))
		{
			goto IL_00aa;
		}
	}

IL_00a3:
	{
		InvalidX509Data_t1630759105 * L_27 = (InvalidX509Data_t1630759105 *)il2cpp_codegen_object_new(InvalidX509Data_t1630759105_il2cpp_TypeInfo_var);
		InvalidX509Data__ctor_m1976349147(L_27, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_27);
	}

IL_00aa:
	{
		Oid_t113668572 * L_28 = (Oid_t113668572 *)il2cpp_codegen_object_new(Oid_t113668572_il2cpp_TypeInfo_var);
		Oid__ctor_m3384108428(L_28, /*hidden argument*/NULL);
		V_4 = L_28;
		Oid_t113668572 * L_29 = V_4;
		Asn1Node_t1770761751 * L_30 = V_2;
		NullCheck(L_30);
		ByteU5BU5D_t3397334013* L_31 = Asn1Node_get_Data_m1676178324(L_30, /*hidden argument*/NULL);
		NullCheck(L_29);
		String_t* L_32 = Oid_Decode_m2891545618(L_29, L_31, /*hidden argument*/NULL);
		V_5 = L_32;
		UTF8Encoding_t111055448 * L_33 = (UTF8Encoding_t111055448 *)il2cpp_codegen_object_new(UTF8Encoding_t111055448_il2cpp_TypeInfo_var);
		UTF8Encoding__ctor_m100325490(L_33, /*hidden argument*/NULL);
		V_6 = L_33;
		String_t* L_34 = V_5;
		if (!L_34)
		{
			goto IL_0223;
		}
	}
	{
		Dictionary_2_t3986656710 * L_35 = ((DistinguishedName_t1881593989_StaticFields*)DistinguishedName_t1881593989_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map0_7();
		if (L_35)
		{
			goto IL_0142;
		}
	}
	{
		Dictionary_2_t3986656710 * L_36 = (Dictionary_2_t3986656710 *)il2cpp_codegen_object_new(Dictionary_2_t3986656710_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2118310873(L_36, 7, /*hidden argument*/Dictionary_2__ctor_m2118310873_MethodInfo_var);
		V_7 = L_36;
		Dictionary_2_t3986656710 * L_37 = V_7;
		NullCheck(L_37);
		Dictionary_2_Add_m1209957957(L_37, _stringLiteral3983620311, 0, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_38 = V_7;
		NullCheck(L_38);
		Dictionary_2_Add_m1209957957(L_38, _stringLiteral998159726, 1, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_39 = V_7;
		NullCheck(L_39);
		Dictionary_2_Add_m1209957957(L_39, _stringLiteral3727043081, 2, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_40 = V_7;
		NullCheck(L_40);
		Dictionary_2_Add_m1209957957(L_40, _stringLiteral3983620316, 3, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_41 = V_7;
		NullCheck(L_41);
		Dictionary_2_Add_m1209957957(L_41, _stringLiteral3983620314, 4, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_42 = V_7;
		NullCheck(L_42);
		Dictionary_2_Add_m1209957957(L_42, _stringLiteral4130327603, 5, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_43 = V_7;
		NullCheck(L_43);
		Dictionary_2_Add_m1209957957(L_43, _stringLiteral3983620325, 6, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_44 = V_7;
		((DistinguishedName_t1881593989_StaticFields*)DistinguishedName_t1881593989_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map0_7(L_44);
	}

IL_0142:
	{
		Dictionary_2_t3986656710 * L_45 = ((DistinguishedName_t1881593989_StaticFields*)DistinguishedName_t1881593989_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map0_7();
		String_t* L_46 = V_5;
		NullCheck(L_45);
		bool L_47 = Dictionary_2_TryGetValue_m2977303364(L_45, L_46, (&V_8), /*hidden argument*/Dictionary_2_TryGetValue_m2977303364_MethodInfo_var);
		if (!L_47)
		{
			goto IL_0223;
		}
	}
	{
		int32_t L_48 = V_8;
		if (L_48 == 0)
		{
			goto IL_017d;
		}
		if (L_48 == 1)
		{
			goto IL_0195;
		}
		if (L_48 == 2)
		{
			goto IL_01ad;
		}
		if (L_48 == 3)
		{
			goto IL_01c5;
		}
		if (L_48 == 4)
		{
			goto IL_01dd;
		}
		if (L_48 == 5)
		{
			goto IL_01f3;
		}
		if (L_48 == 6)
		{
			goto IL_020b;
		}
	}
	{
		goto IL_0223;
	}

IL_017d:
	{
		UTF8Encoding_t111055448 * L_49 = V_6;
		Asn1Node_t1770761751 * L_50 = V_3;
		NullCheck(L_50);
		ByteU5BU5D_t3397334013* L_51 = Asn1Node_get_Data_m1676178324(L_50, /*hidden argument*/NULL);
		NullCheck(L_49);
		String_t* L_52 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_49, L_51);
		DistinguishedName_set_Country_m2844947450(__this, L_52, /*hidden argument*/NULL);
		goto IL_0223;
	}

IL_0195:
	{
		UTF8Encoding_t111055448 * L_53 = V_6;
		Asn1Node_t1770761751 * L_54 = V_3;
		NullCheck(L_54);
		ByteU5BU5D_t3397334013* L_55 = Asn1Node_get_Data_m1676178324(L_54, /*hidden argument*/NULL);
		NullCheck(L_53);
		String_t* L_56 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_53, L_55);
		DistinguishedName_set_Organization_m2141035885(__this, L_56, /*hidden argument*/NULL);
		goto IL_0223;
	}

IL_01ad:
	{
		UTF8Encoding_t111055448 * L_57 = V_6;
		Asn1Node_t1770761751 * L_58 = V_3;
		NullCheck(L_58);
		ByteU5BU5D_t3397334013* L_59 = Asn1Node_get_Data_m1676178324(L_58, /*hidden argument*/NULL);
		NullCheck(L_57);
		String_t* L_60 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_57, L_59);
		DistinguishedName_set_OrganizationalUnit_m1833607010(__this, L_60, /*hidden argument*/NULL);
		goto IL_0223;
	}

IL_01c5:
	{
		UTF8Encoding_t111055448 * L_61 = V_6;
		Asn1Node_t1770761751 * L_62 = V_3;
		NullCheck(L_62);
		ByteU5BU5D_t3397334013* L_63 = Asn1Node_get_Data_m1676178324(L_62, /*hidden argument*/NULL);
		NullCheck(L_61);
		String_t* L_64 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_61, L_63);
		DistinguishedName_set_CommonName_m2980787556(__this, L_64, /*hidden argument*/NULL);
		goto IL_0223;
	}

IL_01dd:
	{
		Asn1Node_t1770761751 * L_65 = V_3;
		NullCheck(L_65);
		ByteU5BU5D_t3397334013* L_66 = Asn1Node_get_Data_m1676178324(L_65, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		String_t* L_67 = Asn1Util_ToHexString_m3932902392(NULL /*static, unused*/, L_66, /*hidden argument*/NULL);
		DistinguishedName_set_SerialNumber_m3659779817(__this, L_67, /*hidden argument*/NULL);
		goto IL_0223;
	}

IL_01f3:
	{
		UTF8Encoding_t111055448 * L_68 = V_6;
		Asn1Node_t1770761751 * L_69 = V_3;
		NullCheck(L_69);
		ByteU5BU5D_t3397334013* L_70 = Asn1Node_get_Data_m1676178324(L_69, /*hidden argument*/NULL);
		NullCheck(L_68);
		String_t* L_71 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_68, L_70);
		DistinguishedName_set_Dnq_m3889139193(__this, L_71, /*hidden argument*/NULL);
		goto IL_0223;
	}

IL_020b:
	{
		UTF8Encoding_t111055448 * L_72 = V_6;
		Asn1Node_t1770761751 * L_73 = V_3;
		NullCheck(L_73);
		ByteU5BU5D_t3397334013* L_74 = Asn1Node_get_Data_m1676178324(L_73, /*hidden argument*/NULL);
		NullCheck(L_72);
		String_t* L_75 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_72, L_74);
		DistinguishedName_set_State_m1863823769(__this, L_75, /*hidden argument*/NULL);
		goto IL_0223;
	}

IL_0223:
	{
		int32_t L_76 = V_0;
		V_0 = ((int32_t)((int32_t)L_76+(int32_t)1));
	}

IL_0228:
	{
		int32_t L_77 = V_0;
		Asn1Node_t1770761751 * L_78 = ___n0;
		NullCheck(L_78);
		int64_t L_79 = Asn1Node_get_ChildNodeCount_m1123088750(L_78, /*hidden argument*/NULL);
		if ((((int64_t)(((int64_t)((int64_t)L_77)))) < ((int64_t)L_79)))
		{
			goto IL_001c;
		}
	}
	{
	}

IL_0236:
	{
		return;
	}
}
// System.String UnityEngine.Purchasing.Security.DistinguishedName::get_Country()
extern "C"  String_t* DistinguishedName_get_Country_m4278360821 (DistinguishedName_t1881593989 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CCountryU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.DistinguishedName::set_Country(System.String)
extern "C"  void DistinguishedName_set_Country_m2844947450 (DistinguishedName_t1881593989 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCountryU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String UnityEngine.Purchasing.Security.DistinguishedName::get_Organization()
extern "C"  String_t* DistinguishedName_get_Organization_m3989970552 (DistinguishedName_t1881593989 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3COrganizationU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.DistinguishedName::set_Organization(System.String)
extern "C"  void DistinguishedName_set_Organization_m2141035885 (DistinguishedName_t1881593989 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3COrganizationU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String UnityEngine.Purchasing.Security.DistinguishedName::get_OrganizationalUnit()
extern "C"  String_t* DistinguishedName_get_OrganizationalUnit_m3911770541 (DistinguishedName_t1881593989 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3COrganizationalUnitU3Ek__BackingField_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.DistinguishedName::set_OrganizationalUnit(System.String)
extern "C"  void DistinguishedName_set_OrganizationalUnit_m1833607010 (DistinguishedName_t1881593989 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3COrganizationalUnitU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String UnityEngine.Purchasing.Security.DistinguishedName::get_Dnq()
extern "C"  String_t* DistinguishedName_get_Dnq_m1662125674 (DistinguishedName_t1881593989 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CDnqU3Ek__BackingField_3();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.DistinguishedName::set_Dnq(System.String)
extern "C"  void DistinguishedName_set_Dnq_m3889139193 (DistinguishedName_t1881593989 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDnqU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.String UnityEngine.Purchasing.Security.DistinguishedName::get_State()
extern "C"  String_t* DistinguishedName_get_State_m1071035862 (DistinguishedName_t1881593989 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CStateU3Ek__BackingField_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.DistinguishedName::set_State(System.String)
extern "C"  void DistinguishedName_set_State_m1863823769 (DistinguishedName_t1881593989 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStateU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.String UnityEngine.Purchasing.Security.DistinguishedName::get_CommonName()
extern "C"  String_t* DistinguishedName_get_CommonName_m481243823 (DistinguishedName_t1881593989 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CCommonNameU3Ek__BackingField_5();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.DistinguishedName::set_CommonName(System.String)
extern "C"  void DistinguishedName_set_CommonName_m2980787556 (DistinguishedName_t1881593989 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCommonNameU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.DistinguishedName::set_SerialNumber(System.String)
extern "C"  void DistinguishedName_set_SerialNumber_m3659779817 (DistinguishedName_t1881593989 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CSerialNumberU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Boolean UnityEngine.Purchasing.Security.DistinguishedName::Equals(UnityEngine.Purchasing.Security.DistinguishedName)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DistinguishedName_Equals_m575298400_MetadataUsageId;
extern "C"  bool DistinguishedName_Equals_m575298400 (DistinguishedName_t1881593989 * __this, DistinguishedName_t1881593989 * ___n20, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DistinguishedName_Equals_m575298400_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t G_B7_0 = 0;
	{
		String_t* L_0 = DistinguishedName_get_Organization_m3989970552(__this, /*hidden argument*/NULL);
		DistinguishedName_t1881593989 * L_1 = ___n20;
		NullCheck(L_1);
		String_t* L_2 = DistinguishedName_get_Organization_m3989970552(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0082;
		}
	}
	{
		String_t* L_4 = DistinguishedName_get_OrganizationalUnit_m3911770541(__this, /*hidden argument*/NULL);
		DistinguishedName_t1881593989 * L_5 = ___n20;
		NullCheck(L_5);
		String_t* L_6 = DistinguishedName_get_OrganizationalUnit_m3911770541(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0082;
		}
	}
	{
		String_t* L_8 = DistinguishedName_get_Dnq_m1662125674(__this, /*hidden argument*/NULL);
		DistinguishedName_t1881593989 * L_9 = ___n20;
		NullCheck(L_9);
		String_t* L_10 = DistinguishedName_get_Dnq_m1662125674(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0082;
		}
	}
	{
		String_t* L_12 = DistinguishedName_get_Country_m4278360821(__this, /*hidden argument*/NULL);
		DistinguishedName_t1881593989 * L_13 = ___n20;
		NullCheck(L_13);
		String_t* L_14 = DistinguishedName_get_Country_m4278360821(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0082;
		}
	}
	{
		String_t* L_16 = DistinguishedName_get_State_m1071035862(__this, /*hidden argument*/NULL);
		DistinguishedName_t1881593989 * L_17 = ___n20;
		NullCheck(L_17);
		String_t* L_18 = DistinguishedName_get_State_m1071035862(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_19 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_16, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0082;
		}
	}
	{
		String_t* L_20 = DistinguishedName_get_CommonName_m481243823(__this, /*hidden argument*/NULL);
		DistinguishedName_t1881593989 * L_21 = ___n20;
		NullCheck(L_21);
		String_t* L_22 = DistinguishedName_get_CommonName_m481243823(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_23 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_20, L_22, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_23));
		goto IL_0083;
	}

IL_0082:
	{
		G_B7_0 = 0;
	}

IL_0083:
	{
		V_0 = (bool)G_B7_0;
		goto IL_0089;
	}

IL_0089:
	{
		bool L_24 = V_0;
		return L_24;
	}
}
// System.String UnityEngine.Purchasing.Security.DistinguishedName::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1092031699;
extern Il2CppCodeGenString* _stringLiteral499841297;
extern Il2CppCodeGenString* _stringLiteral1886947837;
extern Il2CppCodeGenString* _stringLiteral162581756;
extern const uint32_t DistinguishedName_ToString_m3805516334_MetadataUsageId;
extern "C"  String_t* DistinguishedName_ToString_m3805516334 (DistinguishedName_t1881593989 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DistinguishedName_ToString_m3805516334_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral1092031699);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1092031699);
		StringU5BU5D_t1642385972* L_1 = L_0;
		String_t* L_2 = DistinguishedName_get_CommonName_m481243823(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_2);
		StringU5BU5D_t1642385972* L_3 = L_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, _stringLiteral499841297);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral499841297);
		StringU5BU5D_t1642385972* L_4 = L_3;
		String_t* L_5 = DistinguishedName_get_Organization_m3989970552(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_5);
		StringU5BU5D_t1642385972* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, _stringLiteral1886947837);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1886947837);
		StringU5BU5D_t1642385972* L_7 = L_6;
		String_t* L_8 = DistinguishedName_get_OrganizationalUnit_m3911770541(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 5);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_8);
		StringU5BU5D_t1642385972* L_9 = L_7;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 6);
		ArrayElementTypeCheck (L_9, _stringLiteral162581756);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral162581756);
		StringU5BU5D_t1642385972* L_10 = L_9;
		String_t* L_11 = DistinguishedName_get_Country_m4278360821(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 7);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m626692867(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0056;
	}

IL_0056:
	{
		String_t* L_13 = V_0;
		return L_13;
	}
}
// System.Void UnityEngine.Purchasing.Security.GooglePlayReceipt::.ctor(System.String,System.String,System.String,System.String,System.DateTime,UnityEngine.Purchasing.Security.GooglePurchaseState)
extern "C"  void GooglePlayReceipt__ctor_m2219689979 (GooglePlayReceipt_t2643016893 * __this, String_t* ___productID0, String_t* ___transactionID1, String_t* ___packageName2, String_t* ___purchaseToken3, DateTime_t693205669  ___purchaseTime4, int32_t ___purchaseState5, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___productID0;
		GooglePlayReceipt_set_productID_m3807487566(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___transactionID1;
		GooglePlayReceipt_set_transactionID_m1591410145(__this, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___packageName2;
		GooglePlayReceipt_set_packageName_m1507892305(__this, L_2, /*hidden argument*/NULL);
		String_t* L_3 = ___purchaseToken3;
		GooglePlayReceipt_set_purchaseToken_m3110755600(__this, L_3, /*hidden argument*/NULL);
		DateTime_t693205669  L_4 = ___purchaseTime4;
		GooglePlayReceipt_set_purchaseDate_m3851393431(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = ___purchaseState5;
		GooglePlayReceipt_set_purchaseState_m146393314(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Purchasing.Security.GooglePlayReceipt::get_productID()
extern "C"  String_t* GooglePlayReceipt_get_productID_m2031306133 (GooglePlayReceipt_t2643016893 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CproductIDU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.GooglePlayReceipt::set_productID(System.String)
extern "C"  void GooglePlayReceipt_set_productID_m3807487566 (GooglePlayReceipt_t2643016893 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CproductIDU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String UnityEngine.Purchasing.Security.GooglePlayReceipt::get_transactionID()
extern "C"  String_t* GooglePlayReceipt_get_transactionID_m2766075234 (GooglePlayReceipt_t2643016893 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CtransactionIDU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.GooglePlayReceipt::set_transactionID(System.String)
extern "C"  void GooglePlayReceipt_set_transactionID_m1591410145 (GooglePlayReceipt_t2643016893 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CtransactionIDU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String UnityEngine.Purchasing.Security.GooglePlayReceipt::get_packageName()
extern "C"  String_t* GooglePlayReceipt_get_packageName_m2144183668 (GooglePlayReceipt_t2643016893 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CpackageNameU3Ek__BackingField_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.GooglePlayReceipt::set_packageName(System.String)
extern "C"  void GooglePlayReceipt_set_packageName_m1507892305 (GooglePlayReceipt_t2643016893 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CpackageNameU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String UnityEngine.Purchasing.Security.GooglePlayReceipt::get_purchaseToken()
extern "C"  String_t* GooglePlayReceipt_get_purchaseToken_m144480463 (GooglePlayReceipt_t2643016893 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CpurchaseTokenU3Ek__BackingField_3();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.GooglePlayReceipt::set_purchaseToken(System.String)
extern "C"  void GooglePlayReceipt_set_purchaseToken_m3110755600 (GooglePlayReceipt_t2643016893 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CpurchaseTokenU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.DateTime UnityEngine.Purchasing.Security.GooglePlayReceipt::get_purchaseDate()
extern "C"  DateTime_t693205669  GooglePlayReceipt_get_purchaseDate_m1942217228 (GooglePlayReceipt_t2643016893 * __this, const MethodInfo* method)
{
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DateTime_t693205669  L_0 = __this->get_U3CpurchaseDateU3Ek__BackingField_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		DateTime_t693205669  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.GooglePlayReceipt::set_purchaseDate(System.DateTime)
extern "C"  void GooglePlayReceipt_set_purchaseDate_m3851393431 (GooglePlayReceipt_t2643016893 * __this, DateTime_t693205669  ___value0, const MethodInfo* method)
{
	{
		DateTime_t693205669  L_0 = ___value0;
		__this->set_U3CpurchaseDateU3Ek__BackingField_4(L_0);
		return;
	}
}
// UnityEngine.Purchasing.Security.GooglePurchaseState UnityEngine.Purchasing.Security.GooglePlayReceipt::get_purchaseState()
extern "C"  int32_t GooglePlayReceipt_get_purchaseState_m3274356139 (GooglePlayReceipt_t2643016893 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CpurchaseStateU3Ek__BackingField_5();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.GooglePlayReceipt::set_purchaseState(UnityEngine.Purchasing.Security.GooglePurchaseState)
extern "C"  void GooglePlayReceipt_set_purchaseState_m146393314 (GooglePlayReceipt_t2643016893 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CpurchaseStateU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.GooglePlayValidator::.ctor(System.Byte[])
extern Il2CppClass* RSAKey_t446464277_il2cpp_TypeInfo_var;
extern const uint32_t GooglePlayValidator__ctor_m2387657698_MetadataUsageId;
extern "C"  void GooglePlayValidator__ctor_m2387657698 (GooglePlayValidator_t4061171767 * __this, ByteU5BU5D_t3397334013* ___rsaKey0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GooglePlayValidator__ctor_m2387657698_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_0 = ___rsaKey0;
		RSAKey_t446464277 * L_1 = (RSAKey_t446464277 *)il2cpp_codegen_object_new(RSAKey_t446464277_il2cpp_TypeInfo_var);
		RSAKey__ctor_m2562631236(L_1, L_0, /*hidden argument*/NULL);
		__this->set_key_0(L_1);
		return;
	}
}
// UnityEngine.Purchasing.Security.GooglePlayReceipt UnityEngine.Purchasing.Security.GooglePlayValidator::Validate(System.String,System.String)
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidSignatureException_t488933488_il2cpp_TypeInfo_var;
extern Il2CppClass* MiniJson_t838727235_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t309261261_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GooglePlayReceipt_t2643016893_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m307458800_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2548001951;
extern Il2CppCodeGenString* _stringLiteral1819155373;
extern Il2CppCodeGenString* _stringLiteral697716380;
extern Il2CppCodeGenString* _stringLiteral3274632958;
extern Il2CppCodeGenString* _stringLiteral3143385516;
extern Il2CppCodeGenString* _stringLiteral3771777342;
extern const uint32_t GooglePlayValidator_Validate_m56002928_MetadataUsageId;
extern "C"  GooglePlayReceipt_t2643016893 * GooglePlayValidator_Validate_m56002928 (GooglePlayValidator_t4061171767 * __this, String_t* ___receipt0, String_t* ___signature1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GooglePlayValidator_Validate_m56002928_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	Dictionary_2_t309261261 * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	Il2CppObject * V_6 = NULL;
	Il2CppObject * V_7 = NULL;
	Il2CppObject * V_8 = NULL;
	DateTime_t693205669  V_9;
	memset(&V_9, 0, sizeof(V_9));
	DateTime_t693205669  V_10;
	memset(&V_10, 0, sizeof(V_10));
	int32_t V_11 = 0;
	GooglePlayReceipt_t2643016893 * V_12 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_0 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___receipt0;
		NullCheck(L_0);
		ByteU5BU5D_t3397334013* L_2 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		V_0 = L_2;
		String_t* L_3 = ___signature1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_4 = Convert_FromBase64String_m3629466114(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		RSAKey_t446464277 * L_5 = __this->get_key_0();
		ByteU5BU5D_t3397334013* L_6 = V_0;
		ByteU5BU5D_t3397334013* L_7 = V_1;
		NullCheck(L_5);
		bool L_8 = RSAKey_Verify_m2487225620(L_5, L_6, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_002d;
		}
	}
	{
		InvalidSignatureException_t488933488 * L_9 = (InvalidSignatureException_t488933488 *)il2cpp_codegen_object_new(InvalidSignatureException_t488933488_il2cpp_TypeInfo_var);
		InvalidSignatureException__ctor_m883303240(L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_002d:
	{
		String_t* L_10 = ___receipt0;
		IL2CPP_RUNTIME_CLASS_INIT(MiniJson_t838727235_il2cpp_TypeInfo_var);
		Il2CppObject * L_11 = MiniJson_JsonDecode_m1067224953(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_2 = ((Dictionary_2_t309261261 *)CastclassClass(L_11, Dictionary_2_t309261261_il2cpp_TypeInfo_var));
		Dictionary_2_t309261261 * L_12 = V_2;
		NullCheck(L_12);
		Dictionary_2_TryGetValue_m307458800(L_12, _stringLiteral2548001951, (&V_3), /*hidden argument*/Dictionary_2_TryGetValue_m307458800_MethodInfo_var);
		Dictionary_2_t309261261 * L_13 = V_2;
		NullCheck(L_13);
		Dictionary_2_TryGetValue_m307458800(L_13, _stringLiteral1819155373, (&V_4), /*hidden argument*/Dictionary_2_TryGetValue_m307458800_MethodInfo_var);
		Dictionary_2_t309261261 * L_14 = V_2;
		NullCheck(L_14);
		Dictionary_2_TryGetValue_m307458800(L_14, _stringLiteral697716380, (&V_5), /*hidden argument*/Dictionary_2_TryGetValue_m307458800_MethodInfo_var);
		Dictionary_2_t309261261 * L_15 = V_2;
		NullCheck(L_15);
		Dictionary_2_TryGetValue_m307458800(L_15, _stringLiteral3274632958, (&V_6), /*hidden argument*/Dictionary_2_TryGetValue_m307458800_MethodInfo_var);
		Dictionary_2_t309261261 * L_16 = V_2;
		NullCheck(L_16);
		Dictionary_2_TryGetValue_m307458800(L_16, _stringLiteral3143385516, (&V_7), /*hidden argument*/Dictionary_2_TryGetValue_m307458800_MethodInfo_var);
		Dictionary_2_t309261261 * L_17 = V_2;
		NullCheck(L_17);
		Dictionary_2_TryGetValue_m307458800(L_17, _stringLiteral3771777342, (&V_8), /*hidden argument*/Dictionary_2_TryGetValue_m307458800_MethodInfo_var);
		DateTime__ctor_m3270618252((&V_9), ((int32_t)1970), 1, 1, 0, 0, 0, 1, /*hidden argument*/NULL);
		Il2CppObject * L_18 = V_7;
		DateTime_t693205669  L_19 = DateTime_AddMilliseconds_m1813199744((&V_9), (((double)((double)(((int64_t)((int64_t)((*(double*)((double*)UnBox (L_18, Double_t4078015681_il2cpp_TypeInfo_var)))))))))), /*hidden argument*/NULL);
		V_10 = L_19;
		Il2CppObject * L_20 = V_8;
		V_11 = (((int32_t)((int32_t)((*(double*)((double*)UnBox (L_20, Double_t4078015681_il2cpp_TypeInfo_var)))))));
		Il2CppObject * L_21 = V_5;
		Il2CppObject * L_22 = V_3;
		Il2CppObject * L_23 = V_4;
		Il2CppObject * L_24 = V_6;
		DateTime_t693205669  L_25 = V_10;
		int32_t L_26 = V_11;
		GooglePlayReceipt_t2643016893 * L_27 = (GooglePlayReceipt_t2643016893 *)il2cpp_codegen_object_new(GooglePlayReceipt_t2643016893_il2cpp_TypeInfo_var);
		GooglePlayReceipt__ctor_m2219689979(L_27, ((String_t*)CastclassSealed(L_21, String_t_il2cpp_TypeInfo_var)), ((String_t*)CastclassSealed(L_22, String_t_il2cpp_TypeInfo_var)), ((String_t*)CastclassSealed(L_23, String_t_il2cpp_TypeInfo_var)), ((String_t*)CastclassSealed(L_24, String_t_il2cpp_TypeInfo_var)), L_25, L_26, /*hidden argument*/NULL);
		V_12 = L_27;
		goto IL_00e6;
	}

IL_00e6:
	{
		GooglePlayReceipt_t2643016893 * L_28 = V_12;
		return L_28;
	}
}
// System.Void UnityEngine.Purchasing.Security.IAPSecurityException::.ctor()
extern "C"  void IAPSecurityException__ctor_m4130362627 (IAPSecurityException_t3038093501 * __this, const MethodInfo* method)
{
	{
		Exception__ctor_m3886110570(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.IAPSecurityException::.ctor(System.String)
extern "C"  void IAPSecurityException__ctor_m1626861665 (IAPSecurityException_t3038093501 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception__ctor_m485833136(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.InvalidBundleIdException::.ctor()
extern "C"  void InvalidBundleIdException__ctor_m4044292885 (InvalidBundleIdException_t3011288315 * __this, const MethodInfo* method)
{
	{
		IAPSecurityException__ctor_m4130362627(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.InvalidPKCS7Data::.ctor()
extern "C"  void InvalidPKCS7Data__ctor_m1217025319 (InvalidPKCS7Data_t4123278833 * __this, const MethodInfo* method)
{
	{
		IAPSecurityException__ctor_m4130362627(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.InvalidReceiptDataException::.ctor()
extern "C"  void InvalidReceiptDataException__ctor_m2773429078 (InvalidReceiptDataException_t1032464292 * __this, const MethodInfo* method)
{
	{
		IAPSecurityException__ctor_m4130362627(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.InvalidRSAData::.ctor()
extern "C"  void InvalidRSAData__ctor_m3293493135 (InvalidRSAData_t1674954323 * __this, const MethodInfo* method)
{
	{
		IAPSecurityException__ctor_m4130362627(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.InvalidSignatureException::.ctor()
extern "C"  void InvalidSignatureException__ctor_m883303240 (InvalidSignatureException_t488933488 * __this, const MethodInfo* method)
{
	{
		IAPSecurityException__ctor_m4130362627(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.InvalidTimeFormat::.ctor()
extern "C"  void InvalidTimeFormat__ctor_m2055199385 (InvalidTimeFormat_t3933748955 * __this, const MethodInfo* method)
{
	{
		IAPSecurityException__ctor_m4130362627(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.InvalidX509Data::.ctor()
extern "C"  void InvalidX509Data__ctor_m1976349147 (InvalidX509Data_t1630759105 * __this, const MethodInfo* method)
{
	{
		IAPSecurityException__ctor_m4130362627(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.MissingStoreSecretException::.ctor(System.String)
extern "C"  void MissingStoreSecretException__ctor_m800783456 (MissingStoreSecretException_t3547862692 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		IAPSecurityException__ctor_m1626861665(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] UnityEngine.Purchasing.Security.Obfuscator::DeObfuscate(System.Byte[],System.Int32[],System.Int32)
extern Il2CppClass* U3CDeObfuscateU3Ec__AnonStorey0_t1106656056_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t3601859201_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Skip_TisByte_t3683104436_m3962905258_MethodInfo_var;
extern const MethodInfo* Enumerable_Take_TisByte_t3683104436_m3483797502_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisByte_t3683104436_m2323269811_MethodInfo_var;
extern const MethodInfo* U3CDeObfuscateU3Ec__AnonStorey0_U3CU3Em__0_m2910553385_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m4047094542_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisByte_t3683104436_TisByte_t3683104436_m4080074214_MethodInfo_var;
extern const uint32_t Obfuscator_DeObfuscate_m4253116256_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* Obfuscator_DeObfuscate_m4253116256 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, Int32U5BU5D_t3030399641* ___order1, int32_t ___key2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Obfuscator_DeObfuscate_m4253116256_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CDeObfuscateU3Ec__AnonStorey0_t1106656056 * V_0 = NULL;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	int32_t V_2 = 0;
	bool V_3 = false;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	ByteU5BU5D_t3397334013* V_7 = NULL;
	ByteU5BU5D_t3397334013* V_8 = NULL;
	int32_t G_B5_0 = 0;
	{
		U3CDeObfuscateU3Ec__AnonStorey0_t1106656056 * L_0 = (U3CDeObfuscateU3Ec__AnonStorey0_t1106656056 *)il2cpp_codegen_object_new(U3CDeObfuscateU3Ec__AnonStorey0_t1106656056_il2cpp_TypeInfo_var);
		U3CDeObfuscateU3Ec__AnonStorey0__ctor_m1242923309(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDeObfuscateU3Ec__AnonStorey0_t1106656056 * L_1 = V_0;
		int32_t L_2 = ___key2;
		NullCheck(L_1);
		L_1->set_key_0(L_2);
		ByteU5BU5D_t3397334013* L_3 = ___data0;
		NullCheck(L_3);
		V_1 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))));
		ByteU5BU5D_t3397334013* L_4 = ___data0;
		NullCheck(L_4);
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))/(int32_t)((int32_t)20)))+(int32_t)1));
		ByteU5BU5D_t3397334013* L_5 = ___data0;
		NullCheck(L_5);
		V_3 = (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))%(int32_t)((int32_t)20)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		ByteU5BU5D_t3397334013* L_6 = ___data0;
		ByteU5BU5D_t3397334013* L_7 = V_1;
		ByteU5BU5D_t3397334013* L_8 = ___data0;
		NullCheck(L_8);
		Array_Copy_m2363740072(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_6, (Il2CppArray *)(Il2CppArray *)L_7, (((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))), /*hidden argument*/NULL);
		Int32U5BU5D_t3030399641* L_9 = ___order1;
		NullCheck(L_9);
		V_4 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))-(int32_t)1));
		goto IL_00ac;
	}

IL_0043:
	{
		Int32U5BU5D_t3030399641* L_10 = ___order1;
		int32_t L_11 = V_4;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		int32_t L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_5 = L_13;
		bool L_14 = V_3;
		if (!L_14)
		{
			goto IL_0065;
		}
	}
	{
		int32_t L_15 = V_5;
		int32_t L_16 = V_2;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)((int32_t)L_16-(int32_t)1))))))
		{
			goto IL_0065;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_17 = ___data0;
		NullCheck(L_17);
		G_B5_0 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length))))%(int32_t)((int32_t)20)));
		goto IL_0067;
	}

IL_0065:
	{
		G_B5_0 = ((int32_t)20);
	}

IL_0067:
	{
		V_6 = G_B5_0;
		ByteU5BU5D_t3397334013* L_18 = V_1;
		int32_t L_19 = V_4;
		Il2CppObject* L_20 = Enumerable_Skip_TisByte_t3683104436_m3962905258(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_18, ((int32_t)((int32_t)L_19*(int32_t)((int32_t)20))), /*hidden argument*/Enumerable_Skip_TisByte_t3683104436_m3962905258_MethodInfo_var);
		int32_t L_21 = V_6;
		Il2CppObject* L_22 = Enumerable_Take_TisByte_t3683104436_m3483797502(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/Enumerable_Take_TisByte_t3683104436_m3483797502_MethodInfo_var);
		ByteU5BU5D_t3397334013* L_23 = Enumerable_ToArray_TisByte_t3683104436_m2323269811(NULL /*static, unused*/, L_22, /*hidden argument*/Enumerable_ToArray_TisByte_t3683104436_m2323269811_MethodInfo_var);
		V_7 = L_23;
		ByteU5BU5D_t3397334013* L_24 = V_1;
		int32_t L_25 = V_5;
		ByteU5BU5D_t3397334013* L_26 = V_1;
		int32_t L_27 = V_4;
		int32_t L_28 = V_6;
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_24, ((int32_t)((int32_t)L_25*(int32_t)((int32_t)20))), (Il2CppArray *)(Il2CppArray *)L_26, ((int32_t)((int32_t)L_27*(int32_t)((int32_t)20))), L_28, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_29 = V_7;
		ByteU5BU5D_t3397334013* L_30 = V_1;
		int32_t L_31 = V_5;
		int32_t L_32 = V_6;
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_29, 0, (Il2CppArray *)(Il2CppArray *)L_30, ((int32_t)((int32_t)L_31*(int32_t)((int32_t)20))), L_32, /*hidden argument*/NULL);
		int32_t L_33 = V_4;
		V_4 = ((int32_t)((int32_t)L_33-(int32_t)1));
	}

IL_00ac:
	{
		int32_t L_34 = V_4;
		if ((((int32_t)L_34) >= ((int32_t)0)))
		{
			goto IL_0043;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_35 = V_1;
		U3CDeObfuscateU3Ec__AnonStorey0_t1106656056 * L_36 = V_0;
		IntPtr_t L_37;
		L_37.set_m_value_0((void*)(void*)U3CDeObfuscateU3Ec__AnonStorey0_U3CU3Em__0_m2910553385_MethodInfo_var);
		Func_2_t3601859201 * L_38 = (Func_2_t3601859201 *)il2cpp_codegen_object_new(Func_2_t3601859201_il2cpp_TypeInfo_var);
		Func_2__ctor_m4047094542(L_38, L_36, L_37, /*hidden argument*/Func_2__ctor_m4047094542_MethodInfo_var);
		Il2CppObject* L_39 = Enumerable_Select_TisByte_t3683104436_TisByte_t3683104436_m4080074214(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_35, L_38, /*hidden argument*/Enumerable_Select_TisByte_t3683104436_TisByte_t3683104436_m4080074214_MethodInfo_var);
		ByteU5BU5D_t3397334013* L_40 = Enumerable_ToArray_TisByte_t3683104436_m2323269811(NULL /*static, unused*/, L_39, /*hidden argument*/Enumerable_ToArray_TisByte_t3683104436_m2323269811_MethodInfo_var);
		V_8 = L_40;
		goto IL_00d2;
	}

IL_00d2:
	{
		ByteU5BU5D_t3397334013* L_41 = V_8;
		return L_41;
	}
}
// System.Void UnityEngine.Purchasing.Security.Obfuscator/<DeObfuscate>c__AnonStorey0::.ctor()
extern "C"  void U3CDeObfuscateU3Ec__AnonStorey0__ctor_m1242923309 (U3CDeObfuscateU3Ec__AnonStorey0_t1106656056 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte UnityEngine.Purchasing.Security.Obfuscator/<DeObfuscate>c__AnonStorey0::<>m__0(System.Byte)
extern "C"  uint8_t U3CDeObfuscateU3Ec__AnonStorey0_U3CU3Em__0_m2910553385 (U3CDeObfuscateU3Ec__AnonStorey0_t1106656056 * __this, uint8_t ___x0, const MethodInfo* method)
{
	uint8_t V_0 = 0x0;
	{
		uint8_t L_0 = ___x0;
		int32_t L_1 = __this->get_key_0();
		V_0 = (((int32_t)((uint8_t)((int32_t)((int32_t)L_0^(int32_t)L_1)))));
		goto IL_000f;
	}

IL_000f:
	{
		uint8_t L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Purchasing.Security.PKCS7::.ctor(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  void PKCS7__ctor_m1019290992 (PKCS7_t1974940522 * __this, Asn1Node_t1770761751 * ___node0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_0 = ___node0;
		__this->set_root_0(L_0);
		PKCS7_CheckStructure_m676548793(__this, /*hidden argument*/NULL);
		return;
	}
}
// LipingShare.LCLib.Asn1Processor.Asn1Node UnityEngine.Purchasing.Security.PKCS7::get_data()
extern "C"  Asn1Node_t1770761751 * PKCS7_get_data_m139975642 (PKCS7_t1974940522 * __this, const MethodInfo* method)
{
	Asn1Node_t1770761751 * V_0 = NULL;
	{
		Asn1Node_t1770761751 * L_0 = __this->get_U3CdataU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Asn1Node_t1770761751 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.PKCS7::set_data(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  void PKCS7_set_data_m2145946707 (PKCS7_t1974940522 * __this, Asn1Node_t1770761751 * ___value0, const MethodInfo* method)
{
	{
		Asn1Node_t1770761751 * L_0 = ___value0;
		__this->set_U3CdataU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.Purchasing.Security.SignerInfo> UnityEngine.Purchasing.Security.PKCS7::get_sinfos()
extern "C"  List_1_t3491469936 * PKCS7_get_sinfos_m3813593105 (PKCS7_t1974940522 * __this, const MethodInfo* method)
{
	List_1_t3491469936 * V_0 = NULL;
	{
		List_1_t3491469936 * L_0 = __this->get_U3CsinfosU3Ek__BackingField_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		List_1_t3491469936 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.PKCS7::set_sinfos(System.Collections.Generic.List`1<UnityEngine.Purchasing.Security.SignerInfo>)
extern "C"  void PKCS7_set_sinfos_m921812246 (PKCS7_t1974940522 * __this, List_1_t3491469936 * ___value0, const MethodInfo* method)
{
	{
		List_1_t3491469936 * L_0 = ___value0;
		__this->set_U3CsinfosU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.Purchasing.Security.X509Cert> UnityEngine.Purchasing.Security.PKCS7::get_certChain()
extern "C"  List_1_t4145897706 * PKCS7_get_certChain_m1660994762 (PKCS7_t1974940522 * __this, const MethodInfo* method)
{
	List_1_t4145897706 * V_0 = NULL;
	{
		List_1_t4145897706 * L_0 = __this->get_U3CcertChainU3Ek__BackingField_3();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		List_1_t4145897706 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.PKCS7::set_certChain(System.Collections.Generic.List`1<UnityEngine.Purchasing.Security.X509Cert>)
extern "C"  void PKCS7_set_certChain_m766545607 (PKCS7_t1974940522 * __this, List_1_t4145897706 * ___value0, const MethodInfo* method)
{
	{
		List_1_t4145897706 * L_0 = ___value0;
		__this->set_U3CcertChainU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Boolean UnityEngine.Purchasing.Security.PKCS7::Verify(UnityEngine.Purchasing.Security.X509Cert,System.DateTime)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m207414055_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3179888355_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m224626381_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4195044481_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1925205733_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1906304245_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3882097823_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m3075839395_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m4234143072_MethodInfo_var;
extern const uint32_t PKCS7_Verify_m738735600_MetadataUsageId;
extern "C"  bool PKCS7_Verify_m738735600 (PKCS7_t1974940522 * __this, X509Cert_t481809278 * ___cert0, DateTime_t693205669  ___certificateCreationTime1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PKCS7_Verify_m738735600_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	SignerInfo_t4122348804 * V_1 = NULL;
	Enumerator_t3026199610  V_2;
	memset(&V_2, 0, sizeof(V_2));
	X509Cert_t481809278 * V_3 = NULL;
	X509Cert_t481809278 * V_4 = NULL;
	Enumerator_t3680627380  V_5;
	memset(&V_5, 0, sizeof(V_5));
	bool V_6 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B16_0 = 0;
	int32_t G_B19_0 = 0;
	int32_t G_B22_0 = 0;
	int32_t G_B30_0 = 0;
	{
		bool L_0 = __this->get_validStructure_4();
		if (!L_0)
		{
			goto IL_0124;
		}
	}
	{
		V_0 = (bool)1;
		List_1_t3491469936 * L_1 = PKCS7_get_sinfos_m3813593105(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Enumerator_t3026199610  L_2 = List_1_GetEnumerator_m207414055(L_1, /*hidden argument*/List_1_GetEnumerator_m207414055_MethodInfo_var);
		V_2 = L_2;
	}

IL_001c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00e7;
		}

IL_0021:
		{
			SignerInfo_t4122348804 * L_3 = Enumerator_get_Current_m3179888355((&V_2), /*hidden argument*/Enumerator_get_Current_m3179888355_MethodInfo_var);
			V_1 = L_3;
			V_3 = (X509Cert_t481809278 *)NULL;
			List_1_t4145897706 * L_4 = PKCS7_get_certChain_m1660994762(__this, /*hidden argument*/NULL);
			NullCheck(L_4);
			Enumerator_t3680627380  L_5 = List_1_GetEnumerator_m224626381(L_4, /*hidden argument*/List_1_GetEnumerator_m224626381_MethodInfo_var);
			V_5 = L_5;
		}

IL_003a:
		try
		{ // begin try (depth: 2)
			{
				goto IL_006a;
			}

IL_003f:
			{
				X509Cert_t481809278 * L_6 = Enumerator_get_Current_m4195044481((&V_5), /*hidden argument*/Enumerator_get_Current_m4195044481_MethodInfo_var);
				V_4 = L_6;
				X509Cert_t481809278 * L_7 = V_4;
				NullCheck(L_7);
				String_t* L_8 = X509Cert_get_SerialNumber_m3398499893(L_7, /*hidden argument*/NULL);
				SignerInfo_t4122348804 * L_9 = V_1;
				NullCheck(L_9);
				String_t* L_10 = SignerInfo_get_IssuerSerialNumber_m988083912(L_9, /*hidden argument*/NULL);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				bool L_11 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
				if (!L_11)
				{
					goto IL_0069;
				}
			}

IL_0060:
			{
				X509Cert_t481809278 * L_12 = V_4;
				V_3 = L_12;
				goto IL_0076;
			}

IL_0069:
			{
			}

IL_006a:
			{
				bool L_13 = Enumerator_MoveNext_m1925205733((&V_5), /*hidden argument*/Enumerator_MoveNext_m1925205733_MethodInfo_var);
				if (L_13)
				{
					goto IL_003f;
				}
			}

IL_0076:
			{
				IL2CPP_LEAVE(0x89, FINALLY_007b);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_007b;
		}

FINALLY_007b:
		{ // begin finally (depth: 2)
			Enumerator_Dispose_m1906304245((&V_5), /*hidden argument*/Enumerator_Dispose_m1906304245_MethodInfo_var);
			IL2CPP_END_FINALLY(123)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(123)
		{
			IL2CPP_JUMP_TBL(0x89, IL_0089)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_0089:
		{
			X509Cert_t481809278 * L_14 = V_3;
			if (!L_14)
			{
				goto IL_00e6;
			}
		}

IL_008f:
		{
			X509Cert_t481809278 * L_15 = V_3;
			NullCheck(L_15);
			RSAKey_t446464277 * L_16 = X509Cert_get_PubKey_m3319710642(L_15, /*hidden argument*/NULL);
			if (!L_16)
			{
				goto IL_00e6;
			}
		}

IL_009a:
		{
			bool L_17 = V_0;
			if (!L_17)
			{
				goto IL_00aa;
			}
		}

IL_00a1:
		{
			X509Cert_t481809278 * L_18 = V_3;
			DateTime_t693205669  L_19 = ___certificateCreationTime1;
			NullCheck(L_18);
			bool L_20 = X509Cert_CheckCertTime_m317868091(L_18, L_19, /*hidden argument*/NULL);
			G_B16_0 = ((int32_t)(L_20));
			goto IL_00ab;
		}

IL_00aa:
		{
			G_B16_0 = 0;
		}

IL_00ab:
		{
			V_0 = (bool)G_B16_0;
			bool L_21 = V_0;
			if (!L_21)
			{
				goto IL_00d0;
			}
		}

IL_00b2:
		{
			X509Cert_t481809278 * L_22 = V_3;
			NullCheck(L_22);
			RSAKey_t446464277 * L_23 = X509Cert_get_PubKey_m3319710642(L_22, /*hidden argument*/NULL);
			Asn1Node_t1770761751 * L_24 = PKCS7_get_data_m139975642(__this, /*hidden argument*/NULL);
			NullCheck(L_24);
			ByteU5BU5D_t3397334013* L_25 = Asn1Node_get_Data_m1676178324(L_24, /*hidden argument*/NULL);
			SignerInfo_t4122348804 * L_26 = V_1;
			NullCheck(L_26);
			ByteU5BU5D_t3397334013* L_27 = SignerInfo_get_EncryptedDigest_m2178470575(L_26, /*hidden argument*/NULL);
			NullCheck(L_23);
			bool L_28 = RSAKey_Verify_m2487225620(L_23, L_25, L_27, /*hidden argument*/NULL);
			G_B19_0 = ((int32_t)(L_28));
			goto IL_00d1;
		}

IL_00d0:
		{
			G_B19_0 = 0;
		}

IL_00d1:
		{
			V_0 = (bool)G_B19_0;
			bool L_29 = V_0;
			if (!L_29)
			{
				goto IL_00e3;
			}
		}

IL_00d8:
		{
			X509Cert_t481809278 * L_30 = ___cert0;
			X509Cert_t481809278 * L_31 = V_3;
			DateTime_t693205669  L_32 = ___certificateCreationTime1;
			bool L_33 = PKCS7_ValidateChain_m2979125067(__this, L_30, L_31, L_32, /*hidden argument*/NULL);
			G_B22_0 = ((int32_t)(L_33));
			goto IL_00e4;
		}

IL_00e3:
		{
			G_B22_0 = 0;
		}

IL_00e4:
		{
			V_0 = (bool)G_B22_0;
		}

IL_00e6:
		{
		}

IL_00e7:
		{
			bool L_34 = Enumerator_MoveNext_m3882097823((&V_2), /*hidden argument*/Enumerator_MoveNext_m3882097823_MethodInfo_var);
			if (L_34)
			{
				goto IL_0021;
			}
		}

IL_00f3:
		{
			IL2CPP_LEAVE(0x106, FINALLY_00f8);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00f8;
	}

FINALLY_00f8:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3075839395((&V_2), /*hidden argument*/Enumerator_Dispose_m3075839395_MethodInfo_var);
		IL2CPP_END_FINALLY(248)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(248)
	{
		IL2CPP_JUMP_TBL(0x106, IL_0106)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0106:
	{
		bool L_35 = V_0;
		if (!L_35)
		{
			goto IL_011c;
		}
	}
	{
		List_1_t3491469936 * L_36 = PKCS7_get_sinfos_m3813593105(__this, /*hidden argument*/NULL);
		NullCheck(L_36);
		int32_t L_37 = List_1_get_Count_m4234143072(L_36, /*hidden argument*/List_1_get_Count_m4234143072_MethodInfo_var);
		G_B30_0 = ((((int32_t)L_37) > ((int32_t)0))? 1 : 0);
		goto IL_011d;
	}

IL_011c:
	{
		G_B30_0 = 0;
	}

IL_011d:
	{
		V_6 = (bool)G_B30_0;
		goto IL_012c;
	}

IL_0124:
	{
		V_6 = (bool)0;
		goto IL_012c;
	}

IL_012c:
	{
		bool L_38 = V_6;
		return L_38;
	}
}
// System.Boolean UnityEngine.Purchasing.Security.PKCS7::ValidateChain(UnityEngine.Purchasing.Security.X509Cert,UnityEngine.Purchasing.Security.X509Cert,System.DateTime)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m224626381_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4195044481_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1925205733_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1906304245_MethodInfo_var;
extern const uint32_t PKCS7_ValidateChain_m2979125067_MetadataUsageId;
extern "C"  bool PKCS7_ValidateChain_m2979125067 (PKCS7_t1974940522 * __this, X509Cert_t481809278 * ___root0, X509Cert_t481809278 * ___cert1, DateTime_t693205669  ___certificateCreationTime2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PKCS7_ValidateChain_m2979125067_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	X509Cert_t481809278 * V_1 = NULL;
	Enumerator_t3680627380  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		X509Cert_t481809278 * L_0 = ___cert1;
		NullCheck(L_0);
		DistinguishedName_t1881593989 * L_1 = X509Cert_get_Issuer_m3153344599(L_0, /*hidden argument*/NULL);
		X509Cert_t481809278 * L_2 = ___root0;
		NullCheck(L_2);
		DistinguishedName_t1881593989 * L_3 = X509Cert_get_Subject_m640225680(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_4 = DistinguishedName_Equals_m575298400(L_1, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0024;
		}
	}
	{
		X509Cert_t481809278 * L_5 = ___cert1;
		X509Cert_t481809278 * L_6 = ___root0;
		NullCheck(L_5);
		bool L_7 = X509Cert_CheckSignature_m4098105751(L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		goto IL_00e7;
	}

IL_0024:
	{
		List_1_t4145897706 * L_8 = PKCS7_get_certChain_m1660994762(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Enumerator_t3680627380  L_9 = List_1_GetEnumerator_m224626381(L_8, /*hidden argument*/List_1_GetEnumerator_m224626381_MethodInfo_var);
		V_2 = L_9;
	}

IL_0031:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c1;
		}

IL_0036:
		{
			X509Cert_t481809278 * L_10 = Enumerator_get_Current_m4195044481((&V_2), /*hidden argument*/Enumerator_get_Current_m4195044481_MethodInfo_var);
			V_1 = L_10;
			X509Cert_t481809278 * L_11 = V_1;
			X509Cert_t481809278 * L_12 = ___cert1;
			if ((((Il2CppObject*)(X509Cert_t481809278 *)L_11) == ((Il2CppObject*)(X509Cert_t481809278 *)L_12)))
			{
				goto IL_00c0;
			}
		}

IL_0046:
		{
			X509Cert_t481809278 * L_13 = V_1;
			NullCheck(L_13);
			DistinguishedName_t1881593989 * L_14 = X509Cert_get_Subject_m640225680(L_13, /*hidden argument*/NULL);
			X509Cert_t481809278 * L_15 = ___cert1;
			NullCheck(L_15);
			DistinguishedName_t1881593989 * L_16 = X509Cert_get_Issuer_m3153344599(L_15, /*hidden argument*/NULL);
			NullCheck(L_14);
			bool L_17 = DistinguishedName_Equals_m575298400(L_14, L_16, /*hidden argument*/NULL);
			if (!L_17)
			{
				goto IL_00c0;
			}
		}

IL_005c:
		{
			X509Cert_t481809278 * L_18 = V_1;
			DateTime_t693205669  L_19 = ___certificateCreationTime2;
			NullCheck(L_18);
			bool L_20 = X509Cert_CheckCertTime_m317868091(L_18, L_19, /*hidden argument*/NULL);
			if (!L_20)
			{
				goto IL_00c0;
			}
		}

IL_0068:
		{
			X509Cert_t481809278 * L_21 = V_1;
			NullCheck(L_21);
			DistinguishedName_t1881593989 * L_22 = X509Cert_get_Issuer_m3153344599(L_21, /*hidden argument*/NULL);
			X509Cert_t481809278 * L_23 = ___root0;
			NullCheck(L_23);
			DistinguishedName_t1881593989 * L_24 = X509Cert_get_Subject_m640225680(L_23, /*hidden argument*/NULL);
			NullCheck(L_22);
			bool L_25 = DistinguishedName_Equals_m575298400(L_22, L_24, /*hidden argument*/NULL);
			if (!L_25)
			{
				goto IL_00a2;
			}
		}

IL_007f:
		{
			X509Cert_t481809278 * L_26 = V_1;
			NullCheck(L_26);
			String_t* L_27 = X509Cert_get_SerialNumber_m3398499893(L_26, /*hidden argument*/NULL);
			X509Cert_t481809278 * L_28 = ___root0;
			NullCheck(L_28);
			String_t* L_29 = X509Cert_get_SerialNumber_m3398499893(L_28, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_30 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_27, L_29, /*hidden argument*/NULL);
			if (!L_30)
			{
				goto IL_00a2;
			}
		}

IL_0095:
		{
			X509Cert_t481809278 * L_31 = V_1;
			X509Cert_t481809278 * L_32 = ___root0;
			NullCheck(L_31);
			bool L_33 = X509Cert_CheckSignature_m4098105751(L_31, L_32, /*hidden argument*/NULL);
			V_0 = L_33;
			IL2CPP_LEAVE(0xE7, FINALLY_00d2);
		}

IL_00a2:
		{
			X509Cert_t481809278 * L_34 = ___cert1;
			X509Cert_t481809278 * L_35 = V_1;
			NullCheck(L_34);
			bool L_36 = X509Cert_CheckSignature_m4098105751(L_34, L_35, /*hidden argument*/NULL);
			if (!L_36)
			{
				goto IL_00be;
			}
		}

IL_00af:
		{
			X509Cert_t481809278 * L_37 = ___root0;
			X509Cert_t481809278 * L_38 = V_1;
			DateTime_t693205669  L_39 = ___certificateCreationTime2;
			bool L_40 = PKCS7_ValidateChain_m2979125067(__this, L_37, L_38, L_39, /*hidden argument*/NULL);
			V_0 = L_40;
			IL2CPP_LEAVE(0xE7, FINALLY_00d2);
		}

IL_00be:
		{
		}

IL_00c0:
		{
		}

IL_00c1:
		{
			bool L_41 = Enumerator_MoveNext_m1925205733((&V_2), /*hidden argument*/Enumerator_MoveNext_m1925205733_MethodInfo_var);
			if (L_41)
			{
				goto IL_0036;
			}
		}

IL_00cd:
		{
			IL2CPP_LEAVE(0xE0, FINALLY_00d2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00d2;
	}

FINALLY_00d2:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1906304245((&V_2), /*hidden argument*/Enumerator_Dispose_m1906304245_MethodInfo_var);
		IL2CPP_END_FINALLY(210)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(210)
	{
		IL2CPP_JUMP_TBL(0xE7, IL_00e7)
		IL2CPP_JUMP_TBL(0xE0, IL_00e0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00e0:
	{
		V_0 = (bool)0;
		goto IL_00e7;
	}

IL_00e7:
	{
		bool L_42 = V_0;
		return L_42;
	}
}
// System.Void UnityEngine.Purchasing.Security.PKCS7::CheckStructure()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidPKCS7Data_t4123278833_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t4145897706_il2cpp_TypeInfo_var;
extern Il2CppClass* X509Cert_t481809278_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3491469936_il2cpp_TypeInfo_var;
extern Il2CppClass* SignerInfo_t4122348804_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1618176422_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3178217698_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1845338834_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1551955646_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral60261860;
extern const uint32_t PKCS7_CheckStructure_m676548793_MetadataUsageId;
extern "C"  void PKCS7_CheckStructure_m676548793 (PKCS7_t1974940522 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PKCS7_CheckStructure_m676548793_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Asn1Node_t1770761751 * V_0 = NULL;
	int32_t V_1 = 0;
	Asn1Node_t1770761751 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		__this->set_validStructure_4((bool)0);
		Asn1Node_t1770761751 * L_0 = __this->get_root_0();
		NullCheck(L_0);
		uint8_t L_1 = Asn1Node_get_Tag_m2005840874(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)31)))) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_021d;
		}
	}
	{
		Asn1Node_t1770761751 * L_2 = __this->get_root_0();
		NullCheck(L_2);
		int64_t L_3 = Asn1Node_get_ChildNodeCount_m1123088750(L_2, /*hidden argument*/NULL);
		if ((!(((uint64_t)L_3) == ((uint64_t)(((int64_t)((int64_t)2)))))))
		{
			goto IL_021d;
		}
	}
	{
		Asn1Node_t1770761751 * L_4 = __this->get_root_0();
		NullCheck(L_4);
		Asn1Node_t1770761751 * L_5 = Asn1Node_GetChildNode_m4133223467(L_4, 0, /*hidden argument*/NULL);
		V_0 = L_5;
		Asn1Node_t1770761751 * L_6 = V_0;
		NullCheck(L_6);
		uint8_t L_7 = Asn1Node_get_Tag_m2005840874(L_6, /*hidden argument*/NULL);
		if ((!(((uint32_t)((int32_t)((int32_t)L_7&(int32_t)((int32_t)31)))) == ((uint32_t)6))))
		{
			goto IL_0062;
		}
	}
	{
		Asn1Node_t1770761751 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = Asn1Node_GetDataStr_m2973067886(L_8, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_9, _stringLiteral60261860, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0069;
		}
	}

IL_0062:
	{
		InvalidPKCS7Data_t4123278833 * L_11 = (InvalidPKCS7Data_t4123278833 *)il2cpp_codegen_object_new(InvalidPKCS7Data_t4123278833_il2cpp_TypeInfo_var);
		InvalidPKCS7Data__ctor_m1217025319(L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0069:
	{
		Asn1Node_t1770761751 * L_12 = __this->get_root_0();
		NullCheck(L_12);
		Asn1Node_t1770761751 * L_13 = Asn1Node_GetChildNode_m4133223467(L_12, 1, /*hidden argument*/NULL);
		V_0 = L_13;
		Asn1Node_t1770761751 * L_14 = V_0;
		NullCheck(L_14);
		int64_t L_15 = Asn1Node_get_ChildNodeCount_m1123088750(L_14, /*hidden argument*/NULL);
		if ((((int64_t)L_15) == ((int64_t)(((int64_t)((int64_t)1))))))
		{
			goto IL_0089;
		}
	}
	{
		InvalidPKCS7Data_t4123278833 * L_16 = (InvalidPKCS7Data_t4123278833 *)il2cpp_codegen_object_new(InvalidPKCS7Data_t4123278833_il2cpp_TypeInfo_var);
		InvalidPKCS7Data__ctor_m1217025319(L_16, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_0089:
	{
		V_1 = 0;
		Asn1Node_t1770761751 * L_17 = V_0;
		int32_t L_18 = V_1;
		int32_t L_19 = L_18;
		V_1 = ((int32_t)((int32_t)L_19+(int32_t)1));
		NullCheck(L_17);
		Asn1Node_t1770761751 * L_20 = Asn1Node_GetChildNode_m4133223467(L_17, L_19, /*hidden argument*/NULL);
		V_0 = L_20;
		Asn1Node_t1770761751 * L_21 = V_0;
		NullCheck(L_21);
		int64_t L_22 = Asn1Node_get_ChildNodeCount_m1123088750(L_21, /*hidden argument*/NULL);
		if ((((int64_t)L_22) < ((int64_t)(((int64_t)((int64_t)4))))))
		{
			goto IL_00b4;
		}
	}
	{
		Asn1Node_t1770761751 * L_23 = V_0;
		NullCheck(L_23);
		uint8_t L_24 = Asn1Node_get_Tag_m2005840874(L_23, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_24&(int32_t)((int32_t)31)))) == ((int32_t)((int32_t)16))))
		{
			goto IL_00ba;
		}
	}

IL_00b4:
	{
		InvalidPKCS7Data_t4123278833 * L_25 = (InvalidPKCS7Data_t4123278833 *)il2cpp_codegen_object_new(InvalidPKCS7Data_t4123278833_il2cpp_TypeInfo_var);
		InvalidPKCS7Data__ctor_m1217025319(L_25, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_25);
	}

IL_00ba:
	{
		Asn1Node_t1770761751 * L_26 = V_0;
		NullCheck(L_26);
		Asn1Node_t1770761751 * L_27 = Asn1Node_GetChildNode_m4133223467(L_26, 0, /*hidden argument*/NULL);
		V_2 = L_27;
		Asn1Node_t1770761751 * L_28 = V_2;
		NullCheck(L_28);
		uint8_t L_29 = Asn1Node_get_Tag_m2005840874(L_28, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_29&(int32_t)((int32_t)31)))) == ((int32_t)2)))
		{
			goto IL_00d7;
		}
	}
	{
		InvalidPKCS7Data_t4123278833 * L_30 = (InvalidPKCS7Data_t4123278833 *)il2cpp_codegen_object_new(InvalidPKCS7Data_t4123278833_il2cpp_TypeInfo_var);
		InvalidPKCS7Data__ctor_m1217025319(L_30, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_30);
	}

IL_00d7:
	{
		Asn1Node_t1770761751 * L_31 = V_0;
		int32_t L_32 = V_1;
		int32_t L_33 = L_32;
		V_1 = ((int32_t)((int32_t)L_33+(int32_t)1));
		NullCheck(L_31);
		Asn1Node_t1770761751 * L_34 = Asn1Node_GetChildNode_m4133223467(L_31, L_33, /*hidden argument*/NULL);
		V_2 = L_34;
		Asn1Node_t1770761751 * L_35 = V_2;
		NullCheck(L_35);
		uint8_t L_36 = Asn1Node_get_Tag_m2005840874(L_35, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_36&(int32_t)((int32_t)31)))) == ((int32_t)((int32_t)17))))
		{
			goto IL_00f9;
		}
	}
	{
		InvalidPKCS7Data_t4123278833 * L_37 = (InvalidPKCS7Data_t4123278833 *)il2cpp_codegen_object_new(InvalidPKCS7Data_t4123278833_il2cpp_TypeInfo_var);
		InvalidPKCS7Data__ctor_m1217025319(L_37, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_37);
	}

IL_00f9:
	{
		Asn1Node_t1770761751 * L_38 = V_0;
		int32_t L_39 = V_1;
		int32_t L_40 = L_39;
		V_1 = ((int32_t)((int32_t)L_40+(int32_t)1));
		NullCheck(L_38);
		Asn1Node_t1770761751 * L_41 = Asn1Node_GetChildNode_m4133223467(L_38, L_40, /*hidden argument*/NULL);
		V_2 = L_41;
		Asn1Node_t1770761751 * L_42 = V_2;
		NullCheck(L_42);
		uint8_t L_43 = Asn1Node_get_Tag_m2005840874(L_42, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_43&(int32_t)((int32_t)31)))) == ((int32_t)((int32_t)16))))
		{
			goto IL_0128;
		}
	}
	{
		Asn1Node_t1770761751 * L_44 = V_2;
		NullCheck(L_44);
		int64_t L_45 = Asn1Node_get_ChildNodeCount_m1123088750(L_44, /*hidden argument*/NULL);
		if ((((int64_t)L_45) == ((int64_t)(((int64_t)((int64_t)2))))))
		{
			goto IL_0128;
		}
	}
	{
		InvalidPKCS7Data_t4123278833 * L_46 = (InvalidPKCS7Data_t4123278833 *)il2cpp_codegen_object_new(InvalidPKCS7Data_t4123278833_il2cpp_TypeInfo_var);
		InvalidPKCS7Data__ctor_m1217025319(L_46, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_46);
	}

IL_0128:
	{
		Asn1Node_t1770761751 * L_47 = V_2;
		NullCheck(L_47);
		Asn1Node_t1770761751 * L_48 = Asn1Node_GetChildNode_m4133223467(L_47, 1, /*hidden argument*/NULL);
		NullCheck(L_48);
		Asn1Node_t1770761751 * L_49 = Asn1Node_GetChildNode_m4133223467(L_48, 0, /*hidden argument*/NULL);
		PKCS7_set_data_m2145946707(__this, L_49, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_50 = V_0;
		NullCheck(L_50);
		int64_t L_51 = Asn1Node_get_ChildNodeCount_m1123088750(L_50, /*hidden argument*/NULL);
		if ((!(((uint64_t)L_51) == ((uint64_t)(((int64_t)((int64_t)5)))))))
		{
			goto IL_01a5;
		}
	}
	{
		List_1_t4145897706 * L_52 = (List_1_t4145897706 *)il2cpp_codegen_object_new(List_1_t4145897706_il2cpp_TypeInfo_var);
		List_1__ctor_m1618176422(L_52, /*hidden argument*/List_1__ctor_m1618176422_MethodInfo_var);
		PKCS7_set_certChain_m766545607(__this, L_52, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_53 = V_0;
		int32_t L_54 = V_1;
		int32_t L_55 = L_54;
		V_1 = ((int32_t)((int32_t)L_55+(int32_t)1));
		NullCheck(L_53);
		Asn1Node_t1770761751 * L_56 = Asn1Node_GetChildNode_m4133223467(L_53, L_55, /*hidden argument*/NULL);
		V_2 = L_56;
		Asn1Node_t1770761751 * L_57 = V_2;
		NullCheck(L_57);
		int64_t L_58 = Asn1Node_get_ChildNodeCount_m1123088750(L_57, /*hidden argument*/NULL);
		if ((!(((uint64_t)L_58) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_0173;
		}
	}
	{
		InvalidPKCS7Data_t4123278833 * L_59 = (InvalidPKCS7Data_t4123278833 *)il2cpp_codegen_object_new(InvalidPKCS7Data_t4123278833_il2cpp_TypeInfo_var);
		InvalidPKCS7Data__ctor_m1217025319(L_59, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_59);
	}

IL_0173:
	{
		V_3 = 0;
		goto IL_0197;
	}

IL_017a:
	{
		List_1_t4145897706 * L_60 = PKCS7_get_certChain_m1660994762(__this, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_61 = V_2;
		int32_t L_62 = V_3;
		NullCheck(L_61);
		Asn1Node_t1770761751 * L_63 = Asn1Node_GetChildNode_m4133223467(L_61, L_62, /*hidden argument*/NULL);
		X509Cert_t481809278 * L_64 = (X509Cert_t481809278 *)il2cpp_codegen_object_new(X509Cert_t481809278_il2cpp_TypeInfo_var);
		X509Cert__ctor_m2063768312(L_64, L_63, /*hidden argument*/NULL);
		NullCheck(L_60);
		List_1_Add_m3178217698(L_60, L_64, /*hidden argument*/List_1_Add_m3178217698_MethodInfo_var);
		int32_t L_65 = V_3;
		V_3 = ((int32_t)((int32_t)L_65+(int32_t)1));
	}

IL_0197:
	{
		int32_t L_66 = V_3;
		Asn1Node_t1770761751 * L_67 = V_2;
		NullCheck(L_67);
		int64_t L_68 = Asn1Node_get_ChildNodeCount_m1123088750(L_67, /*hidden argument*/NULL);
		if ((((int64_t)(((int64_t)((int64_t)L_66)))) < ((int64_t)L_68)))
		{
			goto IL_017a;
		}
	}
	{
	}

IL_01a5:
	{
		Asn1Node_t1770761751 * L_69 = V_0;
		int32_t L_70 = V_1;
		int32_t L_71 = L_70;
		V_1 = ((int32_t)((int32_t)L_71+(int32_t)1));
		NullCheck(L_69);
		Asn1Node_t1770761751 * L_72 = Asn1Node_GetChildNode_m4133223467(L_69, L_71, /*hidden argument*/NULL);
		V_2 = L_72;
		Asn1Node_t1770761751 * L_73 = V_2;
		NullCheck(L_73);
		uint8_t L_74 = Asn1Node_get_Tag_m2005840874(L_73, /*hidden argument*/NULL);
		if ((!(((uint32_t)((int32_t)((int32_t)L_74&(int32_t)((int32_t)31)))) == ((uint32_t)((int32_t)17)))))
		{
			goto IL_01ce;
		}
	}
	{
		Asn1Node_t1770761751 * L_75 = V_2;
		NullCheck(L_75);
		int64_t L_76 = Asn1Node_get_ChildNodeCount_m1123088750(L_75, /*hidden argument*/NULL);
		if ((!(((uint64_t)L_76) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_01d4;
		}
	}

IL_01ce:
	{
		InvalidPKCS7Data_t4123278833 * L_77 = (InvalidPKCS7Data_t4123278833 *)il2cpp_codegen_object_new(InvalidPKCS7Data_t4123278833_il2cpp_TypeInfo_var);
		InvalidPKCS7Data__ctor_m1217025319(L_77, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_77);
	}

IL_01d4:
	{
		List_1_t3491469936 * L_78 = (List_1_t3491469936 *)il2cpp_codegen_object_new(List_1_t3491469936_il2cpp_TypeInfo_var);
		List_1__ctor_m1845338834(L_78, /*hidden argument*/List_1__ctor_m1845338834_MethodInfo_var);
		PKCS7_set_sinfos_m921812246(__this, L_78, /*hidden argument*/NULL);
		V_4 = 0;
		goto IL_0207;
	}

IL_01e7:
	{
		List_1_t3491469936 * L_79 = PKCS7_get_sinfos_m3813593105(__this, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_80 = V_2;
		int32_t L_81 = V_4;
		NullCheck(L_80);
		Asn1Node_t1770761751 * L_82 = Asn1Node_GetChildNode_m4133223467(L_80, L_81, /*hidden argument*/NULL);
		SignerInfo_t4122348804 * L_83 = (SignerInfo_t4122348804 *)il2cpp_codegen_object_new(SignerInfo_t4122348804_il2cpp_TypeInfo_var);
		SignerInfo__ctor_m2022425308(L_83, L_82, /*hidden argument*/NULL);
		NullCheck(L_79);
		List_1_Add_m1551955646(L_79, L_83, /*hidden argument*/List_1_Add_m1551955646_MethodInfo_var);
		int32_t L_84 = V_4;
		V_4 = ((int32_t)((int32_t)L_84+(int32_t)1));
	}

IL_0207:
	{
		int32_t L_85 = V_4;
		Asn1Node_t1770761751 * L_86 = V_2;
		NullCheck(L_86);
		int64_t L_87 = Asn1Node_get_ChildNodeCount_m1123088750(L_86, /*hidden argument*/NULL);
		if ((((int64_t)(((int64_t)((int64_t)L_85)))) < ((int64_t)L_87)))
		{
			goto IL_01e7;
		}
	}
	{
		__this->set_validStructure_4((bool)1);
	}

IL_021d:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.RSAKey::.ctor(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  void RSAKey__ctor_m79110961 (RSAKey_t446464277 * __this, Asn1Node_t1770761751 * ___n0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_0 = ___n0;
		RSACryptoServiceProvider_t4229286967 * L_1 = RSAKey_ParseNode_m1392584771(__this, L_0, /*hidden argument*/NULL);
		RSAKey_set_rsa_m66955140(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.RSAKey::.ctor(System.Byte[])
extern Il2CppClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern Il2CppClass* Asn1Parser_t914015216_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t RSAKey__ctor_m2562631236_MetadataUsageId;
extern "C"  void RSAKey__ctor_m2562631236 (RSAKey_t446464277 * __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RSAKey__ctor_m2562631236_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MemoryStream_t743994179 * V_0 = NULL;
	Asn1Parser_t914015216 * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_0 = ___data0;
		MemoryStream_t743994179 * L_1 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m4073175573(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		Asn1Parser_t914015216 * L_2 = (Asn1Parser_t914015216 *)il2cpp_codegen_object_new(Asn1Parser_t914015216_il2cpp_TypeInfo_var);
		Asn1Parser__ctor_m2929298044(L_2, /*hidden argument*/NULL);
		V_1 = L_2;
		Asn1Parser_t914015216 * L_3 = V_1;
		MemoryStream_t743994179 * L_4 = V_0;
		NullCheck(L_3);
		Asn1Parser_LoadData_m2612632163(L_3, L_4, /*hidden argument*/NULL);
		Asn1Parser_t914015216 * L_5 = V_1;
		NullCheck(L_5);
		Asn1Node_t1770761751 * L_6 = Asn1Parser_get_RootNode_m542146928(L_5, /*hidden argument*/NULL);
		RSACryptoServiceProvider_t4229286967 * L_7 = RSAKey_ParseNode_m1392584771(__this, L_6, /*hidden argument*/NULL);
		RSAKey_set_rsa_m66955140(__this, L_7, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x41, FINALLY_0034);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0034;
	}

FINALLY_0034:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t743994179 * L_8 = V_0;
			if (!L_8)
			{
				goto IL_0040;
			}
		}

IL_003a:
		{
			MemoryStream_t743994179 * L_9 = V_0;
			NullCheck(L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_9);
		}

IL_0040:
		{
			IL2CPP_END_FINALLY(52)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(52)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0041:
	{
		return;
	}
}
// System.Security.Cryptography.RSACryptoServiceProvider UnityEngine.Purchasing.Security.RSAKey::get_rsa()
extern "C"  RSACryptoServiceProvider_t4229286967 * RSAKey_get_rsa_m255744627 (RSAKey_t446464277 * __this, const MethodInfo* method)
{
	RSACryptoServiceProvider_t4229286967 * V_0 = NULL;
	{
		RSACryptoServiceProvider_t4229286967 * L_0 = __this->get_U3CrsaU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RSACryptoServiceProvider_t4229286967 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.RSAKey::set_rsa(System.Security.Cryptography.RSACryptoServiceProvider)
extern "C"  void RSAKey_set_rsa_m66955140 (RSAKey_t446464277 * __this, RSACryptoServiceProvider_t4229286967 * ___value0, const MethodInfo* method)
{
	{
		RSACryptoServiceProvider_t4229286967 * L_0 = ___value0;
		__this->set_U3CrsaU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Boolean UnityEngine.Purchasing.Security.RSAKey::Verify(System.Byte[],System.Byte[])
extern Il2CppClass* SHA1Managed_t7268864_il2cpp_TypeInfo_var;
extern const uint32_t RSAKey_Verify_m2487225620_MetadataUsageId;
extern "C"  bool RSAKey_Verify_m2487225620 (RSAKey_t446464277 * __this, ByteU5BU5D_t3397334013* ___message0, ByteU5BU5D_t3397334013* ___signature1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RSAKey_Verify_m2487225620_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SHA1Managed_t7268864 * V_0 = NULL;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	bool V_2 = false;
	{
		SHA1Managed_t7268864 * L_0 = (SHA1Managed_t7268864 *)il2cpp_codegen_object_new(SHA1Managed_t7268864_il2cpp_TypeInfo_var);
		SHA1Managed__ctor_m2252382529(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		SHA1Managed_t7268864 * L_1 = V_0;
		ByteU5BU5D_t3397334013* L_2 = ___message0;
		NullCheck(L_1);
		ByteU5BU5D_t3397334013* L_3 = HashAlgorithm_ComputeHash_m3637856778(L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		RSACryptoServiceProvider_t4229286967 * L_4 = RSAKey_get_rsa_m255744627(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_5 = V_1;
		ByteU5BU5D_t3397334013* L_6 = ___signature1;
		NullCheck(L_4);
		bool L_7 = RSACryptoServiceProvider_VerifyHash_m1335750435(L_4, L_5, (String_t*)NULL, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		goto IL_0023;
	}

IL_0023:
	{
		bool L_8 = V_2;
		return L_8;
	}
}
// System.Security.Cryptography.RSACryptoServiceProvider UnityEngine.Purchasing.Security.RSAKey::ParseNode(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* RSACryptoServiceProvider_t4229286967_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidRSAData_t1674954323_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2506820213;
extern const uint32_t RSAKey_ParseNode_m1392584771_MetadataUsageId;
extern "C"  RSACryptoServiceProvider_t4229286967 * RSAKey_ParseNode_m1392584771 (RSAKey_t446464277 * __this, Asn1Node_t1770761751 * ___n0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RSAKey_ParseNode_m1392584771_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Asn1Node_t1770761751 * V_0 = NULL;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	ByteU5BU5D_t3397334013* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	RSACryptoServiceProvider_t4229286967 * V_5 = NULL;
	RSACryptoServiceProvider_t4229286967 * V_6 = NULL;
	{
		Asn1Node_t1770761751 * L_0 = ___n0;
		NullCheck(L_0);
		uint8_t L_1 = Asn1Node_get_Tag_m2005840874(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)31)))) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_0104;
		}
	}
	{
		Asn1Node_t1770761751 * L_2 = ___n0;
		NullCheck(L_2);
		int64_t L_3 = Asn1Node_get_ChildNodeCount_m1123088750(L_2, /*hidden argument*/NULL);
		if ((!(((uint64_t)L_3) == ((uint64_t)(((int64_t)((int64_t)2)))))))
		{
			goto IL_0104;
		}
	}
	{
		Asn1Node_t1770761751 * L_4 = ___n0;
		NullCheck(L_4);
		Asn1Node_t1770761751 * L_5 = Asn1Node_GetChildNode_m4133223467(L_4, 0, /*hidden argument*/NULL);
		NullCheck(L_5);
		uint8_t L_6 = Asn1Node_get_Tag_m2005840874(L_5, /*hidden argument*/NULL);
		if ((!(((uint32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)31)))) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_0104;
		}
	}
	{
		Asn1Node_t1770761751 * L_7 = ___n0;
		NullCheck(L_7);
		Asn1Node_t1770761751 * L_8 = Asn1Node_GetChildNode_m4133223467(L_7, 0, /*hidden argument*/NULL);
		NullCheck(L_8);
		Asn1Node_t1770761751 * L_9 = Asn1Node_GetChildNode_m4133223467(L_8, 0, /*hidden argument*/NULL);
		NullCheck(L_9);
		uint8_t L_10 = Asn1Node_get_Tag_m2005840874(L_9, /*hidden argument*/NULL);
		if ((!(((uint32_t)((int32_t)((int32_t)L_10&(int32_t)((int32_t)31)))) == ((uint32_t)6))))
		{
			goto IL_0104;
		}
	}
	{
		Asn1Node_t1770761751 * L_11 = ___n0;
		NullCheck(L_11);
		Asn1Node_t1770761751 * L_12 = Asn1Node_GetChildNode_m4133223467(L_11, 0, /*hidden argument*/NULL);
		NullCheck(L_12);
		Asn1Node_t1770761751 * L_13 = Asn1Node_GetChildNode_m4133223467(L_12, 0, /*hidden argument*/NULL);
		NullCheck(L_13);
		String_t* L_14 = Asn1Node_GetDataStr_m2973067886(L_13, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_14, _stringLiteral2506820213, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0104;
		}
	}
	{
		Asn1Node_t1770761751 * L_16 = ___n0;
		NullCheck(L_16);
		Asn1Node_t1770761751 * L_17 = Asn1Node_GetChildNode_m4133223467(L_16, 1, /*hidden argument*/NULL);
		NullCheck(L_17);
		uint8_t L_18 = Asn1Node_get_Tag_m2005840874(L_17, /*hidden argument*/NULL);
		if ((!(((uint32_t)((int32_t)((int32_t)L_18&(int32_t)((int32_t)31)))) == ((uint32_t)3))))
		{
			goto IL_0104;
		}
	}
	{
		Asn1Node_t1770761751 * L_19 = ___n0;
		NullCheck(L_19);
		Asn1Node_t1770761751 * L_20 = Asn1Node_GetChildNode_m4133223467(L_19, 1, /*hidden argument*/NULL);
		NullCheck(L_20);
		Asn1Node_t1770761751 * L_21 = Asn1Node_GetChildNode_m4133223467(L_20, 0, /*hidden argument*/NULL);
		V_0 = L_21;
		Asn1Node_t1770761751 * L_22 = V_0;
		NullCheck(L_22);
		int64_t L_23 = Asn1Node_get_ChildNodeCount_m1123088750(L_22, /*hidden argument*/NULL);
		if ((!(((uint64_t)L_23) == ((uint64_t)(((int64_t)((int64_t)2)))))))
		{
			goto IL_0103;
		}
	}
	{
		Asn1Node_t1770761751 * L_24 = V_0;
		NullCheck(L_24);
		Asn1Node_t1770761751 * L_25 = Asn1Node_GetChildNode_m4133223467(L_24, 0, /*hidden argument*/NULL);
		NullCheck(L_25);
		ByteU5BU5D_t3397334013* L_26 = Asn1Node_get_Data_m1676178324(L_25, /*hidden argument*/NULL);
		V_1 = L_26;
		ByteU5BU5D_t3397334013* L_27 = V_1;
		NullCheck(L_27);
		V_2 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_27)->max_length))))-(int32_t)1))));
		ByteU5BU5D_t3397334013* L_28 = V_1;
		ByteU5BU5D_t3397334013* L_29 = V_2;
		ByteU5BU5D_t3397334013* L_30 = V_1;
		NullCheck(L_30);
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_28, 1, (Il2CppArray *)(Il2CppArray *)L_29, 0, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length))))-(int32_t)1)), /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_31 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		String_t* L_32 = Convert_ToBase64String_m1936815455(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		V_3 = L_32;
		Asn1Node_t1770761751 * L_33 = V_0;
		NullCheck(L_33);
		Asn1Node_t1770761751 * L_34 = Asn1Node_GetChildNode_m4133223467(L_33, 1, /*hidden argument*/NULL);
		NullCheck(L_34);
		ByteU5BU5D_t3397334013* L_35 = Asn1Node_get_Data_m1676178324(L_34, /*hidden argument*/NULL);
		String_t* L_36 = Convert_ToBase64String_m1936815455(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		V_4 = L_36;
		RSACryptoServiceProvider_t4229286967 * L_37 = (RSACryptoServiceProvider_t4229286967 *)il2cpp_codegen_object_new(RSACryptoServiceProvider_t4229286967_il2cpp_TypeInfo_var);
		RSACryptoServiceProvider__ctor_m1532797528(L_37, /*hidden argument*/NULL);
		V_5 = L_37;
		RSACryptoServiceProvider_t4229286967 * L_38 = V_5;
		String_t* L_39 = V_3;
		String_t* L_40 = V_4;
		String_t* L_41 = RSAKey_ToXML_m3567447122(__this, L_39, L_40, /*hidden argument*/NULL);
		NullCheck(L_38);
		VirtActionInvoker1< String_t* >::Invoke(8 /* System.Void System.Security.Cryptography.AsymmetricAlgorithm::FromXmlString(System.String) */, L_38, L_41);
		RSACryptoServiceProvider_t4229286967 * L_42 = V_5;
		V_6 = L_42;
		goto IL_010a;
	}

IL_0103:
	{
	}

IL_0104:
	{
		InvalidRSAData_t1674954323 * L_43 = (InvalidRSAData_t1674954323 *)il2cpp_codegen_object_new(InvalidRSAData_t1674954323_il2cpp_TypeInfo_var);
		InvalidRSAData__ctor_m3293493135(L_43, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_43);
	}

IL_010a:
	{
		RSACryptoServiceProvider_t4229286967 * L_44 = V_6;
		return L_44;
	}
}
// System.String UnityEngine.Purchasing.Security.RSAKey::ToXML(System.String,System.String)
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3654533673;
extern Il2CppCodeGenString* _stringLiteral4120229221;
extern Il2CppCodeGenString* _stringLiteral2551469331;
extern const uint32_t RSAKey_ToXML_m3567447122_MetadataUsageId;
extern "C"  String_t* RSAKey_ToXML_m3567447122 (RSAKey_t446464277 * __this, String_t* ___modulus0, String_t* ___exponent1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RSAKey_ToXML_m3567447122_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3654533673);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3654533673);
		StringU5BU5D_t1642385972* L_1 = L_0;
		String_t* L_2 = ___modulus0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_2);
		StringU5BU5D_t1642385972* L_3 = L_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, _stringLiteral4120229221);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral4120229221);
		StringU5BU5D_t1642385972* L_4 = L_3;
		String_t* L_5 = ___exponent1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_5);
		StringU5BU5D_t1642385972* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, _stringLiteral2551469331);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral2551469331);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m626692867(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		goto IL_0032;
	}

IL_0032:
	{
		String_t* L_8 = V_0;
		return L_8;
	}
}
// System.Void UnityEngine.Purchasing.Security.SignerInfo::.ctor(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern Il2CppClass* InvalidPKCS7Data_t4123278833_il2cpp_TypeInfo_var;
extern Il2CppClass* UnsupportedSignerInfoVersion_t2780725255_il2cpp_TypeInfo_var;
extern Il2CppClass* Asn1Util_t2059476207_il2cpp_TypeInfo_var;
extern const uint32_t SignerInfo__ctor_m2022425308_MetadataUsageId;
extern "C"  void SignerInfo__ctor_m2022425308 (SignerInfo_t4122348804 * __this, Asn1Node_t1770761751 * ___n0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SignerInfo__ctor_m2022425308_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Asn1Node_t1770761751 * V_0 = NULL;
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_0 = ___n0;
		NullCheck(L_0);
		int64_t L_1 = Asn1Node_get_ChildNodeCount_m1123088750(L_0, /*hidden argument*/NULL);
		if ((((int64_t)L_1) == ((int64_t)(((int64_t)((int64_t)5))))))
		{
			goto IL_001a;
		}
	}
	{
		InvalidPKCS7Data_t4123278833 * L_2 = (InvalidPKCS7Data_t4123278833 *)il2cpp_codegen_object_new(InvalidPKCS7Data_t4123278833_il2cpp_TypeInfo_var);
		InvalidPKCS7Data__ctor_m1217025319(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001a:
	{
		Asn1Node_t1770761751 * L_3 = ___n0;
		NullCheck(L_3);
		Asn1Node_t1770761751 * L_4 = Asn1Node_GetChildNode_m4133223467(L_3, 0, /*hidden argument*/NULL);
		V_0 = L_4;
		Asn1Node_t1770761751 * L_5 = V_0;
		NullCheck(L_5);
		uint8_t L_6 = Asn1Node_get_Tag_m2005840874(L_5, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)31)))) == ((int32_t)2)))
		{
			goto IL_0037;
		}
	}
	{
		InvalidPKCS7Data_t4123278833 * L_7 = (InvalidPKCS7Data_t4123278833 *)il2cpp_codegen_object_new(InvalidPKCS7Data_t4123278833_il2cpp_TypeInfo_var);
		InvalidPKCS7Data__ctor_m1217025319(L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0037:
	{
		Asn1Node_t1770761751 * L_8 = V_0;
		NullCheck(L_8);
		ByteU5BU5D_t3397334013* L_9 = Asn1Node_get_Data_m1676178324(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		int32_t L_10 = 0;
		uint8_t L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		SignerInfo_set_Version_m3486820030(__this, L_11, /*hidden argument*/NULL);
		int32_t L_12 = SignerInfo_get_Version_m890152389(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)1))))
		{
			goto IL_005f;
		}
	}
	{
		Asn1Node_t1770761751 * L_13 = V_0;
		NullCheck(L_13);
		ByteU5BU5D_t3397334013* L_14 = Asn1Node_get_Data_m1676178324(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0065;
		}
	}

IL_005f:
	{
		UnsupportedSignerInfoVersion_t2780725255 * L_15 = (UnsupportedSignerInfoVersion_t2780725255 *)il2cpp_codegen_object_new(UnsupportedSignerInfoVersion_t2780725255_il2cpp_TypeInfo_var);
		UnsupportedSignerInfoVersion__ctor_m1609509651(L_15, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}

IL_0065:
	{
		Asn1Node_t1770761751 * L_16 = ___n0;
		NullCheck(L_16);
		Asn1Node_t1770761751 * L_17 = Asn1Node_GetChildNode_m4133223467(L_16, 1, /*hidden argument*/NULL);
		V_0 = L_17;
		Asn1Node_t1770761751 * L_18 = V_0;
		NullCheck(L_18);
		uint8_t L_19 = Asn1Node_get_Tag_m2005840874(L_18, /*hidden argument*/NULL);
		if ((!(((uint32_t)((int32_t)((int32_t)L_19&(int32_t)((int32_t)31)))) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_008a;
		}
	}
	{
		Asn1Node_t1770761751 * L_20 = V_0;
		NullCheck(L_20);
		int64_t L_21 = Asn1Node_get_ChildNodeCount_m1123088750(L_20, /*hidden argument*/NULL);
		if ((((int64_t)L_21) == ((int64_t)(((int64_t)((int64_t)2))))))
		{
			goto IL_0090;
		}
	}

IL_008a:
	{
		InvalidPKCS7Data_t4123278833 * L_22 = (InvalidPKCS7Data_t4123278833 *)il2cpp_codegen_object_new(InvalidPKCS7Data_t4123278833_il2cpp_TypeInfo_var);
		InvalidPKCS7Data__ctor_m1217025319(L_22, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	}

IL_0090:
	{
		Asn1Node_t1770761751 * L_23 = V_0;
		NullCheck(L_23);
		Asn1Node_t1770761751 * L_24 = Asn1Node_GetChildNode_m4133223467(L_23, 1, /*hidden argument*/NULL);
		V_0 = L_24;
		Asn1Node_t1770761751 * L_25 = V_0;
		NullCheck(L_25);
		uint8_t L_26 = Asn1Node_get_Tag_m2005840874(L_25, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_26&(int32_t)((int32_t)31)))) == ((int32_t)2)))
		{
			goto IL_00ad;
		}
	}
	{
		InvalidPKCS7Data_t4123278833 * L_27 = (InvalidPKCS7Data_t4123278833 *)il2cpp_codegen_object_new(InvalidPKCS7Data_t4123278833_il2cpp_TypeInfo_var);
		InvalidPKCS7Data__ctor_m1217025319(L_27, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_27);
	}

IL_00ad:
	{
		Asn1Node_t1770761751 * L_28 = V_0;
		NullCheck(L_28);
		ByteU5BU5D_t3397334013* L_29 = Asn1Node_get_Data_m1676178324(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		String_t* L_30 = Asn1Util_ToHexString_m3932902392(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		SignerInfo_set_IssuerSerialNumber_m3572187615(__this, L_30, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_31 = ___n0;
		NullCheck(L_31);
		Asn1Node_t1770761751 * L_32 = Asn1Node_GetChildNode_m4133223467(L_31, 4, /*hidden argument*/NULL);
		V_0 = L_32;
		Asn1Node_t1770761751 * L_33 = V_0;
		NullCheck(L_33);
		uint8_t L_34 = Asn1Node_get_Tag_m2005840874(L_33, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_34&(int32_t)((int32_t)31)))) == ((int32_t)4)))
		{
			goto IL_00db;
		}
	}
	{
		InvalidPKCS7Data_t4123278833 * L_35 = (InvalidPKCS7Data_t4123278833 *)il2cpp_codegen_object_new(InvalidPKCS7Data_t4123278833_il2cpp_TypeInfo_var);
		InvalidPKCS7Data__ctor_m1217025319(L_35, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_35);
	}

IL_00db:
	{
		Asn1Node_t1770761751 * L_36 = V_0;
		NullCheck(L_36);
		ByteU5BU5D_t3397334013* L_37 = Asn1Node_get_Data_m1676178324(L_36, /*hidden argument*/NULL);
		SignerInfo_set_EncryptedDigest_m3655085066(__this, L_37, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Purchasing.Security.SignerInfo::get_Version()
extern "C"  int32_t SignerInfo_get_Version_m890152389 (SignerInfo_t4122348804 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CVersionU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.SignerInfo::set_Version(System.Int32)
extern "C"  void SignerInfo_set_Version_m3486820030 (SignerInfo_t4122348804 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CVersionU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String UnityEngine.Purchasing.Security.SignerInfo::get_IssuerSerialNumber()
extern "C"  String_t* SignerInfo_get_IssuerSerialNumber_m988083912 (SignerInfo_t4122348804 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CIssuerSerialNumberU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.SignerInfo::set_IssuerSerialNumber(System.String)
extern "C"  void SignerInfo_set_IssuerSerialNumber_m3572187615 (SignerInfo_t4122348804 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CIssuerSerialNumberU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Byte[] UnityEngine.Purchasing.Security.SignerInfo::get_EncryptedDigest()
extern "C"  ByteU5BU5D_t3397334013* SignerInfo_get_EncryptedDigest_m2178470575 (SignerInfo_t4122348804 * __this, const MethodInfo* method)
{
	ByteU5BU5D_t3397334013* V_0 = NULL;
	{
		ByteU5BU5D_t3397334013* L_0 = __this->get_U3CEncryptedDigestU3Ek__BackingField_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		ByteU5BU5D_t3397334013* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.SignerInfo::set_EncryptedDigest(System.Byte[])
extern "C"  void SignerInfo_set_EncryptedDigest_m3655085066 (SignerInfo_t4122348804 * __this, ByteU5BU5D_t3397334013* ___value0, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = ___value0;
		__this->set_U3CEncryptedDigestU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.StoreNotSupportedException::.ctor(System.String)
extern "C"  void StoreNotSupportedException__ctor_m4169319163 (StoreNotSupportedException_t958190973 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		IAPSecurityException__ctor_m1626861665(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.UnsupportedSignerInfoVersion::.ctor()
extern "C"  void UnsupportedSignerInfoVersion__ctor_m1609509651 (UnsupportedSignerInfoVersion_t2780725255 * __this, const MethodInfo* method)
{
	{
		IAPSecurityException__ctor_m4130362627(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.X509Cert::.ctor(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  void X509Cert__ctor_m2063768312 (X509Cert_t481809278 * __this, Asn1Node_t1770761751 * ___n0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_0 = ___n0;
		X509Cert_ParseNode_m3118026731(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.X509Cert::.ctor(System.Byte[])
extern Il2CppClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern Il2CppClass* Asn1Parser_t914015216_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t X509Cert__ctor_m3040169225_MetadataUsageId;
extern "C"  void X509Cert__ctor_m3040169225 (X509Cert_t481809278 * __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (X509Cert__ctor_m3040169225_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MemoryStream_t743994179 * V_0 = NULL;
	Asn1Parser_t914015216 * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_0 = ___data0;
		MemoryStream_t743994179 * L_1 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m4073175573(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		Asn1Parser_t914015216 * L_2 = (Asn1Parser_t914015216 *)il2cpp_codegen_object_new(Asn1Parser_t914015216_il2cpp_TypeInfo_var);
		Asn1Parser__ctor_m2929298044(L_2, /*hidden argument*/NULL);
		V_1 = L_2;
		Asn1Parser_t914015216 * L_3 = V_1;
		MemoryStream_t743994179 * L_4 = V_0;
		NullCheck(L_3);
		Asn1Parser_LoadData_m2612632163(L_3, L_4, /*hidden argument*/NULL);
		Asn1Parser_t914015216 * L_5 = V_1;
		NullCheck(L_5);
		Asn1Node_t1770761751 * L_6 = Asn1Parser_get_RootNode_m542146928(L_5, /*hidden argument*/NULL);
		X509Cert_ParseNode_m3118026731(__this, L_6, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x3B, FINALLY_002e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002e;
	}

FINALLY_002e:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t743994179 * L_7 = V_0;
			if (!L_7)
			{
				goto IL_003a;
			}
		}

IL_0034:
		{
			MemoryStream_t743994179 * L_8 = V_0;
			NullCheck(L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_8);
		}

IL_003a:
		{
			IL2CPP_END_FINALLY(46)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(46)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.String UnityEngine.Purchasing.Security.X509Cert::get_SerialNumber()
extern "C"  String_t* X509Cert_get_SerialNumber_m3398499893 (X509Cert_t481809278 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_U3CSerialNumberU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.X509Cert::set_SerialNumber(System.String)
extern "C"  void X509Cert_set_SerialNumber_m3397980220 (X509Cert_t481809278 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CSerialNumberU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.DateTime UnityEngine.Purchasing.Security.X509Cert::get_ValidAfter()
extern "C"  DateTime_t693205669  X509Cert_get_ValidAfter_m1286975886 (X509Cert_t481809278 * __this, const MethodInfo* method)
{
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DateTime_t693205669  L_0 = __this->get_U3CValidAfterU3Ek__BackingField_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		DateTime_t693205669  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.X509Cert::set_ValidAfter(System.DateTime)
extern "C"  void X509Cert_set_ValidAfter_m451723427 (X509Cert_t481809278 * __this, DateTime_t693205669  ___value0, const MethodInfo* method)
{
	{
		DateTime_t693205669  L_0 = ___value0;
		__this->set_U3CValidAfterU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.DateTime UnityEngine.Purchasing.Security.X509Cert::get_ValidBefore()
extern "C"  DateTime_t693205669  X509Cert_get_ValidBefore_m641622829 (X509Cert_t481809278 * __this, const MethodInfo* method)
{
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DateTime_t693205669  L_0 = __this->get_U3CValidBeforeU3Ek__BackingField_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		DateTime_t693205669  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.X509Cert::set_ValidBefore(System.DateTime)
extern "C"  void X509Cert_set_ValidBefore_m2131887164 (X509Cert_t481809278 * __this, DateTime_t693205669  ___value0, const MethodInfo* method)
{
	{
		DateTime_t693205669  L_0 = ___value0;
		__this->set_U3CValidBeforeU3Ek__BackingField_2(L_0);
		return;
	}
}
// UnityEngine.Purchasing.Security.RSAKey UnityEngine.Purchasing.Security.X509Cert::get_PubKey()
extern "C"  RSAKey_t446464277 * X509Cert_get_PubKey_m3319710642 (X509Cert_t481809278 * __this, const MethodInfo* method)
{
	RSAKey_t446464277 * V_0 = NULL;
	{
		RSAKey_t446464277 * L_0 = __this->get_U3CPubKeyU3Ek__BackingField_3();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RSAKey_t446464277 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.X509Cert::set_PubKey(UnityEngine.Purchasing.Security.RSAKey)
extern "C"  void X509Cert_set_PubKey_m2350741731 (X509Cert_t481809278 * __this, RSAKey_t446464277 * ___value0, const MethodInfo* method)
{
	{
		RSAKey_t446464277 * L_0 = ___value0;
		__this->set_U3CPubKeyU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.Security.X509Cert::set_SelfSigned(System.Boolean)
extern "C"  void X509Cert_set_SelfSigned_m2342926440 (X509Cert_t481809278 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CSelfSignedU3Ek__BackingField_4(L_0);
		return;
	}
}
// UnityEngine.Purchasing.Security.DistinguishedName UnityEngine.Purchasing.Security.X509Cert::get_Subject()
extern "C"  DistinguishedName_t1881593989 * X509Cert_get_Subject_m640225680 (X509Cert_t481809278 * __this, const MethodInfo* method)
{
	DistinguishedName_t1881593989 * V_0 = NULL;
	{
		DistinguishedName_t1881593989 * L_0 = __this->get_U3CSubjectU3Ek__BackingField_5();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		DistinguishedName_t1881593989 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.X509Cert::set_Subject(UnityEngine.Purchasing.Security.DistinguishedName)
extern "C"  void X509Cert_set_Subject_m3399824887 (X509Cert_t481809278 * __this, DistinguishedName_t1881593989 * ___value0, const MethodInfo* method)
{
	{
		DistinguishedName_t1881593989 * L_0 = ___value0;
		__this->set_U3CSubjectU3Ek__BackingField_5(L_0);
		return;
	}
}
// UnityEngine.Purchasing.Security.DistinguishedName UnityEngine.Purchasing.Security.X509Cert::get_Issuer()
extern "C"  DistinguishedName_t1881593989 * X509Cert_get_Issuer_m3153344599 (X509Cert_t481809278 * __this, const MethodInfo* method)
{
	DistinguishedName_t1881593989 * V_0 = NULL;
	{
		DistinguishedName_t1881593989 * L_0 = __this->get_U3CIssuerU3Ek__BackingField_6();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		DistinguishedName_t1881593989 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.X509Cert::set_Issuer(UnityEngine.Purchasing.Security.DistinguishedName)
extern "C"  void X509Cert_set_Issuer_m1817635190 (X509Cert_t481809278 * __this, DistinguishedName_t1881593989 * ___value0, const MethodInfo* method)
{
	{
		DistinguishedName_t1881593989 * L_0 = ___value0;
		__this->set_U3CIssuerU3Ek__BackingField_6(L_0);
		return;
	}
}
// LipingShare.LCLib.Asn1Processor.Asn1Node UnityEngine.Purchasing.Security.X509Cert::get_Signature()
extern "C"  Asn1Node_t1770761751 * X509Cert_get_Signature_m3266380160 (X509Cert_t481809278 * __this, const MethodInfo* method)
{
	Asn1Node_t1770761751 * V_0 = NULL;
	{
		Asn1Node_t1770761751 * L_0 = __this->get_U3CSignatureU3Ek__BackingField_8();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Asn1Node_t1770761751 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.Security.X509Cert::set_Signature(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  void X509Cert_set_Signature_m1044505155 (X509Cert_t481809278 * __this, Asn1Node_t1770761751 * ___value0, const MethodInfo* method)
{
	{
		Asn1Node_t1770761751 * L_0 = ___value0;
		__this->set_U3CSignatureU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Boolean UnityEngine.Purchasing.Security.X509Cert::CheckCertTime(System.DateTime)
extern "C"  bool X509Cert_CheckCertTime_m317868091 (X509Cert_t481809278 * __this, DateTime_t693205669  ___time0, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		DateTime_t693205669  L_0 = X509Cert_get_ValidAfter_m1286975886(__this, /*hidden argument*/NULL);
		int32_t L_1 = DateTime_CompareTo_m1511117942((&___time0), L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) < ((int32_t)0)))
		{
			goto IL_0029;
		}
	}
	{
		DateTime_t693205669  L_2 = X509Cert_get_ValidBefore_m641622829(__this, /*hidden argument*/NULL);
		int32_t L_3 = DateTime_CompareTo_m1511117942((&___time0), L_2, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)((((int32_t)L_3) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_002a;
	}

IL_0029:
	{
		G_B3_0 = 0;
	}

IL_002a:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0030;
	}

IL_0030:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.Purchasing.Security.X509Cert::CheckSignature(UnityEngine.Purchasing.Security.X509Cert)
extern "C"  bool X509Cert_CheckSignature_m4098105751 (X509Cert_t481809278 * __this, X509Cert_t481809278 * ___signer0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		DistinguishedName_t1881593989 * L_0 = X509Cert_get_Issuer_m3153344599(__this, /*hidden argument*/NULL);
		X509Cert_t481809278 * L_1 = ___signer0;
		NullCheck(L_1);
		DistinguishedName_t1881593989 * L_2 = X509Cert_get_Subject_m640225680(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_3 = DistinguishedName_Equals_m575298400(L_0, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003a;
		}
	}
	{
		X509Cert_t481809278 * L_4 = ___signer0;
		NullCheck(L_4);
		RSAKey_t446464277 * L_5 = X509Cert_get_PubKey_m3319710642(L_4, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_6 = __this->get_rawTBSCertificate_9();
		Asn1Node_t1770761751 * L_7 = X509Cert_get_Signature_m3266380160(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		ByteU5BU5D_t3397334013* L_8 = Asn1Node_get_Data_m1676178324(L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_9 = RSAKey_Verify_m2487225620(L_5, L_6, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0041;
	}

IL_003a:
	{
		V_0 = (bool)0;
		goto IL_0041;
	}

IL_0041:
	{
		bool L_10 = V_0;
		return L_10;
	}
}
// System.Void UnityEngine.Purchasing.Security.X509Cert::ParseNode(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern Il2CppClass* InvalidX509Data_t1630759105_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* Asn1Util_t2059476207_il2cpp_TypeInfo_var;
extern Il2CppClass* DistinguishedName_t1881593989_il2cpp_TypeInfo_var;
extern Il2CppClass* RSAKey_t446464277_il2cpp_TypeInfo_var;
extern const uint32_t X509Cert_ParseNode_m3118026731_MetadataUsageId;
extern "C"  void X509Cert_ParseNode_m3118026731 (X509Cert_t481809278 * __this, Asn1Node_t1770761751 * ___root0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (X509Cert_ParseNode_m3118026731_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Asn1Node_t1770761751 * V_0 = NULL;
	Asn1Node_t1770761751 * V_1 = NULL;
	{
		Asn1Node_t1770761751 * L_0 = ___root0;
		NullCheck(L_0);
		uint8_t L_1 = Asn1Node_get_Tag_m2005840874(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)31)))) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_001e;
		}
	}
	{
		Asn1Node_t1770761751 * L_2 = ___root0;
		NullCheck(L_2);
		int64_t L_3 = Asn1Node_get_ChildNodeCount_m1123088750(L_2, /*hidden argument*/NULL);
		if ((((int64_t)L_3) == ((int64_t)(((int64_t)((int64_t)3))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		InvalidX509Data_t1630759105 * L_4 = (InvalidX509Data_t1630759105 *)il2cpp_codegen_object_new(InvalidX509Data_t1630759105_il2cpp_TypeInfo_var);
		InvalidX509Data__ctor_m1976349147(L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0024:
	{
		Asn1Node_t1770761751 * L_5 = ___root0;
		NullCheck(L_5);
		Asn1Node_t1770761751 * L_6 = Asn1Node_GetChildNode_m4133223467(L_5, 0, /*hidden argument*/NULL);
		__this->set_TbsCertificate_7(L_6);
		Asn1Node_t1770761751 * L_7 = __this->get_TbsCertificate_7();
		NullCheck(L_7);
		int64_t L_8 = Asn1Node_get_ChildNodeCount_m1123088750(L_7, /*hidden argument*/NULL);
		if ((((int64_t)L_8) >= ((int64_t)(((int64_t)((int64_t)7))))))
		{
			goto IL_0049;
		}
	}
	{
		InvalidX509Data_t1630759105 * L_9 = (InvalidX509Data_t1630759105 *)il2cpp_codegen_object_new(InvalidX509Data_t1630759105_il2cpp_TypeInfo_var);
		InvalidX509Data__ctor_m1976349147(L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0049:
	{
		Asn1Node_t1770761751 * L_10 = __this->get_TbsCertificate_7();
		NullCheck(L_10);
		int64_t L_11 = Asn1Node_get_DataLength_m4095104165(L_10, /*hidden argument*/NULL);
		if ((int64_t)(((int64_t)((int64_t)L_11+(int64_t)(((int64_t)((int64_t)4)))))) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		__this->set_rawTBSCertificate_9(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)(((intptr_t)((int64_t)((int64_t)L_11+(int64_t)(((int64_t)((int64_t)4))))))))));
		Asn1Node_t1770761751 * L_12 = ___root0;
		NullCheck(L_12);
		ByteU5BU5D_t3397334013* L_13 = Asn1Node_get_Data_m1676178324(L_12, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_14 = __this->get_rawTBSCertificate_9();
		ByteU5BU5D_t3397334013* L_15 = __this->get_rawTBSCertificate_9();
		NullCheck(L_15);
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_13, 0, (Il2CppArray *)(Il2CppArray *)L_14, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))), /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_16 = __this->get_TbsCertificate_7();
		NullCheck(L_16);
		Asn1Node_t1770761751 * L_17 = Asn1Node_GetChildNode_m4133223467(L_16, 1, /*hidden argument*/NULL);
		V_0 = L_17;
		Asn1Node_t1770761751 * L_18 = V_0;
		NullCheck(L_18);
		uint8_t L_19 = Asn1Node_get_Tag_m2005840874(L_18, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_19&(int32_t)((int32_t)31)))) == ((int32_t)2)))
		{
			goto IL_00a0;
		}
	}
	{
		InvalidX509Data_t1630759105 * L_20 = (InvalidX509Data_t1630759105 *)il2cpp_codegen_object_new(InvalidX509Data_t1630759105_il2cpp_TypeInfo_var);
		InvalidX509Data__ctor_m1976349147(L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20);
	}

IL_00a0:
	{
		Asn1Node_t1770761751 * L_21 = V_0;
		NullCheck(L_21);
		ByteU5BU5D_t3397334013* L_22 = Asn1Node_get_Data_m1676178324(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Asn1Util_t2059476207_il2cpp_TypeInfo_var);
		String_t* L_23 = Asn1Util_ToHexString_m3932902392(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		X509Cert_set_SerialNumber_m3397980220(__this, L_23, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_24 = __this->get_TbsCertificate_7();
		NullCheck(L_24);
		Asn1Node_t1770761751 * L_25 = Asn1Node_GetChildNode_m4133223467(L_24, 3, /*hidden argument*/NULL);
		DistinguishedName_t1881593989 * L_26 = (DistinguishedName_t1881593989 *)il2cpp_codegen_object_new(DistinguishedName_t1881593989_il2cpp_TypeInfo_var);
		DistinguishedName__ctor_m3658482605(L_26, L_25, /*hidden argument*/NULL);
		X509Cert_set_Issuer_m1817635190(__this, L_26, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_27 = __this->get_TbsCertificate_7();
		NullCheck(L_27);
		Asn1Node_t1770761751 * L_28 = Asn1Node_GetChildNode_m4133223467(L_27, 5, /*hidden argument*/NULL);
		DistinguishedName_t1881593989 * L_29 = (DistinguishedName_t1881593989 *)il2cpp_codegen_object_new(DistinguishedName_t1881593989_il2cpp_TypeInfo_var);
		DistinguishedName__ctor_m3658482605(L_29, L_28, /*hidden argument*/NULL);
		X509Cert_set_Subject_m3399824887(__this, L_29, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_30 = __this->get_TbsCertificate_7();
		NullCheck(L_30);
		Asn1Node_t1770761751 * L_31 = Asn1Node_GetChildNode_m4133223467(L_30, 4, /*hidden argument*/NULL);
		V_1 = L_31;
		Asn1Node_t1770761751 * L_32 = V_1;
		NullCheck(L_32);
		uint8_t L_33 = Asn1Node_get_Tag_m2005840874(L_32, /*hidden argument*/NULL);
		if ((!(((uint32_t)((int32_t)((int32_t)L_33&(int32_t)((int32_t)31)))) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_0109;
		}
	}
	{
		Asn1Node_t1770761751 * L_34 = V_1;
		NullCheck(L_34);
		int64_t L_35 = Asn1Node_get_ChildNodeCount_m1123088750(L_34, /*hidden argument*/NULL);
		if ((((int64_t)L_35) == ((int64_t)(((int64_t)((int64_t)2))))))
		{
			goto IL_010f;
		}
	}

IL_0109:
	{
		InvalidX509Data_t1630759105 * L_36 = (InvalidX509Data_t1630759105 *)il2cpp_codegen_object_new(InvalidX509Data_t1630759105_il2cpp_TypeInfo_var);
		InvalidX509Data__ctor_m1976349147(L_36, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_36);
	}

IL_010f:
	{
		Asn1Node_t1770761751 * L_37 = V_1;
		NullCheck(L_37);
		Asn1Node_t1770761751 * L_38 = Asn1Node_GetChildNode_m4133223467(L_37, 0, /*hidden argument*/NULL);
		DateTime_t693205669  L_39 = X509Cert_ParseTime_m1888822247(__this, L_38, /*hidden argument*/NULL);
		X509Cert_set_ValidAfter_m451723427(__this, L_39, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_40 = V_1;
		NullCheck(L_40);
		Asn1Node_t1770761751 * L_41 = Asn1Node_GetChildNode_m4133223467(L_40, 1, /*hidden argument*/NULL);
		DateTime_t693205669  L_42 = X509Cert_ParseTime_m1888822247(__this, L_41, /*hidden argument*/NULL);
		X509Cert_set_ValidBefore_m2131887164(__this, L_42, /*hidden argument*/NULL);
		DistinguishedName_t1881593989 * L_43 = X509Cert_get_Subject_m640225680(__this, /*hidden argument*/NULL);
		DistinguishedName_t1881593989 * L_44 = X509Cert_get_Issuer_m3153344599(__this, /*hidden argument*/NULL);
		NullCheck(L_43);
		bool L_45 = DistinguishedName_Equals_m575298400(L_43, L_44, /*hidden argument*/NULL);
		X509Cert_set_SelfSigned_m2342926440(__this, L_45, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_46 = __this->get_TbsCertificate_7();
		NullCheck(L_46);
		Asn1Node_t1770761751 * L_47 = Asn1Node_GetChildNode_m4133223467(L_46, 6, /*hidden argument*/NULL);
		RSAKey_t446464277 * L_48 = (RSAKey_t446464277 *)il2cpp_codegen_object_new(RSAKey_t446464277_il2cpp_TypeInfo_var);
		RSAKey__ctor_m79110961(L_48, L_47, /*hidden argument*/NULL);
		X509Cert_set_PubKey_m2350741731(__this, L_48, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_49 = ___root0;
		NullCheck(L_49);
		Asn1Node_t1770761751 * L_50 = Asn1Node_GetChildNode_m4133223467(L_49, 2, /*hidden argument*/NULL);
		X509Cert_set_Signature_m1044505155(__this, L_50, /*hidden argument*/NULL);
		return;
	}
}
// System.DateTime UnityEngine.Purchasing.Security.X509Cert::ParseTime(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern Il2CppClass* UTF8Encoding_t111055448_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidTimeFormat_t3933748955_il2cpp_TypeInfo_var;
extern const uint32_t X509Cert_ParseTime_m1888822247_MetadataUsageId;
extern "C"  DateTime_t693205669  X509Cert_ParseTime_m1888822247 (X509Cert_t481809278 * __this, Asn1Node_t1770761751 * ___n0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (X509Cert_ParseTime_m1888822247_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	DateTime_t693205669  V_8;
	memset(&V_8, 0, sizeof(V_8));
	{
		UTF8Encoding_t111055448 * L_0 = (UTF8Encoding_t111055448 *)il2cpp_codegen_object_new(UTF8Encoding_t111055448_il2cpp_TypeInfo_var);
		UTF8Encoding__ctor_m100325490(L_0, /*hidden argument*/NULL);
		Asn1Node_t1770761751 * L_1 = ___n0;
		NullCheck(L_1);
		ByteU5BU5D_t3397334013* L_2 = Asn1Node_get_Data_m1676178324(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_3 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_0, L_2);
		V_0 = L_3;
		String_t* L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m1606060069(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)((int32_t)13))))
		{
			goto IL_0032;
		}
	}
	{
		String_t* L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m1606060069(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)((int32_t)15))))
		{
			goto IL_0032;
		}
	}
	{
		InvalidTimeFormat_t3933748955 * L_8 = (InvalidTimeFormat_t3933748955 *)il2cpp_codegen_object_new(InvalidTimeFormat_t3933748955_il2cpp_TypeInfo_var);
		InvalidTimeFormat__ctor_m2055199385(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0032:
	{
		String_t* L_9 = V_0;
		String_t* L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = String_get_Length_m1606060069(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		Il2CppChar L_12 = String_get_Chars_m4230566705(L_9, ((int32_t)((int32_t)L_11-(int32_t)1)), /*hidden argument*/NULL);
		if ((((int32_t)L_12) == ((int32_t)((int32_t)90))))
		{
			goto IL_004d;
		}
	}
	{
		InvalidTimeFormat_t3933748955 * L_13 = (InvalidTimeFormat_t3933748955 *)il2cpp_codegen_object_new(InvalidTimeFormat_t3933748955_il2cpp_TypeInfo_var);
		InvalidTimeFormat__ctor_m2055199385(L_13, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}

IL_004d:
	{
		V_1 = 0;
		V_2 = 0;
		String_t* L_14 = V_0;
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m1606060069(L_14, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_009c;
		}
	}
	{
		String_t* L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = String_Substring_m12482732(L_16, 0, 2, /*hidden argument*/NULL);
		int32_t L_18 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		int32_t L_19 = V_2;
		if ((((int32_t)L_19) < ((int32_t)((int32_t)50))))
		{
			goto IL_0082;
		}
	}
	{
		int32_t L_20 = V_2;
		V_2 = ((int32_t)((int32_t)L_20+(int32_t)((int32_t)1900)));
		goto IL_0092;
	}

IL_0082:
	{
		int32_t L_21 = V_2;
		if ((((int32_t)L_21) >= ((int32_t)((int32_t)50))))
		{
			goto IL_0092;
		}
	}
	{
		int32_t L_22 = V_2;
		V_2 = ((int32_t)((int32_t)L_22+(int32_t)((int32_t)2000)));
	}

IL_0092:
	{
		int32_t L_23 = V_1;
		V_1 = ((int32_t)((int32_t)L_23+(int32_t)2));
		goto IL_00b0;
	}

IL_009c:
	{
		String_t* L_24 = V_0;
		NullCheck(L_24);
		String_t* L_25 = String_Substring_m12482732(L_24, 0, 4, /*hidden argument*/NULL);
		int32_t L_26 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		V_2 = L_26;
		int32_t L_27 = V_1;
		V_1 = ((int32_t)((int32_t)L_27+(int32_t)4));
	}

IL_00b0:
	{
		String_t* L_28 = V_0;
		int32_t L_29 = V_1;
		NullCheck(L_28);
		String_t* L_30 = String_Substring_m12482732(L_28, L_29, 2, /*hidden argument*/NULL);
		int32_t L_31 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		V_3 = L_31;
		int32_t L_32 = V_1;
		V_1 = ((int32_t)((int32_t)L_32+(int32_t)2));
		String_t* L_33 = V_0;
		int32_t L_34 = V_1;
		NullCheck(L_33);
		String_t* L_35 = String_Substring_m12482732(L_33, L_34, 2, /*hidden argument*/NULL);
		int32_t L_36 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		V_4 = L_36;
		int32_t L_37 = V_1;
		V_1 = ((int32_t)((int32_t)L_37+(int32_t)2));
		String_t* L_38 = V_0;
		int32_t L_39 = V_1;
		NullCheck(L_38);
		String_t* L_40 = String_Substring_m12482732(L_38, L_39, 2, /*hidden argument*/NULL);
		int32_t L_41 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		V_5 = L_41;
		int32_t L_42 = V_1;
		V_1 = ((int32_t)((int32_t)L_42+(int32_t)2));
		String_t* L_43 = V_0;
		int32_t L_44 = V_1;
		NullCheck(L_43);
		String_t* L_45 = String_Substring_m12482732(L_43, L_44, 2, /*hidden argument*/NULL);
		int32_t L_46 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		V_6 = L_46;
		int32_t L_47 = V_1;
		V_1 = ((int32_t)((int32_t)L_47+(int32_t)2));
		String_t* L_48 = V_0;
		int32_t L_49 = V_1;
		NullCheck(L_48);
		String_t* L_50 = String_Substring_m12482732(L_48, L_49, 2, /*hidden argument*/NULL);
		int32_t L_51 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		V_7 = L_51;
		int32_t L_52 = V_1;
		V_1 = ((int32_t)((int32_t)L_52+(int32_t)2));
		int32_t L_53 = V_2;
		int32_t L_54 = V_3;
		int32_t L_55 = V_4;
		int32_t L_56 = V_5;
		int32_t L_57 = V_6;
		int32_t L_58 = V_7;
		DateTime_t693205669  L_59;
		memset(&L_59, 0, sizeof(L_59));
		DateTime__ctor_m3270618252(&L_59, L_53, L_54, L_55, L_56, L_57, L_58, 1, /*hidden argument*/NULL);
		V_8 = L_59;
		goto IL_0125;
	}

IL_0125:
	{
		DateTime_t693205669  L_60 = V_8;
		return L_60;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
