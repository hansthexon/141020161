﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;
// Code
struct Code_t686167511;
// MediaPlayerCtrl
struct MediaPlayerCtrl_t1284484152;
// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// DataService
struct DataService_t2602786891;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager2
struct  GameManager2_t3349110545  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject GameManager2::StarBracket
	GameObject_t1756533147 * ___StarBracket_2;
	// UnityEngine.GameObject GameManager2::PlanetBracket
	GameObject_t1756533147 * ___PlanetBracket_3;
	// System.Int32 GameManager2::currentscore
	int32_t ___currentscore_4;
	// UnityEngine.UI.Text GameManager2::CurrentScoretext
	Text_t356221433 * ___CurrentScoretext_5;
	// UnityEngine.UI.Text GameManager2::StarScoreText
	Text_t356221433 * ___StarScoreText_6;
	// UnityEngine.UI.Text GameManager2::PlanerScoreText
	Text_t356221433 * ___PlanerScoreText_7;
	// System.String GameManager2::StarScore
	String_t* ___StarScore_8;
	// System.String GameManager2::PlanetScore
	String_t* ___PlanetScore_9;
	// System.Boolean GameManager2::cantrack
	bool ___cantrack_10;
	// System.Boolean GameManager2::politeplanet
	bool ___politeplanet_11;
	// UnityEngine.GameObject GameManager2::videoManager
	GameObject_t1756533147 * ___videoManager_12;
	// Code GameManager2::codeobject
	Code_t686167511 * ___codeobject_13;
	// MediaPlayerCtrl GameManager2::player
	MediaPlayerCtrl_t1284484152 * ___player_14;
	// System.String GameManager2::tempfilename
	String_t* ___tempfilename_15;
	// UnityEngine.UI.RawImage GameManager2::videoscreen
	RawImage_t2749640213 * ___videoscreen_16;
	// UnityEngine.GameObject GameManager2::mainpanel
	GameObject_t1756533147 * ___mainpanel_17;
	// UnityEngine.GameObject GameManager2::Imagsets
	GameObject_t1756533147 * ___Imagsets_18;
	// System.String GameManager2::filepath
	String_t* ___filepath_19;
	// DataService GameManager2::ds
	DataService_t2602786891 * ___ds_20;
	// UnityEngine.GameObject GameManager2::Scorepanel
	GameObject_t1756533147 * ___Scorepanel_21;
	// System.Int32 GameManager2::i
	int32_t ___i_22;

public:
	inline static int32_t get_offset_of_StarBracket_2() { return static_cast<int32_t>(offsetof(GameManager2_t3349110545, ___StarBracket_2)); }
	inline GameObject_t1756533147 * get_StarBracket_2() const { return ___StarBracket_2; }
	inline GameObject_t1756533147 ** get_address_of_StarBracket_2() { return &___StarBracket_2; }
	inline void set_StarBracket_2(GameObject_t1756533147 * value)
	{
		___StarBracket_2 = value;
		Il2CppCodeGenWriteBarrier(&___StarBracket_2, value);
	}

	inline static int32_t get_offset_of_PlanetBracket_3() { return static_cast<int32_t>(offsetof(GameManager2_t3349110545, ___PlanetBracket_3)); }
	inline GameObject_t1756533147 * get_PlanetBracket_3() const { return ___PlanetBracket_3; }
	inline GameObject_t1756533147 ** get_address_of_PlanetBracket_3() { return &___PlanetBracket_3; }
	inline void set_PlanetBracket_3(GameObject_t1756533147 * value)
	{
		___PlanetBracket_3 = value;
		Il2CppCodeGenWriteBarrier(&___PlanetBracket_3, value);
	}

	inline static int32_t get_offset_of_currentscore_4() { return static_cast<int32_t>(offsetof(GameManager2_t3349110545, ___currentscore_4)); }
	inline int32_t get_currentscore_4() const { return ___currentscore_4; }
	inline int32_t* get_address_of_currentscore_4() { return &___currentscore_4; }
	inline void set_currentscore_4(int32_t value)
	{
		___currentscore_4 = value;
	}

	inline static int32_t get_offset_of_CurrentScoretext_5() { return static_cast<int32_t>(offsetof(GameManager2_t3349110545, ___CurrentScoretext_5)); }
	inline Text_t356221433 * get_CurrentScoretext_5() const { return ___CurrentScoretext_5; }
	inline Text_t356221433 ** get_address_of_CurrentScoretext_5() { return &___CurrentScoretext_5; }
	inline void set_CurrentScoretext_5(Text_t356221433 * value)
	{
		___CurrentScoretext_5 = value;
		Il2CppCodeGenWriteBarrier(&___CurrentScoretext_5, value);
	}

	inline static int32_t get_offset_of_StarScoreText_6() { return static_cast<int32_t>(offsetof(GameManager2_t3349110545, ___StarScoreText_6)); }
	inline Text_t356221433 * get_StarScoreText_6() const { return ___StarScoreText_6; }
	inline Text_t356221433 ** get_address_of_StarScoreText_6() { return &___StarScoreText_6; }
	inline void set_StarScoreText_6(Text_t356221433 * value)
	{
		___StarScoreText_6 = value;
		Il2CppCodeGenWriteBarrier(&___StarScoreText_6, value);
	}

	inline static int32_t get_offset_of_PlanerScoreText_7() { return static_cast<int32_t>(offsetof(GameManager2_t3349110545, ___PlanerScoreText_7)); }
	inline Text_t356221433 * get_PlanerScoreText_7() const { return ___PlanerScoreText_7; }
	inline Text_t356221433 ** get_address_of_PlanerScoreText_7() { return &___PlanerScoreText_7; }
	inline void set_PlanerScoreText_7(Text_t356221433 * value)
	{
		___PlanerScoreText_7 = value;
		Il2CppCodeGenWriteBarrier(&___PlanerScoreText_7, value);
	}

	inline static int32_t get_offset_of_StarScore_8() { return static_cast<int32_t>(offsetof(GameManager2_t3349110545, ___StarScore_8)); }
	inline String_t* get_StarScore_8() const { return ___StarScore_8; }
	inline String_t** get_address_of_StarScore_8() { return &___StarScore_8; }
	inline void set_StarScore_8(String_t* value)
	{
		___StarScore_8 = value;
		Il2CppCodeGenWriteBarrier(&___StarScore_8, value);
	}

	inline static int32_t get_offset_of_PlanetScore_9() { return static_cast<int32_t>(offsetof(GameManager2_t3349110545, ___PlanetScore_9)); }
	inline String_t* get_PlanetScore_9() const { return ___PlanetScore_9; }
	inline String_t** get_address_of_PlanetScore_9() { return &___PlanetScore_9; }
	inline void set_PlanetScore_9(String_t* value)
	{
		___PlanetScore_9 = value;
		Il2CppCodeGenWriteBarrier(&___PlanetScore_9, value);
	}

	inline static int32_t get_offset_of_cantrack_10() { return static_cast<int32_t>(offsetof(GameManager2_t3349110545, ___cantrack_10)); }
	inline bool get_cantrack_10() const { return ___cantrack_10; }
	inline bool* get_address_of_cantrack_10() { return &___cantrack_10; }
	inline void set_cantrack_10(bool value)
	{
		___cantrack_10 = value;
	}

	inline static int32_t get_offset_of_politeplanet_11() { return static_cast<int32_t>(offsetof(GameManager2_t3349110545, ___politeplanet_11)); }
	inline bool get_politeplanet_11() const { return ___politeplanet_11; }
	inline bool* get_address_of_politeplanet_11() { return &___politeplanet_11; }
	inline void set_politeplanet_11(bool value)
	{
		___politeplanet_11 = value;
	}

	inline static int32_t get_offset_of_videoManager_12() { return static_cast<int32_t>(offsetof(GameManager2_t3349110545, ___videoManager_12)); }
	inline GameObject_t1756533147 * get_videoManager_12() const { return ___videoManager_12; }
	inline GameObject_t1756533147 ** get_address_of_videoManager_12() { return &___videoManager_12; }
	inline void set_videoManager_12(GameObject_t1756533147 * value)
	{
		___videoManager_12 = value;
		Il2CppCodeGenWriteBarrier(&___videoManager_12, value);
	}

	inline static int32_t get_offset_of_codeobject_13() { return static_cast<int32_t>(offsetof(GameManager2_t3349110545, ___codeobject_13)); }
	inline Code_t686167511 * get_codeobject_13() const { return ___codeobject_13; }
	inline Code_t686167511 ** get_address_of_codeobject_13() { return &___codeobject_13; }
	inline void set_codeobject_13(Code_t686167511 * value)
	{
		___codeobject_13 = value;
		Il2CppCodeGenWriteBarrier(&___codeobject_13, value);
	}

	inline static int32_t get_offset_of_player_14() { return static_cast<int32_t>(offsetof(GameManager2_t3349110545, ___player_14)); }
	inline MediaPlayerCtrl_t1284484152 * get_player_14() const { return ___player_14; }
	inline MediaPlayerCtrl_t1284484152 ** get_address_of_player_14() { return &___player_14; }
	inline void set_player_14(MediaPlayerCtrl_t1284484152 * value)
	{
		___player_14 = value;
		Il2CppCodeGenWriteBarrier(&___player_14, value);
	}

	inline static int32_t get_offset_of_tempfilename_15() { return static_cast<int32_t>(offsetof(GameManager2_t3349110545, ___tempfilename_15)); }
	inline String_t* get_tempfilename_15() const { return ___tempfilename_15; }
	inline String_t** get_address_of_tempfilename_15() { return &___tempfilename_15; }
	inline void set_tempfilename_15(String_t* value)
	{
		___tempfilename_15 = value;
		Il2CppCodeGenWriteBarrier(&___tempfilename_15, value);
	}

	inline static int32_t get_offset_of_videoscreen_16() { return static_cast<int32_t>(offsetof(GameManager2_t3349110545, ___videoscreen_16)); }
	inline RawImage_t2749640213 * get_videoscreen_16() const { return ___videoscreen_16; }
	inline RawImage_t2749640213 ** get_address_of_videoscreen_16() { return &___videoscreen_16; }
	inline void set_videoscreen_16(RawImage_t2749640213 * value)
	{
		___videoscreen_16 = value;
		Il2CppCodeGenWriteBarrier(&___videoscreen_16, value);
	}

	inline static int32_t get_offset_of_mainpanel_17() { return static_cast<int32_t>(offsetof(GameManager2_t3349110545, ___mainpanel_17)); }
	inline GameObject_t1756533147 * get_mainpanel_17() const { return ___mainpanel_17; }
	inline GameObject_t1756533147 ** get_address_of_mainpanel_17() { return &___mainpanel_17; }
	inline void set_mainpanel_17(GameObject_t1756533147 * value)
	{
		___mainpanel_17 = value;
		Il2CppCodeGenWriteBarrier(&___mainpanel_17, value);
	}

	inline static int32_t get_offset_of_Imagsets_18() { return static_cast<int32_t>(offsetof(GameManager2_t3349110545, ___Imagsets_18)); }
	inline GameObject_t1756533147 * get_Imagsets_18() const { return ___Imagsets_18; }
	inline GameObject_t1756533147 ** get_address_of_Imagsets_18() { return &___Imagsets_18; }
	inline void set_Imagsets_18(GameObject_t1756533147 * value)
	{
		___Imagsets_18 = value;
		Il2CppCodeGenWriteBarrier(&___Imagsets_18, value);
	}

	inline static int32_t get_offset_of_filepath_19() { return static_cast<int32_t>(offsetof(GameManager2_t3349110545, ___filepath_19)); }
	inline String_t* get_filepath_19() const { return ___filepath_19; }
	inline String_t** get_address_of_filepath_19() { return &___filepath_19; }
	inline void set_filepath_19(String_t* value)
	{
		___filepath_19 = value;
		Il2CppCodeGenWriteBarrier(&___filepath_19, value);
	}

	inline static int32_t get_offset_of_ds_20() { return static_cast<int32_t>(offsetof(GameManager2_t3349110545, ___ds_20)); }
	inline DataService_t2602786891 * get_ds_20() const { return ___ds_20; }
	inline DataService_t2602786891 ** get_address_of_ds_20() { return &___ds_20; }
	inline void set_ds_20(DataService_t2602786891 * value)
	{
		___ds_20 = value;
		Il2CppCodeGenWriteBarrier(&___ds_20, value);
	}

	inline static int32_t get_offset_of_Scorepanel_21() { return static_cast<int32_t>(offsetof(GameManager2_t3349110545, ___Scorepanel_21)); }
	inline GameObject_t1756533147 * get_Scorepanel_21() const { return ___Scorepanel_21; }
	inline GameObject_t1756533147 ** get_address_of_Scorepanel_21() { return &___Scorepanel_21; }
	inline void set_Scorepanel_21(GameObject_t1756533147 * value)
	{
		___Scorepanel_21 = value;
		Il2CppCodeGenWriteBarrier(&___Scorepanel_21, value);
	}

	inline static int32_t get_offset_of_i_22() { return static_cast<int32_t>(offsetof(GameManager2_t3349110545, ___i_22)); }
	inline int32_t get_i_22() const { return ___i_22; }
	inline int32_t* get_address_of_i_22() { return &___i_22; }
	inline void set_i_22(int32_t value)
	{
		___i_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
