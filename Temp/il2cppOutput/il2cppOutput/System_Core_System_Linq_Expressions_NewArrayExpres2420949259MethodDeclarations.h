﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Expressions.NewArrayExpression
struct NewArrayExpression_t2420949259;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.Expression>
struct ReadOnlyCollection_1_t300650360;

#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.Expression> System.Linq.Expressions.NewArrayExpression::get_Expressions()
extern "C"  ReadOnlyCollection_1_t300650360 * NewArrayExpression_get_Expressions_m1542969646 (NewArrayExpression_t2420949259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
