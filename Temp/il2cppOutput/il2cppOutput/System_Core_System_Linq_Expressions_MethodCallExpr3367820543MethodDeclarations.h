﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Expressions.MethodCallExpression
struct MethodCallExpression_t3367820543;
// System.Linq.Expressions.Expression
struct Expression_t114864668;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.Expression>
struct ReadOnlyCollection_1_t300650360;

#include "codegen/il2cpp-codegen.h"

// System.Linq.Expressions.Expression System.Linq.Expressions.MethodCallExpression::get_Object()
extern "C"  Expression_t114864668 * MethodCallExpression_get_Object_m2833811513 (MethodCallExpression_t3367820543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Linq.Expressions.MethodCallExpression::get_Method()
extern "C"  MethodInfo_t * MethodCallExpression_get_Method_m3076540940 (MethodCallExpression_t3367820543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.Expression> System.Linq.Expressions.MethodCallExpression::get_Arguments()
extern "C"  ReadOnlyCollection_1_t300650360 * MethodCallExpression_get_Arguments_m119024691 (MethodCallExpression_t3367820543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
