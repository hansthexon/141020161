﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Dictionary_2_t955353609;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3644857047.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2702806444_gshared (Enumerator_t3644857047 * __this, Dictionary_2_t955353609 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2702806444(__this, ___host0, method) ((  void (*) (Enumerator_t3644857047 *, Dictionary_2_t955353609 *, const MethodInfo*))Enumerator__ctor_m2702806444_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3590516315_gshared (Enumerator_t3644857047 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3590516315(__this, method) ((  Il2CppObject * (*) (Enumerator_t3644857047 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3590516315_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1853304655_gshared (Enumerator_t3644857047 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1853304655(__this, method) ((  void (*) (Enumerator_t3644857047 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1853304655_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Dispose()
extern "C"  void Enumerator_Dispose_m3479638240_gshared (Enumerator_t3644857047 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3479638240(__this, method) ((  void (*) (Enumerator_t3644857047 *, const MethodInfo*))Enumerator_Dispose_m3479638240_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3324362619_gshared (Enumerator_t3644857047 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3324362619(__this, method) ((  bool (*) (Enumerator_t3644857047 *, const MethodInfo*))Enumerator_MoveNext_m3324362619_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2280704857_gshared (Enumerator_t3644857047 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2280704857(__this, method) ((  int32_t (*) (Enumerator_t3644857047 *, const MethodInfo*))Enumerator_get_Current_m2280704857_gshared)(__this, method)
