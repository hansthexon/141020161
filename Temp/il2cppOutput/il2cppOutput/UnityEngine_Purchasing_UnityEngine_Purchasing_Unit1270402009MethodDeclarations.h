﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.UnityPurchasing/<FetchAndMergeProducts>c__AnonStorey4
struct U3CFetchAndMergeProductsU3Ec__AnonStorey4_t1270402009;
// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>
struct HashSet_1_t275936122;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.UnityPurchasing/<FetchAndMergeProducts>c__AnonStorey4::.ctor()
extern "C"  void U3CFetchAndMergeProductsU3Ec__AnonStorey4__ctor_m1406120661 (U3CFetchAndMergeProductsU3Ec__AnonStorey4_t1270402009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UnityPurchasing/<FetchAndMergeProducts>c__AnonStorey4::<>m__A(System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>)
extern "C"  void U3CFetchAndMergeProductsU3Ec__AnonStorey4_U3CU3Em__A_m3897206634 (U3CFetchAndMergeProductsU3Ec__AnonStorey4_t1270402009 * __this, HashSet_1_t275936122 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
