﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// MediaPlayerCtrl
struct MediaPlayerCtrl_t1284484152;
// System.String
struct String_t;
// UnityEngine.UI.RawImage
struct RawImage_t2749640213;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_t2252321495  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean GameManager::cantrack
	bool ___cantrack_2;
	// UnityEngine.UI.Text GameManager::updatetext
	Text_t356221433 * ___updatetext_3;
	// UnityEngine.GameObject GameManager::MainCam
	GameObject_t1756533147 * ___MainCam_4;
	// UnityEngine.GameObject GameManager::ArCam
	GameObject_t1756533147 * ___ArCam_5;
	// UnityEngine.GameObject GameManager::videoManager
	GameObject_t1756533147 * ___videoManager_6;
	// UnityEngine.GameObject GameManager::videoprefab
	GameObject_t1756533147 * ___videoprefab_7;
	// MediaPlayerCtrl GameManager::player
	MediaPlayerCtrl_t1284484152 * ___player_8;
	// System.String GameManager::tempfilename
	String_t* ___tempfilename_9;
	// UnityEngine.UI.RawImage GameManager::videoscreen
	RawImage_t2749640213 * ___videoscreen_10;
	// UnityEngine.GameObject GameManager::mainpanel
	GameObject_t1756533147 * ___mainpanel_11;
	// UnityEngine.GameObject GameManager::Imagsets
	GameObject_t1756533147 * ___Imagsets_12;
	// System.String GameManager::filepath
	String_t* ___filepath_13;
	// System.Int32 GameManager::i
	int32_t ___i_14;

public:
	inline static int32_t get_offset_of_cantrack_2() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___cantrack_2)); }
	inline bool get_cantrack_2() const { return ___cantrack_2; }
	inline bool* get_address_of_cantrack_2() { return &___cantrack_2; }
	inline void set_cantrack_2(bool value)
	{
		___cantrack_2 = value;
	}

	inline static int32_t get_offset_of_updatetext_3() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___updatetext_3)); }
	inline Text_t356221433 * get_updatetext_3() const { return ___updatetext_3; }
	inline Text_t356221433 ** get_address_of_updatetext_3() { return &___updatetext_3; }
	inline void set_updatetext_3(Text_t356221433 * value)
	{
		___updatetext_3 = value;
		Il2CppCodeGenWriteBarrier(&___updatetext_3, value);
	}

	inline static int32_t get_offset_of_MainCam_4() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___MainCam_4)); }
	inline GameObject_t1756533147 * get_MainCam_4() const { return ___MainCam_4; }
	inline GameObject_t1756533147 ** get_address_of_MainCam_4() { return &___MainCam_4; }
	inline void set_MainCam_4(GameObject_t1756533147 * value)
	{
		___MainCam_4 = value;
		Il2CppCodeGenWriteBarrier(&___MainCam_4, value);
	}

	inline static int32_t get_offset_of_ArCam_5() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___ArCam_5)); }
	inline GameObject_t1756533147 * get_ArCam_5() const { return ___ArCam_5; }
	inline GameObject_t1756533147 ** get_address_of_ArCam_5() { return &___ArCam_5; }
	inline void set_ArCam_5(GameObject_t1756533147 * value)
	{
		___ArCam_5 = value;
		Il2CppCodeGenWriteBarrier(&___ArCam_5, value);
	}

	inline static int32_t get_offset_of_videoManager_6() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___videoManager_6)); }
	inline GameObject_t1756533147 * get_videoManager_6() const { return ___videoManager_6; }
	inline GameObject_t1756533147 ** get_address_of_videoManager_6() { return &___videoManager_6; }
	inline void set_videoManager_6(GameObject_t1756533147 * value)
	{
		___videoManager_6 = value;
		Il2CppCodeGenWriteBarrier(&___videoManager_6, value);
	}

	inline static int32_t get_offset_of_videoprefab_7() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___videoprefab_7)); }
	inline GameObject_t1756533147 * get_videoprefab_7() const { return ___videoprefab_7; }
	inline GameObject_t1756533147 ** get_address_of_videoprefab_7() { return &___videoprefab_7; }
	inline void set_videoprefab_7(GameObject_t1756533147 * value)
	{
		___videoprefab_7 = value;
		Il2CppCodeGenWriteBarrier(&___videoprefab_7, value);
	}

	inline static int32_t get_offset_of_player_8() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___player_8)); }
	inline MediaPlayerCtrl_t1284484152 * get_player_8() const { return ___player_8; }
	inline MediaPlayerCtrl_t1284484152 ** get_address_of_player_8() { return &___player_8; }
	inline void set_player_8(MediaPlayerCtrl_t1284484152 * value)
	{
		___player_8 = value;
		Il2CppCodeGenWriteBarrier(&___player_8, value);
	}

	inline static int32_t get_offset_of_tempfilename_9() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___tempfilename_9)); }
	inline String_t* get_tempfilename_9() const { return ___tempfilename_9; }
	inline String_t** get_address_of_tempfilename_9() { return &___tempfilename_9; }
	inline void set_tempfilename_9(String_t* value)
	{
		___tempfilename_9 = value;
		Il2CppCodeGenWriteBarrier(&___tempfilename_9, value);
	}

	inline static int32_t get_offset_of_videoscreen_10() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___videoscreen_10)); }
	inline RawImage_t2749640213 * get_videoscreen_10() const { return ___videoscreen_10; }
	inline RawImage_t2749640213 ** get_address_of_videoscreen_10() { return &___videoscreen_10; }
	inline void set_videoscreen_10(RawImage_t2749640213 * value)
	{
		___videoscreen_10 = value;
		Il2CppCodeGenWriteBarrier(&___videoscreen_10, value);
	}

	inline static int32_t get_offset_of_mainpanel_11() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___mainpanel_11)); }
	inline GameObject_t1756533147 * get_mainpanel_11() const { return ___mainpanel_11; }
	inline GameObject_t1756533147 ** get_address_of_mainpanel_11() { return &___mainpanel_11; }
	inline void set_mainpanel_11(GameObject_t1756533147 * value)
	{
		___mainpanel_11 = value;
		Il2CppCodeGenWriteBarrier(&___mainpanel_11, value);
	}

	inline static int32_t get_offset_of_Imagsets_12() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___Imagsets_12)); }
	inline GameObject_t1756533147 * get_Imagsets_12() const { return ___Imagsets_12; }
	inline GameObject_t1756533147 ** get_address_of_Imagsets_12() { return &___Imagsets_12; }
	inline void set_Imagsets_12(GameObject_t1756533147 * value)
	{
		___Imagsets_12 = value;
		Il2CppCodeGenWriteBarrier(&___Imagsets_12, value);
	}

	inline static int32_t get_offset_of_filepath_13() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___filepath_13)); }
	inline String_t* get_filepath_13() const { return ___filepath_13; }
	inline String_t** get_address_of_filepath_13() { return &___filepath_13; }
	inline void set_filepath_13(String_t* value)
	{
		___filepath_13 = value;
		Il2CppCodeGenWriteBarrier(&___filepath_13, value);
	}

	inline static int32_t get_offset_of_i_14() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___i_14)); }
	inline int32_t get_i_14() const { return ___i_14; }
	inline int32_t* get_address_of_i_14() { return &___i_14; }
	inline void set_i_14(int32_t value)
	{
		___i_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
