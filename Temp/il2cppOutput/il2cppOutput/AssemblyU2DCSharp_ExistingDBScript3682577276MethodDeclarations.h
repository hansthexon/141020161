﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExistingDBScript
struct ExistingDBScript_t3682577276;
// System.Collections.Generic.IEnumerable`1<Album>
struct IEnumerable_1_t1434645190;
// System.Collections.Generic.IEnumerable`1<Person>
struct IEnumerable_1_t3534044808;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ExistingDBScript::.ctor()
extern "C"  void ExistingDBScript__ctor_m2933922893 (ExistingDBScript_t3682577276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExistingDBScript::Start()
extern "C"  void ExistingDBScript_Start_m3336213085 (ExistingDBScript_t3682577276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExistingDBScript::ToConsole(System.Collections.Generic.IEnumerable`1<Album>)
extern "C"  void ExistingDBScript_ToConsole_m3589776897 (ExistingDBScript_t3682577276 * __this, Il2CppObject* ___alum0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExistingDBScript::ToConsole(System.Collections.Generic.IEnumerable`1<Person>)
extern "C"  void ExistingDBScript_ToConsole_m950542795 (ExistingDBScript_t3682577276 * __this, Il2CppObject* ___people0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExistingDBScript::ToConsole(System.String)
extern "C"  void ExistingDBScript_ToConsole_m3109437519 (ExistingDBScript_t3682577276 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
