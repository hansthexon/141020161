﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HandController
struct HandController_t317030225;

#include "codegen/il2cpp-codegen.h"

// System.Void HandController::.ctor()
extern "C"  void HandController__ctor_m2735024900 (HandController_t317030225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HandController::Start()
extern "C"  void HandController_Start_m3391107260 (HandController_t317030225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HandController::Update()
extern "C"  void HandController_Update_m685454973 (HandController_t317030225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HandController::DisableHand()
extern "C"  void HandController_DisableHand_m745907459 (HandController_t317030225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HandController::EnableHand()
extern "C"  void HandController_EnableHand_m712423532 (HandController_t317030225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
