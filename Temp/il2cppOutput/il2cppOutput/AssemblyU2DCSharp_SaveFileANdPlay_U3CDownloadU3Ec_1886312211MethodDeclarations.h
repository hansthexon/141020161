﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SaveFileANdPlay/<Download>c__IteratorE
struct U3CDownloadU3Ec__IteratorE_t1886312211;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SaveFileANdPlay/<Download>c__IteratorE::.ctor()
extern "C"  void U3CDownloadU3Ec__IteratorE__ctor_m1956577024 (U3CDownloadU3Ec__IteratorE_t1886312211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SaveFileANdPlay/<Download>c__IteratorE::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDownloadU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m923541296 (U3CDownloadU3Ec__IteratorE_t1886312211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SaveFileANdPlay/<Download>c__IteratorE::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDownloadU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m2994050488 (U3CDownloadU3Ec__IteratorE_t1886312211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SaveFileANdPlay/<Download>c__IteratorE::MoveNext()
extern "C"  bool U3CDownloadU3Ec__IteratorE_MoveNext_m644151600 (U3CDownloadU3Ec__IteratorE_t1886312211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SaveFileANdPlay/<Download>c__IteratorE::Dispose()
extern "C"  void U3CDownloadU3Ec__IteratorE_Dispose_m2623258561 (U3CDownloadU3Ec__IteratorE_t1886312211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SaveFileANdPlay/<Download>c__IteratorE::Reset()
extern "C"  void U3CDownloadU3Ec__IteratorE_Reset_m2788030663 (U3CDownloadU3Ec__IteratorE_t1886312211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
