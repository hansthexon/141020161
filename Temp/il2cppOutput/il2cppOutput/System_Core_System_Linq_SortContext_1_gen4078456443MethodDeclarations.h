﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.SortContext`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct SortContext_1_t4078456443;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Linq_SortDirection759359329.h"

// System.Void System.Linq.SortContext`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.ctor(System.Linq.SortDirection,System.Linq.SortContext`1<TElement>)
extern "C"  void SortContext_1__ctor_m1039086743_gshared (SortContext_1_t4078456443 * __this, int32_t ___direction0, SortContext_1_t4078456443 * ___child_context1, const MethodInfo* method);
#define SortContext_1__ctor_m1039086743(__this, ___direction0, ___child_context1, method) ((  void (*) (SortContext_1_t4078456443 *, int32_t, SortContext_1_t4078456443 *, const MethodInfo*))SortContext_1__ctor_m1039086743_gshared)(__this, ___direction0, ___child_context1, method)
