﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Console
struct  Console_t55256533  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text Console::tttt
	Text_t356221433 * ___tttt_2;
	// System.String Console::output
	String_t* ___output_4;
	// System.String Console::stack
	String_t* ___stack_5;

public:
	inline static int32_t get_offset_of_tttt_2() { return static_cast<int32_t>(offsetof(Console_t55256533, ___tttt_2)); }
	inline Text_t356221433 * get_tttt_2() const { return ___tttt_2; }
	inline Text_t356221433 ** get_address_of_tttt_2() { return &___tttt_2; }
	inline void set_tttt_2(Text_t356221433 * value)
	{
		___tttt_2 = value;
		Il2CppCodeGenWriteBarrier(&___tttt_2, value);
	}

	inline static int32_t get_offset_of_output_4() { return static_cast<int32_t>(offsetof(Console_t55256533, ___output_4)); }
	inline String_t* get_output_4() const { return ___output_4; }
	inline String_t** get_address_of_output_4() { return &___output_4; }
	inline void set_output_4(String_t* value)
	{
		___output_4 = value;
		Il2CppCodeGenWriteBarrier(&___output_4, value);
	}

	inline static int32_t get_offset_of_stack_5() { return static_cast<int32_t>(offsetof(Console_t55256533, ___stack_5)); }
	inline String_t* get_stack_5() const { return ___stack_5; }
	inline String_t** get_address_of_stack_5() { return &___stack_5; }
	inline void set_stack_5(String_t* value)
	{
		___stack_5 = value;
		Il2CppCodeGenWriteBarrier(&___stack_5, value);
	}
};

struct Console_t55256533_StaticFields
{
public:
	// System.String Console::myLog
	String_t* ___myLog_3;

public:
	inline static int32_t get_offset_of_myLog_3() { return static_cast<int32_t>(offsetof(Console_t55256533_StaticFields, ___myLog_3)); }
	inline String_t* get_myLog_3() const { return ___myLog_3; }
	inline String_t** get_address_of_myLog_3() { return &___myLog_3; }
	inline void set_myLog_3(String_t* value)
	{
		___myLog_3 = value;
		Il2CppCodeGenWriteBarrier(&___myLog_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
