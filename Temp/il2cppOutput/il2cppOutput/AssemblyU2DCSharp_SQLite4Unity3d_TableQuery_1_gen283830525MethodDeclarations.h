﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableQuery_1_gen4026329353MethodDeclarations.h"

// System.Void SQLite4Unity3d.TableQuery`1<Person>::.ctor(SQLite4Unity3d.SQLiteConnection,SQLite4Unity3d.TableMapping)
#define TableQuery_1__ctor_m277758651(__this, ___conn0, ___table1, method) ((  void (*) (TableQuery_1_t283830525 *, SQLiteConnection_t3529499386 *, TableMapping_t3898710812 *, const MethodInfo*))TableQuery_1__ctor_m2525682756_gshared)(__this, ___conn0, ___table1, method)
// System.Void SQLite4Unity3d.TableQuery`1<Person>::.ctor(SQLite4Unity3d.SQLiteConnection)
#define TableQuery_1__ctor_m2606773881(__this, ___conn0, method) ((  void (*) (TableQuery_1_t283830525 *, SQLiteConnection_t3529499386 *, const MethodInfo*))TableQuery_1__ctor_m821176910_gshared)(__this, ___conn0, method)
// System.Collections.IEnumerator SQLite4Unity3d.TableQuery`1<Person>::System.Collections.IEnumerable.GetEnumerator()
#define TableQuery_1_System_Collections_IEnumerable_GetEnumerator_m1807452348(__this, method) ((  Il2CppObject * (*) (TableQuery_1_t283830525 *, const MethodInfo*))TableQuery_1_System_Collections_IEnumerable_GetEnumerator_m2306269163_gshared)(__this, method)
// SQLite4Unity3d.SQLiteConnection SQLite4Unity3d.TableQuery`1<Person>::get_Connection()
#define TableQuery_1_get_Connection_m2357786357(__this, method) ((  SQLiteConnection_t3529499386 * (*) (TableQuery_1_t283830525 *, const MethodInfo*))TableQuery_1_get_Connection_m2018438234_gshared)(__this, method)
// System.Void SQLite4Unity3d.TableQuery`1<Person>::set_Connection(SQLite4Unity3d.SQLiteConnection)
#define TableQuery_1_set_Connection_m3979996498(__this, ___value0, method) ((  void (*) (TableQuery_1_t283830525 *, SQLiteConnection_t3529499386 *, const MethodInfo*))TableQuery_1_set_Connection_m3046933261_gshared)(__this, ___value0, method)
// SQLite4Unity3d.TableMapping SQLite4Unity3d.TableQuery`1<Person>::get_Table()
#define TableQuery_1_get_Table_m2111603319(__this, method) ((  TableMapping_t3898710812 * (*) (TableQuery_1_t283830525 *, const MethodInfo*))TableQuery_1_get_Table_m2172867230_gshared)(__this, method)
// System.Void SQLite4Unity3d.TableQuery`1<Person>::set_Table(SQLite4Unity3d.TableMapping)
#define TableQuery_1_set_Table_m2365664426(__this, ___value0, method) ((  void (*) (TableQuery_1_t283830525 *, TableMapping_t3898710812 *, const MethodInfo*))TableQuery_1_set_Table_m3580780791_gshared)(__this, ___value0, method)
// SQLite4Unity3d.TableQuery`1<T> SQLite4Unity3d.TableQuery`1<Person>::Where(System.Linq.Expressions.Expression`1<System.Func`2<T,System.Boolean>>)
#define TableQuery_1_Where_m3530527899(__this, ___predExpr0, method) ((  TableQuery_1_t283830525 * (*) (TableQuery_1_t283830525 *, Expression_1_t1516544404 *, const MethodInfo*))TableQuery_1_Where_m4178518972_gshared)(__this, ___predExpr0, method)
// SQLite4Unity3d.TableQuery`1<T> SQLite4Unity3d.TableQuery`1<Person>::Take(System.Int32)
#define TableQuery_1_Take_m3595464083(__this, ___n0, method) ((  TableQuery_1_t283830525 * (*) (TableQuery_1_t283830525 *, int32_t, const MethodInfo*))TableQuery_1_Take_m2215285898_gshared)(__this, ___n0, method)
// SQLite4Unity3d.TableQuery`1<T> SQLite4Unity3d.TableQuery`1<Person>::Skip(System.Int32)
#define TableQuery_1_Skip_m2160061201(__this, ___n0, method) ((  TableQuery_1_t283830525 * (*) (TableQuery_1_t283830525 *, int32_t, const MethodInfo*))TableQuery_1_Skip_m2335687758_gshared)(__this, ___n0, method)
// T SQLite4Unity3d.TableQuery`1<Person>::ElementAt(System.Int32)
#define TableQuery_1_ElementAt_m361960652(__this, ___index0, method) ((  Person_t3241917763 * (*) (TableQuery_1_t283830525 *, int32_t, const MethodInfo*))TableQuery_1_ElementAt_m4176579987_gshared)(__this, ___index0, method)
// SQLite4Unity3d.TableQuery`1<T> SQLite4Unity3d.TableQuery`1<Person>::Deferred()
#define TableQuery_1_Deferred_m478872772(__this, method) ((  TableQuery_1_t283830525 * (*) (TableQuery_1_t283830525 *, const MethodInfo*))TableQuery_1_Deferred_m2927110549_gshared)(__this, method)
// System.Void SQLite4Unity3d.TableQuery`1<Person>::AddWhere(System.Linq.Expressions.Expression)
#define TableQuery_1_AddWhere_m2981705531(__this, ___pred0, method) ((  void (*) (TableQuery_1_t283830525 *, Expression_t114864668 *, const MethodInfo*))TableQuery_1_AddWhere_m752477770_gshared)(__this, ___pred0, method)
// SQLite4Unity3d.SQLiteCommand SQLite4Unity3d.TableQuery`1<Person>::GenerateCommand(System.String)
#define TableQuery_1_GenerateCommand_m2094976361(__this, ___selectionList0, method) ((  SQLiteCommand_t2935685145 * (*) (TableQuery_1_t283830525 *, String_t*, const MethodInfo*))TableQuery_1_GenerateCommand_m4193037006_gshared)(__this, ___selectionList0, method)
// SQLite4Unity3d.TableQuery`1/CompileResult<T> SQLite4Unity3d.TableQuery`1<Person>::CompileExpr(System.Linq.Expressions.Expression,System.Collections.Generic.List`1<System.Object>)
#define TableQuery_1_CompileExpr_m1802146888(__this, ___expr0, ___queryArgs1, method) ((  CompileResult_t421292104 * (*) (TableQuery_1_t283830525 *, Expression_t114864668 *, List_1_t2058570427 *, const MethodInfo*))TableQuery_1_CompileExpr_m3818611973_gshared)(__this, ___expr0, ___queryArgs1, method)
// System.Object SQLite4Unity3d.TableQuery`1<Person>::ConvertTo(System.Object,System.Type)
#define TableQuery_1_ConvertTo_m1382453145(__this /* static, unused */, ___obj0, ___t1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Type_t *, const MethodInfo*))TableQuery_1_ConvertTo_m2602095610_gshared)(__this /* static, unused */, ___obj0, ___t1, method)
// System.String SQLite4Unity3d.TableQuery`1<Person>::CompileNullBinaryExpression(System.Linq.Expressions.BinaryExpression,SQLite4Unity3d.TableQuery`1/CompileResult<T>)
#define TableQuery_1_CompileNullBinaryExpression_m2642925618(__this, ___expression0, ___parameter1, method) ((  String_t* (*) (TableQuery_1_t283830525 *, BinaryExpression_t2159924157 *, CompileResult_t421292104 *, const MethodInfo*))TableQuery_1_CompileNullBinaryExpression_m4251067313_gshared)(__this, ___expression0, ___parameter1, method)
// System.String SQLite4Unity3d.TableQuery`1<Person>::GetSqlName(System.Linq.Expressions.Expression)
#define TableQuery_1_GetSqlName_m3098205421(__this, ___expr0, method) ((  String_t* (*) (TableQuery_1_t283830525 *, Expression_t114864668 *, const MethodInfo*))TableQuery_1_GetSqlName_m1128366698_gshared)(__this, ___expr0, method)
// System.Int32 SQLite4Unity3d.TableQuery`1<Person>::Count()
#define TableQuery_1_Count_m3626303820(__this, method) ((  int32_t (*) (TableQuery_1_t283830525 *, const MethodInfo*))TableQuery_1_Count_m3413050791_gshared)(__this, method)
// System.Int32 SQLite4Unity3d.TableQuery`1<Person>::Count(System.Linq.Expressions.Expression`1<System.Func`2<T,System.Boolean>>)
#define TableQuery_1_Count_m1895431707(__this, ___predExpr0, method) ((  int32_t (*) (TableQuery_1_t283830525 *, Expression_1_t1516544404 *, const MethodInfo*))TableQuery_1_Count_m3405720028_gshared)(__this, ___predExpr0, method)
// System.Collections.Generic.IEnumerator`1<T> SQLite4Unity3d.TableQuery`1<Person>::GetEnumerator()
#define TableQuery_1_GetEnumerator_m2144069865(__this, method) ((  Il2CppObject* (*) (TableQuery_1_t283830525 *, const MethodInfo*))TableQuery_1_GetEnumerator_m710809644_gshared)(__this, method)
// T SQLite4Unity3d.TableQuery`1<Person>::First()
#define TableQuery_1_First_m2817551702(__this, method) ((  Person_t3241917763 * (*) (TableQuery_1_t283830525 *, const MethodInfo*))TableQuery_1_First_m3018168943_gshared)(__this, method)
// T SQLite4Unity3d.TableQuery`1<Person>::FirstOrDefault()
#define TableQuery_1_FirstOrDefault_m1642169474(__this, method) ((  Person_t3241917763 * (*) (TableQuery_1_t283830525 *, const MethodInfo*))TableQuery_1_FirstOrDefault_m3828705525_gshared)(__this, method)
// System.String SQLite4Unity3d.TableQuery`1<Person>::<GenerateCommand>m__11(SQLite4Unity3d.BaseTableQuery/Ordering)
#define TableQuery_1_U3CGenerateCommandU3Em__11_m735174867(__this /* static, unused */, ___o0, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, Ordering_t1038862794 *, const MethodInfo*))TableQuery_1_U3CGenerateCommandU3Em__11_m1866588696_gshared)(__this /* static, unused */, ___o0, method)
// System.String SQLite4Unity3d.TableQuery`1<Person>::<CompileExpr>m__12(SQLite4Unity3d.TableQuery`1/CompileResult<T>)
#define TableQuery_1_U3CCompileExprU3Em__12_m2265324374(__this /* static, unused */, ___a0, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, CompileResult_t421292104 *, const MethodInfo*))TableQuery_1_U3CCompileExprU3Em__12_m1764005629_gshared)(__this /* static, unused */, ___a0, method)
