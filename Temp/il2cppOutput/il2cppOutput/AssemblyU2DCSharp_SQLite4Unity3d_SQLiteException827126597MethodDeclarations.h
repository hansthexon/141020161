﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.SQLiteException
struct SQLiteException_t827126597;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLite3_Result1450270481.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SQLite4Unity3d.SQLiteException::.ctor(SQLite4Unity3d.SQLite3/Result,System.String)
extern "C"  void SQLiteException__ctor_m883776103 (SQLiteException_t827126597 * __this, int32_t ___r0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.SQLite3/Result SQLite4Unity3d.SQLiteException::get_Result()
extern "C"  int32_t SQLiteException_get_Result_m2590503734 (SQLiteException_t827126597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteException::set_Result(SQLite4Unity3d.SQLite3/Result)
extern "C"  void SQLiteException_set_Result_m2380489637 (SQLiteException_t827126597 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.SQLiteException SQLite4Unity3d.SQLiteException::New(SQLite4Unity3d.SQLite3/Result,System.String)
extern "C"  SQLiteException_t827126597 * SQLiteException_New_m2473415881 (Il2CppObject * __this /* static, unused */, int32_t ___r0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
