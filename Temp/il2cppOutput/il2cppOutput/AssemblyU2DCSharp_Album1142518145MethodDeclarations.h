﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Album
struct Album_t1142518145;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Album::.ctor()
extern "C"  void Album__ctor_m730639062 (Album_t1142518145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Album::get_AlbumId()
extern "C"  int32_t Album_get_AlbumId_m2197329565 (Album_t1142518145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Album::set_AlbumId(System.Int32)
extern "C"  void Album_set_AlbumId_m461198610 (Album_t1142518145 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Album::get_Title()
extern "C"  String_t* Album_get_Title_m2079655486 (Album_t1142518145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Album::set_Title(System.String)
extern "C"  void Album_set_Title_m366983587 (Album_t1142518145 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Album::get_ArtistId()
extern "C"  String_t* Album_get_ArtistId_m870157104 (Album_t1142518145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Album::set_ArtistId(System.String)
extern "C"  void Album_set_ArtistId_m3054804691 (Album_t1142518145 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
