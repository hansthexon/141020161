﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.AppleTangle
struct AppleTangle_t53875121;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.Security.AppleTangle::.ctor()
extern "C"  void AppleTangle__ctor_m1226795583 (AppleTangle_t53875121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.AppleTangle::.cctor()
extern "C"  void AppleTangle__cctor_m3030347248 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.Purchasing.Security.AppleTangle::Data()
extern "C"  ByteU5BU5D_t3397334013* AppleTangle_Data_m3366264679 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
