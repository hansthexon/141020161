﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.VuforiaManagerImpl/TrackableResultData
struct TrackableResultData_t1947527974;
struct TrackableResultData_t1947527974_marshaled_pinvoke;
struct TrackableResultData_t1947527974_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct TrackableResultData_t1947527974;
struct TrackableResultData_t1947527974_marshaled_pinvoke;

extern "C" void TrackableResultData_t1947527974_marshal_pinvoke(const TrackableResultData_t1947527974& unmarshaled, TrackableResultData_t1947527974_marshaled_pinvoke& marshaled);
extern "C" void TrackableResultData_t1947527974_marshal_pinvoke_back(const TrackableResultData_t1947527974_marshaled_pinvoke& marshaled, TrackableResultData_t1947527974& unmarshaled);
extern "C" void TrackableResultData_t1947527974_marshal_pinvoke_cleanup(TrackableResultData_t1947527974_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct TrackableResultData_t1947527974;
struct TrackableResultData_t1947527974_marshaled_com;

extern "C" void TrackableResultData_t1947527974_marshal_com(const TrackableResultData_t1947527974& unmarshaled, TrackableResultData_t1947527974_marshaled_com& marshaled);
extern "C" void TrackableResultData_t1947527974_marshal_com_back(const TrackableResultData_t1947527974_marshaled_com& marshaled, TrackableResultData_t1947527974& unmarshaled);
extern "C" void TrackableResultData_t1947527974_marshal_com_cleanup(TrackableResultData_t1947527974_marshaled_com& marshaled);
