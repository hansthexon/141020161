﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataService/<getVideo>c__AnonStoreyF
struct  U3CgetVideoU3Ec__AnonStoreyF_t3098235058  : public Il2CppObject
{
public:
	// System.String DataService/<getVideo>c__AnonStoreyF::code
	String_t* ___code_0;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(U3CgetVideoU3Ec__AnonStoreyF_t3098235058, ___code_0)); }
	inline String_t* get_code_0() const { return ___code_0; }
	inline String_t** get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(String_t* value)
	{
		___code_0 = value;
		Il2CppCodeGenWriteBarrier(&___code_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
