﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Animator
struct Animator_t69676727;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DogActions
struct  DogActions_t825509883  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text DogActions::timerLabel
	Text_t356221433 * ___timerLabel_2;
	// UnityEngine.GameObject DogActions::Mouth
	GameObject_t1756533147 * ___Mouth_3;
	// System.Single DogActions::time
	float ___time_4;
	// System.Boolean DogActions::cansleep
	bool ___cansleep_5;
	// System.Boolean DogActions::cancross
	bool ___cancross_6;
	// UnityEngine.Animator DogActions::doganim
	Animator_t69676727 * ___doganim_7;

public:
	inline static int32_t get_offset_of_timerLabel_2() { return static_cast<int32_t>(offsetof(DogActions_t825509883, ___timerLabel_2)); }
	inline Text_t356221433 * get_timerLabel_2() const { return ___timerLabel_2; }
	inline Text_t356221433 ** get_address_of_timerLabel_2() { return &___timerLabel_2; }
	inline void set_timerLabel_2(Text_t356221433 * value)
	{
		___timerLabel_2 = value;
		Il2CppCodeGenWriteBarrier(&___timerLabel_2, value);
	}

	inline static int32_t get_offset_of_Mouth_3() { return static_cast<int32_t>(offsetof(DogActions_t825509883, ___Mouth_3)); }
	inline GameObject_t1756533147 * get_Mouth_3() const { return ___Mouth_3; }
	inline GameObject_t1756533147 ** get_address_of_Mouth_3() { return &___Mouth_3; }
	inline void set_Mouth_3(GameObject_t1756533147 * value)
	{
		___Mouth_3 = value;
		Il2CppCodeGenWriteBarrier(&___Mouth_3, value);
	}

	inline static int32_t get_offset_of_time_4() { return static_cast<int32_t>(offsetof(DogActions_t825509883, ___time_4)); }
	inline float get_time_4() const { return ___time_4; }
	inline float* get_address_of_time_4() { return &___time_4; }
	inline void set_time_4(float value)
	{
		___time_4 = value;
	}

	inline static int32_t get_offset_of_cansleep_5() { return static_cast<int32_t>(offsetof(DogActions_t825509883, ___cansleep_5)); }
	inline bool get_cansleep_5() const { return ___cansleep_5; }
	inline bool* get_address_of_cansleep_5() { return &___cansleep_5; }
	inline void set_cansleep_5(bool value)
	{
		___cansleep_5 = value;
	}

	inline static int32_t get_offset_of_cancross_6() { return static_cast<int32_t>(offsetof(DogActions_t825509883, ___cancross_6)); }
	inline bool get_cancross_6() const { return ___cancross_6; }
	inline bool* get_address_of_cancross_6() { return &___cancross_6; }
	inline void set_cancross_6(bool value)
	{
		___cancross_6 = value;
	}

	inline static int32_t get_offset_of_doganim_7() { return static_cast<int32_t>(offsetof(DogActions_t825509883, ___doganim_7)); }
	inline Animator_t69676727 * get_doganim_7() const { return ___doganim_7; }
	inline Animator_t69676727 ** get_address_of_doganim_7() { return &___doganim_7; }
	inline void set_doganim_7(Animator_t69676727 * value)
	{
		___doganim_7 = value;
		Il2CppCodeGenWriteBarrier(&___doganim_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
