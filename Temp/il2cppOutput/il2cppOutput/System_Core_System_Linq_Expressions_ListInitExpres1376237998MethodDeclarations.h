﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Expressions.ListInitExpression
struct ListInitExpression_t1376237998;
// System.Linq.Expressions.NewExpression
struct NewExpression_t1045017810;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.ElementInit>
struct ReadOnlyCollection_1_t4083992128;

#include "codegen/il2cpp-codegen.h"

// System.Linq.Expressions.NewExpression System.Linq.Expressions.ListInitExpression::get_NewExpression()
extern "C"  NewExpression_t1045017810 * ListInitExpression_get_NewExpression_m3566588823 (ListInitExpression_t1376237998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.ElementInit> System.Linq.Expressions.ListInitExpression::get_Initializers()
extern "C"  ReadOnlyCollection_1_t4083992128 * ListInitExpression_get_Initializers_m1722337357 (ListInitExpression_t1376237998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
