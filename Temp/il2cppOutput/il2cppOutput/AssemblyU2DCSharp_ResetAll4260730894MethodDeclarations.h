﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResetAll
struct ResetAll_t4260730894;

#include "codegen/il2cpp-codegen.h"

// System.Void ResetAll::.ctor()
extern "C"  void ResetAll__ctor_m2435368531 (ResetAll_t4260730894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResetAll::Start()
extern "C"  void ResetAll_Start_m3872422063 (ResetAll_t4260730894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResetAll::Update()
extern "C"  void ResetAll_Update_m3124920028 (ResetAll_t4260730894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
