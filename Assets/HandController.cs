﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class HandController : MonoBehaviour {


	public GameObject codeBit;
	CanvasGroup cg;

	// Use this for initialization
	void Start () {
		cg=gameObject.GetComponent<CanvasGroup> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void DisableHand(){

		codeBit.SetActive (false);
		//StartCoroutine (showlyreduce ());
	}


	public void EnableHand(){

		codeBit.SetActive (true);
		//StartCoroutine (showlyreduce ());
	}
}
